//
//  FTFileManager.h
//  FTrack_Take2
//
//  Created by Martin Lalev on 10/6/11.
//  Copyright 2011 mART. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MPLFilenameManager : NSObject {

}

+ (NSString*) copyFile:(NSString*)oldFilename to:(NSString*)newFilename overwrite:(BOOL)overwrite existed:(BOOL*)existed;
+ (NSString*) localCopyOfResource:(NSString*)resource ofType:(NSString*)type to:(NSString*)filename overwrite:(BOOL)overwrite existed:(BOOL*)existed;
+ (NSString*) localCopyOfResource:(NSString*)resource ofType:(NSString*)type overwrite:(BOOL)overwrite existed:(BOOL*)existed;

@end
