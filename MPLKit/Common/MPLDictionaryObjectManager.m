//
//  MPLDictionaryObjectManager.m
//  MPLKit
//
//  Created by Martin Lalev on 4/21/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLDictionaryObjectManager.h"
#import "MPLDictionaryObject.h"

@implementation MPLDictionaryObjectManager



// Initializer
- (id) initWithObjectClassType:(Class)classType {
    if ([super init]) {
        self.objectClassType = classType;
        self.indexedObjects = [NSMutableDictionary dictionary];
    }
    return self;
}

// Indexed objects management
- (void) indexedNewObject:(MPLDictionaryObject*)object { }
- (MPLDictionaryObject*) cachedObjectForDictionary:(NSDictionary*)dictionary {
    MPLDictionaryObject* newCategory = [[self.objectClassType alloc] initWithDictionary:dictionary];
    MPLDictionaryObject* oldCategory = self[newCategory.objectId];
    
    if (oldCategory == nil) {
        self[newCategory.objectId] = newCategory;
        [self indexedNewObject:newCategory];
        return newCategory;
    }
    else {
        oldCategory.dictionary = newCategory.dictionary;
        return oldCategory;
    }
}

- (MPLDictionaryObject*)objectForKeyedSubscript:(id)key {
    if (key == nil || [key isKindOfClass:[NSNull class]]) {
        return nil;
    }
    else if ([key isKindOfClass:[NSString class]]) {
        return [self.indexedObjects objectForKey:key];
    }
    else if ([key isKindOfClass:[NSDictionary class]] && (NSDictionary*)key[@"id"] != nil) {
        return [self cachedObjectForDictionary:(NSDictionary*)key];
    }
    else {
        return nil;
    }
}
- (void)setObject:(MPLDictionaryObject*)obj forKeyedSubscript:(id)key {
    if (obj != nil) {
        [self.indexedObjects setObject:obj forKey:key];
    }
}

- (NSArray*) itemsFromKeysArray:(NSArray*)keys {
    NSMutableArray* result = [NSMutableArray array];
    for (id key in keys){
        id item = self[key];
        if (item != nil) [result addObject:item];
    }
    return result;
}
- (NSArray*) sortedObjectsWithPredicate:(NSPredicate*)predicate andComparator:(NSComparisonResult(^)(MPLDictionaryObject* obj1, MPLDictionaryObject* obj2))comparator {
    NSArray* result = predicate ? [self.indexedObjects.allValues filteredArrayUsingPredicate:predicate] : self.indexedObjects.allValues;
    return [result sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return comparator((MPLDictionaryObject*)obj1,(MPLDictionaryObject*)obj2);
    }];
}



@end
