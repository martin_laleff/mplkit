//
//  MPLDictionaryObjectManager.h
//  MPLKit
//
//  Created by Martin Lalev on 4/21/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MPLDictionaryObject;
@interface MPLDictionaryObjectManager : NSObject

@property (nonatomic, weak) Class objectClassType;
@property (nonatomic, strong) NSMutableDictionary* indexedObjects;

- (id) initWithObjectClassType:(Class)classType;

- (void) indexedNewObject:(MPLDictionaryObject*)object;

- (void)setObject:(MPLDictionaryObject*)obj forKeyedSubscript:(id)key;
- (MPLDictionaryObject*)objectForKeyedSubscript:(id)key;

- (NSArray*) itemsFromKeysArray:(NSArray*)keys;
- (NSArray*) sortedObjectsWithPredicate:(NSPredicate*)predicate andComparator:(NSComparisonResult(^)(MPLDictionaryObject* obj1, MPLDictionaryObject* obj2))comparator;

@end
