//
//  MPLPushNotificationsManager.h
//  MPLKit
//
//  Created by Martin Lalev on 3/27/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

@class MPLApplication;

@interface MPLPushNotificationsManager : NSObject

@property (nonatomic, assign) BOOL cancelReceiveHandle;
@property (nonatomic, strong) NSDictionary* receivedRemoteNotification;

@property (nonatomic, assign) UIRemoteNotificationType notificationsType;
@property (nonatomic, readonly) UIUserNotificationType notificationsType_iOS8;
@property (nonatomic, readonly) BOOL userSettingsAlertsEnabled;

- (id) initWithNotificationsType:(UIRemoteNotificationType)type;

@end




@interface MPLPushNotificationsManager (Registration)

- (void) changeStateForRegisteredForRemoteNotificationsTo:(BOOL)enabled;

+ (NSString*)stringWithDeviceToken:(NSData*)token;
- (void) failedToRegisterWithError:(NSError*)error;
- (void) didRegisterWithDeviceToken:(NSData*)tokenData finished:(void(^)(void))finished;
- (void) willSetRegistered:(BOOL)registered finished:(void(^)(void))finished;

- (void) application:(MPLApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
- (void) application:(MPLApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;
- (void) application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings;
- (void) application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler;

@end




@interface MPLPushNotificationsManager (Handling)

- (void) application:(MPLApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo;
- (void) alertAndHandleWithCancelButton:(NSString*)cancelButton andHandleButton:(NSString*)handleButton;
- (void) handleRemoteNotification;
- (void) handleRemoteNotificationPassively;
- (void) handleOnStartup;
- (void) receivedRemoteNotificationAtActiveState;
- (void) handleOnEnterForeground;
- (void) cancelHandleOnReceive;

@end




//@interface MPLPushNotificationsManager (RegisterTick)
//
//@property (nonatomic, assign) NSInteger askTickValue;
//@property (nonatomic, retain) NSString* notificationTickKey;
//@property (nonatomic, assign) NSInteger notificationsRequestTick;
//
//- (id) initWithNotificationsType:(UIRemoteNotificationType)type andNotificationTickKey:(NSString*)tickKey andAskTickValue:(NSInteger)askValue;
//
//@end