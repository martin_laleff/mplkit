//
//  MPLGradientLayer.m
//  MPLKit
//
//  Created by Martin Lalev on 4/21/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLGradientLayer.h"

@implementation MPLGradientLayer

- (id) initWithFrame:(CGRect)frame directionalPoint:(CGPoint)endPoint {
    if ([super init]) {
        self.colors = [NSMutableArray array];
        self.locations = [NSMutableArray array];
        self.layer = [CAGradientLayer layer];
        self.layer.frame = frame;
        self.layer.startPoint = CGPointZero;
        self.layer.endPoint = endPoint;
    }
    return self;
}
- (void) addColor:(UIColor*)color atLocation:(float)location {
    [self.colors addObject:(NSObject*)color.CGColor];
    [self.locations addObject:@(location)];
    [self.layer setColors:self.colors];
    [self.layer setLocations:self.locations];
}

+ (CAGradientLayer*) generateLayerWithFrame:(CGRect)frame directionalPoint:(CGPoint)endPoint andPointsAddition:(void(^)(MPLGradientLayer*l))addPoints {
    MPLGradientLayer* l = [[MPLGradientLayer alloc] initWithFrame:frame directionalPoint:endPoint];
    addPoints(l);
    CAGradientLayer* gl = l.layer;
    return gl;
}

+ (CAGradientLayer*) generateLayerForView:(MPLView*)v directionalPoint:(CGPoint)endPoint andPointsAddition:(void(^)(MPLGradientLayer*l))addPoints {
    return [self generateLayerWithFrame:v.bounds directionalPoint:endPoint andPointsAddition:addPoints];
}

+ (CAGradientLayer*) generateHorizontalGradientLayerForView:(MPLView*)v withPointsAddition:(void(^)(MPLGradientLayer*l))addPoints {
    return [self generateLayerForView:v directionalPoint:CGPointMake(1, 0) andPointsAddition:addPoints];
}
+ (CAGradientLayer*) generateVerticalGradientLayerForView:(MPLView*)v withPointsAddition:(void(^)(MPLGradientLayer*l))addPoints {
    return [self generateLayerForView:v directionalPoint:CGPointMake(0, 1) andPointsAddition:addPoints];
}

@end
