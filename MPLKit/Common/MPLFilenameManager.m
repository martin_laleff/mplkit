//
//  FTFileManager.m
//  FTrack_Take2
//
//  Created by Martin Lalev on 10/6/11.
//  Copyright 2011 mART. All rights reserved.
//

#import "MPLFilenameManager.h"


@implementation MPLFilenameManager
+ (NSString*) copyFile:(NSString*)oldFilename to:(NSString*)newFilename overwrite:(BOOL)overwrite existed:(BOOL*)existed {
    (*existed) = [[NSFileManager defaultManager] fileExistsAtPath:newFilename];
    if ([[NSFileManager defaultManager] fileExistsAtPath:newFilename]) {
        if (overwrite)
            [[NSFileManager defaultManager] removeItemAtPath:newFilename error:nil];
        else
            return newFilename;
    }
    NSError* error = nil;
	NSString* result = ([[NSFileManager defaultManager] copyItemAtPath:oldFilename toPath:newFilename error:&error] ? newFilename : nil);
    return result;
}

+ (NSString*) localCopyOfResource:(NSString*)resource ofType:(NSString*)type to:(NSString*)filename overwrite:(BOOL)overwrite existed:(BOOL*)existed
{
    return [MPLFilenameManager copyFile:[[NSBundle mainBundle] pathForResource:resource ofType:type] to:filename overwrite:overwrite existed:existed];
}
+ (NSString*) localCopyOfResource:(NSString*)resource ofType:(NSString*)type overwrite:(BOOL)overwrite existed:(BOOL*)existed
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
	NSString *documentsDir = [paths objectAtIndex:0];
	NSString* filename = [documentsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", resource, type]];
	return [MPLFilenameManager localCopyOfResource:resource ofType:type to:filename overwrite:overwrite existed:existed];
}

@end
