//
//  MPLGradientLayer.h
//  MPLKit
//
//  Created by Martin Lalev on 4/21/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPLGradientLayer : NSObject

@property (nonatomic, strong) CAGradientLayer* layer;
@property (nonatomic, strong) NSMutableArray* colors;
@property (nonatomic, strong) NSMutableArray* locations;

- (void) addColor:(UIColor*)color atLocation:(float)location;
+ (CAGradientLayer*) generateLayerWithFrame:(CGRect)frame directionalPoint:(CGPoint)endPoint andPointsAddition:(void(^)(MPLGradientLayer*l))addPoints;
+ (CAGradientLayer*) generateLayerForView:(MPLView*)v directionalPoint:(CGPoint)endPoint andPointsAddition:(void(^)(MPLGradientLayer*l))addPoints;
+ (CAGradientLayer*) generateHorizontalGradientLayerForView:(MPLView*)v withPointsAddition:(void(^)(MPLGradientLayer*l))addPoints;
+ (CAGradientLayer*) generateVerticalGradientLayerForView:(MPLView*)v withPointsAddition:(void(^)(MPLGradientLayer*l))addPoints;

@end
