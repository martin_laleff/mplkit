//
//  MPLDictionaryObject.h
//  MPLKit
//
//  Created by Martin Lalev on 4/20/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPLDictionaryObject : NSObject

@property (nonatomic, strong) NSDictionary* dictionary;
@property (weak, nonatomic, readonly) NSString* objectId;

- (id) initWithDictionary:(NSDictionary*)dictionary;
- (void) willUpdateWithDictionary:(NSDictionary*)dictionary commit:(void (^)(NSDictionary* dictionary))commitUpdate;
- (void) didUpdate;

@end
