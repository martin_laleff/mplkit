//
//  FTAction.m
//  FTrack_Take2
//
//  Created by Administrator on 07/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MPLAction.h"
// Small Change

@implementation MPLAction

@synthesize Target;
@synthesize Selector;
@synthesize actionBlock;

@synthesize Control;

@synthesize Parameters;



- (id) parameterAtIndex:(int)index { return [self.Parameters objectForKey:[NSString stringWithFormat:@"%d",index]]; }
+ (NSMutableDictionary*) parametersForObjects:(id)parameter, ... {
	NSMutableDictionary* result = [NSMutableDictionary dictionary];
	id eachObject;
	va_list varList;
	int i = 0;
	if (parameter)
	{
		[result setObject:parameter forKey:[NSString stringWithFormat:@"%d",i]];
		va_start(varList, parameter);
		while ((eachObject = va_arg(varList, id))) {
			i++;
			[result setObject:eachObject forKey:[NSString stringWithFormat:@"%d",i]];
		}
		va_end(varList);
	}
	return result;
}
+ (NSMutableDictionary*) parametersForObject:(id)parameter { return [MPLAction parametersForObjects:parameter, nil]; }
+ (NSMutableDictionary*) parametersForObject:(id)parameter forKey:(NSString*)key { return (!parameter ? [NSMutableDictionary dictionary] : [NSMutableDictionary dictionaryWithObject:parameter forKey:key]); }



- (id) initWithTarget:(id)target andSelector:(SEL)selector {
	if ([self init]) {
		self.Target = target;
		self.Selector = selector;
	}
	return self;
}
- (id) initWithBlock:(id(^)(MPLAction*))action {
	if ([self init]) {
		self.actionBlock = action;
	}
	return self;
}
+ (MPLAction*) actionWithTarget:(id)target andSelector:(SEL)selector { return [[MPLAction alloc] initWithTarget:target andSelector:selector]; }
+ (MPLAction*) actionWithActionBlock:(id(^)(MPLAction*))action { return [[MPLAction alloc] initWithBlock:action]; }
+ (MPLAction*) actionWithBlock:(void(^)(MPLAction*))action { return [[MPLAction alloc] initWithBlock:^id(MPLAction* a){ action(a); return nil; }]; }
- (void) dealloc {
	self.Control = nil;
	self.Target = nil;
}


- (id) perform {
    id result = nil;
    if (self.actionBlock != nil) {
        result = self.actionBlock(self);
    }
    else {
        SEL selector = self.Selector;
        IMP implementn = [self.Target methodForSelector:selector];
        ((void (*)(id, SEL,id))implementn)(self.Target, self.Selector,self);
    }
	return result;
}
- (id) performWithParameters:(NSMutableDictionary*)parameters forControl:(MPLView*)control {
	self.Control = control;
	self.Parameters = parameters;
	return [self perform];
}
- (id) performInControl:(MPLView*)control {
	return [self performWithParameters:self.Parameters forControl:control];
}


static NSMutableDictionary* _synchronizedObjects;
+ (NSMutableDictionary*) synchronizedObjects {
    if (_synchronizedObjects == nil)
        _synchronizedObjects = [NSMutableDictionary dictionary];
    return _synchronizedObjects;
}
+ (BOOL) objectIsSynchronized:(id)obj {
    return [[[self synchronizedObjects] objectForKey:[NSValue valueWithPointer:(__bridge const void *)obj]] boolValue];
}
+ (void) setObject:(id)obj synchronized:(BOOL)sync {
    BOOL isSynced = [self objectIsSynchronized:obj];
    if (isSynced == sync) return;
    if (sync) [[self synchronizedObjects] setObject:@(YES) forKey:[NSValue valueWithPointer:(__bridge const void *)obj]];
    if (!sync) [[self synchronizedObjects] removeObjectForKey:[NSValue valueWithPointer:(__bridge const void *)obj]];
}
+ (void) performSynchronized:(void(^)(void(^finished)(void)))block basedOn:(id)object {
    if ([self objectIsSynchronized:object]) return;
    [self setObject:object synchronized:YES];
    block(^{
        [self setObject:object synchronized:NO];
    });
}
+ (void) performAfter:(float)delayInSeconds block:(void(^)(void))block {
    if (delayInSeconds == 0)
        block();
    else
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), block);
}
+ (void) performInBackgroundWithCallbackAndReturn:(id(^)(void(^finished)(void)))backgroundThread finished:(void(^)(id result))finished {
    __strong __block id something = nil;
    [self performInBackgroundWithCallback:^(void(^bgfinished)(void)) {
        something = backgroundThread(bgfinished);
    } finished:^{
        finished(something);
    }];
}
+ (void) performInBackgroundWithReturn:(id(^)(void))backgroundThread finished:(void(^)(id result))finished {
    __strong __block id something = nil;
    [self performInBackgroundWithCallback:^(void(^bgfinished)(void)) {
        something = backgroundThread();
        bgfinished();
    } finished:^{
        finished(something);
    }];
}
+ (void) performInBackground:(void(^)(void))backgroundThread finished:(void(^)(void))finished {
    [self performInBackgroundWithCallback:^(void(^bgfinished)(void)) {
        backgroundThread();
        bgfinished();
    } finished:^{
        finished();
    }];
}
+ (void) performInBackgroundWithCallback:(void(^)(void(^finished)(void)))backgroundThread finished:(void(^)(void))finished {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        backgroundThread(^{
            dispatch_async(dispatch_get_main_queue(), ^(void){
                finished();
            });
        });
    });
}


@end
