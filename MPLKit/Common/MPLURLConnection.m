//
//  FTHTTPRequest.m
//  TestApp
//
//  Created by Martin Lalev on 4/17/13.
//  Copyright (c) 2013 Shtrak. All rights reserved.
//

#import "MPLURLConnection.h"
#import "NSString+SBJSON.h"
#import "NSObject+SBJSON.h"
#import "NSString+URLEncoding.h"



@implementation MPLURLConnection

@synthesize Response;
@synthesize Connection;
@synthesize Data;
@synthesize LocalCertificates;
@synthesize CustomCA;
@synthesize SelfSignedCert;
@synthesize SendDate;
@synthesize Finished;

- (id) initWithURL:(NSURL *)URL
       cachePolicy:(NSURLRequestCachePolicy)cachePolicy
   timeoutInterval:(NSTimeInterval)timeoutInterval
{
    if ([super initWithURL:URL cachePolicy:cachePolicy timeoutInterval:timeoutInterval])
    {
        [self setAllHTTPHeaderFields:[NSMutableDictionary dictionary]];
    }
    return self;
}

- (void) setSSLAuthWithCertificates:(NSArray*)certs andCustomCA:(BOOL)customCA andSelfSignedCert:(BOOL)selfSignedCert {
    self.LocalCertificates = certs;
    self.CustomCA = customCA;
    self.SelfSignedCert = selfSignedCert;
}

- (id) initWithURL:(NSURL *)URL
       cachePolicy:(NSURLRequestCachePolicy)cachePolicy
   timeoutInterval:(NSTimeInterval)timeoutInterval
        andCookies:(NSDictionary*)cookies
        andHeaders:(NSDictionary*)headers
    andContentType:(NSString*)contentType
     andMethodType:(NSString*)methodType
       andBodyData:(NSData*)bodyData
{
    if ([self initWithURL:URL cachePolicy:cachePolicy timeoutInterval:timeoutInterval])
    {
        [self changeHTTPMethod:methodType];
        [self changeContentType:contentType];
        [self changeHTTPBody:bodyData];
        for (NSString* headerKey in [headers allKeys])
            [self addHeader:[headers objectForKey:headerKey] withKey:headerKey];
        for (NSString* cookieKey in [cookies allKeys])
            [self addCookie:[cookies objectForKey:cookieKey] withName:cookieKey];
    }
    return self;
}
- (id) initWithURL:(NSURL *)URL
       cachePolicy:(NSURLRequestCachePolicy)cachePolicy
   timeoutInterval:(NSTimeInterval)timeoutInterval
        andCookies:(NSDictionary*)cookies
        andHeaders:(NSDictionary*)headers
    andContentType:(NSString*)contentType
     andMethodType:(NSString*)methodType
     andBodyString:(NSString*)bodyString
{
    return [self initWithURL:URL
                 cachePolicy:cachePolicy
             timeoutInterval:timeoutInterval
                  andCookies:cookies
                  andHeaders:headers
              andContentType:contentType
               andMethodType:methodType
                 andBodyData:[bodyString dataUsingEncoding:NSUTF8StringEncoding]
            ];
}

#pragma mark -
#pragma mark Property settings

- (void) addHeader:(id)value withKey:(NSString*)name {
    [self setValue:[value description] forHTTPHeaderField:name];
}

- (void) changeContentType:(NSString*)contentType {
    [self addHeader:contentType withKey:@"Content-Type"];  //Common Content Types: (SOAP) @"text/xml; charset=utf-8";  (POST) @"application/x-www-form-urlencoded";
}
- (void) changeHTTPBody:(NSData*)bodyData {
    [self setHTTPBody:bodyData];
    [self addHeader:[NSString stringWithFormat:@"%lu", (unsigned long)[bodyData length]] withKey:@"Content-Length"];
}
- (void) changeHTTPMethod:(NSString*)methodType {
    [self setHTTPMethod:methodType];
}

- (void) addCookie:(NSString*)value withName:(NSString*)name andOriginURL:(NSURL*)originURL {
    NSHTTPCookie* cookie = [NSHTTPCookie cookieWithProperties:[NSDictionary dictionaryWithObjectsAndKeys:
                                                               @"http://www.shtrak.bg/", NSHTTPCookieOriginURL,
                                                               @"\\", NSHTTPCookiePath,
                                                               name, NSHTTPCookieName,
                                                               value, NSHTTPCookieValue,
                                                               nil
                                                               ]];
    NSDictionary* cookieD = [NSHTTPCookie requestHeaderFieldsWithCookies:@[cookie]];
    [self addHeader:[[cookieD allValues] objectAtIndex:0] withKey:[[cookieD allKeys] objectAtIndex:0]];
}
- (void) addCookie:(NSString*)value withName:(NSString*)name {
    [self addCookie:value withName:name andOriginURL:self.URL];
}

#pragma mark -
#pragma mark Lifecycle

- (void) finishedWithResponse:(NSURLResponse*)response andData:(NSData*)data andError:(NSError*)error {
    self.Finished(response, data, error);
    self.Response = nil;
    self.Data = nil;
    self.Connection = nil;
}
- (void) startWithFinishedPlistBlock:(FTURLFinishedIDBlock)finished {
    [self startWithFinishedDataBlock:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSError* err2;
        NSPropertyListFormat format;
        id result =
        [NSPropertyListSerialization propertyListWithData:data
                                                  options:NSPropertyListMutableContainersAndLeaves
                                                   format:&format
                                                    error:&err2
         ];
        
        finished(response, result, error);
    }];
}
- (void) startWithFinishedIDBlock:(FTURLFinishedIDBlock)finished {
    [self startWithFinishedStringBlock:^(NSURLResponse *response, NSString *string, NSError *error) {
        //        [NSString
        //         stringWithCString:[[string JSONValue] cStringUsingEncoding:NSUTF8StringEncoding]
        //         encoding:NSNonLossyASCIIStringEncoding];
        finished(response, [string JSONValue], error);
    }];
}
- (void) startWithFinishedStringBlock:(FTURLFinishedStringBlock)finished {
    [self startWithFinishedDataBlock:^(NSURLResponse *response, NSData *data, NSError *error) {
        finished(response, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding], error);
    }];
}
- (void) startWithFinishedDataBlock:(FTURLFinishedDataBlock)finished {
    self.SendDate = [NSDate date];
    self.Finished = finished;
    BOOL isIOS5OrLater = !NO;//[[NSURLConnection class] respondsToSelector:@selector(sendAsynchronousRequest:queue:completionHandler:)];
    if (isIOS5OrLater)
    {
        [NSURLConnection sendAsynchronousRequest:self
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse* response, NSData* data, NSError* error){
                                   self.Finished(response, data, error);
                               }
         ];
    }
    else
    {
        self.Connection = [NSURLConnection connectionWithRequest:self delegate:self];
    }
}
- (void) cancel { [self.Connection cancel]; }

#pragma mark -
#pragma mark NSURLConnection Delegate methods

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self finishedWithResponse:self.Response andData:self.Data andError:error];
}
- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData*)data {
    [self.Data appendData:data];
}
- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse*)response {
    self.Response = response;
    self.Data = [NSMutableData data];
}
- (void) connectionDidFinishLoading:(NSURLConnection*)connection {
    [self finishedWithResponse:self.Response andData:self.Data andError:nil];
}



- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return (!self.CustomCA && !self.SelfSignedCert ? NO : [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]);
}
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if (self.CustomCA) {
        CFMutableArrayRef localCertificates = CFArrayCreateMutable(NULL, self.LocalCertificates.count, NULL);
        for (NSString* certFile in self.LocalCertificates) {
            NSData* localCertData = [NSData dataWithContentsOfFile:certFile];
            SecCertificateRef localCert = SecCertificateCreateWithData(NULL, (__bridge CFDataRef)localCertData);
            CFArrayAppendValue(localCertificates, localCert);
        }
        SecTrustSetAnchorCertificates(challenge.protectionSpace.serverTrust, localCertificates);
        
        SecTrustResultType result;
        OSStatus status = SecTrustEvaluate(challenge.protectionSpace.serverTrust, &result);
        
        if (status == errSecSuccess && (result == kSecTrustResultProceed || result == kSecTrustResultUnspecified))
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        else
            [challenge.sender cancelAuthenticationChallenge:challenge];
    }
    else if (self.SelfSignedCert) {
        NSData* remoteCertData = (NSData*)CFBridgingRelease(SecCertificateCopyData(SecTrustGetCertificateAtIndex(challenge.protectionSpace.serverTrust, 0)));
        
        for (NSString* certFile in self.LocalCertificates) {
            NSData* localCertData = [NSData dataWithContentsOfFile:certFile];
            if ([localCertData isEqualToData:remoteCertData]) {
                [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
                return;
            }
        }
        
        [challenge.sender cancelAuthenticationChallenge: challenge];
    }
}



+ (NSString*) escapeStringPercented:(NSString*)unescaped
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                               NULL,
                                                               (CFStringRef)unescaped,
                                                               NULL,
                                                               (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                               kCFStringEncodingUTF8));
}


@end



@implementation MPLResponseDescriptor

//@synthesize connection;
@synthesize response;
@synthesize result;
@synthesize error;

@dynamic statusCode;
@dynamic jsonResult;
@dynamic success;
@dynamic responseError;
@dynamic serverClientSecondsOffset;
@dynamic timedOut;

- (long) statusCode { return self.response.statusCode; }
- (id) jsonResult { return [self.result JSONValue]; }
- (BOOL) success { return (self.statusCode/100 == 2); }
- (BOOL) timedOut {
    // <= -1001 ;;; >= -1021
    // <= -1100 ;;; >= -1103
    return (self.error.code <= NSURLErrorTimedOut && self.error.code >= NSURLErrorRequestBodyStreamExhausted) && (self.error.code <= NSURLErrorFileDoesNotExist && self.error.code >= NSURLErrorDataLengthExceedsMaximum);
}
- (NSDictionary*) responseError { return (self.jsonResult == nil || ![self.jsonResult isKindOfClass:[NSDictionary class]] ? nil : [self.jsonResult objectForKey:@"errors"]); }
- (long) serverClientSecondsOffset {
    NSDate* currentDate = [NSDate date];
    NSString* responseDateHeader = [self.response.allHeaderFields objectForKey:@"Date"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    dateFormatter.dateFormat = @"EEE, dd MMM yyyy HH:mm:ss zzz";
    NSDate* serverDate = [dateFormatter dateFromString:responseDateHeader];
    
    if (!serverDate) return 0;
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSSecondCalendarUnit fromDate:currentDate toDate:serverDate options:0];
    return components.second; // fix response dates by -offset
}

+ (MPLResponseDescriptor*) responseDescriptorForResponse:(NSHTTPURLResponse*)response andConnection:(MPLURLConnection*)connection andResult:(NSString*)result andError:(NSError*)error {
    MPLResponseDescriptor* rd = [[MPLResponseDescriptor alloc] init];
//    rd.connection = connection;
    rd.response = response;
    rd.result = result;
    rd.error = error;
    return rd;
}

@end



@implementation MPLWebService

@synthesize serviceReachability;
@dynamic serverReachable;

@synthesize usesSSL;
@synthesize urlHost;
@synthesize sslCerfificates;
@synthesize useCustomCA;
@synthesize useSelfSignedCert;

- (BOOL) serverReachable { return self.serviceReachability.reachable; }

- (id) initWithHost:(NSString*)host sslCertificates:(NSArray*)ssls useCustomCA:(BOOL)cca useSelfSignedCert:(BOOL)ssc {
    if ([super init]) {
        self.StandardArrayParam = YES;
        self.usesSSL = YES;
        self.urlHost = host;
        self.sslCerfificates = ssls;
        self.useCustomCA = cca;
        self.useSelfSignedCert = ssc;
        self.serviceReachability = [Reachability reachabilityWithHostName:self.urlHost];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    }
    return self;
}

- (void) logResponse:(MPLResponseDescriptor *)response forConnection:(MPLURLConnection *)connection {
    //    if (!self.loggingEnabled) return;
    NSString *logString = [NSString stringWithFormat:@"METHOD(%@)\nURL(%@)\nBODY(%@)\nHEADERS(%@)\nSTATUS(%ld)\nRESPONSE(%@)\nERROR(%@)\n\n\n\n", connection.HTTPMethod, connection.URL, [[NSString alloc] initWithData:connection.HTTPBody encoding:NSUTF8StringEncoding], connection.allHTTPHeaderFields, response.statusCode, response.result, response.error];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentTXTPath = [documentsDirectory stringByAppendingPathComponent:@"Logs.txt"];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:documentTXTPath])
        [@"" writeToFile:documentTXTPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    NSFileHandle *myHandle = [NSFileHandle fileHandleForWritingAtPath:documentTXTPath];
    [myHandle seekToEndOfFile];
    [myHandle writeData:[logString dataUsingEncoding:NSUTF8StringEncoding]];
    
}
- (void) reachabilityChanged:(NSNotification *)note {}

- (void) appendToParametersString:(NSMutableString*)parametersString key:(NSString*)key value:(id)v arrayItem:(BOOL)arrayItem bodyParameter:(BOOL)bodyParam {
    NSString* separator = (parametersString.length == 0 ? (bodyParam ? @"" : @"?") : @"&");
    if (arrayItem && self.StandardArrayParam) key = [NSString stringWithFormat:@"%@[]",key];
    if ([v isKindOfClass:[NSString class]]) v = [v urlEncodeUsingEncoding:NSUTF8StringEncoding];
    [parametersString appendFormat:@"%@%@=%@", separator, [key urlEncodeUsingEncoding:NSUTF8StringEncoding], v];
}
- (NSString*) parametersStringForParameters:(NSDictionary*)parameters bodyParameters:(BOOL)body {
    if (!parameters) parameters = @{};
    NSMutableString* parametersString = [NSMutableString string];
    for (NSString* param in parameters.allKeys) {
        id value = [parameters objectForKey:param];
        
        if ([value isKindOfClass:[NSArray class]]) {
            for (id v in value)
                [self appendToParametersString:parametersString key:param value:v arrayItem:YES bodyParameter:body];
        }
        else
            [self appendToParametersString:parametersString key:param value:value arrayItem:NO bodyParameter:body];
    }
    return parametersString;
}

- (void) performConnection:(MPLURLConnection*)conn withCallback:(void(^)(MPLResponseDescriptor*))callback andTimeoutCallback:(void(^)(MPLURLConnection*))timeoutCallback {
    [self performConnection:conn withCallback:^(MPLResponseDescriptor *rd) {
        if (rd.timedOut)
        {
            timeoutCallback(conn);
        }
        else callback(rd);
    }];
}

- (void) performConnection:(MPLURLConnection*)conn withCallback:(void(^)(MPLResponseDescriptor*))callback {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [conn startWithFinishedStringBlock:^(NSURLResponse *response, NSString* result, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        callback([MPLResponseDescriptor responseDescriptorForResponse:(NSHTTPURLResponse*)response andConnection:conn andResult:result andError:error]);
    }];
}
- (MPLURLConnection*) connectionForMethodName:(NSString*)methodName andResourceName:(NSString*)resourceName andURLParameters:(NSDictionary*)urlParameters andBodyParameters:(NSDictionary*)bodyParameters andHeaders:(NSDictionary*)headers andCookies:(NSDictionary*)cookies andTimeout:(NSTimeInterval)timeout andContentType:(NSString*)contentType {
    NSString* parametersStringURL = [self parametersStringForParameters:urlParameters bodyParameters:NO];
    NSString* parametersStringBody = [self parametersStringForParameters:bodyParameters bodyParameters:YES];
    NSString* urlString = [NSString stringWithFormat:@"http%@://%@/%@%@", usesSSL ? @"s" : @"", self.urlHost, resourceName, parametersStringURL];
    
    MPLURLConnection* conn = [[MPLURLConnection alloc] initWithURL:[NSURL URLWithString:urlString]
                                                       cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                   timeoutInterval:timeout
                                                        andCookies:cookies
                                                        andHeaders:headers
                                                    andContentType:contentType
                                                     andMethodType:methodName
                                                     andBodyString:parametersStringBody
                              ];
    if (self.sslCerfificates.count > 0) [conn setSSLAuthWithCertificates:self.sslCerfificates andCustomCA:self.useCustomCA andSelfSignedCert:self.useSelfSignedCert];
    return conn;
}
- (void) performConnectionForMethodName:(NSString*)methodName andResourceName:(NSString*)resourceName andURLParameters:(NSDictionary*)urlParameters andBodyParameters:(NSDictionary*)bodyParameters andHeaders:(NSDictionary*)headers andCookies:(NSDictionary*)cookies andTimeout:(NSTimeInterval)timeout andContentType:(NSString*)contentType withCallback:(void(^)(MPLResponseDescriptor*))callback andTimeoutCallback:(void(^)(MPLURLConnection*))timeoutCallback {
    MPLURLConnection* conn = [self connectionForMethodName:methodName andResourceName:resourceName andURLParameters:urlParameters andBodyParameters:bodyParameters andHeaders:headers andCookies:cookies andTimeout:timeout andContentType:contentType];
    [self performConnection:conn withCallback:callback andTimeoutCallback:timeoutCallback];
}

@end
