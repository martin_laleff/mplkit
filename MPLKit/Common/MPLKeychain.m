//
//  MPLKeychain.m
//  MPLKit
//
//  Created by Martin Lalev on 3/26/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLKeychain.h"
#import <CommonCrypto/CommonDigest.h>
#import "MPLBase64Encoder.h"

static NSString* keychainAccessGroup;

@implementation MPLKeychain

+ (void) setKeychainAccessGroup:(NSString*)value { keychainAccessGroup = value; }
+ (NSString*) keychainAccessGroup { return keychainAccessGroup; }

+ (KeychainItemWrapper*) keychainForName:(NSString*)name andAccessGroup:(NSString*)accessGroup {
    KeychainItemWrapper* kiw = [[KeychainItemWrapper alloc] initWithIdentifier:name accessGroup:accessGroup];
    if ([[kiw objectForKey:(__bridge id)(kSecAttrAccount)] length] == 0) {
        
        CFUUIDRef uuid = CFUUIDCreate(NULL);
        NSString* accountName = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, uuid));
        CFRelease(uuid);
        
        [kiw setObject:accountName forKey:(__bridge id)(kSecAttrAccount)];
        [kiw setObject:(__bridge id)(kSecAttrAccessibleAlways) forKey:(__bridge id)(kSecAttrAccessible)];
        
    }
    return kiw;
}

+ (void) setObject:(id)obj forKeychain:(KeychainItemWrapper*)keychain {
    [keychain setObject:[MPLBase64Encoder encodeData:[NSKeyedArchiver archivedDataWithRootObject:obj]] forKey:(__bridge id)(kSecValueData)];
}
+ (void) setObject:(id)obj forKeychainWithName:(NSString*)name andAccessGroup:(NSString*)accessGroup {
    KeychainItemWrapper* kiw = [self keychainForName:name andAccessGroup:accessGroup];
    [self setObject:obj forKeychain:kiw];
}

+ (id) objectForKeychain:(KeychainItemWrapper*)keychainWraper {
    id item = [MPLBase64Encoder decodeString:[keychainWraper objectForKey:(__bridge id)kSecValueData]];
    if (item) {
        @try{ item = [NSKeyedUnarchiver unarchiveObjectWithData:item]; }
        @catch(NSException* e) { MPLLog(@"Unable to unarchive item!"); item = nil; }
    }
    return item;
}
+ (id) objectForKeychainWithName:(NSString*)name andAccessGroup:(NSString*)accessGroup {
    KeychainItemWrapper* kiw = [self keychainForName:name andAccessGroup:accessGroup];
    id item = [self objectForKeychain:kiw];
    return item;
}

@end
