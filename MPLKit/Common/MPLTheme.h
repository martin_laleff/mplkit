//
//  MPLTheme.h
//  MPLKit
//
//  Created by Martin Lalev on 12/7/15.
//  Copyright © 2015 Martin Lalev. All rights reserved.
//

#define MPLPROPERTYNAME [[[NSString stringWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"]" withString:@""] stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"-[%@ ", NSStringFromClass(self.class)] withString:@""]
#define MPLPROPERTYSETNAME_NOTLC [MPLPROPERTYNAME substringWithRange:NSMakeRange(3, MPLPROPERTYNAME.length-4)]
#define MPLThemeInitialize(name) - (id) initWithDictionary:(NSDictionary *)otherDictionary { return [super initWithDictionary:otherDictionary andName:name]; }
#define MPLThemeSynthesizeColor(g) \
- (UIColor*) g { return [self colorForKey:MPLPROPERTYNAME]; } \
- (void) set##g :(UIColor *)value { [self setColor:value forKey:MPLPROPERTYSETNAME_NOTLC]; } \
- (NSString*) key_##g { return [MPLPROPERTYNAME substringFromIndex:4]; }

#define stringFor(g) g
#define MPLThemePropertyColor(g) \
@property (nonatomic, assign) UIColor* g; \
@property (nonatomic, readonly) NSString* key_##g;

#define MPLCanUseEdgePan __IPHONE_OS_VERSION_MAX_ALLOWED > 60100
@class MPLThemeViewAttatchment;
@interface MPLTheme : NSObject

@property (nonatomic, strong) NSMutableDictionary* objects;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, assign) int preventNotifications;
@property (nonatomic, strong) NSMutableArray* preventedNotifications;

- (id) initWithDictionary:(NSDictionary*)otherDictionary andName:(NSString*)name;
- (NSString*) notificationNameForKey:(NSString*)key;
- (UIColor*) colorForKey:(id)key;
- (void) setColor:(UIColor*)color forKey:(id)key;
- (MPLThemeViewAttatchment*) attachmentToKey:(NSString*)key andParameters:(NSDictionary*)parameters andActionBlock:(void (^)(MPLThemeViewAttatchment* themeAttatchment))actionBlock;
- (MPLThemeViewAttatchment*) attachmentToKey:(NSString*)key andActionBlock:(void (^)(MPLThemeViewAttatchment* themeAttatchment))actionBlock;

@end

@class MPLView;
@interface MPLThemeViewAttatchment : NSObject
@property (nonatomic, strong) MPLTheme* theme;
@property (nonatomic, strong) NSString* key;
@property (nonatomic, strong) NSMutableDictionary* parameters;
@property (nonatomic, copy) void (^actionBlock)(MPLThemeViewAttatchment* themeAttatchment);
@property (weak, nonatomic, readonly) NSString* noteName;
@property (weak, nonatomic, readonly) id value;
@property (weak, nonatomic, readonly) UIColor* colorValue;
- (void) performAction;
+ (instancetype) themeViewAttatchmentTheme:(MPLTheme*)theme andKey:(NSString*)key andParameters:(NSDictionary*)parameters andActionBlock:(void (^)(MPLThemeViewAttatchment* themeAttatchment))actionBlock;
@end
