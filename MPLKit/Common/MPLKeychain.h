//
//  MPLKeychain.h
//  MPLKit
//
//  Created by Martin Lalev on 3/26/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeychainItemWrapper.h"




@interface MPLKeychain : NSObject

+ (void) setKeychainAccessGroup:(NSString*)value;
+ (void) setObject:(id)obj forKeychainWithName:(NSString*)name andAccessGroup:(NSString*)accessGroup;
+ (id) objectForKeychainWithName:(NSString*)name andAccessGroup:(NSString*)accessGroup;

@end
