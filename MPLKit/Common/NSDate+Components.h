//
//  NSDate+Components.h
//  MPLKit
//
//  Created by Martin Lalev on 4/17/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Components)

@property (nonatomic, readonly) NSDateComponents* dateComponents;

@property (nonatomic, readonly) NSInteger year;
@property (nonatomic, readonly) NSInteger month;
@property (nonatomic, readonly) NSInteger day;
@property (nonatomic, readonly) NSInteger weekday;
@property (nonatomic, readonly) NSInteger weekdayToCurrentCalendar;
@property (nonatomic, readonly) NSInteger hour;
@property (nonatomic, readonly) NSInteger minute;
@property (nonatomic, readonly) NSInteger second;

@property (nonatomic, readonly) NSInteger weekdayBase1;
@property (nonatomic, readonly) NSInteger weekdayBase2;

@property (nonatomic, readonly) NSInteger daysInMonth;
- (NSInteger) weekdayForBase:(NSInteger)base;

- (NSDate*) dateByAddingSeconds:(int)seconds;
- (NSDate*) dateByAddingMinutes:(int)minutes;
- (NSDate*) dateByAddingHours:(int)hours;
- (NSDate*) dateByAddingDays:(int)days;
//- (NSDate*) dateByAddingMonths:(int)months;
//- (NSDate*) dateByAddingYears:(int)years;
- (NSDate*) dateByAddingDays:(int)days hours:(int)hours minutes:(int)minutes seconds:(int)seconds;

- (NSDate*) dateBySettingDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year weekOfYear:(NSInteger)weekOfYear yearForWeekOfYear:(NSInteger)yearForWeekOfYear;
- (NSDate*) dateBySettingHour:(NSInteger)hour minute:(NSInteger)minute;

- (NSString*) formatWith:(NSString*)formatString;
- (NSString*) formatWith:(NSString*)formatString withLocaleIdentifier:(NSString*)localeIdentifier;
- (NSString*) formatWith:(NSString*)formatString withLocale:(NSLocale*)locale;

+ (NSDate*) dateForYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;

+ (NSArray*) weekdays;
+ (NSArray*) weekdaysWithLocaleIdentifier:(NSString*)localeIdentifier;
+ (NSArray*) weekdaysWithLocale:(NSLocale*)locale;

@end


@interface NSString (DateConvertion)

- (NSDate*) dateWithFormat:(NSString*)formatString;

@end