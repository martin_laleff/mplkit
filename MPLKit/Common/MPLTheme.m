//
//  MPLTheme.m
//  MPLKit
//
//  Created by Martin Lalev on 12/7/15.
//  Copyright © 2015 Martin Lalev. All rights reserved.
//

#import "MPLTheme.h"

@implementation MPLTheme

@synthesize objects = _objects;
@synthesize name = _name;
@synthesize preventNotifications = _preventNotifications;
@synthesize preventedNotifications = _preventedNotifications;

- (void) setPreventNotifications:(int)preventNotifications {
    _preventNotifications = preventNotifications;
    if (preventNotifications == 0) {
        while (self.preventedNotifications.count > 0) {
            NSString* noteName = [self.preventedNotifications objectAtIndex:0];
            [[NSNotificationCenter defaultCenter] postNotificationName:noteName object:nil userInfo:nil];
            [self.preventedNotifications removeObjectAtIndex:0];
        }
    }
}

- (id) initWithDictionary:(NSDictionary*)otherDictionary andName:(NSString*)name {
    if (![self init]) return self;
    self.name = name;
    self.objects = [NSMutableDictionary dictionaryWithDictionary:otherDictionary];
    self.preventedNotifications = [NSMutableArray array];
    return self;
}

- (NSString*) notificationNameForKey:(NSString*)key {
    return [NSString stringWithFormat:@"%@_%@", self.name, key];
}
- (id) objectForKeyedSubscript: (id) key {
    return self.objects[key];
}
- (void) setObject: (id) thing  forKeyedSubscript: (id) key {
    self.objects[key] = thing;
    NSString* noteName = [self notificationNameForKey:key];
    if (self.preventNotifications != 0) {
        if (![self.preventedNotifications containsObject:noteName])
            [self.preventedNotifications addObject:noteName];
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:noteName object:nil userInfo:nil];
    }
}

- (UIColor*) colorForKey:(id)key {
    NSString* strColor = self[key];
    float r = 0, g = 0, b = 0;
    if ([strColor rangeOfString:@"_"].length == 1) {
        NSArray* comp = [strColor componentsSeparatedByString:@"_"];
        r = [[comp objectAtIndex:0] floatValue];
        g = [[comp objectAtIndex:1] floatValue];
        b = [[comp objectAtIndex:2] floatValue];
    }
    else if ([strColor rangeOfString:@","].length == 1) {
        NSArray* comp = [strColor componentsSeparatedByString:@","];
        r = [[comp objectAtIndex:0] intValue]/255.;
        g = [[comp objectAtIndex:1] intValue]/255.;
        b = [[comp objectAtIndex:2] intValue]/255.;
    }
    return [UIColor colorWithRed:r green:g blue:b alpha:1];
}
- (void) setColor:(UIColor*)color forKey:(id)key {
    CGFloat r,g,b;
    [color getRed:&r green:&g blue:&b alpha:nil];
    self[key] = [NSString stringWithFormat:@"%.5f_%.5f_%.5f",r,g,b];
}
- (MPLThemeViewAttatchment*) attachmentToKey:(NSString*)key andParameters:(NSDictionary*)parameters andActionBlock:(void (^)(MPLThemeViewAttatchment* themeAttatchment))actionBlock {
    return [MPLThemeViewAttatchment themeViewAttatchmentTheme:self andKey:key andParameters:parameters andActionBlock:actionBlock];
}
- (MPLThemeViewAttatchment*) attachmentToKey:(NSString*)key andActionBlock:(void (^)(MPLThemeViewAttatchment* themeAttatchment))actionBlock {
    return [MPLThemeViewAttatchment themeViewAttatchmentTheme:self andKey:key andParameters:@{} andActionBlock:actionBlock];
}

@end


@implementation MPLThemeViewAttatchment
@synthesize theme = _theme;
@synthesize key = _key;
@synthesize parameters = _parameters;
@synthesize actionBlock = _actionBlock;

@synthesize value;
@synthesize colorValue;
- (id) value { return self.theme.objects[self.key]; }
- (UIColor*) colorValue { return [self.theme colorForKey:self.key]; }
- (void) performAction { self.actionBlock(self); }
- (NSString*) noteName { return [self.theme notificationNameForKey:self.key]; }
+ (instancetype) themeViewAttatchmentTheme:(MPLTheme*)theme andKey:(NSString*)key andParameters:(NSDictionary*)parameters andActionBlock:(void (^)(MPLThemeViewAttatchment* themeAttatchment))actionBlock {
    MPLThemeViewAttatchment* tva = [[MPLThemeViewAttatchment alloc] init];
    tva.theme = theme;
    tva.key = key;
    tva.parameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    tva.actionBlock = actionBlock;
    [[NSNotificationCenter defaultCenter] addObserver:tva selector:@selector(performAction) name:tva.noteName object:nil];
    return tva;
}
- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:self.noteName object:nil];
}

@end
