//
//  MPLDictionaryObjectSet.h
//  MPLKit
//
//  Created by Martin Lalev on 4/25/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLDictionaryObjectManager.h"

@interface MPLDictionaryObjectSet : NSObject

@property (nonatomic, strong) NSDate* lastUpdateDate;
@property (nonatomic, assign) BOOL upToDate;
@property (nonatomic, strong) MPLDictionaryObjectManager* manager;
@property (nonatomic, strong) NSArray* items;
@property (nonatomic, strong) NSMutableDictionary* parameters;

- (id) initWithParameters:(NSMutableDictionary*)parameters andManager:(MPLDictionaryObjectManager*)manager;

- (void) loadItemsSuccess:(void(^)(NSArray* items, BOOL wereCached))loadSuccess fail:(void(^)(id error))loadFail;
- (void) updateItemsSuccess:(void(^)(NSArray* dictionaries))loadSuccess fail:(void(^)(id error))loadFail;

- (void) didUpdateParameterWithKey:(NSString*)key;
- (void) updateParamaterWithKey:(NSString*)key toValue:(id)value;
- (BOOL) compareParamaterWithKey:(NSString*)key currentValue:(id)currentValue withValue:(id)value;
- (BOOL) compareArrayWithCurrentValue:(NSArray*)currentValue andUpdatedValue:(NSArray*)updatedValue;

- (NSArray*) dictionariesToItems:(NSArray*)items;

- (instancetype) updateItemsByAdding:(NSArray*)itemsToAdd;
- (instancetype) updateItemsByRemoving:(NSArray*)itemsToRemove;

@end
