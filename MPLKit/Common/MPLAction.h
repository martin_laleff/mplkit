//
//  FTAction.h
//  FTrack_Take2
//
//  Created by Administrator on 07/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MPLView;

@interface MPLAction : NSObject {
	SEL Selector;
	id __weak Target;
	MPLView* __weak Control;
    id (^actionBlock)(MPLAction*);

	NSMutableDictionary* Parameters;
}

@property (nonatomic, assign) SEL Selector;
@property (nonatomic, weak) id Target;
@property (nonatomic, copy) id (^actionBlock)(MPLAction*);

@property (nonatomic, weak) MPLView* Control;

@property (nonatomic, strong) NSMutableDictionary* Parameters;

- (id) initWithTarget:(id)target andSelector:(SEL)selector;

+ (MPLAction*) actionWithTarget:(id)target andSelector:(SEL)selector;
+ (MPLAction*) actionWithBlock:(void(^)(MPLAction* action))action;

+ (NSMutableDictionary*) parametersForObjects:(id)parameter, ...;
+ (NSMutableDictionary*) parametersForObject:(id)parameter;
+ (NSMutableDictionary*) parametersForObject:(id)parameter forKey:(NSString*)key;

- (id) parameterAtIndex:(int)index;
- (id) perform;
- (id) performWithParameters:(NSMutableDictionary*)parameters forControl:(MPLView*)control;
- (id) performInControl:(MPLView*)control;

+ (void) performAfter:(float)delayInSeconds block:(void(^)(void))block;
+ (void) performSynchronized:(void(^)(void(^finished)(void)))block basedOn:(id)object;
+ (void) performInBackground:(void(^)(void))backgroundThread finished:(void(^)(void))finished;
+ (void) performInBackgroundWithCallback:(void(^)(void(^finished)(void)))backgroundThread finished:(void(^)(void))finished;
+ (void) performInBackgroundWithReturn:(id(^)(void))backgroundThread finished:(void(^)(id result))finished;
+ (void) performInBackgroundWithCallbackAndReturn:(id(^)(void(^finished)(void)))backgroundThread finished:(void(^)(id result))finished;

@end

