//
//  NSDate+Components.m
//  MPLKit
//
//  Created by Martin Lalev on 4/17/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "NSDate+Components.h"

@implementation NSDate (Components)

// http://unicode.org/reports/tr35/tr35-6.html#Date_Format_Patterns
- (NSDateComponents*) dateComponents {
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
//    [calendar setTimeZone:[NSTimeZone localTimeZone]];
//    [calendar setFirstWeekday:2]; // Sunday == 1, Saturday == 7
//    NSUInteger adjustedWeekdayOrdinal = [calendar ordinalityOfUnit:NSWeekdayCalendarUnit inUnit:NSWeekCalendarUnit forDate:[NSDate date]];
    return [calendar componentsInTimeZone:[NSTimeZone localTimeZone] fromDate:self];
//    return [calendar components:
//            NSCalendarUnitDay |
//            NSCalendarUnitMonth |
//            NSCalendarUnitYear |
//            NSCalendarUnitHour |
//            NSCalendarUnitMinute |
//            NSCalendarUnitSecond |
//            NSCalendarUnitNanosecond |
//            NSCalendarUnitEra |
//            NSCalendarUnitQuarter |
//            NSCalendarUnitWeekday |
//            NSCalendarUnitTimeZone |
//            NSCalendarUnitYearForWeekOfYear |
//            NSCalendarUnitWeekOfYear |
//            NSCalendarUnitWeekOfMonth |
//            NSCalendarUnitCalendar |
//            NSCalendarUnitWeekdayOrdinal
//                                           fromDate:self];
}

- (NSDate*) dateBySettingDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year weekOfYear:(NSInteger)weekOfYear yearForWeekOfYear:(NSInteger)yearForWeekOfYear {
    NSDateComponents* c = self.dateComponents;
    c.day = day;
    c.month = month;
    c.year = year;
    c.yearForWeekOfYear = yearForWeekOfYear;
    c.weekOfYear = weekOfYear;
    NSDate* d = [[NSCalendar currentCalendar] dateFromComponents:c];
    return d;
}

- (NSDate*) dateBySettingHour:(NSInteger)hour minute:(NSInteger)minute {
    NSDateComponents* c = self.dateComponents;
    c.hour = hour;
    c.minute = minute;
    return [[NSCalendar currentCalendar] dateFromComponents:c];
}
- (NSInteger) day { return self.dateComponents.day; }
- (NSInteger) month { return self.dateComponents.month; }
- (NSInteger) year { return self.dateComponents.year; }
- (NSInteger) hour { return self.dateComponents.hour; }
- (NSInteger) minute { return self.dateComponents.minute; }
- (NSInteger) second { return self.dateComponents.second; }
- (NSInteger) weekday { return self.dateComponents.weekday; }
- (NSInteger) weekdayToCurrentCalendar {
    NSUInteger result = self.dateComponents.weekday - ([NSCalendar currentCalendar].firstWeekday-1);
    return (result == 0 ? 7 : result);
}
+ (NSArray*) weekdays { return [self weekdaysWithLocale:[NSLocale currentLocale]]; }
+ (NSArray*) weekdaysWithLocaleIdentifier:(NSString*)localeIdentifier { return [self weekdaysWithLocale:[NSLocale localeWithLocaleIdentifier:localeIdentifier]]; }
+ (NSArray*) weekdaysWithLocale:(NSLocale*)locale {
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    if (locale != nil) [df setLocale:locale];
    NSMutableArray* weekdays = [NSMutableArray arrayWithArray:[df weekdaySymbols]];
    for (NSUInteger firstWeekDay = [NSCalendar currentCalendar].firstWeekday-1;firstWeekDay>0;firstWeekDay--) {
        [weekdays addObject:[weekdays firstObject]];
        [weekdays removeObjectAtIndex:0];
    }
    return weekdays;
}

- (NSInteger) weekdayBase1 {
    NSInteger weekday = self.weekday;
    if ([NSCalendar currentCalendar].firstWeekday != 1) {
        weekday += ([NSCalendar currentCalendar].firstWeekday - 1);
        if (weekday > 7) weekday-=7;
    }
    return weekday;
}
- (NSInteger) weekdayForBase:(NSInteger)base {
    if (base < 1 || base > 7) return 0;
    NSInteger weekday = self.weekdayBase1;
    weekday -= (base-1);
    if (weekday < 0) weekday += 7;
    return weekday;
}
- (NSInteger) weekdayBase2 { return [self weekdayForBase:2]; }

- (NSString*) formatWith:(NSString*)formatString {
    return [self formatWith:formatString withLocale:nil];
}
- (NSString*) formatWith:(NSString*)formatString withLocaleIdentifier:(NSString*)localeIdentifier {
    return [self formatWith:formatString withLocale:localeIdentifier == nil ? nil : [NSLocale localeWithLocaleIdentifier:localeIdentifier]];
}
- (NSString*) formatWith:(NSString*)formatString withLocale:(NSLocale*)locale {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    if (locale != nil) [formatter setLocale:locale];
    [formatter setTimeZone:[NSTimeZone localTimeZone]]; //systemTimeZone
    [formatter setDateFormat:formatString];
    NSString* result = [formatter stringFromDate:self];
    return result;
}

- (NSDate*) dateByAddingSeconds:(int)seconds {
    return [self dateByAddingTimeInterval:seconds];
}
- (NSDate*) dateByAddingMinutes:(int)minutes {
    return [self dateByAddingSeconds:minutes*60];
}
- (NSDate*) dateByAddingHours:(int)hours {
    return [self dateByAddingMinutes:hours*60];
}
- (NSDate*) dateByAddingDays:(int)days {
    return [self dateByAddingHours:days*24];
}
- (NSDate*) dateByAddingDays:(int)days hours:(int)hours minutes:(int)minutes seconds:(int)seconds {
    return [[[[self dateByAddingSeconds:seconds] dateByAddingMinutes:minutes] dateByAddingHours:hours] dateByAddingDays:days];
}

+ (NSDate*) dateForYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    return [[NSString stringWithFormat:@"%04li-%02li-%02li",(long)year,(long)month,(long)day] dateWithFormat:@"yyyy-MM-dd"];
}

- (NSInteger) daysInMonth
{
    NSDateComponents* comps = [[NSDateComponents alloc] init];
    [comps setMonth:self.month];
    return [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:self].length;
}

@end

@implementation NSString (DateConvertion)

- (NSDate*) dateWithFormat:(NSString*)formatString {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]]; //systemTimeZone
    [formatter setDateFormat:formatString];
    NSDate* result = [formatter dateFromString:self];
    return result;
}

@end