//
//  FTPListFileManager.m
//  FTrack_Take2
//
//  Created by Administrator on 29/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MPLPListManager.h"


@implementation MPLPListManager

- (id) initWithFilename:(NSString*)filename
{
	if ([super init])
	{
		self.filename = filename;
    }
    return self;
}
- (id) initWithFilename:(NSString *)filename version:(NSString*)version dataGeneration:(NSDictionary*(^)(void))generateNewData dataLoader:(void(^)(void))loadData {
    BOOL existed = NO;
    if ([self initWithFilename:[MPLFilenameManager localCopyOfResource:filename ofType:@"plist" overwrite:NO existed:&existed]]) {
        [self loadData];
        if (![self.version isEqualToString:version]) {
            NSMutableDictionary* d = [NSMutableDictionary dictionaryWithDictionary:generateNewData()];
            d[@"version"] = version;
            self.item = d;
            [self saveData];
        }
        loadData();
    }
    return self;
}

- (NSString*) version { return self.item[@"version"]; }


- (NSData*) readDataFor:(NSData*)data { return data; }
- (NSData*) writeDataFor:(NSData*)data { return data; }
- (NSString*) loadData {
    NSString* error = nil;
    NSData* data = [NSData dataWithContentsOfFile:self.filename];
    self.item = [NSPropertyListSerialization propertyListFromData:[self readDataFor:data] mutabilityOption:NSPropertyListMutableContainersAndLeaves format:&format errorDescription:&error];
    return error;
}
- (void) saveData
{
	NSString* error;
	NSData* data = [NSPropertyListSerialization dataFromPropertyList:self.item format:format errorDescription:&error];
	if (error)
		MPLLog(@"Save plist error: %@", error);
    else
        [[self writeDataFor:data] writeToFile:self.filename atomically:YES];
}


+ (MPLPListManager*) plistManagerForFilename:(NSString*)filename {
	return [[MPLPListManager alloc] initWithFilename:filename];
}
+ (MPLPListManager*) plistManagerForResource:(NSString*)resource {
	return [MPLPListManager plistManagerForFilename:[[NSBundle mainBundle] pathForResource:resource ofType:@"plist"]];
}

+ (MPLPListManager*) loadedPlistManagerForFilename:(NSString*)filename {
	MPLPListManager* plist = [MPLPListManager plistManagerForFilename:filename];
    return ([plist loadData].length > 0 ? nil : plist);
}
+ (MPLPListManager*) loadedPlistManagerForResource:(NSString*)resource {
	MPLPListManager* plist = [MPLPListManager plistManagerForResource:resource];
    return ([plist loadData].length > 0 ? nil : plist);
}

@end
