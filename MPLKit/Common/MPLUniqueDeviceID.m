//
//  MYUDID.m
//  TestApp
//
//  Created by Martin Lalev on 10/7/13.
//  Copyright (c) 2013 Shtrak. All rights reserved.
//

#import "MPLUniqueDeviceID.h"

@implementation MPLUniqueDeviceID

static NSString* accessGroup;

@synthesize keychainWraper = _keychainWraper;

+ (void) setAccessGroup:(NSString*)value {
    accessGroup = value;
}
+ (NSString*) accessGroup {
    return accessGroup;
}

+ (KeychainItemWrapper*) createKeychainItem {
    KeychainItemWrapper* keychainWraper = [[KeychainItemWrapper alloc] initWithIdentifier:@"UUID" accessGroup:[MPLUniqueDeviceID accessGroup]];
    [keychainWraper setObject:@"UUID" forKey:(__bridge id)kSecAttrAccount];
    return keychainWraper;
}

+ (NSString*) udid {
    KeychainItemWrapper* keychainWraper = [MPLUniqueDeviceID createKeychainItem];
    NSString* result = [keychainWraper objectForKey:(__bridge id)kSecValueData];
    if (!result || result.length == 0) {
        [self resetUDID];
        result = [keychainWraper objectForKey:(__bridge id)kSecValueData];
    }
    return result;
}
+ (NSString*) genereteUUID {
    CFUUIDRef cfuuid = CFUUIDCreate(NULL);
    NSString* value = (NSString*)CFBridgingRelease(CFUUIDCreateString(NULL, cfuuid));
    CFRelease(cfuuid);
    return value;
}
+ (void) resetUDID {
    KeychainItemWrapper* keychainWraper = [MPLUniqueDeviceID createKeychainItem];
    [keychainWraper setObject:[self genereteUUID] forKey:(__bridge id)kSecValueData];
}

@end
