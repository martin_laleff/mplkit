//
//  MPLPushNotificationsManager.m
//  MPLKit
//
//  Created by Martin Lalev on 3/27/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLPushNotificationsManager.h"



@implementation MPLPushNotificationsManager

- (id) initWithNotificationsType:(UIRemoteNotificationType)type {
    if ([super init]) {
        self.notificationsType = type;
        self.receivedRemoteNotification = nil;
    }
    return self;
}
- (UIUserNotificationType) notificationsType_iOS8 {
    UIUserNotificationType type = UIUserNotificationTypeNone;
    if (self.notificationsType & UIRemoteNotificationTypeAlert) type |= UIUserNotificationTypeAlert;
    if (self.notificationsType & UIRemoteNotificationTypeBadge) type |= UIUserNotificationTypeBadge;
    if (self.notificationsType & UIRemoteNotificationTypeSound) type |= UIUserNotificationTypeSound;
    return type;
}
- (BOOL) userSettingsAlertsEnabled {
    if ([__MPLApplication respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        return (__MPLApplication.currentUserNotificationSettings.types & UIUserNotificationTypeAlert);
    }
    else
        return (__MPLApplication.enabledRemoteNotificationTypes & UIRemoteNotificationTypeAlert);
}

@end





@implementation MPLPushNotificationsManager (Registration)

+ (NSString*)stringWithDeviceToken:(NSData*)token {
    const char* data = [token bytes];
    NSMutableString* tokenString = [NSMutableString string];
    for (int i = 0; i < [token length]; i++) [tokenString appendFormat:@"%02.2hhX", data[i]];
    return [tokenString copy];
}

- (void) changeStateForRegisteredForRemoteNotificationsTo:(BOOL)enabled {
    [self willSetRegistered:enabled finished:^{
        if (enabled) {
            if ([__MPLApplication respondsToSelector:@selector(registerForRemoteNotifications)])
                [__MPLApplication registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:self.notificationsType_iOS8 categories:nil]];
            else
                [__MPLApplication registerForRemoteNotificationTypes:self.notificationsType];
        }
        else
            [__MPLApplication unregisterForRemoteNotifications];
    }];
}



#pragma mark -
#pragma mark Application delegation

- (void) application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [__MPLApplication registerForRemoteNotifications];
}
- (void) application:(MPLApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [self didRegisterWithDeviceToken:deviceToken finished:^{ }];
}
- (void) application:(MPLApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [self failedToRegisterWithError:error];
}
- (void) application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler { completionHandler(); }



#pragma mark -
#pragma mark Methods to override

- (void) didRegisterWithDeviceToken:(NSData*)tokenData finished:(void(^)(void))finished { finished(); } // Add device token
- (void) willSetRegistered:(BOOL)registered finished:(void(^)(void))finished { finished(); } // Login checks and delete devie
- (void) failedToRegisterWithError:(NSError*)error {}

@end





@implementation MPLPushNotificationsManager (Handling)

#pragma mark -
#pragma mark Common

- (void) handleOnStartup {
    if (__MPLApplication.launchedWithRemoteNotification == nil) return;
    self.receivedRemoteNotification = __MPLApplication.launchedWithRemoteNotification;
    __MPLApplication.launchedWithRemoteNotification = nil;
    [self handleRemoteNotification];
}
- (void) cancelHandleOnReceive { self.cancelReceiveHandle = YES; }
- (void) handleOnEnterForeground {
    if (self.receivedRemoteNotification != nil) [self handleRemoteNotification];
}
- (void) alertAndHandleWithCancelButton:(NSString*)cancelButton andHandleButton:(NSString*)handleButton {
    if (!self.userSettingsAlertsEnabled)
        [self handleRemoteNotificationPassively];
    else
        [MPLAlert alertWithTitle:@"" message:[[self.receivedRemoteNotification objectForKey:@"aps"] objectForKey:@"alert"] andStyle:UIAlertViewStyleDefault finishedDelegate:^(NSString* input1, NSString* input2, NSInteger index) {
            if (index == 1)
                [self handleRemoteNotification];
            else
                [self handleRemoteNotificationPassively];
        } cancelButtonTitle:cancelButton andMoreButtonTitles:(handleButton == nil ? @[] : @[handleButton])];
}



#pragma mark -
#pragma mark Application delegation

- (void) application:(MPLApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    self.receivedRemoteNotification = userInfo;
    if (__MPLApplication.applicationState == UIApplicationStateActive) {
        [self receivedRemoteNotificationAtActiveState];
    }
    else {
        if (self.cancelReceiveHandle) {
            self.cancelReceiveHandle = NO;
        }
        else {
            [self handleRemoteNotification];
        }
        // This part is handled by the handleOnEnterForeground
    }
}



#pragma mark -
#pragma mark Methods to override

- (void) handleRemoteNotification { self.receivedRemoteNotification = nil; }
- (void) handleRemoteNotificationPassively { self.receivedRemoteNotification = nil; }
- (void) receivedRemoteNotificationAtActiveState { self.receivedRemoteNotification = nil; }

@end





//#import <objc/runtime.h>
//@implementation MPLPushNotificationsManager (RegisterTick)
//
//#define aoRetain(lc,UC,t) - (void)set##UC:(t)object { objc_setAssociatedObject(self, @selector(lc), object, OBJC_ASSOCIATION_RETAIN_NONATOMIC); } - (t) lc { return objc_getAssociatedObject(self, @selector(lc)); }
//#define aoInteger(lc,UC) - (void)set##UC:(NSInteger)object { objc_setAssociatedObject(self, @selector(lc), @(object), OBJC_ASSOCIATION_ASSIGN); } - (NSInteger) lc { return [objc_getAssociatedObject(self, @selector(lc)) integerValue]; }
//
//aoRetain(notificationTickKey, NotificationTickKey, NSString*)
//aoInteger(askTickValue, AskTickValue)
//
//- (id) initWithNotificationsType:(UIRemoteNotificationType)type andNotificationTickKey:(NSString*)tickKey andAskTickValue:(NSInteger)askValue {
//    if ([self initWithNotificationsType:type]) {
//        self.askTickValue = askValue;
//        self.notificationTickKey = tickKey;
//    }
//    return self;
//}
//
//- (NSInteger) notificationsRequestTick {
//    return [[MPLOpenUDID objectForKeychainWithName:self.notificationTickKey andAccessGroup:nil] integerValue];
//}
//- (void) setNotificationsRequestTick:(NSInteger)value {
//    if (value == -1) return;
//    [MPLOpenUDID setObject:@(value) forKeychainWithName:self.notificationTickKey andAccessGroup:nil];
//    if (value == self.askTickValue) self.registeredForRemoteNotifications = YES;
//}
//
//
//@end
