//
//  MPLWebServiceMockup.h
//  MPLKit
//
//  Created by Martin Lalev on 4/24/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPLWebServiceMockup : MPLPListManager

- (void) getResourceNameComponenets:(NSArray *)resourceNameComponents andParameters:(NSDictionary *)urlParameters andHeaders:(NSDictionary *)headers andCookies:(NSDictionary *)cookies withCallback:(void (^)(id))callback;
- (void) postResourceNameComponenets:(NSArray *)resourceNameComponents andParameters:(NSDictionary *)urlParameters andHeaders:(NSDictionary *)headers andCookies:(NSDictionary *)cookies withCallback:(void (^)(id))callback;
- (void) putResourceNameComponenets:(NSArray *)resourceNameComponents andParameters:(NSDictionary *)urlParameters andHeaders:(NSDictionary *)headers andCookies:(NSDictionary *)cookies withCallback:(void (^)(id))callback;
- (void) deleteResourceNameComponenets:(NSArray *)resourceNameComponents andParameters:(NSDictionary *)urlParameters andHeaders:(NSDictionary *)headers andCookies:(NSDictionary *)cookies withCallback:(void (^)(id))callback;

- (void) performConnectionForMethodName:(NSString *)methodName andResourceName:(NSString *)resourceName andURLParameters:(NSDictionary *)urlParameters andBodyParameters:(NSDictionary *)bodyParameters andHeaders:(NSDictionary *)headers andCookies:(NSDictionary *)cookies andTimeout:(NSTimeInterval)timeout andContentType:(NSString *)contentType withCallback:(void (^)(MPLResponseDescriptor *))callback andTimeoutCallback:(void (^)(MPLURLConnection *))timeoutCallback;

@end
