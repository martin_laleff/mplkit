//
//  FTPListFileManager.h
//  FTrack_Take2
//
//  Created by Administrator on 29/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPLPListManager : NSObject {
	NSPropertyListFormat format;
}

@property (nonatomic, strong) NSString* filename;
@property (nonatomic, strong) id item;
@property (weak, nonatomic, readonly) NSString* version;

- (id) initWithFilename:(NSString*)filename;
- (id) initWithFilename:(NSString *)file_name version:(NSString*)version dataGeneration:(NSDictionary*(^)(void))generateNewData dataLoader:(void(^)(void))loadData;
- (NSData*) readDataFor:(NSData*)data;
- (NSData*) writeDataFor:(NSData*)data;
- (NSString*) loadData;
- (void) saveData;
+ (MPLPListManager*) plistManagerForFilename:(NSString*)location;
+ (MPLPListManager*) plistManagerForResource:(NSString*)resourceName;
+ (MPLPListManager*) loadedPlistManagerForFilename:(NSString*)filename;
+ (MPLPListManager*) loadedPlistManagerForResource:(NSString*)resource;

@end
