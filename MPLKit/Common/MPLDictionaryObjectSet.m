//
//  MPLDictionaryObjectSet.m
//  MPLKit
//
//  Created by Martin Lalev on 4/25/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLDictionaryObjectSet.h"

@implementation MPLDictionaryObjectSet

- (void) setUpToDate:(BOOL)upToDate {
    self.lastUpdateDate = (upToDate ? [NSDate date] : nil);
}
- (BOOL) upToDate {
    return self.lastUpdateDate != nil && [[self.lastUpdateDate dateByAddingHours:1] compare:[NSDate date]] == NSOrderedDescending;
}
- (id) initWithParameters:(NSMutableDictionary *)parameters andManager:(MPLDictionaryObjectManager*)manager {
    if ([super init]) {
        self.manager = manager;
        self.upToDate = NO;
        self.items = @[];
        self.parameters = parameters;
        for (NSString* key in self.parameters.allKeys)
            [self didUpdateParameterWithKey:key];
    }
    return self;
}

- (NSArray*) dictionariesToItems:(NSArray*)items {
    NSMutableArray* result = [NSMutableArray array];
    for (NSDictionary* d in items) [result addObject:self.manager[d]];
    return result;
}

- (void) loadItemsSuccess:(void (^)(NSArray* items, BOOL wereCached))loadSuccess fail:(void (^)(id))loadFail {
    if (self.upToDate) {
        [MPLAction performAfter:.2 block:^{
            loadSuccess(self.items, YES);
        }];
    }
    else {
        [self updateItemsSuccess:^(NSArray *dictionaries) {
            self.items = [self dictionariesToItems:dictionaries];
            self.upToDate = YES;
            loadSuccess(self.items, YES);
        } fail:^(id error) {
            loadFail(error);
        }];
    }
}
- (void) updateItemsSuccess:(void (^)(NSArray *))loadSuccess fail:(void (^)(id))loadFail {
    loadSuccess([NSMutableArray array]);
}

- (void) willUpdateParameterWithKey:(NSString*)key {}
- (void) didUpdateParameterWithKey:(NSString*)key {}
- (void) updateParamaterWithKey:(NSString *)key toValue:(id)value {
    id currentValue = self.parameters[key];
    if (![self compareParamaterWithKey:key currentValue:currentValue withValue:value]) {
        [self willUpdateParameterWithKey:key];
        self.upToDate = NO;
        self.parameters[key] = value;
        [self didUpdateParameterWithKey:key];
    }
}
- (BOOL) compareArrayWithCurrentValue:(NSArray*)currentValue andUpdatedValue:(NSArray*)updatedValue {
    NSArray* currentVenueIDs = (NSArray*)currentValue;
    NSArray* updatedVenueIDs = (NSArray*)updatedValue;
    
    NSMutableSet* intersectedSet = [NSMutableSet setWithArray:currentVenueIDs];
    [intersectedSet intersectSet:[NSSet setWithArray:updatedVenueIDs]];
    
    return intersectedSet.count == currentVenueIDs.count && intersectedSet.count == updatedVenueIDs.count;
}
- (BOOL) compareParamaterWithKey:(NSString*)key currentValue:(id)currentValue withValue:(id)value {
    if ([currentValue class] == [currentValue class]) {
        if ([currentValue isKindOfClass:[NSString class]])
            return [currentValue isEqualToString:value];
        if ([currentValue isKindOfClass:[NSNumber class]])
            return [currentValue isEqualToNumber:value];
        if ([currentValue isKindOfClass:[NSArray class]])
            return [self compareArrayWithCurrentValue:currentValue andUpdatedValue:value];
        return NO;
    }
    return NO;
}

- (instancetype) updateItemsByAdding:(NSArray*)itemsToAdd {
    NSMutableArray* updatedArray = [NSMutableArray arrayWithArray:self.items];
    [updatedArray addObjectsFromArray:itemsToAdd];
    self.items = updatedArray;
    return self;
}
- (instancetype) updateItemsByRemoving:(NSArray*)itemsToRemove {
    NSMutableArray* updatedArray = [NSMutableArray arrayWithArray:self.items];
    [updatedArray removeObjectsInArray:itemsToRemove];
    self.items = updatedArray;
    return self;
}

@end
