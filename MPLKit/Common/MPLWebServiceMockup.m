//
//  MPLWebServiceMockup.m
//  MPLKit
//
//  Created by Martin Lalev on 4/24/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLWebServiceMockup.h"

@implementation MPLWebServiceMockup

- (void) getResourceNameComponenets:(NSArray *)resourceNameComponents andParameters:(NSDictionary *)urlParameters andHeaders:(NSDictionary *)headers andCookies:(NSDictionary *)cookies withCallback:(void (^)(id))callback {
    
}
- (void) postResourceNameComponenets:(NSArray *)resourceNameComponents andParameters:(NSDictionary *)urlParameters andHeaders:(NSDictionary *)headers andCookies:(NSDictionary *)cookies withCallback:(void (^)(id))callback {
    
}
- (void) putResourceNameComponenets:(NSArray *)resourceNameComponents andParameters:(NSDictionary *)urlParameters andHeaders:(NSDictionary *)headers andCookies:(NSDictionary *)cookies withCallback:(void (^)(id))callback {
    
}
- (void) deleteResourceNameComponenets:(NSArray *)resourceNameComponents andParameters:(NSDictionary *)urlParameters andHeaders:(NSDictionary *)headers andCookies:(NSDictionary *)cookies withCallback:(void (^)(id))callback {
    
}

- (void) performConnectionForMethodName:(NSString *)methodName andResourceName:(NSString *)resourceName andURLParameters:(NSDictionary *)urlParameters andBodyParameters:(NSDictionary *)bodyParameters andHeaders:(NSDictionary *)headers andCookies:(NSDictionary *)cookies andTimeout:(NSTimeInterval)timeout andContentType:(NSString *)contentType withCallback:(void (^)(MPLResponseDescriptor *))callback andTimeoutCallback:(void (^)(MPLURLConnection *))timeoutCallback {
    NSArray* resourceNameComponents = [resourceName componentsSeparatedByString:@"/"];
    
    if ([methodName isEqualToString:@"GET"]) {
        [self getResourceNameComponenets:resourceNameComponents andParameters:urlParameters andHeaders:headers andCookies:cookies withCallback:^(id result) {
            callback([MPLResponseDescriptor responseDescriptorForResponse:[[NSHTTPURLResponse alloc] initWithURL:nil statusCode:200 HTTPVersion:@"" headerFields:@{}]
                                                            andConnection:nil
                                                                andResult:[result JSONRepresentation]
                                                                 andError:nil
                      ]);
        }];
    }
    if ([methodName isEqualToString:@"POST"]) {
        [self postResourceNameComponenets:resourceNameComponents andParameters:bodyParameters andHeaders:headers andCookies:cookies withCallback:^(id result) {
            callback([MPLResponseDescriptor responseDescriptorForResponse:[[NSHTTPURLResponse alloc] initWithURL:nil statusCode:200 HTTPVersion:@"" headerFields:@{}]
                                                            andConnection:nil
                                                                andResult:[result JSONRepresentation]
                                                                 andError:nil
                      ]);
        }];
    }
    if ([methodName isEqualToString:@"PUT"]) {
        [self putResourceNameComponenets:resourceNameComponents andParameters:bodyParameters andHeaders:headers andCookies:cookies withCallback:^(id result) {
            callback([MPLResponseDescriptor responseDescriptorForResponse:[[NSHTTPURLResponse alloc] initWithURL:nil statusCode:200 HTTPVersion:@"" headerFields:@{}]
                                                            andConnection:nil
                                                                andResult:[result JSONRepresentation]
                                                                 andError:nil
                      ]);
        }];
    }
    if ([methodName isEqualToString:@"DELETE"]) {
        [self deleteResourceNameComponenets:resourceNameComponents andParameters:bodyParameters andHeaders:headers andCookies:cookies withCallback:^(id result) {
            callback([MPLResponseDescriptor responseDescriptorForResponse:[[NSHTTPURLResponse alloc] initWithURL:nil statusCode:200 HTTPVersion:@"" headerFields:@{}]
                                                            andConnection:nil
                                                                andResult:[result JSONRepresentation]
                                                                 andError:nil
                      ]);
        }];
    }
}

@end
