//
//  MYUDID.h
//  TestApp
//
//  Created by Martin Lalev on 10/7/13.
//  Copyright (c) 2013 Shtrak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeychainItemWrapper.h"

@interface MPLUniqueDeviceID : NSObject

@property (strong, nonatomic) KeychainItemWrapper* keychainWraper;

+ (void) setAccessGroup:(NSString*)accessGroup;
+ (NSString*) accessGroup;
+ (NSString*) udid;
+ (void) resetUDID;

@end
