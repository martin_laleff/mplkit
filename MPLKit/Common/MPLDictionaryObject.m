//
//  MPLDictionaryObject.m
//  MPLKit
//
//  Created by Martin Lalev on 4/20/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLDictionaryObject.h"

@implementation MPLDictionaryObject

- (NSString*) objectId { return [self.dictionary objectForKey:@"id"]; }

- (id) initWithDictionary:(NSDictionary*)dictionary {
    if ([super init]) {
        self.dictionary = dictionary;
    }
    return self;
}

- (void) setDictionary:(NSDictionary *)dictionary {
    [self willUpdateWithDictionary:dictionary commit:^(NSDictionary *dictionary) {
        if (_dictionary == nil) {
            _dictionary = dictionary;
        }
        else {
            NSMutableDictionary* updatedDictionary = [NSMutableDictionary dictionaryWithDictionary:_dictionary];
            [updatedDictionary setValuesForKeysWithDictionary:dictionary];
            _dictionary = updatedDictionary;
        }
        [self didUpdate];
    }];
}

- (void) willUpdateWithDictionary:(NSDictionary*)dictionary commit:(void (^)(NSDictionary* dictionary))commitUpdate {
    commitUpdate(dictionary);
}
- (void) didUpdate {
}

@end
