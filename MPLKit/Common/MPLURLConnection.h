//
//  FTHTTPRequest.h
//  TestApp
//
//  Created by Martin Lalev on 4/17/13.
//  Copyright (c) 2013 Shtrak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
typedef void(^FTURLFinishedDataBlock)(NSURLResponse *response, NSData *data, NSError *error);
typedef void(^FTURLFinishedStringBlock)(NSURLResponse *response, NSString *string, NSError *error);
typedef void(^FTURLFinishedIDBlock)(NSURLResponse *response, id result, NSError *error);

@interface MPLURLConnection : NSMutableURLRequest<NSURLConnectionDelegate>
{
    NSURLConnection* Connection;
    NSURLResponse* Response;
    NSMutableData* Data;
    FTURLFinishedDataBlock Finished;
    NSArray* LocalCertificates;
    BOOL CustomCA;
    BOOL SelfSignedCert;
    NSDate* SendDate;
}

@property (nonatomic, strong) NSURLConnection* Connection;
@property (nonatomic, strong) NSURLResponse* Response;
@property (nonatomic, strong) NSMutableData* Data;
@property (nonatomic, strong) NSArray* LocalCertificates;
@property (nonatomic, assign) BOOL CustomCA;
@property (nonatomic, assign) BOOL SelfSignedCert;
@property (nonatomic, strong) NSDate* SendDate;
@property (nonatomic, copy) FTURLFinishedDataBlock Finished;

- (id) initWithURL:(NSURL *)URL
       cachePolicy:(NSURLRequestCachePolicy)cachePolicy
   timeoutInterval:(NSTimeInterval)timeoutInterval
        andCookies:(NSDictionary*)cookies
        andHeaders:(NSDictionary*)headers
    andContentType:(NSString*)contentType
     andMethodType:(NSString*)methodType
       andBodyData:(NSData*)bodyData
;
- (id) initWithURL:(NSURL *)URL
       cachePolicy:(NSURLRequestCachePolicy)cachePolicy
   timeoutInterval:(NSTimeInterval)timeoutInterval
        andCookies:(NSDictionary*)cookies
        andHeaders:(NSDictionary*)headers
    andContentType:(NSString*)contentType
     andMethodType:(NSString*)methodType
     andBodyString:(NSString*)bodyString
;

- (void) setSSLAuthWithCertificates:(NSArray*)certs andCustomCA:(BOOL)customCA andSelfSignedCert:(BOOL)selfSignedCert;

- (void) changeContentType:(NSString*)contentType;
- (void) changeHTTPBody:(NSData*)bodyData;
- (void) changeHTTPMethod:(NSString*)methodType;

- (void) addHeader:(id)value withKey:(NSString*)name;

- (void) addCookie:(NSString*)value withName:(NSString*)name;
- (void) addCookie:(NSString*)value withName:(NSString*)name andOriginURL:(NSURL*)originURL;

- (void) startWithFinishedIDBlock:(FTURLFinishedIDBlock)finished;
- (void) startWithFinishedPlistBlock:(FTURLFinishedIDBlock)finished;
- (void) startWithFinishedStringBlock:(FTURLFinishedStringBlock)finished;
- (void) startWithFinishedDataBlock:(FTURLFinishedDataBlock)finished;
- (void) cancel;

@end



@interface MPLResponseDescriptor : NSObject {
//    MPLURLConnection* connection;
    NSHTTPURLResponse *response;
    NSString *result;
    NSError *error;
}
//@property (nonatomic, retain) MPLURLConnection* connection;
@property (nonatomic, strong) NSHTTPURLResponse *response;
@property (nonatomic, strong) NSString *result;
@property (nonatomic, strong) NSError *error;

@property (nonatomic, readonly) long statusCode;
@property (weak, nonatomic, readonly) id jsonResult;
@property (nonatomic, readonly) BOOL success;
@property (weak, nonatomic, readonly) NSDictionary* responseError;
@property (nonatomic, readonly) long serverClientSecondsOffset;
@property (nonatomic, readonly) BOOL timedOut;

+ (MPLResponseDescriptor*) responseDescriptorForResponse:(NSURLResponse*)response andConnection:(MPLURLConnection*)connection andResult:(NSString*)result andError:(NSError*)error;

@end


@interface MPLWebService : NSObject
{
    Reachability* serviceReachability;
    BOOL usesSSL;
    NSString* urlHost;
    NSArray* sslCerfificates;
    BOOL useCustomCA;
    BOOL useSelfSignedCert;
}
@property (nonatomic, strong) Reachability* serviceReachability;
@property (nonatomic, readonly) BOOL serverReachable;
@property (nonatomic, assign) BOOL usesSSL;
@property (nonatomic, strong) NSString* urlHost;
@property (nonatomic, strong) NSArray* sslCerfificates;
@property (nonatomic, assign) BOOL useCustomCA;
@property (nonatomic, assign) BOOL useSelfSignedCert;
@property (nonatomic, assign) BOOL StandardArrayParam;

- (id) initWithHost:(NSString*)host sslCertificates:(NSArray*)ssls useCustomCA:(BOOL)cca useSelfSignedCert:(BOOL)ssc;
- (MPLURLConnection*) connectionForMethodName:(NSString*)methodName andResourceName:(NSString*)resourceName andURLParameters:(NSDictionary*)urlParameters andBodyParameters:(NSDictionary*)bodyParameters andHeaders:(NSDictionary*)headers andCookies:(NSDictionary*)cookies andTimeout:(NSTimeInterval)timeout andContentType:(NSString*)contentType;
- (void) performConnection:(MPLURLConnection*)conn withCallback:(void(^)(MPLResponseDescriptor*))callback andTimeoutCallback:(void(^)(MPLURLConnection*))timeout;
- (void) performConnection:(MPLURLConnection*)conn withCallback:(void(^)(MPLResponseDescriptor*))callback;
- (void) performConnectionForMethodName:(NSString*)methodName andResourceName:(NSString*)resourceName andURLParameters:(NSDictionary*)urlParameters andBodyParameters:(NSDictionary*)bodyParameters andHeaders:(NSDictionary*)headers andCookies:(NSDictionary*)cookies andTimeout:(NSTimeInterval)timeout andContentType:(NSString*)contentType withCallback:(void(^)(MPLResponseDescriptor*))callback andTimeoutCallback:(void(^)(MPLURLConnection*))timeoutCallback;
- (void) reachabilityChanged:(NSNotification *)note;

@end

