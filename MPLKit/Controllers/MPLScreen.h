//
//  MPLScreen.h
//  MPLKit
//
//  Created by Martin Lalev on 4/6/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MPLScreen;
@protocol MPLScreenDelegate <NSObject>

@optional
- (void) screen:(MPLScreen*)screen willOpenAnimated:(BOOL)animated finished:(void(^)(void))finished;
@optional
- (void) screen:(MPLScreen*)screen didOpenAnimated:(BOOL)animated finished:(void(^)(void))finished;

@optional
- (void) screen:(MPLScreen*)screen willCloseAnimated:(BOOL)animated finished:(void(^)(void))finished;
@optional
- (void) screen:(MPLScreen*)screen didCloseAnimated:(BOOL)animated finished:(void(^)(void))finished;

@optional
- (void) screen:(MPLScreen*)screen willStashAnimated:(BOOL)animated finished:(void(^)(void))finished;
@optional
- (void) screen:(MPLScreen*)screen didStashAnimated:(BOOL)animated finished:(void(^)(void))finished;

@optional
- (void) screen:(MPLScreen*)screen willUnstashAnimated:(BOOL)animated finished:(void(^)(void))finished;
@optional
- (void) screen:(MPLScreen*)screen didUnstashAnimated:(BOOL)animated finished:(void(^)(void))finished;

@end




@interface MPLScreen : NSObject

@property (nonatomic, strong) id<MPLScreenDelegate> screenDelegate;
@property (nonatomic, strong) NSMutableArray* subScreens;

- (id) init;
- (void) createScreen;
- (void) destroyScreen;

- (void) adjustToKeyboardSize:(CGSize)keyboardSize;



@property (nonatomic, strong) MPLTransition* mainTransition;
@property (nonatomic, readonly) BOOL isClosed;
@property (nonatomic, assign) BOOL isOpened;
- (void) openScreenAnimated:(BOOL)animated finished:(void(^)(void))finishedBlock;
- (void) closeScreenAnimated:(BOOL)animated finished:(void(^)(void))finishedBlock;

- (void) screenWillOpenAnimated:(BOOL)animated finished:(void(^)(void))finished;
- (void) screenDidOpenAnimated:(BOOL)animated finished:(void(^)(void))finished;
- (void) screenWillCloseAnimated:(BOOL)animated finished:(void(^)(void))finished;
- (void) screenDidCloseAnimated:(BOOL)animated finished:(void(^)(void))finished;

- (void) setCurrentTransitionTo:(MPLTransition*)t;
- (void) setStashTransitionTo:(MPLTransition*)t;


@property (nonatomic, strong) MPLTransition* stashTransition;
@property (nonatomic, assign) BOOL isStashed;
- (void) stashScreenAnimated:(BOOL)animated finished:(void(^)(void))finishedBlock;
- (void) unstashScreenAnimated:(BOOL)animated finished:(void(^)(void))finishedBlock;

- (void) screenWillStashAnimated:(BOOL)animated finished:(void(^)(void))finished;
- (void) screenDidStashAnimated:(BOOL)animated finished:(void(^)(void))finished;
- (void) screenWillUnstashAnimated:(BOOL)animated finished:(void(^)(void))finished;
- (void) screenDidUnstashAnimated:(BOOL)animated finished:(void(^)(void))finished;

@end
