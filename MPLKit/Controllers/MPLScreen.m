//
//  MPLScreen.m
//  MPLKit
//
//  Created by Martin Lalev on 4/6/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLScreen.h"

@implementation MPLScreen



#pragma mark -
#pragma mark Initializations

- (id) init {
    if ([super init]) {
        self.subScreens = [NSMutableArray array];
        self.mainTransition = [MPLTransition emptyTransition];
        self.stashTransition = [MPLTransition emptyTransition];
    }
    return self;
}

- (void) createScreen {
    [self foreachSubScreen:^(MPLScreen *subScreen) {
        [subScreen createScreen];
    }];
    self.isOpened = NO;
}
- (void) destroyScreen {
    [self.mainTransition clear];
    [self.stashTransition clear];
    self.mainTransition = nil;
    self.stashTransition = nil;
    self.screenDelegate = nil;
    
    [self foreachSubScreen:^(MPLScreen *subScreen) {
        [subScreen destroyScreen];
    }];
    self.subScreens = nil;
}



#pragma mark -
#pragma mark Commons

- (void) foreachSubScreen:(void(^)(MPLScreen*subScreen))iterator {
    for (MPLNavigationSubScreen* subScreen in self.subScreens) iterator(subScreen);
}
- (void) adjustToKeyboardSize:(CGSize)keyboardSize {
    [self foreachSubScreen:^(MPLScreen *subScreen) {
        [subScreen adjustToKeyboardSize:keyboardSize];
    }];
}



#pragma mark -
#pragma mark Open/Close

- (BOOL) isClosed { return self.mainTransition.transitionState == 0; }
- (BOOL) isOpened { return self.mainTransition.transitionState == 1; }
- (void) setIsOpened:(BOOL)isOpened { self.mainTransition.transitionState = (isOpened ? 1 : 0); }
- (void) openScreenAnimated:(BOOL)animated finished:(void(^)(void))finishedBlock {
    if (self.isOpened) finishedBlock();
    else {
        [self screenWillOpenAnimated:animated finished:^{
            [self.mainTransition transitionStateTo:1 animated:animated finished:^{
                [self screenDidOpenAnimated:animated finished:finishedBlock];
            }];
        }];
    }
}
- (void) closeScreenAnimated:(BOOL)animated finished:(void(^)(void))finishedBlock {
    if (self.isClosed) finishedBlock();
    else {
        [self screenWillCloseAnimated:animated finished:^{
            [self.mainTransition transitionStateTo:0 animated:animated finished:^{
                [self screenDidCloseAnimated:animated finished:finishedBlock];
            }];
        }];
    }
}

- (void) screenWillOpenAnimated:(BOOL)animated finished:(void(^)(void))finished {
    if ([self.screenDelegate respondsToSelector:@selector(screen:willOpenAnimated:finished:)])
        [self.screenDelegate screen:self willOpenAnimated:animated finished:finished];
    else
        finished();
}
- (void) screenDidOpenAnimated:(BOOL)animated finished:(void(^)(void))finished {
    if ([self.screenDelegate respondsToSelector:@selector(screen:didOpenAnimated:finished:)])
        [self.screenDelegate screen:self didOpenAnimated:animated finished:finished];
    else
        finished();
}

- (void) screenWillCloseAnimated:(BOOL)animated finished:(void(^)(void))finished {
    if ([self.screenDelegate respondsToSelector:@selector(screen:willCloseAnimated:finished:)])
        [self.screenDelegate screen:self willCloseAnimated:animated finished:finished];
    else
        finished();
}
- (void) screenDidCloseAnimated:(BOOL)animated finished:(void(^)(void))finished {
    if ([self.screenDelegate respondsToSelector:@selector(screen:didCloseAnimated:finished:)])
        [self.screenDelegate screen:self didCloseAnimated:animated finished:finished];
    else
        finished();
}


- (void) setCurrentTransitionTo:(MPLTransition*)t {
    [self.mainTransition clear];
    self.mainTransition = nil;
    float state = self.mainTransition.transitionState;
    self.mainTransition = t;
    self.mainTransition.transitionState = state;
}
- (void) setStashTransitionTo:(MPLTransition*)t {
    [self.stashTransition clear];
    self.stashTransition = nil;
    self.stashTransition = t;
}



#pragma mark -
#pragma mark Stash/Unstash

- (BOOL) isStashed { return self.stashTransition.transitionState == 0; }
- (void) setIsStashed:(BOOL)isStashed { self.stashTransition.transitionState = (isStashed ? 0 : 1); }
- (void) stashScreenAnimated:(BOOL)animated finished:(void(^)(void))finished {
    [self screenWillStashAnimated:animated finished:^{
        [self.stashTransition transitionStateTo:0 animated:animated finished:^{
            [self screenDidStashAnimated:animated finished:^{
                finished();
            }];
        }];
    }];
}
- (void) unstashScreenAnimated:(BOOL)animated finished:(void(^)(void))finished {
    [self screenWillUnstashAnimated:animated finished:^{
        [self.stashTransition transitionStateTo:1 animated:animated finished:^{
            [self screenDidUnstashAnimated:animated finished:^{
                finished();
            }];
        }];
    }];
}

- (void) screenWillStashAnimated:(BOOL)animated finished:(void(^)(void))finished {
    if ([self.screenDelegate respondsToSelector:@selector(screen:willStashAnimated:finished:)])
        [self.screenDelegate screen:self willStashAnimated:animated finished:finished];
    else
        finished();
}
- (void) screenDidStashAnimated:(BOOL)animated finished:(void(^)(void))finished {
    if ([self.screenDelegate respondsToSelector:@selector(screen:didStashAnimated:finished:)])
        [self.screenDelegate screen:self didStashAnimated:animated finished:finished];
    else
        finished();
}

- (void) screenWillUnstashAnimated:(BOOL)animated finished:(void(^)(void))finished {
    if ([self.screenDelegate respondsToSelector:@selector(screen:willUnstashAnimated:finished:)])
        [self.screenDelegate screen:self willUnstashAnimated:animated finished:finished];
    else
        finished();
}
- (void) screenDidUnstashAnimated:(BOOL)animated finished:(void(^)(void))finished {
    if ([self.screenDelegate respondsToSelector:@selector(screen:didUnstashAnimated:finished:)])
        [self.screenDelegate screen:self didUnstashAnimated:animated finished:finished];
    else
        finished();
}



@end
