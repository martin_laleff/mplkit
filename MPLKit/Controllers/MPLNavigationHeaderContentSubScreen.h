//
//  MPLNavigationHeaderContentSubScreen.h
//  MPLKit
//
//  Created by Martin Lalev on 4/1/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//



#import "MPLHeaderContentViews.h"
#import "MPLNavigationScreen.h"


@interface MPLNavigationHeaderContentSubScreen : MPLNavigationSubScreen

@property (weak, nonatomic, readonly) MPLNavigationHeaderContentSubScreen* topController;
@property (nonatomic, strong) MPLNavigationHeaderContentScreen* navigationScreen;
@property (weak, nonatomic, readonly) MPLHeaderContentView* navigationView;
@property (nonatomic, strong) MPLHeaderView* headerSubView;
@property (nonatomic, strong) MPLPanel* contentSubView;


- (void) contentViewLoader:(MPLPanel*)v;
- (void) headerViewLoader:(MPLHeaderView*)v;
- (MPLPanel*) generateContentViewWithLoader:(void(^)(MPLPanel* v))loader;
- (MPLHeaderView*) generateHeaderViewWithLoader:(void(^)(MPLHeaderView* v))loader;


@property (weak, nonatomic, readonly) MPLTransition* headerOnlyFadeTransition;
@property (weak, nonatomic, readonly) MPLTransition* slideToLeftTransition;
@property (weak, nonatomic, readonly) MPLTransition* slideToRightTransition;
@property (weak, nonatomic, readonly) MPLTransition* fadeTransition;

- (instancetype) pushWithFadeInAnimated:(BOOL)animated finished:(void (^)(void))finished;
- (instancetype) pushFromLeftAnimated:(BOOL)animated finished:(void(^)(void))finished;
- (instancetype) pushFromRightAnimated:(BOOL)animated finished:(void(^)(void))finished;

@end
