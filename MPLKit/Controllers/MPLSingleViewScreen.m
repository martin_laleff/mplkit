//
//  MPLSingleViewScreen.m
//  MPLKit
//
//  Created by Martin Lalev on 4/6/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLSingleViewScreen.h"

@implementation MPLSingleViewScreen

- (id) initAsApplicationRootScreen {
    if ([self init]) {
        [self loadAsApplicationRootScreen];
    }
    return self;
}
- (void) loadAsApplicationRootScreen {
    [self createScreenWithView:[self generateViewWithLoader:^(MPLPanel* v) {
        v.frame = [UIScreen mainScreen].bounds;
        v.mask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [[MPLApplication SharedApplication] setRootControllerForBaseView:v];
    }]];
    [[MPLApplication SharedApplication] createSplashImageView];
}

- (void) destroyScreen {
    [super destroyScreen];
    [((MPLPanel*)self.view.parentView) removeSubview:self.view andDestroy:YES];
    self.view = nil;
}
- (void) createScreenWithView:(MPLPanel*)v {
    self.view = v;
    [self createScreen];
}
- (MPLPanel*) generateViewWithLoader:(void(^)(MPLPanel* v))loader {
    return [MPLPanel create:^(MPLPanel* v) { loader(v); }];
}

- (void) createScreenWithViewWithLoader:(void(^)(MPLPanel*v))loader {
    [self createScreenWithView:[self generateViewWithLoader:loader]];
}
- (void) createScreenWithViewInFullSizeInParent:(MPLPanel*)parent {
    [self createScreenWithViewWithLoader:^(MPLPanel *v) {
        [v addTo:parent];
        [v fillWidth];
        [v fillHeight];
    }];
}


- (MPLTransition*) slideFromBelowTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return 1-self.view.view.transform.ty/self.view.heightBounds;
        }];
        [t assignSetter:^(float value) {
            self.view.view.transform = CGAffineTransformMakeTranslation(0, (1-value)*self.view.heightBounds);
        }];
    }];
}
- (MPLTransition*) slideFromAboveTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return self.view.view.transform.ty/self.view.heightBounds+1;
        }];
        [t assignSetter:^(float value) {
            self.view.view.transform = CGAffineTransformMakeTranslation(0, (value-1)*self.view.heightBounds);
        }];
    }];
}
- (MPLTransition*) slideFromRightTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return 1-self.view.view.transform.tx/self.view.widthBounds;
        }];
        [t assignSetter:^(float value) {
            self.view.view.transform = CGAffineTransformMakeTranslation((1-value)*self.view.widthBounds, 0);
        }];
    }];
}
- (MPLTransition*) slideFromLeftTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return self.view.view.transform.tx/self.view.widthBounds+1;
        }];
        [t assignSetter:^(float value) {
            self.view.view.transform = CGAffineTransformMakeTranslation((value-1)*self.view.widthBounds, 0);
        }];
    }];
}
- (MPLTransition*) fadeInTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return self.view.alpha;
        }];
        [t assignSetter:^(float value) {
            self.view.alpha = value;
        }];
    }];
}
- (MPLTransition*) fadeOutTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return 1-self.view.alpha;
        }];
        [t assignSetter:^(float value) {
            self.view.alpha = 1-value;
        }];
    }];
}
- (MPLTransition*) fadeInTransitionWithZoom {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return self.view.alpha;
        }];
        [t assignSetter:^(float value) {
            self.view.alpha = value;
            CATransform3D t = CATransform3DIdentity;
            t.m34 = -1/1000.;
            t = CATransform3DTranslate(t, 0, 0, (+1)* 100 *(1-value));
            self.view.view.layer.transform = t;
        }];
    }];
}
- (MPLTransition*) fadeOutTransitionWithZoom {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return 1-self.view.alpha;
        }];
        [t assignSetter:^(float value) {
            self.view.alpha = 1-value;
            CATransform3D t = CATransform3DIdentity;
            t.m34 = -1/1000.;
            t = CATransform3DTranslate(t, 0, 0, (-1)* 100 *(1-value));
            self.view.view.layer.transform = t;
        }];
    }];
}

@end
