//
//  MPLNavigationHeaderContentSubScreen.m
//  MPLKit
//
//  Created by Martin Lalev on 4/1/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLNavigationHeaderContentSubScreen.h"



@implementation MPLNavigationHeaderContentSubScreen

- (MPLNavigationHeaderContentSubScreen*) topController { return (MPLNavigationHeaderContentSubScreen*)self.navigationScreen.topSubScreen; }
- (MPLHeaderContentView*) navigationView { return self.navigationScreen.view; }

- (void) createScreen {
    self.headerSubView = [self generateHeaderViewWithLoader:^(MPLHeaderView *v) { [v addTo:self.navigationView.headerContainerView]; }];
    self.contentSubView = [self generateContentViewWithLoader:^(MPLPanel *v) { [v addTo:self.navigationView.contentContainerView]; }];
    [self contentViewLoader:self.contentSubView];
    [self headerViewLoader:self.headerSubView];
    [super createScreen];
}
- (void) destroyScreen {
    [super destroyScreen];
    [self.headerSubView destroyView];
    [self.contentSubView destroyView];
    self.headerSubView = nil;
    self.contentSubView = nil;
    self.navigationScreen = nil;
}

- (void) headerViewLoader:(MPLHeaderView*)v {
    [v fillWidth];
    [v fillHeight];
}
- (void) contentViewLoader:(MPLPanel*)v {
    [v fillWidth];
    [v fillHeight];
}
- (MPLHeaderView*) generateHeaderViewWithLoader:(void (^)(MPLHeaderView *))loader { return [MPLHeaderView create:^(MPLHeaderView* v) { loader(v); }]; }
- (MPLPanel*) generateContentViewWithLoader:(void (^)(MPLPanel *))loader { return [MPLPanel create:^(MPLPanel* v) { loader(v); }]; }



#pragma mark -
#pragma mark Transitions management

- (MPLTransition*) headerOnlyFadeTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return self.headerSubView.view.alpha;
        }];
        [t assignSetter:^(float value) {
            self.headerSubView.view.alpha = value;
        }];
    }];
}
- (MPLTransition*) slideToLeftTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return self.contentSubView.view.alpha;
        }];
        [t assignSetter:^(float value) {
            self.contentSubView.view.transform = CGAffineTransformMakeTranslation(+(1-value)*self.contentSubView.widthBounds, 0);
            self.headerSubView.view.transform = CGAffineTransformMakeTranslation(+(1-value)*self.headerSubView.widthBounds, 0);
            self.contentSubView.view.alpha = value;
            self.headerSubView.view.alpha = value;
        }];
    }];
}
- (MPLTransition*) slideToRightTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return self.contentSubView.view.alpha;
        }];
        [t assignSetter:^(float value) {
            self.contentSubView.view.transform = CGAffineTransformMakeTranslation(-(1-value)*self.contentSubView.widthBounds, 0);
            self.headerSubView.view.transform = CGAffineTransformMakeTranslation(-(1-value)*self.headerSubView.widthBounds, 0);
            self.contentSubView.view.alpha = value;
            self.headerSubView.view.alpha = value;
        }];
        [t assignBegin:^(float position, void (^finishedBlock)(void)) {
            finishedBlock();
        }];
    }];
}
- (MPLTransition*) fadeTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        [t assignGetter:^float{
            return self.contentSubView.view.alpha;
        }];
        [t assignSetter:^(float value) {
            self.contentSubView.view.transform = CGAffineTransformTranslate(CGAffineTransformMakeScale(1-(1-value)*0.2, 1-(1-value)*0.2), 0, -(1-value)*100);
            self.headerSubView.view.transform = CGAffineTransformIdentity;
            self.contentSubView.view.alpha = value;
            self.headerSubView.view.alpha = value;
        }];
    }];
}



#pragma mark -
#pragma mark Custom push methods

- (instancetype) pushWithFadeInAnimated:(BOOL)animated finished:(void (^)(void))finished {
    [self setCurrentTransitionTo:self.fadeTransition];
    [self.topController setStashTransitionTo:self.topController.fadeTransition];
    [self pushAnimated:animated stashing:MPLNavigationTransitionStashBefore finished:finished];
    return self;
}
- (instancetype) pushFromRightAnimated:(BOOL)animated finished:(void (^)(void))finished {
    [self setCurrentTransitionTo:self.slideToLeftTransition];
    [self.topController setStashTransitionTo:self.topController.slideToRightTransition];
    [self pushAnimated:animated stashing:MPLNavigationTransitionStashTogether finished:finished];
    return self;
}
- (instancetype) pushFromLeftAnimated:(BOOL)animated finished:(void (^)(void))finished {
    [self setCurrentTransitionTo:self.slideToRightTransition];
    [self.topController setStashTransitionTo:self.topController.slideToLeftTransition];
    [self pushAnimated:animated stashing:MPLNavigationTransitionStashTogether finished:finished];
    return self;
}

@end
