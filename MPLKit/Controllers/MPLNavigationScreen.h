//
//  MPLNavigationScreen
//  MPLKit
//
//  Created by Martin Lalev on 12/29/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLSingleViewScreen.h"
#import "MPLNavigationSubScreen.h"



@interface MPLNavigationScreen : MPLSingleViewScreen

@property (nonatomic, strong) NSMutableArray* subScreensStack;
@property (weak, nonatomic, readonly) MPLNavigationSubScreen* rootSubScreen;
@property (weak, nonatomic, readonly) MPLNavigationSubScreen* topSubScreen;
@property (weak, nonatomic, readonly) MPLNavigationSubScreen* previousSubScreen;
- (MPLNavigationSubScreen*) subScreenBefore:(MPLNavigationSubScreen*)subScreen;



//- (void) hideScreenAnimated:(BOOL)animated finished:(void(^)(void))finishedBlock;

- (void) pushSubScreen:(MPLNavigationSubScreen*)subScreen animated:(BOOL)animated stashing:(MPLNavigationTransitionStash)stashing finished:(void(^)(void))finished;
- (void) popSubScreen:(MPLNavigationSubScreen*)subScreen animated:(BOOL)animated finished:(void(^)(void))finished;
- (void) pushFinishedFor:(MPLNavigationSubScreen*)subScreen finished:(void(^)(void))finished;
- (void) popFinishedFor:(MPLNavigationSubScreen*)subScreen finished:(void(^)(void))finished;

- (void) goBackToSubScreen:(MPLNavigationSubScreen*)subScreen animated:(BOOL)animated finished:(void(^)(void))finished;

@end