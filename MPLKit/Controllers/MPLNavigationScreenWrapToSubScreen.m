//
//  MPLNavigationScreenWrapToSubScreen.m
//  MPLKit
//
//  Created by Martin Lalev on 4/5/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLNavigationScreenWrapToSubScreen.h"

@implementation MPLNavigationScreenWrapToSubScreen

- (id) initWithNavigationScreen:(MPLNavigationScreen *)navigationScreen andWrappedScreen:(MPLNavigationScreen*)wrappedScreen {
    if ([super initWithNavigationScreen:navigationScreen]) {
        self.wrappedScreen = wrappedScreen;
        self.wrappedScreen.screenDelegate = self;
    }
    return self;
}
- (void) screen:(MPLScreen *)screen didOpenAnimated:(BOOL)animated finished:(void (^)(void))finished {
    [self.navigationScreen pushFinishedFor:self finished:finished];
}
- (void) screen:(MPLScreen *)screen didCloseAnimated:(BOOL)animated finished:(void (^)(void))finished {
    [self.navigationScreen popFinishedFor:self finished:finished];
}

- (BOOL) isStashed { return self.wrappedScreen.isOpened && self.navigationScreen.topSubScreen != self; }
- (void) stashScreenAnimated:(BOOL)animated finished:(void (^)(void))finished {
    [self.wrappedScreen stashScreenAnimated:animated finished:finished];
}
- (void) unstashScreenAnimated:(BOOL)animated finished:(void (^)(void))finished {
    [self.wrappedScreen unstashScreenAnimated:animated finished:finished];
}
- (BOOL) isOpened { return self.wrappedScreen.isOpened && self.navigationScreen.topSubScreen == self; }
- (void) openScreenAnimated:(BOOL)animated finished:(void (^)(void))finished {
    [self.wrappedScreen openScreenAnimated:animated finished:finished];
}
- (void) closeScreenAnimated:(BOOL)animated finished:(void (^)(void))finished {
    [self.wrappedScreen closeScreenAnimated:animated finished:finished];
}

@end



@implementation MPLNavigationSubScreen (Wrappable)

- (instancetype) wrapMainScreenToSubScreenForRoot:(MPLNavigationScreen*)rootScreen {
    [[[MPLNavigationScreenWrapToSubScreen alloc] initWithNavigationScreen:rootScreen andWrappedScreen:self.navigationScreen] autorelease];
    return self;
}

@end



@implementation MPLNavigationScreen (Wrappable)

- (instancetype) wrapScreenToSubScreenForRoot:(MPLNavigationScreen*)rootScreen {
    [[[MPLNavigationScreenWrapToSubScreen alloc] initWithNavigationScreen:rootScreen andWrappedScreen:self] autorelease];
    return self;
}

@end

