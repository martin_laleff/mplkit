//
//  MPLNavigationScreenWrapToSubScreen.h
//  MPLKit
//
//  Created by Martin Lalev on 4/5/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPLNavigationScreenWrapToSubScreen : MPLNavigationSubScreen<MPLScreenDelegate>

@property (nonatomic, retain) MPLNavigationScreen* wrappedScreen;

@end

@interface MPLNavigationSubScreen (Wrappable)

- (instancetype) wrapMainScreenToSubScreenForRoot:(MPLNavigationScreen*)rootScreen;

@end

@interface MPLNavigationScreen (Wrappable)

- (instancetype) wrapScreenToSubScreenForRoot:(MPLNavigationScreen*)rootScreen;

@end

