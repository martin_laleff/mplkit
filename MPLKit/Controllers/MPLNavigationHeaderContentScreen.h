//
//  MPLNavigationHeaderContentScreen.h
//  MPLKit
//
//  Created by Martin Lalev on 3/30/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLNavigationScreen.h"
#import "MPLHeaderContentViews.h"



@interface MPLNavigationHeaderContentScreen : MPLNavigationScreen

@property (nonatomic, strong) MPLHeaderContentView* view;

@end




