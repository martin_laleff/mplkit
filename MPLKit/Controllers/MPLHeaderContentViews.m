//
//  MPLNavigationHeaderContentView.m
//  MPLKit
//
//  Created by Martin Lalev on 4/7/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLHeaderContentViews.h"

@implementation MPLHeaderContentView

- (void) loaderWithHeaderHeight:(float)headerHeight {
    [MPLPanel create:^(MPLPanel* v) { self.headerContainerView = v; [self headerLoader:v withHeaderHeight:headerHeight]; }];
    [MPLPanel create:^(MPLPanel* v) { self.contentContainerView = v; [self contentLoader:v]; }];
}
- (void) headerLoader:(MPLPanel*)v withHeaderHeight:(float)headerHeight {
    [v addTo:self];
    [v fillWidth];
    [v putAtTopWithHeight:headerHeight];
}
- (void) contentLoader:(MPLPanel*)v {
    [v addTo:self];
    [v fillWidth];
    [v fillHeightFromTop:self.headerContainerView atDistance:0 toParentBottomAtDistance:0];
}

@end



@implementation MPLHeaderView

- (void) loaderForView:(MPLView*)v positionedAt:(MPLHeaderViewPosition)position atDistance:(float)distance withWidth:(float)width andAction:(void(^)(MPLView* v))action {
    [v addTo:self];
    
    if (position == MPLHeaderViewPositionLeft)
        [v putAtLeftAtDistance:distance andWidth:width];
    else if (position == MPLHeaderViewPositionRight)
        [v putAtRightAtDistance:distance andWidth:width];
    else
        [v putAtCenterBetweenLeft:nil andRight:nil withWidth:width];
    
    [v fillHeight];
    [v assignTouchAction:action];
}
- (void) loaderForCenteredView:(MPLView*)v positionedAt:(MPLHeaderViewPosition)position withMiddleViewWidth:(float)mvwidth andPadding:(float)padding andAction:(void(^)(MPLView* v))action {
    [self loaderForView:v positionedAt:position atDistance:padding withWidth:(self.widthBounds-mvwidth)/2.-2*padding andAction:action];
}

- (void) loaderForImageView:(MPLImage*)v withImage:(UIImage*)image andContentMode:(UIViewContentMode)contentMode {
    v.image = image;
    v.contentMode = contentMode;
}
- (void) loaderForLabelView:(MPLLabel*)v withText:(NSString*)text andAlignment:(NSTextAlignment)alignment andColor:(UIColor*)color andFont:(UIFont*)font andNumberOfLines:(int)numberOfLines {
    [v textLoaderWithText:text andTextAlignment:alignment andColor:color andFont:font];
    v.numberOfLines = numberOfLines;
}



- (MPLImage*) addImageViewPoistionedAt:(MPLHeaderViewPosition)position withImage:(UIImage*)image andContentMode:(UIViewContentMode)contentMode atDistance:(float)distance withWidth:(float)width andAction:(void(^)(MPLView* v))action {
    return [MPLImage create:^(MPLImage *v) {
        [self loaderForView:v positionedAt:position atDistance:distance withWidth:width andAction:action];
        [self loaderForImageView:v withImage:image andContentMode:contentMode];
    }];
}
- (MPLImage*) addCenteredImageViewPoistionedAt:(MPLHeaderViewPosition)position withImage:(UIImage*)image andContentMode:(UIViewContentMode)contentMode withMiddleViewWidth:(float)mvwidth andPadding:(float)padding andAction:(void(^)(MPLView* v))action {
    return [MPLImage create:^(MPLImage *v) {
        [self loaderForCenteredView:v positionedAt:position withMiddleViewWidth:mvwidth andPadding:padding andAction:action];
        [self loaderForImageView:v withImage:image andContentMode:contentMode];
    }];
}



- (MPLLabel*) addLabelViewPoistionedAt:(MPLHeaderViewPosition)position withText:(NSString*)text andAlignment:(NSTextAlignment)alignment andColor:(UIColor*)color andFont:(UIFont*)font andNumberOfLines:(int)numberOfLines atDistance:(float)distance withWidth:(float)width andAction:(void(^)(MPLView* v))action {
    return [MPLLabel create:^(MPLLabel *v) {
        [self loaderForView:v positionedAt:position atDistance:distance withWidth:width andAction:action];
        [self loaderForLabelView:v withText:text andAlignment:alignment andColor:color andFont:font andNumberOfLines:numberOfLines];
    }];
}
- (MPLLabel*) addCenteredLabelViewPoistionedAt:(MPLHeaderViewPosition)position withText:(NSString*)text andColor:(UIColor*)color andFont:(UIFont*)font andNumberOfLines:(int)numberOfLines withMiddleViewWidth:(float)mvwidth andPadding:(float)padding andAction:(void(^)(MPLView* v))action {
    return [MPLLabel create:^(MPLLabel *v) {
        [self loaderForCenteredView:v positionedAt:position withMiddleViewWidth:mvwidth andPadding:padding andAction:action];
        [self loaderForLabelView:v withText:text andAlignment:NSTextAlignmentCenter andColor:color andFont:font andNumberOfLines:numberOfLines];
    }];
}



@end
