//
//  MPLSingleViewScreen.h
//  MPLKit
//
//  Created by Martin Lalev on 4/6/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLScreen.h"

@interface MPLSingleViewScreen : MPLScreen

@property (nonatomic, strong) MPLPanel* view;

@property (weak, nonatomic, readonly) MPLTransition* slideFromBelowTransition;
@property (weak, nonatomic, readonly) MPLTransition* slideFromAboveTransition;
@property (weak, nonatomic, readonly) MPLTransition* slideFromLeftTransition;
@property (weak, nonatomic, readonly) MPLTransition* slideFromRightTransition;
@property (weak, nonatomic, readonly) MPLTransition* fadeInTransition;
@property (weak, nonatomic, readonly) MPLTransition* fadeOutTransition;
@property (weak, nonatomic, readonly) MPLTransition* fadeInTransitionWithZoom;
@property (weak, nonatomic, readonly) MPLTransition* fadeOutTransitionWithZoom;

- (id) initAsApplicationRootScreen;
- (void) loadAsApplicationRootScreen;

- (void) createScreenWithView:(MPLPanel*)v;

- (MPLPanel*) generateViewWithLoader:(void(^)(MPLPanel* v))loader;
- (void) createScreenWithViewWithLoader:(void(^)(MPLPanel*v))loader;
- (void) createScreenWithViewInFullSizeInParent:(MPLPanel*)parent;

@end
