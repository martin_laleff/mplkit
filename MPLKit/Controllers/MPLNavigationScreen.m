//
//  MPLNavigationScreen.m
//  MPLKit
//
//  Created by Martin Lalev on 12/29/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLNavigationScreen.h"

@implementation MPLNavigationScreen



#pragma mark -
#pragma mark Initialization and Screen stack management

- (id) init {
    if ([super init]) { self.subScreensStack = [NSMutableArray array]; }
    return self;
}

- (MPLNavigationSubScreen*) rootSubScreen {
    return (MPLNavigationSubScreen*)[self.subScreens firstObject];
}
- (MPLNavigationSubScreen*) topSubScreen {
    return self.subScreensStack.count == 0 ? nil : (MPLNavigationSubScreen*)[self.subScreensStack lastObject];
}
- (MPLNavigationSubScreen*) previousSubScreen {
    return [self subScreenBefore:self.topSubScreen];
}
- (MPLNavigationSubScreen*) subScreenBefore:(MPLNavigationSubScreen *)subScreen {
    NSInteger index = [self.subScreensStack indexOfObject:subScreen];
    return (index > 0 && index < self.subScreensStack.count ? [self.subScreensStack objectAtIndex:index-1] : nil);
}



#pragma mark -
#pragma mark Open / Close management

- (void) screenWillOpenAnimated:(BOOL)animated finished:(void(^)(void))finished {
    [super screenWillOpenAnimated:animated finished:^{
        if (self.topSubScreen == nil) {
            [self pushSubScreen:self.rootSubScreen animated:NO stashing:MPLNavigationTransitionStashTogether finished:^{
                finished();
            }];
        }
        else
            finished();
    }];
}
- (void) screenDidCloseAnimated:(BOOL)animated finished:(void (^)(void))finished {
    [self goBackToSubScreen:nil animated:NO finished:^{
        [super screenDidCloseAnimated:animated finished:finished];
    }];
}



#pragma mark -
#pragma mark SubScreen push/pop

- (void) pushFinishedFor:(MPLNavigationSubScreen*)subScreen finished:(void(^)(void))finished {
    if (self.topSubScreen != subScreen)
        [self.subScreensStack addObject:subScreen];
    finished();
}
- (void) pushSubScreen:(MPLNavigationSubScreen*)subScreen animated:(BOOL)animated stashing:(MPLNavigationTransitionStash)stashing finished:(void(^)(void))finished {
    
    subScreen.openedWithStash = stashing;
    switch (stashing) {
        case MPLNavigationTransitionStashBefore: {
            [self.topSubScreen stashScreenAnimated:animated finished:^{
                [subScreen openScreenAnimated:animated finished:^{ [self pushFinishedFor:subScreen finished:finished]; }];
            }];
            break;
        }
        case MPLNavigationTransitionStashAfter: {
            [subScreen openScreenAnimated:animated finished:^{
                [self.topSubScreen stashScreenAnimated:animated finished:^{ [self pushFinishedFor:subScreen finished:finished]; }];
            }];
            break;
        }
        default: {
            [subScreen openScreenAnimated:animated finished:^{ [self pushFinishedFor:subScreen finished:finished]; }];
            [self.topSubScreen stashScreenAnimated:animated finished:^{}];
            break;
        }
    }
}
- (void) popFinishedFor:(MPLNavigationSubScreen*)subScreen finished:(void(^)(void))finished {
//    if (self.topSubScreen == subScreen)
        [self.subScreensStack removeObject:subScreen];
    finished();
}
- (void) popSubScreen:(MPLNavigationSubScreen*)subScreen animated:(BOOL)animated finished:(void(^)(void))finished {
    MPLNavigationTransitionStash stashing = subScreen.closeWithStash;
    
    MPLNavigationSubScreen* previousSubScreen = [self subScreenBefore:subScreen];
    switch (stashing) {
        case MPLNavigationTransitionStashBefore: {
            [previousSubScreen unstashScreenAnimated:animated finished:^{
                [subScreen closeScreenAnimated:animated finished:^{ [self popFinishedFor:subScreen finished:finished]; }];
            }];
            break;
        }
        case MPLNavigationTransitionStashAfter: {
            [subScreen closeScreenAnimated:animated finished:^{
                [previousSubScreen unstashScreenAnimated:animated finished:^{ [self popFinishedFor:subScreen finished:finished]; }];
            }];
            break;
        }
        default: {
            [subScreen closeScreenAnimated:animated finished:^{ [self popFinishedFor:subScreen finished:finished]; }];
            [previousSubScreen unstashScreenAnimated:animated finished:^{}];
            break;
        }
    }
}
- (void) goBackToSubScreen:(MPLNavigationSubScreen*)subScreen animated:(BOOL)animated finished:(void(^)(void))finished {
    if (![self.subScreensStack containsObject:subScreen] && subScreen != nil) {
        finished();
        return;
    }
    
    if (self.topSubScreen != subScreen) {
        [self popSubScreen:self.topSubScreen animated:animated finished:^{
            [self goBackToSubScreen:subScreen animated:animated finished:finished];
        }];
    }
    else {
        finished();
    }
}

@end
