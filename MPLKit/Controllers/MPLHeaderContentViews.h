//
//  MPLNavigationHeaderContentView.h
//  MPLKit
//
//  Created by Martin Lalev on 4/7/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPLHeaderContentView : MPLPanel

@property (nonatomic, strong) MPLPanel* headerContainerView;
@property (nonatomic, strong) MPLPanel* contentContainerView;

- (void) loaderWithHeaderHeight:(float)headerHeight;
- (void) headerLoader:(MPLPanel*)v withHeaderHeight:(float)headerHeight;
- (void) contentLoader:(MPLPanel*)v;

@end



typedef enum : NSUInteger {
    MPLHeaderViewPositionLeft,
    MPLHeaderViewPositionRight
} MPLHeaderViewPosition;
@interface MPLHeaderView : MPLPanel

- (void) loaderForCenteredView:(MPLView*)v positionedAt:(MPLHeaderViewPosition)position withMiddleViewWidth:(float)mvwidth andPadding:(float)padding andAction:(void(^)(MPLView* v))action;
- (void) loaderForView:(MPLView*)v positionedAt:(MPLHeaderViewPosition)position atDistance:(float)distance withWidth:(float)width andAction:(void(^)(MPLView* v))action;
- (void) loaderForImageView:(MPLImage*)v withImage:(UIImage*)image andContentMode:(UIViewContentMode)contentMode;
- (void) loaderForLabelView:(MPLLabel*)v withText:(NSString*)text andAlignment:(NSTextAlignment)alignment andColor:(UIColor*)color andFont:(UIFont*)font andNumberOfLines:(int)numberOfLines;

- (MPLImage*) addImageViewPoistionedAt:(MPLHeaderViewPosition)position withImage:(UIImage*)image andContentMode:(UIViewContentMode)contentMode atDistance:(float)distance withWidth:(float)width andAction:(void(^)(MPLView* v))action;
- (MPLImage*) addCenteredImageViewPoistionedAt:(MPLHeaderViewPosition)position withImage:(UIImage*)image andContentMode:(UIViewContentMode)contentMode withMiddleViewWidth:(float)mvwidth andPadding:(float)padding andAction:(void(^)(MPLView* v))action;
- (MPLLabel*) addLabelViewPoistionedAt:(MPLHeaderViewPosition)position withText:(NSString*)text andAlignment:(NSTextAlignment)alignment andColor:(UIColor*)color andFont:(UIFont*)font andNumberOfLines:(int)numberOfLines atDistance:(float)distance withWidth:(float)width andAction:(void(^)(MPLView* v))action;
- (MPLLabel*) addCenteredLabelViewPoistionedAt:(MPLHeaderViewPosition)position withText:(NSString*)text andColor:(UIColor*)color andFont:(UIFont*)font andNumberOfLines:(int)numberOfLines withMiddleViewWidth:(float)mvwidth andPadding:(float)padding andAction:(void(^)(MPLView* v))action;

@end
