//
//  MPLNavigationHeaderContentScreen.m
//  MPLKit
//
//  Created by Martin Lalev on 3/30/15.
//  Copyright (c) 2015 Martin Lalev. All rights reserved.
//

#import "MPLNavigationHeaderContentScreen.h"

@implementation MPLNavigationHeaderContentScreen

- (MPLPanel*) generateViewWithLoader:(void (^)(MPLPanel *))loader {
    return [MPLHeaderContentView create:^(MPLHeaderContentView* v) { loader(v); }];
}

@end
