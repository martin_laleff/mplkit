//
//  MPLKit.h
//  MPLKit
//
//  Created by Martin Lalev on 3/25/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MPLAction.h"
#import "MPLFilenameManager.h"
#import "MPLPListManager.h"
#import "MPLUniqueDeviceID.h"
#import "MPLURLConnection.h"
#import "MPLMarqueeLabel.h"
#import "MPLAlert.h"
#import "MPLApplication.h"
#import "MPLImage.h"
#import "MPLLabel.h"
#import "MPLPage.h"
#import "MPLPanel.h"
#import "MPLScrollPanel.h"
#import "MPLSegment.h"
#import "MPLSlider.h"
#import "MPLSwitch.h"
#import "MPLTextField.h"
#import "MPLTextView.h"
#import "MPLView.h"
#import "MPLViewController.h"
#import "MPLTableView.h"
#import "MPLWebView.h"
#import "MPLSlideItemsPanel.h"
#import "MPLStackView.h"
#import "MPLBase64Encoder.h"
#import "NSData+AES256.h"
#import "NSString+URLEncoding.h"
#import "UIImage+ImageEffects.h"
#import "UIDevice+Hardware.h"
#import "MPLOpenUDID.h"
#import "MPLKeychain.h"
#import "MPLPushNotificationsManager.h"
#import "NSObject+SBJSON.h"
#import "NSString+SBJSON.h"
#import "NSString+HTML.h"
#import "MPLScreen.h"
#import "MPLNavigationScreen.h"
#import "MPLNavigationSubScreen.h"
#import "MPLNavigationHeaderContentScreen.h"
#import "MPLNavigationHeaderContentSubScreen.h"
#import "NSDate+Components.h"
#import "MPLDictionaryObject.h"
#import "MPLDictionaryObjectSet.h"
#import "MPLDictionaryObjectManager.h"
#import "MPLGradientLayer.h"
#import "MPLWebServiceMockup.h"

//CGSize CGSizeScaleToFit(CGSize sizeToScale, CGSize sizeToFitIn) {
//    float ratio = MIN(sizeToFitIn.width/sizeToScale.width, sizeToFitIn.height/sizeToScale.height);
//    return CGSizeMake(sizeToScale.width*ratio, sizeToScale.height/ratio);
//}
//CGSize CGSizeScaleToFill(CGSize sizeToScale, CGSize sizeToFitIn) {
//    float ratio = MAX(sizeToFitIn.width/sizeToScale.width, sizeToFitIn.height/sizeToScale.height);
//    return CGSizeMake(sizeToScale.width*ratio, sizeToScale.height/ratio);
//}

#ifdef DEBUG
#define MPLLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define MPLLog(...)
#endif

#define __MPLApplication [MPLApplication SharedApplication]
#define __MPLScreenSize [UIScreen mainScreen].bounds.size
#define __MPLScreenSizeW MPLScreenSize.width
#define __MPLScreenSizeH MPLScreenSize.height

