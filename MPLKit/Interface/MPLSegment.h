//
//  FTSegment.h
//  FTrack_Take2
//
//  Created by Administrator on 08/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLView.h"

@interface MPLSegment : MPLView

@property (nonatomic, assign) NSInteger selectedIndex;
@property (weak, nonatomic, readonly) UISegmentedControl* segmentedControlView;

- (void) insertItem:(id)item atIndex:(NSUInteger)index animated:(BOOL)animated;
- (void) addItem:(id)item animated:(BOOL)animated;
- (void) removeItemAtIndex:(NSUInteger)index animated:(BOOL)animated;

- (void) loaderWithItems:(NSArray*)items andSelectedIndex:(int)selectedIndex andAction:(MPLAction*)selectionChange;
- (void) loaderWithItems:(NSArray*)items andAction:(MPLAction*)selectionChanged;

@end
