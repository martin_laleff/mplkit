//
//  FTTextbox.m
//  FTrack_Take2
//
//  Created by Martin Lalev on 9/6/11.
//  Copyright 2011 mART. All rights reserved.
//

#import "MPLTextField.h"


@implementation MPLTextField

- (UITextField*)textFieldView { return (UITextField*)self.view; }
- (UIView*) createView { return [[UITextField alloc] init]; }

- (void) loaderWithPlaceholder:(NSString*)placeholder
                 andIsPassword:(BOOL)isPassword
               andKeyboardType:(UIKeyboardType)keyboardType
              andTextAlignment:(NSTextAlignment)textAlignment
{
    self.textFieldView.placeholder = placeholder;
    self.textFieldView.secureTextEntry = isPassword;
    self.textFieldView.keyboardType = keyboardType;
    self.textFieldView.textAlignment = textAlignment;
}

+ (MPLTextField*) create:(void(^)(MPLTextField* v))creater {
    MPLTextField* v = [[MPLTextField alloc] init];
    v.textFieldView.borderStyle = UITextBorderStyleRoundedRect;
    v.textFieldView.returnKeyType = UIReturnKeyDone;
    v.textFieldView.delegate = v;
    v.textFieldView.rightViewMode = UITextFieldViewModeAlways;
    v.textFieldView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    creater(v);
    return v;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [self performAction:@"Done"];
    return YES;
}
- (void) textFieldDidEndEditing:(UITextField *)textField {
    [self performAction:@"DidEndEditing"];
}

- (void) indicateValid:(BOOL)isValid {
    [self indicateValid:isValid withViewToShake:self];
}
- (void) indicateValid:(BOOL)isValid withViewToShake:(MPLView*)viewToShake {
    UIView* rightView = nil;
    if (!isValid) {
        rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        rightView.backgroundColor = [UIColor redColor];
        [viewToShake shake:0 withMaxShakes:6 andDistance:10 andDuration:0.1];
    }
    [self.textFieldView setRightView:rightView];
}
- (BOOL) isEmail {
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"];
    return self.textFieldView.text.length > 0 && [emailTest evaluateWithObject:self.textFieldView.text];
}
- (void) enableToolbarAccessoryWithBarButtonGenerator:(void (^)(NSMutableArray *))barButtonsGenerator {
    self.toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    self.toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    NSMutableArray* barButtons = [NSMutableArray array];
    barButtonsGenerator(barButtons);
    [self.toolbar setItems:barButtons animated:NO];
    self.textFieldView.inputAccessoryView = self.toolbar;
}


@end
