//
//  MPLStackView.m
//  Posledno
//
//  Created by Martin Lalev on 12/12/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLStackView.h"

@implementation MPLStackView

- (CGSize) calculateContentSize {
    float minX = FLT_MAX;
    float minY = FLT_MAX;
    float maxX = 0;
    float maxY = 0;
    for (MPLView* v in self.subViews.allValues) {
        if (!v.visible || v.view.hidden) continue;
        float l = v.xOffset;
        float t = v.yOffset;
        float r = v.xOffset+v.widthFrame;
        float b = v.yOffset+v.heightFrame;
        if (l > minX) minX = l;
        if (t > minY) minY = t;
        if (r > maxX) maxX = r;
        if (b > maxY) maxY = b;
    }
    return CGSizeMake(maxX, maxY);
}


- (void) resetSize {
    self.frame = CGRectMake(self.xOffset, self.yOffset, self.widthFrame, self.calculateContentSize.height);
}
- (void) loader {
    self.lastAddedView = nil;
}

- (void) addView:(MPLView*)v withHeight:(float)height withHorizontalMargin:(float)horizontalMargin {
    [v addTo:self];
    [v fillWidthWithDistance:horizontalMargin];
    [v putAtTopNextTo:self.lastAddedView atDistance:self.spacing andHeight:height];
    self.lastAddedView = v;
    self.spacing = horizontalMargin;
    
    if (self.mask & UIViewAutoresizingFlexibleTopMargin && self.mask & UIViewAutoresizingFlexibleBottomMargin) {
        self.heightBounds = self.lastAddedView.yOffset + self.lastAddedView.heightFrame;
    }
    else if (self.mask & UIViewAutoresizingFlexibleBottomMargin) {
        self.heightFrame = self.lastAddedView.yOffset + self.lastAddedView.heightFrame;
    }
    else if (self.mask & UIViewAutoresizingFlexibleTopMargin) {
        float bottom = self.yOffset + self.heightBounds;
        self.heightFrame = self.lastAddedView.yOffset + self.lastAddedView.heightFrame;
        self.yOffset = bottom-self.heightBounds;
    }
}
- (void) addView:(MPLView*)v withHeight:(float)height { [self addView:v withHeight:height withHorizontalMargin:5]; }
- (void) assignNextSpacing:(float)spacing {
    self.spacing = spacing;
}

- (MPLImage*) addImage:(UIImage*)image withHeight:(float)height {
    return [MPLImage create:^(MPLImage *v) {
        [self addView:v withHeight:height];
        v.image = image;
        v.contentMode = UIViewContentModeCenter;
    }];
}
- (MPLImage*) addImage:(UIImage*)image {
    return [MPLImage create:^(MPLImage *v) {
        [self addView:v withHeight:image.size.height];
        v.image = image;
        v.contentMode = UIViewContentModeCenter;
    }];
}

- (MPLLabel*) addLabelWithText:(NSString*)text andAlignment:(NSTextAlignment)alignment andFont:(UIFont*)font andColor:(UIColor*)color andHeight:(float)height {
    return [MPLLabel create:^(MPLLabel *v) {
        [self addView:v withHeight:height withHorizontalMargin:15];
        [v textLoaderWithText:text andTextAlignment:alignment andColor:color andFont:font];
        v.numberOfLines = 0;
    }];
}
- (MPLLabel*) addLabelWithText:(NSString*)text andAlignment:(NSTextAlignment)alignment andFont:(UIFont*)font andColor:(UIColor*)color {
    return [self addLabelWithText:text andAlignment:alignment andFont:font andColor:color andHeight:[MPLLabel heightForLabelWithText:text andWidth:self.widthBounds-20 andFont:font andAlignment:alignment]];
}

- (MPLLabel*) addButtonWithText:(NSString*)text andBackgroundColor:(UIColor*)backgroundColor andTextColor:(UIColor*)textColor andFont:(UIFont*)font andAction:(void(^)(void))action {
    return [MPLLabel create:^(MPLLabel *v) {
        [self addView:v withHeight:50];
        
        [v textLoaderWithText:text andTextAlignment:NSTextAlignmentCenter andColor:textColor andFont:font];
        v.backgroundColor = backgroundColor;
        [v assignTouchAction:^(MPLView *v) {
            action();
        }];
    }];
}

@end
