//
//  FTPage.h
//  FTrack_Take2
//
//  Created by Administrator on 21/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLView.h"


@interface MPLPage : MPLView

@property (weak, nonatomic, readonly) UIPageControl* pageControlView;

@property (nonatomic, assign) NSInteger numberOfPages;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) BOOL hidesForSinglePage;

- (instancetype) assignValueChangeActionObject:(MPLAction*)action;
- (instancetype) assignValueChangeAction:(void(^)(MPLPage* page))action;

- (void) loaderWithCurrentPage:(NSInteger)currentPage
		 andNumberOfPages:(NSInteger)numberOfPages
	andHidesForSinglePage:(BOOL)hidesForSinglePage
	andValueChangedAction:(MPLAction*)valueChangedAction
;
+ (MPLPage*) create:(void(^)(MPLPage* v))creater;

@end
