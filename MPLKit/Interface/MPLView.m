//
//  FTControl.m
//  FTrack_Take2
//
//  Created by Administrator on 05/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MPLView.h"
#import "MPLApplication.h"


@implementation MPLTransition

@synthesize defaultDuration = _defaultDuration;
- (float) transitionState {
    return self.transitionStateGet();
}
- (void) setTransitionState:(float)transitionState {
    self.transitionStateSet(transitionState);
}
@synthesize transitionStateGet = _transitionStateGet;
@synthesize transitionStateSet = _transitionStateSet;
@synthesize fixTransitionStateEnabled = _fixTransitionStateEnabled;
@synthesize beginTransitionStateTo = _beginTransitionStateTo;
@synthesize endTransitionStateTo = _endTransitionStateTo;

- (instancetype) assignGetter:(float (^)(void))getter { self.transitionStateGet = getter; return self; }
- (instancetype) assignSetter:(void (^)(float value))setter { self.transitionStateSet = setter; return self; }
- (instancetype) assignFixer:(void (^)(BOOL enabled))fixer { self.fixTransitionStateEnabled = fixer; return self; }
- (instancetype) assignBegin:(void(^)(float position, void(^finishedBlock)(void)))beginner { self.beginTransitionStateTo = beginner; return self; }
- (instancetype) assignEnd:(void(^)(float position, void(^finishedBlock)(void)))ender { self.endTransitionStateTo = ender; return self; }

- (void) clear {
    [self assignSetter:nil];
    [self assignGetter:nil];
    [self assignFixer:nil];
    [self assignBegin:nil];
    [self assignEnd:nil];
}

- (id) initWithGet:(float (^)(void))getter set:(void (^)(float))setter andFixTransition:(void (^)(BOOL))fixTransitionStateEnabled andBegin:(void (^)(float, void (^)(void)))beginTransitionStateTo andEnd:(void (^)(float, void (^)(void)))endTransitionStateTo {
    if ([super init]) {
        self.defaultDuration = .3;
        self.defaultOptions = UIViewAnimationOptionCurveEaseOut;
        self.transitionStateGet = getter;
        self.transitionStateSet = setter;
        self.fixTransitionStateEnabled = fixTransitionStateEnabled;
        self.beginTransitionStateTo = beginTransitionStateTo;
        self.endTransitionStateTo = endTransitionStateTo;
    }
    return self;
}
+ (MPLTransition*) create:(void(^)(MPLTransition* t))creater {
    MPLTransition* t = [MPLTransition transitionWithGet:^float{ return 0; } set:^(float value) {} andFixTransition:[MPLTransition fixTransitionEmpty] andBegin:[MPLTransition beginEmpty] andEnd:[MPLTransition endEmpty]];
    creater(t);
    return t;
}
+ (MPLTransition*) transitionWithGet:(float(^)(void))getter set:(void(^)(float value))setter andFixTransition:(void(^)(BOOL enabled))fixTransitionStateEnabled andBegin:(void(^)(float position, void(^finishedBlock)(void)))beginTransitionStateTo andEnd:(void(^)(float position, void(^finishedBlock)(void)))endTransitionStateTo {
    return [[MPLTransition alloc] initWithGet:getter set:setter andFixTransition:fixTransitionStateEnabled andBegin:beginTransitionStateTo andEnd:endTransitionStateTo];
}
+ (void(^)(BOOL enabled)) fixTransitionEmpty { return ^(BOOL enabled){}; }
+ (void(^)(float position, void(^finishedBlock)(void))) beginEmpty { return ^(float position, void(^finishedBlock)(void)){ finishedBlock(); }; }
+ (void(^)(float position, void(^finishedBlock)(void))) endEmpty { return ^(float position, void(^finishedBlock)(void)){ finishedBlock(); }; }

- (void) setTransitionStateFixed:(float)value {
    if (value < 0) value = 0;
    if (value > 1) value = 1;
    [self setTransitionState:value];
}

- (void) transitionStateTo:(float)position duration:(float)duration bounce:(float)bounce finished:(void(^)(void))finishedBlock {
    void (^setToPosition)(float, float, BOOL, void(^)(void)) = ^(float p, float duration, BOOL fix, void(^complete)(void)){
        [UIView animateWithDuration:duration delay:0 options:self.defaultOptions animations:^{
            if (fix && p <= 0) self.fixTransitionStateEnabled(NO);
            self.transitionState = p;
        } completion:^(BOOL finished) { complete(); }];
    };
    
    if (self.transitionState == position) {
        self.beginTransitionStateTo(position,^{
            self.fixTransitionStateEnabled(position > 0);
            self.endTransitionStateTo(position, finishedBlock);
        });
    }
    else {
        self.beginTransitionStateTo(position, ^{
            if (position > 0) self.fixTransitionStateEnabled(YES);
            
            if (bounce == 0) setToPosition(position, duration, YES, ^{
                self.endTransitionStateTo(position,finishedBlock);
            });
            else
                setToPosition(position + bounce * (position - self.transitionState), duration, NO, ^{
                    setToPosition(position, duration*4/5., YES, ^{
                        self.endTransitionStateTo(position, finishedBlock);
                    });
                });
        });
    }
}
- (void) transitionStateTo:(float)position duration:(float)duration finished:(void(^)(void))finishedBlock {
    [self transitionStateTo:position duration:duration bounce:0 finished:finishedBlock];
}
- (void) transitionStateTo:(float)position animated:(BOOL)animated finished:(void(^)(void))finishedBlock {
    [self transitionStateTo:position duration:animated ? self.defaultDuration : 0 bounce:0 finished:finishedBlock];
}

+ (MPLTransition*) emptyTransition {
    return [MPLTransition create:^(MPLTransition *t) {
        __block float fakeValue = 0;
        [t assignGetter:^float{
            return fakeValue;
        }];
        [t assignSetter:^(float value) {
            fakeValue = value;
        }];
    }];
}

@end






@implementation MPLView



#pragma mark -
#pragma mark Mask

- (UIViewAutoresizing) mask { return self.view.autoresizingMask; }
- (void) setMask:(UIViewAutoresizing)mask { self.view.autoresizingMask = mask; }
- (void) resetMask:(void(^)(MPLView* v))reset {
    self.mask = UIViewAutoresizingNone;
    reset(self);
}



#pragma mark -
#pragma mark Alpha and visibility

- (float) alpha { return self.view.alpha; }
- (void) setAlpha:(float)alpha { self.view.alpha = alpha; }

- (BOOL) visible { return self.alpha != 0; }
- (void) setVisible:(BOOL)visible { self.alpha = (visible ? 1 : 0); }



#pragma mark -
#pragma mark Frame and offset

- (CGRect) frame { return self.view.frame; }

- (CGSize) sizeFrame { return self.frame.size; }
- (CGFloat) widthFrame { return self.sizeFrame.width; }
- (CGFloat) heightFrame { return self.sizeFrame.height; }

- (CGPoint) offset { return self.frame.origin; }
- (CGFloat) xOffset { return self.offset.x; }
- (CGFloat) yOffset { return self.offset.y; }

- (void) setFrame:(CGRect)value { self.view.frame = value; }

- (void) setSizeFrame:(CGSize)size { self.frame = (CGRect){self.frame.origin,size}; }
- (void) setWidthFrame:(CGFloat)width { self.sizeFrame = CGSizeMake(width, self.heightFrame); }
- (void) setHeightFrame:(CGFloat)height { self.sizeFrame = CGSizeMake(self.widthFrame, height); }

- (void) setOffset:(CGPoint)offset { self.frame = (CGRect){offset,self.frame.size}; }
- (void) setXOffset:(CGFloat)xOffset { self.offset = CGPointMake(xOffset, self.yOffset); }
- (void) setYOffset:(CGFloat)yOffset { self.offset = CGPointMake(self.xOffset, yOffset); }



#pragma mark -
#pragma mark Bounds and center

- (CGRect) bounds { return self.view.bounds; }

- (CGSize) sizeBounds { return self.bounds.size; }
- (CGFloat) widthBounds { return self.sizeBounds.width; }
- (CGFloat) heightBounds { return self.sizeBounds.height; }

- (CGPoint) center { return self.view.center; }
- (CGFloat) xCenter { return self.center.x; }
- (CGFloat) yCenter { return self.center.y; }

- (void) setBounds:(CGRect)value { self.view.bounds = value; }

- (void) setSizeBounds:(CGSize)size { self.bounds = (CGRect){CGPointZero,size}; }
- (void) setWidthBounds:(CGFloat)width { self.sizeBounds = CGSizeMake(width, self.heightBounds); }
- (void) setHeightBounds:(CGFloat)height { self.sizeBounds = CGSizeMake(self.widthBounds, height); }

- (void) setCenter:(CGPoint)center { self.view.center = center; }
- (void) setXCenter:(CGFloat)xCenter { self.center = CGPointMake(xCenter, self.yCenter); }
- (void) setYCenter:(CGFloat)yCenter { self.center = CGPointMake(self.xCenter, yCenter); }



#pragma mark -
#pragma mark Content calculation

- (CGPoint) maxContentCorner {
    float maxX = 0;
    float maxY = 0;
    for (UIView* v in self.view.subviews) {
        if (v.alpha == 0 || v.hidden) continue;
        float r = v.frame.origin.x+v.frame.size.width;
        float b = v.frame.origin.y+v.frame.size.height;
        if (r > maxX) maxX = r;
        if (b > maxY) maxY = b;
    }
    return CGPointMake(maxX, maxY);
}
- (CGPoint) minContentCorner {
    float minX = FLT_MAX;
    float minY = FLT_MAX;
    for (UIView* v in self.view.subviews) {
        if (v.alpha == 0 || v.hidden) continue;
        float l = v.frame.origin.x;
        float t = v.frame.origin.y;
        if (l < minX) minX = l;
        if (t < minY) minY = t;
    }
    return CGPointMake(minX, minY);
}
- (CGSize) contentSize {
    return CGSizeMake(self.maxContentCorner.x-self.minContentCorner.x,self.maxContentCorner.y-self.minContentCorner.y);
}



#pragma mark -
#pragma mark Parent

- (instancetype) addTo:(MPLPanel*)parent forKey:(NSString*)key {
    [parent addSubview:self forKey:key];
    return self;
}
- (instancetype) addTo:(MPLPanel*)parent {
    NSMutableString* randomKey = [NSMutableString string];
    for (int j = 0; j < 20; j++) {
        [randomKey appendString:[NSString stringWithFormat:@"%c",'a'+arc4random()%('z'-'a')]];
    }
    return [self addTo:parent forKey:randomKey];
}
- (void) createInside:(UIView *)parent {
    [parent addSubview:self.view];
}
- (void) removeFromSuperview {
    [self.view removeFromSuperview];
    self.parentView = nil;
}



#pragma mark -
#pragma mark Transformations

- (void) rotate90:(BOOL)left {
    self.bounds = CGRectMake(0,0, self.heightBounds, self.widthBounds);
    self.mask = UIViewAutoresizingNone;
    self.view.transform = CGAffineTransformMakeRotation((left?-1:+1)*M_PI/2);
}



#pragma mark -
#pragma mark Horizontal frame/mask setup

- (instancetype) putAtLeftNextTo:(MPLView*)relativeView atDistance:(float)distance andWidth:(float)width {
    self.xOffset = (relativeView == nil ? 0 : relativeView.xOffset + relativeView.widthFrame) + distance;
    self.widthFrame = width;
    self.mask |= UIViewAutoresizingFlexibleRightMargin;
    return self;
}
- (instancetype) putAtLeftAtDistance:(float)distance andWidth:(float)width {
    return [self putAtLeftNextTo:nil atDistance:distance andWidth:width];
}
- (instancetype) putAtLeftWithWidth:(float)width {
    return [self putAtLeftNextTo:nil atDistance:0 andWidth:width];
}
- (instancetype) putAtRightNextTo:(MPLView*)relativeView atDistance:(float)distance andWidth:(float)width {
    self.xOffset = (relativeView == nil ? self.parentView.widthFrame : relativeView.xOffset) - distance - width;
    self.widthFrame = width;
    self.mask |= UIViewAutoresizingFlexibleLeftMargin;
    return self;
}
- (instancetype) putAtRightAtDistance:(float)distance andWidth:(float)width {
    return [self putAtRightNextTo:nil atDistance:distance andWidth:width];
}
- (instancetype) putAtRightWithWidth:(float)width {
    return [self putAtRightNextTo:nil atDistance:0 andWidth:width];
}
- (instancetype) putAtLeftNextTo:(MPLView*)relativeLeftView atLeftDistance:(float)leftDistance atRightNextTo:(MPLView*)relativeRightView atRightDistance:(float)rightDistance withFixedWidth:(BOOL)fixedWidth {
    float l = (relativeLeftView == nil ? 0 : relativeLeftView.xOffset + relativeLeftView.widthFrame) + leftDistance;
    float r = (relativeRightView == nil ? self.parentView.widthFrame : relativeRightView.xOffset) - rightDistance;
    self.xOffset = l;
    self.widthFrame = r-l;
    if (!fixedWidth)
        self.mask |= UIViewAutoresizingFlexibleWidth;
    else
        self.mask |= (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
    return self;
}
- (instancetype) putAtLeftNextTo:(MPLView*)relativeLeftView atDistance:(float)leftDistance toRightAtDistance:(float)rightDistance withFixedWidth:(BOOL)fixedWidth {
    return [self putAtLeftNextTo:relativeLeftView atLeftDistance:leftDistance atRightNextTo:nil atRightDistance:rightDistance withFixedWidth:fixedWidth];
}
- (instancetype) putAtRightNextTo:(MPLView*)relativeRightView atDistance:(float)rightDistance toLeftAtDistance:(float)leftDistance withFixedWidth:(BOOL)fixedWidth {
    return [self putAtLeftNextTo:nil atLeftDistance:leftDistance atRightNextTo:relativeRightView atRightDistance:rightDistance withFixedWidth:fixedWidth];
}
- (instancetype) putAtLeftAtDistance:(float)leftDistance toRightAtDistance:(float)rightDistance withFixedWidth:(BOOL)fixedWidth {
    return [self putAtLeftNextTo:nil atLeftDistance:leftDistance atRightNextTo:nil atRightDistance:rightDistance withFixedWidth:fixedWidth];
}
- (instancetype) fillWidthWithDistance:(float)distance {
    return [self putAtLeftAtDistance:distance toRightAtDistance:distance withFixedWidth:NO];
}
- (instancetype) fillWidth {
    return [self fillWidthWithDistance:0];
}
- (instancetype) putBetweenLeft:(MPLView*)relativeLeftView atDistance:(float)leftDistance andRight:(MPLView*)relativeRightView atDistance:(float)rightDistance withWidth:(float)width andAlignment:(float)alignment {
    float l = (relativeLeftView == nil ? 0 : relativeLeftView.xOffset + relativeLeftView.widthFrame) + leftDistance;
    float r = (relativeRightView == nil ? self.parentView.widthFrame : relativeRightView.xOffset) - rightDistance;
    float c = l+(r-l)*alignment;
    self.xOffset = c-width/2;
    self.widthFrame = width;
    self.mask |= UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    return self;
}
- (instancetype) putBetweenLeft:(MPLView*)relativeLeftView andRight:(MPLView*)relativeRightView withWidth:(float)width andAlignment:(float)alignment {
    return [self putBetweenLeft:relativeLeftView atDistance:0 andRight:relativeRightView atDistance:0 withWidth:width andAlignment:alignment];
}
- (instancetype) putAtCenterBetweenLeft:(MPLView*)relativeLeftView atDistance:(float)leftDistance andRight:(MPLView*)relativeRightView atDistance:(float)rightDistance withWidth:(float)width {
    return [self putBetweenLeft:relativeLeftView atDistance:leftDistance andRight:relativeRightView atDistance:rightDistance withWidth:width andAlignment:0.5];
}
- (instancetype) putAtCenterBetweenLeft:(MPLView*)relativeLeftView andRight:(MPLView*)relativeRightView withWidth:(float)width {
    return [self putAtCenterBetweenLeft:relativeLeftView atDistance:0 andRight:relativeRightView atDistance:0 withWidth:width];
}



#pragma mark -
#pragma mark Vertical frame/mask setup

- (instancetype) putAtTopNextTo:(MPLView*)relativeView atDistance:(float)distance andHeight:(float)height {
    self.yOffset = (relativeView == nil ? 0 : relativeView.yOffset + relativeView.heightFrame) + distance;
    self.heightFrame = height;
    self.mask |= UIViewAutoresizingFlexibleBottomMargin;
    return self;
}
- (instancetype) putAtTopAtDistance:(float)distance andHeight:(float)height {
    return [self putAtTopNextTo:nil atDistance:distance andHeight:height];
}
- (instancetype) putAtTopWithHeight:(float)height {
    return [self putAtTopNextTo:nil atDistance:0 andHeight:height];
}
- (instancetype) putAtBottomNextTo:(MPLView*)relativeView atDistance:(float)distance andHeight:(float)height {
    self.yOffset = (relativeView == nil ? self.parentView.heightFrame : relativeView.yOffset) - distance - height;
    self.heightFrame = height;
    self.mask |= UIViewAutoresizingFlexibleTopMargin;
    return self;
}
- (instancetype) putAtBottomAtDistance:(float)distance andHeight:(float)height {
    return [self putAtBottomNextTo:nil atDistance:distance andHeight:height];
}
- (instancetype) putAtBottomWithHeight:(float)height {
    return [self putAtBottomNextTo:nil atDistance:0 andHeight:height];
}
- (instancetype) fillHeightFromTop:(MPLView*)relativeTopView atTopDistance:(float)topDistance andBottom:(MPLView*)relativeBottomView atBottomDistance:(float)bottomDistance {
    float t = (relativeTopView == nil ? 0 : relativeTopView.yOffset + relativeTopView.heightFrame) + topDistance;
    float b = (relativeBottomView == nil ? self.parentView.heightFrame : relativeBottomView.yOffset) - bottomDistance;
    self.yOffset = t;
    self.heightFrame = b-t;
    self.mask |= UIViewAutoresizingFlexibleHeight;
    return self;
}
- (instancetype) fillHeightFromTop:(MPLView*)relativeTopView atDistance:(float)topDistance toParentBottomAtDistance:(float)bottomDistance {
    return [self fillHeightFromTop:relativeTopView atTopDistance:topDistance andBottom:nil atBottomDistance:bottomDistance];
}
- (instancetype) fillHeightFromBottom:(MPLView*)relativeBottomView atDistance:(float)bottomDistance toParentTopAtDistance:(float)topDistance {
    return [self fillHeightFromTop:nil atTopDistance:topDistance andBottom:relativeBottomView atBottomDistance:bottomDistance];
}
- (instancetype) fillHeightWithTopDistance:(float)topDistance andBottomDistance:(float)bottomDistance {
    return [self fillHeightFromTop:nil atTopDistance:topDistance andBottom:nil atBottomDistance:bottomDistance];
}
- (instancetype) fillHeightWithDistance:(float)distance {
    return [self fillHeightWithTopDistance:distance andBottomDistance:distance];
}
- (instancetype) fillHeight {
    return [self fillHeightWithDistance:0];
}
- (instancetype) putBetweenTop:(MPLView*)relativeTopView atDistance:(float)topDistance andBottom:(MPLView*)relativeBottomView atDistance:(float)bottomDistance withHeight:(float)height andAlignment:(float)alignment {
    float l = (relativeTopView == nil ? 0 : relativeTopView.yOffset + relativeTopView.heightFrame) + topDistance;
    float r = (relativeBottomView == nil ? self.parentView.heightFrame : relativeBottomView.yOffset) - bottomDistance;
    float c = l+(r-l)*alignment;
    self.yOffset = c-height/2;
    self.heightFrame = height;
    self.mask |= UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    return self;
}
- (instancetype) putBetweenTop:(MPLView*)relativeTopView andBottom:(MPLView*)relativeBottomView withHeight:(float)height andAlignment:(float)alignment {
    return [self putBetweenTop:relativeTopView atDistance:0 andBottom:relativeBottomView atDistance:0 withHeight:height andAlignment:alignment];
}
- (instancetype) putAtCenterBetweenTop:(MPLView*)relativeTopView atDistance:(float)topDistance andBottom:(MPLView*)relativeBottomView atDistance:(float)bottomDistance withHeight:(float)height {
    return [self putBetweenTop:relativeTopView atDistance:topDistance andBottom:relativeBottomView atDistance:bottomDistance withHeight:height andAlignment:0.5];
}
- (instancetype) putAtCenterBetweenTop:(MPLView*)relativeTopView andBottom:(MPLView*)relativeBottomView withHeight:(float)height {
    return [self putAtCenterBetweenTop:relativeTopView atDistance:0 andBottom:relativeBottomView atDistance:0 withHeight:height];
}



#pragma mark -
#pragma mark View lifecycle management

- (id) init {
    if ([super init])
    {
        self.view = [self createView];
        self.mask = UIViewAutoresizingNone;
        
        self.actions = [NSMutableDictionary dictionary];
        self.customData = [NSMutableDictionary dictionary];
        
        self.themeAttatchments = [NSMutableDictionary dictionary];
        self.themeAttatchmentsKeyMappings = [NSMutableDictionary dictionary];
        
        self.PanAllowScroll = YES;
        self.PanInvocations = [NSMutableDictionary dictionary];
        self.LeftEdgePanInvocations = [NSMutableDictionary dictionary];
        self.RightEdgePanInvocations = [NSMutableDictionary dictionary];
        self.TapInvocations = [NSMutableDictionary dictionary];
        self.DoubleTapInvocations = [NSMutableDictionary dictionary];
        self.LongPressInvocations = [NSMutableDictionary dictionary];
        
        [self addDefaultActionHandlers];
    }
    return self;
}

- (UIView*) createView {
    return [[UIView alloc] init];
}
- (void) destroyView {
    [self removeFromSuperview];
    while (self.actions.count > 0) [self.actions removeObjectForKey:[self.actions.allKeys objectAtIndex:0]];
    self.actions = nil;
    self.view = nil;
    [self.customData removeAllObjects];
    self.customData = nil;
    self.action_button = nil;
    self.touchAction = nil;
    self.touchBlock = nil;
    while (self.PanInvocations.count > 0)
        [self disablePanForKey:self.PanInvocations.allKeys[0]];
    while (self.LeftEdgePanInvocations.count > 0)
        [self disableLeftEdgePanForKey:self.LeftEdgePanInvocations.allKeys[0]];
    while (self.RightEdgePanInvocations.count > 0)
        [self disableRightEdgePanForKey:self.RightEdgePanInvocations.allKeys[0]];
    while (self.TapInvocations.count > 0)
        [self disableTapForKey:self.TapInvocations.allKeys[0]];
    while (self.DoubleTapInvocations.count > 0)
        [self disableDoubleTapForKey:self.DoubleTapInvocations.allKeys[0]];
    while (self.LongPressInvocations.count > 0)
        [self disableLongPressForKey:self.LongPressInvocations.allKeys[0]];
    self.PanInvocations = nil;
    self.LeftEdgePanInvocations = nil;
    self.RightEdgePanInvocations = nil;
    self.TapInvocations = nil;
    self.DoubleTapInvocations = nil;
    self.LongPressInvocations = nil;
    if (self.frameObservers.count > 0) [self removeObserver:self forKeyPath:@"frame"];
    self.frameObservers = nil;

    while (self.themeAttatchmentsKeyMappings.count > 0) [self removeThemeAttatchmentFor:[self.themeAttatchmentsKeyMappings.allKeys objectAtIndex:0]];
}
- (void) dealloc {
    [self destroyView];
}




+ (void) stackVertically:(NSArray*)views {
    float y = 0;
    float currentSpacing = 0;
    
    for (NSArray* viewSpacingArray in views) {
        float spacingTop = [[viewSpacingArray objectAtIndex:0] floatValue];
        MPLView* v = [viewSpacingArray objectAtIndex:1];
        float spacingBottom = [[viewSpacingArray objectAtIndex:2] floatValue];
        
        if (!v.visible || v.view.hidden) continue;
        
        y += MAX(currentSpacing, spacingTop);
        v.yOffset = y;
        y += v.heightBounds;
        currentSpacing = spacingBottom;
    }
}
+ (void) stackViewVerticaly:(MPLView*)v ontoOffset:(float*)y withPadding:(float)padding {
    if (!v.visible || v.view.hidden) return;
    v.yOffset = (*y);
    (*y) += v.heightBounds + padding;
}



#pragma mark -
#pragma mark Common methods

- (void) addFrameObserver:(void(^)(MPLView* v))observer {
    if (self.frameObservers == nil) {
        self.frameObservers = [NSMutableArray array];
        [self addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
    }
    void(^block)(MPLView* v) = [observer copy];
    [self.frameObservers addObject:block];
}
- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    for (void(^observer)(MPLView* v) in self.frameObservers) observer(self);
}

- (void) setAnchorPoint:(CGPoint)anchorPoint {
    CGPoint newPoint = CGPointMake(self.view.bounds.size.width * anchorPoint.x, self.view.bounds.size.height * anchorPoint.y);
    CGPoint oldPoint = CGPointMake(self.view.bounds.size.width * self.view.layer.anchorPoint.x, self.view.bounds.size.height * self.view.layer.anchorPoint.y);
    
    newPoint = CGPointApplyAffineTransform(newPoint, self.view.transform);
    oldPoint = CGPointApplyAffineTransform(oldPoint, self.view.transform);
    
    CGPoint position = self.view.layer.position;
    
    position.x -= oldPoint.x;
    position.x += newPoint.x;
    
    position.y -= oldPoint.y;
    position.y += newPoint.y;
    
    self.view.layer.position = position;
    self.view.layer.anchorPoint = anchorPoint;
}

- (void) bringToFront {
    [self.view.superview bringSubviewToFront:self.view];
}
- (void) sendToBack {
    [self.view.superview sendSubviewToBack:self.view];
}

- (void) addCustomData:(id)source forKey:(NSString*)key {
    if (!source) return;
    [self.customData setObject:source forKey:key];
}

- (void) shake:(int)shakes withMaxShakes:(int)maxShakes andDistance:(float)distance andDuration:(float)duration {
    [UIView animateWithDuration:duration
                          delay:0//.005
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.view.transform = CGAffineTransformMakeTranslation(distance*(shakes == maxShakes ? 0 : 1 - 2*(shakes%2)), 0);
                     } completion:^(BOOL finished) {
                         if(shakes >= maxShakes)
                             self.view.transform = CGAffineTransformIdentity;
                         else
                             [self shake:shakes+1 withMaxShakes:maxShakes andDistance:distance andDuration:duration];
                     }];
}

- (void) setUserInteractionDisableCount:(int)value {
    _userInteractionDisableCount = value;
    self.view.userInteractionEnabled = (_userInteractionDisableCount == 0);
}

- (void) colorizeForLayer:(int)layer {
    int l = layer%8;
    float r = l < 4;//(arc4random()%255)/255.;
    float g = l % 4 == 0 || l%4 == 1;//(arc4random()%255)/255.;
    float b = l%2 == 0;// (arc4random()%255)/255.;
    float a = 1;
    //7 -> 000 black
    //6 -> 001 blue
    //5 -> 010 green
    //4 -> 011 cyan
    //3 -> 100 red
    //2 -> 101 purple
    //1 -> 110 yellow
    //0 -> 111 white
    
    // b -> 0245
    // g -> 0145
    // r -> 0123
    self.backgroundColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
}

- (void) setShadowWithRadius:(float)radius opacity:(float)opacity offset:(CGSize)offset color:(UIColor*)color {
    self.view.layer.shadowColor = color.CGColor;
    self.view.layer.shadowOffset = offset;
    self.view.layer.shadowOpacity = opacity;
    self.view.layer.shadowRadius = radius;
    self.view.layer.shouldRasterize = YES;
    self.view.layer.rasterizationScale = 2;
}
- (void) setShadowWithRadius:(float)radius opacity:(float)opacity offset:(CGSize)offset {
    [self setShadowWithRadius:radius opacity:opacity offset:offset color:[UIColor blackColor]];
}
- (void) setShadowWithRadius:(float)radius opacity:(float)opacity {
    [self setShadowWithRadius:radius opacity:opacity offset:CGSizeZero color:[UIColor blackColor]];
}



#pragma mark -
#pragma mark Background, contents, mask, snapshot


- (void) drawView {
    if ([self.view respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]) {
        [self.view drawViewHierarchyInRect:self.bounds afterScreenUpdates:YES];
    }
    else {
        [self.view.layer drawInContext:UIGraphicsGetCurrentContext()];
    }
}
- (UIImage*) viewImage {
    UIGraphicsBeginImageContext(self.bounds.size);
    [self drawView];
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (UIImage*) viewImageScaledTo:(float)scale {
    UIGraphicsBeginImageContext(CGSizeMake(self.widthBounds*scale, self.heightBounds*scale));
    [self.view drawViewHierarchyInRect:CGRectMake(0, 0, self.widthBounds*scale, self.heightBounds*scale) afterScreenUpdates:NO];
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIColor*) backgroundColor { return self.view.backgroundColor; }
- (void) setBackgroundColor:(UIColor *)backgroundColor { self.view.backgroundColor = (backgroundColor ? backgroundColor : [UIColor clearColor]); }

- (UIImage*) backgroundImage { return [UIImage imageWithCGImage:((CGImageRef)self.view.layer.contents)]; }
- (void) setBackgroundImage:(UIImage *)value {
    self.view.layer.contents = (id)value.CGImage;
}

- (UIImage*) makeContextImage:(void(^)(CGContextRef context, CGSize size))maskSetter {
    CGSize s = self.sizeBounds;
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [[UIColor blackColor] setFill];
    
    maskSetter(context,s);
    
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (void) setContentsWithCGContext:(void(^)(CGContextRef context, CGSize size))maskSetter {
    self.view.layer.contents = (id)[self makeContextImage:maskSetter].CGImage;
}
- (void) setMaskWithCGContext:(void(^)(CGContextRef context, CGSize size))maskSetter {
    [self setMaskFromImage:[self makeContextImage:maskSetter]];
}
- (void) setMaskFromImage:(UIImage*)image {
    CALayer* mask = [CALayer layer];
    mask.contents = (id)image.CGImage;
    mask.frame = self.bounds;
    self.view.layer.mask = mask;
}






#pragma mark -
#pragma mark Activity indication management

- (void) startActivityWithProcessing:(void(^)(void(^processingFinished)(void)))processing andFinished:(void(^)(void))finishedBlock {
    [self showActivityAnimated:YES andFinished:^{
        processing(^{
            [self hideActivityAnimated:YES andFinished:finishedBlock];
        });
    }];
}
- (void) hideActivityAnimated:(BOOL)animated andFinished:(void(^)(void))finishedBlock {
    self.activityIndicatorsCount--;
    if (self.activityIndicatorsCount != 0) { finishedBlock(); return; }
    
    UIView* activityBackground = self.activityIndicatorView.superview;
    [UIView animateWithDuration:(animated ? .3 : 0)
                     animations:^{ activityBackground.alpha = 0; }
                     completion:^(BOOL finished) {
                         [self.activityIndicatorView stopAnimating];
                         [self.activityIndicatorView removeFromSuperview];
                         self.activityIndicatorView = nil;
                         [activityBackground removeFromSuperview];
                         finishedBlock();
                     }
     ];
}

- (void) showActivityAnimated:(BOOL)animated withSize:(float)size andStyle:(UIActivityIndicatorViewStyle)style andDarkAlpha:(float)alpha andFinished:(void(^)(void))finishedBlock {
    [self showActivityAnimated:animated withSize:size andFrame:self.bounds andStyle:style andDarkAlpha:alpha andFinished:finishedBlock];
}
- (void) showActivityAnimated:(BOOL)animated andFinished:(void(^)(void))finishedBlock {
    [self showActivityAnimated:animated withSize:30 andStyle:UIActivityIndicatorViewStyleWhite andDarkAlpha:.65 andFinished:finishedBlock];
}
- (void) displayActivity:(BOOL)display animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock {
    if (display) [self showActivityAnimated:animated andFinished:finishedBlock];
    else [self hideActivityAnimated:animated andFinished:finishedBlock];
}

- (void) showActivityAnimated:(BOOL)animated withSize:(float)size andFrame:(CGRect)frame andStyle:(UIActivityIndicatorViewStyle)style andDarkAlpha:(float)alpha andFinished:(void(^)(void))finishedBlock {
    self.activityIndicatorsCount++;
    if (self.activityIndicatorsCount > 1) { finishedBlock(); return; }
    
    UIView* activityBackground = [[UIView alloc] init];
    activityBackground.frame = frame;
    activityBackground.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    activityBackground.backgroundColor = [UIColor colorWithWhite:0 alpha:alpha];
    activityBackground.alpha = 0;
    [self.view addSubview:activityBackground];
    [self.view bringSubviewToFront:activityBackground];
    
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    self.activityIndicatorView.bounds = CGRectMake(0, 0, size, size);
    self.activityIndicatorView.center = CGPointMake(frame.size.width/2, frame.size.height/2);
    self.activityIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [activityBackground addSubview:self.activityIndicatorView];
    [self.activityIndicatorView startAnimating];
    
    [UIView animateWithDuration:(animated ? .3 : 0)
                     animations:^{ activityBackground.alpha = 1; }
                     completion:^(BOOL finished) { finishedBlock(); }
     ];
}







#pragma mark -
#pragma mark Actions management

- (MPLAction*) actionForTouch {
    return [MPLAction actionWithBlock:^(MPLAction *action) { [self actionButtonTouched:nil event:nil]; }];
}
- (void) actionButtonTouched:(id)sender event:(UIEvent *)event {
    if (self.touchBlock != nil) self.touchBlock(self);
    else if (self.touchAction != nil) [self.touchAction performInControl:self];
}
- (void) assignTouchButton {
    self.action_button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.action_button.frame = self.bounds;
    self.action_button.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.action_button.exclusiveTouch = YES;
    [self.action_button addTarget:self action:@selector(actionButtonTouched:event:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.action_button];
    self.view.userInteractionEnabled = YES;
}
- (void) unassignTouchButton {
    [self.action_button removeFromSuperview];
    self.action_button = nil;
}
- (instancetype) assignTouchActionObject:(MPLAction*)action {
    self.touchAction = action;
    if (!self.action_button && action) [self assignTouchButton];
    if (self.action_button && !action) [self unassignTouchButton];
    return self;
}
- (instancetype) assignTouchAction:(void(^)(MPLView*))action {
    self.touchBlock = action;
    if (!self.action_button && action) [self assignTouchButton];
    if (self.action_button && !action) [self unassignTouchButton];
    return self;
}

- (void) addAction:(MPLAction *)action forKey:(NSString*)actionKey {
    if (!action) return;
    [self.actions setObject:action forKey:actionKey];
}
- (void) addAction:(MPLAction*)action forEventType:(UIControlEvents)type {
    [self addAction:action forKey:[NSString stringWithFormat:@"EventType_%d",(int)type]];
}
- (void) performAction:(NSString*)actionKey forView:(MPLView*)__view {
    MPLAction* action = (MPLAction*)[self.actions objectForKey:actionKey];
    if (action)
        [action performInControl:__view];
}
- (void) performAction:(NSString*)actionKey {
    [self performAction:actionKey forView:self];
}
- (void) performActionForControlEvent:(UIControlEvents)controlEvent {
    [self performAction:[NSString stringWithFormat:@"EventType_%d",(int)controlEvent]];
}
- (MPLAction*) actionForKey:(NSString*)key {
    return [self.actions objectForKey:key];
}
- (MPLAction*) actionForControlEvent:(UIControlEvents)controlEvent {
    return [self actionForKey:[NSString stringWithFormat:@"EventType_%d",(int)controlEvent]];
}

- (void) _addAction:(MPLAction *)action forEventType:(UIControlEvents)type {
    UIControl* control = (UIControl*)self.view;
    [control addTarget:self action:@selector(actionEditingChanged:) forControlEvents:UIControlEventEditingChanged];
}
- (void) addDefaultActionHandlers {
    if ([self.view isKindOfClass:[UIControl class]])
    {
        UIControl* control = (UIControl*)self.view;
        [control addTarget:self action:@selector(actionEditingChanged:) forControlEvents:UIControlEventEditingChanged];
        [control addTarget:self action:@selector(actionEditingDidBegin:) forControlEvents:UIControlEventEditingDidBegin];
        [control addTarget:self action:@selector(actionEditingDidEnd:) forControlEvents:UIControlEventEditingDidEnd];
        [control addTarget:self action:@selector(actionEditingDidEndOnExit:) forControlEvents:UIControlEventEditingDidEndOnExit];
        [control addTarget:self action:@selector(actionTouchCancel:) forControlEvents:UIControlEventTouchCancel];
        [control addTarget:self action:@selector(actionTouchDown:) forControlEvents:UIControlEventTouchDown];
        [control addTarget:self action:@selector(actionTouchDownRepeat:) forControlEvents:UIControlEventTouchDownRepeat];
        [control addTarget:self action:@selector(actionTouchDragEnter:) forControlEvents:UIControlEventTouchDragEnter];
        [control addTarget:self action:@selector(actionTouchDragExit:) forControlEvents:UIControlEventTouchDragExit];
        [control addTarget:self action:@selector(actionTouchDragInside:) forControlEvents:UIControlEventTouchDragInside];
        [control addTarget:self action:@selector(actionTouchDragOutside:) forControlEvents:UIControlEventTouchDragOutside];
        [control addTarget:self action:@selector(actionTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [control addTarget:self action:@selector(actionTouchUpOutside:) forControlEvents:UIControlEventTouchUpOutside];
        [control addTarget:self action:@selector(actionValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
}
- (void) actionEditingChanged:(id)sender { [self performActionForControlEvent:UIControlEventEditingChanged]; }
- (void) actionEditingDidBegin:(id)sender { [self performActionForControlEvent:UIControlEventEditingDidBegin]; }
- (void) actionEditingDidEnd:(id)sender { [self performActionForControlEvent:UIControlEventEditingDidEnd]; }
- (void) actionEditingDidEndOnExit:(id)sender { [self performActionForControlEvent:UIControlEventEditingDidEndOnExit]; }
- (void) actionTouchCancel:(id)sender { [self performActionForControlEvent:UIControlEventTouchCancel]; }
- (void) actionTouchDown:(id)sender { [self performActionForControlEvent:UIControlEventTouchDown]; }
- (void) actionTouchDownRepeat:(id)sender { [self performActionForControlEvent:UIControlEventTouchDownRepeat]; }
- (void) actionTouchDragEnter:(id)sender { [self performActionForControlEvent:UIControlEventTouchDragEnter]; }
- (void) actionTouchDragExit:(id)sender { [self performActionForControlEvent:UIControlEventTouchDragExit]; }
- (void) actionTouchDragInside:(id)sender { [self performActionForControlEvent:UIControlEventTouchDragInside]; }
- (void) actionTouchDragOutside:(id)sender { [self performActionForControlEvent:UIControlEventTouchDragOutside]; }
- (void) actionTouchUpInside:(id)sender { [self performActionForControlEvent:UIControlEventTouchUpInside]; }
- (void) actionTouchUpOutside:(id)sender { [self performActionForControlEvent:UIControlEventTouchUpOutside]; }
- (void) actionValueChanged:(id)sender { [self performActionForControlEvent:UIControlEventValueChanged]; }











#pragma mark -
#pragma mark Theme management

@synthesize themeAttatchments = _themeAttatchments;
@synthesize themeAttatchmentsKeyMappings = _themeAttatchmentsKeyMappings;

- (MPLThemeViewAttatchment*) themeAttatchmentForKey:(NSString*)themeAttatchementKey {
    NSString* noteName = self.themeAttatchmentsKeyMappings[themeAttatchementKey];
    return (!noteName ? nil : self.themeAttatchments[noteName][themeAttatchementKey]);
}
- (void) removeThemeAttatchmentFor:(NSString*)themeAttatchementKey {
    MPLThemeViewAttatchment* themeAttatchment = [self themeAttatchmentForKey:themeAttatchementKey];
    if (themeAttatchment != nil) {
        //        [[NSNotificationCenter defaultCenter] removeObserver:self name:themeAttatchment.noteName object:themeAttatchementKey];
        [self.themeAttatchments[themeAttatchment.noteName] removeObjectForKey:themeAttatchementKey];
        [self.themeAttatchmentsKeyMappings removeObjectForKey:themeAttatchementKey];
    }
}
- (void) addThemeAttatchment:(MPLThemeViewAttatchment*)themeAttatchment forKey:(NSString*)themeAttatchementKey {
    NSString* noteName = themeAttatchment.noteName;
    self.themeAttatchmentsKeyMappings[themeAttatchementKey] = noteName;
    
    if (!self.themeAttatchments[noteName]) self.themeAttatchments[noteName] = [NSMutableDictionary dictionary];
    self.themeAttatchments[noteName][themeAttatchementKey] = themeAttatchment;
    
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeChanged:) name:noteName object:nil];
}
- (instancetype) attatch:(NSString*)themeAttatchementKey toTheme:(MPLTheme*)theme forKey:(NSString*)key withParameters:(NSDictionary*)parameters andAction:(void (^)(MPLThemeViewAttatchment* themeAttatchment))actionBlock {
    [self removeThemeAttatchmentFor:themeAttatchementKey];
    
    MPLThemeViewAttatchment* themeAttatchment = [theme attachmentToKey:key andParameters:parameters andActionBlock:actionBlock];
    [themeAttatchment performAction];
    
    [self addThemeAttatchment:themeAttatchment forKey:themeAttatchementKey];
    
    return self;
}
- (instancetype) attatch:(NSString*)themeAttatchementKey toTheme:(MPLTheme*)theme forKey:(NSString*)key andAction:(void (^)(MPLThemeViewAttatchment* themeAttatchment))actionBlock {
    return [self attatch:themeAttatchementKey toTheme:theme forKey:key withParameters:@{} andAction:actionBlock];
}
//- (void) themeChanged:(NSNotification*)note {
//    for (MPLThemeViewAttatchment* tva in [self.themeAttatchments[note.name] allValues])
//        [tva performAction];
//}
static NSString* _mpl_attatchmentBackgroundColorAlphaParameter = @"alpha";
static NSString* _mpl_attatchmentBackgroundColorDurationParameter = @"duration";
- (instancetype) attatchBackgroundColorToTheme:(MPLTheme*)theme forKey:(NSString*)key withAlpha:(float)alpha withDuration:(float)duration {
    NSDictionary* params = @{
                             _mpl_attatchmentBackgroundColorAlphaParameter:@(alpha),
                             _mpl_attatchmentBackgroundColorDurationParameter:@(duration),
                             };
    return [self attatch:@"backgroundColor" toTheme:theme forKey:key withParameters:params andAction:^(MPLThemeViewAttatchment* themeAttatchment) {
        [UIView animateWithDuration:[themeAttatchment.parameters[_mpl_attatchmentBackgroundColorDurationParameter] floatValue]
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionAllowAnimatedContent
                         animations:^{
                             self.backgroundColor = [themeAttatchment.colorValue colorWithAlphaComponent:[themeAttatchment.parameters[_mpl_attatchmentBackgroundColorAlphaParameter] floatValue]];
                         }
                         completion:^(BOOL finished) {
                         }
         ];
    }];
}
- (instancetype) attatchBackgroundColorToTheme:(MPLTheme*)theme forKey:(NSString*)key {
    return [self attatch:@"backgroundColor" toTheme:theme forKey:key andAction:^(MPLThemeViewAttatchment* themeAttatchment) {
        self.backgroundColor = [themeAttatchment.theme colorForKey:themeAttatchment.key];
    }];
}
























#pragma mark -
#pragma mark Gestures management

@synthesize PanActive = _PanActive;
@synthesize PanAllowScroll = _PanAllowScroll;
@synthesize PanLocationNow = _PanLocationNow;
@synthesize PanLocationLast = _PanLocationLast;
@synthesize PanLocationOnStart = _PanLocationOnStart;
@synthesize PanStart = _PanStart;
@synthesize PanLast = _PanLast;
@synthesize PanNow = _PanNow;
@synthesize PanHRight = _PanHRight;
@synthesize PanVDown = _PanVDown;

@synthesize PanGesture = _PanGesture;
#if MPLCanUseEdgePan
@synthesize LeftEdgePanGesture = _LeftEdgePanGesture;
@synthesize RightEdgePanGesture = _RightEdgePanGesture;
#else
#endif
@synthesize TapGesture = _TapGesture;
@synthesize DoubleTapGesture = _DoubleTapGesture;
@synthesize LongPressGesture = _LongPressGesture;

@synthesize PanInvocations = _PanInvocations;
@synthesize LeftEdgePanInvocations = _LeftEdgePanInvocations;
@synthesize RightEdgePanInvocations = _RightEdgePanInvocations;
@synthesize TapInvocations = _TapInvocations;
@synthesize DoubleTapInvocations = _DoubleTapInvocations;
@synthesize LongPressInvocations = _LongPressInvocations;

@dynamic PanHorizontal;
@dynamic PanVertical;
@dynamic PanLeft;
@dynamic PanRight;
@dynamic PanUp;
@dynamic PanDown;
@dynamic PanDistanceToLastPan;
@dynamic PanDistanceToStart;
@dynamic PanDistanceToStartPrevious;

@synthesize PanStarted = _PanStarted;
@synthesize PanStartHorizontal = _PanStartHorizontal;
@synthesize PanStartVertical = _PanStartVertical;
@synthesize PanStartUp = _PanStartUp;
@synthesize PanStartDown = _PanStartDown;
@synthesize PanStartLeft = _PanStartLeft;
@synthesize PanStartRight = _PanStartRight;


- (CGSize) panDistanceFrom:(CGPoint)p1 to:(CGPoint)p2 {
    return CGSizeMake(p1.x - p2.x, p1.y - p2.y);
}
- (CGSize) PanDistanceToStart {
    return [self panDistanceFrom:self.PanLocationNow to:self.PanLocationOnStart];
}
- (CGSize) PanDistanceToStartPrevious {
    return [self panDistanceFrom:self.PanLocationLast to:self.PanLocationOnStart];
}
- (CGSize) PanDistanceToLastPan {
    return [self panDistanceFrom:self.PanLocationNow to:self.PanLocationLast];
}
- (BOOL) PanHorizontal {
    return (ABS(self.PanDistanceToStart.width) > ABS(self.PanDistanceToStart.height));
}
- (BOOL) PanVertical {
    return (ABS(self.PanDistanceToStart.width) < ABS(self.PanDistanceToStart.height));
}
- (BOOL) PanLeft {
    return (self.PanHorizontal && self.PanDistanceToStart.width < 0);
}
- (BOOL) PanRight {
    return (self.PanHorizontal && self.PanDistanceToStart.width > 0);
}
- (BOOL) PanUp {
    return (self.PanVertical && self.PanDistanceToStart.height < 0);
}
- (BOOL) PanDown {
    return (self.PanVertical && self.PanDistanceToStart.height > 0);
}
- (float) PanVelocityHorizontal {
    return -self.PanDistanceToLastPan.width/[self.lastPanDate timeIntervalSinceNow];
}
//- (float) PanVelocityVertical {
//    return -self.PanDistanceToLastPan.height/[self.lastPanDate timeIntervalSinceNow];
//}
#if MPLCanUseEdgePan
- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        if ([otherGestureRecognizer.view isKindOfClass:[UIScrollView class]] || [otherGestureRecognizer.view isKindOfClass:[UITableView class]]) {
            return self.PanAllowScroll;
        }
    }
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && ![gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        if ([otherGestureRecognizer.view isKindOfClass:[UIScrollView class]]) {
            return self.PanAllowScroll;
        }
    }
    if ([gestureRecognizer.view isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer.view isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return !NO;
    }
    if ([gestureRecognizer.view isKindOfClass:[UIScreenEdgePanGestureRecognizer class]] && [otherGestureRecognizer.view isKindOfClass:[UIPanGestureRecognizer class]]) {
        return !YES;
    }
    return NO;
}
#else
- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        if ([otherGestureRecognizer.view isKindOfClass:[UIScrollView class]] || [otherGestureRecognizer.view isKindOfClass:[UITableView class]]) {
            return self.PanAllowScroll;
        }
    }
    return NO;
}
#endif

- (void) pan:(UIGestureRecognizer*)panGesture withActions:(NSMutableDictionary*)panactions {
    if (panGesture.numberOfTouches > 1)
        return;
    
    self.lastPanDate = self.currentPanDate;
    self.currentPanDate = [NSDate date];
    CGPoint p = [panGesture locationInView:self.view];
    CGPoint l = [self.view convertPoint:p toView:nil];
    
    self.PanLast = self.PanNow;
    self.PanNow = p;
    
    self.PanLocationLast = self.PanLocationNow;
    self.PanLocationNow = l;
    
    if (panGesture.state == UIGestureRecognizerStateBegan)
    {
        self.lastPanDate = self.currentPanDate;
        self.PanActive = YES;
        
        self.PanStart = p;
        self.PanLast = p;
        
        self.PanLocationOnStart = l;
        self.PanLocationLast = l;
    }
    
    if (self.PanLocationNow.x > self.PanLocationLast.x) self.PanHRight = YES;
    if (self.PanLocationNow.x < self.PanLocationLast.x) self.PanHRight = NO;
    if (self.PanLocationNow.y > self.PanLocationLast.y) self.PanVDown = YES;
    if (self.PanLocationNow.y < self.PanLocationLast.y) self.PanVDown = NO;
    
    if (!CGSizeEqualToSize(self.PanDistanceToStart, CGSizeZero) && !self->_PanStarted) {
        self->_PanStarted = YES;
        self->_PanStartHorizontal = self.PanHorizontal;
        self->_PanStartVertical = !self.PanHorizontal;
        self->_PanStartUp = (self.PanStartVertical && !self.PanVDown);
        self->_PanStartDown = (self.PanStartVertical && self.PanVDown);
        self->_PanStartLeft = (self.PanHorizontal && !self.PanHRight);
        self->_PanStartRight = (self.PanHorizontal && self.PanHRight);
    }
    
    for (MPLAction* pi in panactions.allValues)
        if (pi) [pi performWithParameters:[NSMutableDictionary dictionaryWithObject:panGesture forKey:@"Gesture"] forControl:self];
    
    if (panGesture.state == UIGestureRecognizerStateEnded || panGesture.state == UIGestureRecognizerStateFailed || panGesture.state == UIGestureRecognizerStateCancelled)
        self.PanActive = YES;
    
    if (self.PanGesture.state == UIGestureRecognizerStateEnded) {
        self->_PanStarted = NO;
    }
}
- (void) panned:(UIPanGestureRecognizer*)panGesture {
    [self pan:panGesture withActions:self.PanInvocations];
}
#if MPLCanUseEdgePan
- (void) leftEdgePanned:(UIScreenEdgePanGestureRecognizer*)panGesture {
    [self pan:panGesture withActions:self.LeftEdgePanInvocations];
}
- (void) rightEdgePanned:(UIScreenEdgePanGestureRecognizer*)panGesture {
    [self pan:panGesture withActions:self.RightEdgePanInvocations];
}
#else
#endif
- (void) tapped:(UITapGestureRecognizer*)tapGesture {
    for (MPLAction* ti in self.TapInvocations.allValues)
        if (ti) [ti performInControl:self];
}
- (void) doubleTapped:(UITapGestureRecognizer*)tapGesture {
    for (MPLAction* ti in self.DoubleTapInvocations.allValues)
        if (ti) [ti performInControl:self];
}
- (void) longPressed:(UILongPressGestureRecognizer*)longPressGesture {
    for (MPLAction* li in self.LongPressInvocations.allValues)
        if (li) [li performInControl:self];
}

- (void) disablePanForKey:(NSString*)key {
    [self.PanInvocations removeObjectForKey:key];
    if (self.PanInvocations.count == 0)
    {
        [self.view removeGestureRecognizer:self.PanGesture];
        self.PanGesture = nil;
    }
}
#if MPLCanUseEdgePan
- (void) disableLeftEdgePanForKey:(NSString *)key {
    [self.LeftEdgePanInvocations removeObjectForKey:key];
    if (self.LeftEdgePanInvocations.count == 0) {
        [self.view removeGestureRecognizer:self.LeftEdgePanGesture];
        self.LeftEdgePanGesture = nil;
    }
}
- (void) disableRightEdgePanForKey:(NSString *)key {
    [self.RightEdgePanInvocations removeObjectForKey:key];
    if (self.RightEdgePanInvocations.count == 0) {
        [self.view removeGestureRecognizer:self.RightEdgePanGesture];
        self.RightEdgePanGesture = nil;
    }
}
#else
#endif
- (void) disableTapForKey:(NSString*)key {
    [self.TapInvocations removeObjectForKey:key];
    if (self.TapInvocations.count == 0)
    {
        [self.view removeGestureRecognizer:self.TapGesture];
        self.TapGesture = nil;
    }
}
- (void) disableDoubleTapForKey:(NSString*)key {
    [self.DoubleTapInvocations removeObjectForKey:key];
    if (self.DoubleTapInvocations.count == 0)
    {
        [self.view removeGestureRecognizer:self.DoubleTapGesture];
        self.DoubleTapGesture = nil;
    }
}
- (void) disableLongPressForKey:(NSString*)key {
    [self.LongPressInvocations removeObjectForKey:key];
    if (self.LongPressInvocations.count == 0)
    {
        [self.view removeGestureRecognizer:self.LongPressGesture];
        self.LongPressGesture = nil;
    }
}

- (void) enablePanWithAction:(MPLAction*)invocation forKey:(NSString*)key {
    [self.PanInvocations setObject:invocation forKey:key];
    if (self.view != nil && self.PanGesture == nil)
    {
        self.PanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panned:)];
        self.PanGesture.delegate = self;
        [self.view addGestureRecognizer:self.PanGesture];
    }
}
#if MPLCanUseEdgePan
- (void) enableLeftEdgePanWithAction:(MPLAction *)action forKey:(NSString *)key {
    [self.LeftEdgePanInvocations setObject:action forKey:key];
    if (self.view != nil && self.LeftEdgePanGesture == nil)    {
        self.LeftEdgePanGesture = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(leftEdgePanned:)];
        self.LeftEdgePanGesture.edges = UIRectEdgeLeft;
        self.LeftEdgePanGesture.delegate = self;
        [self.view addGestureRecognizer:self.LeftEdgePanGesture];
    }
}
- (void) enableRightEdgePanWithAction:(MPLAction *)action forKey:(NSString *)key {
    [self.RightEdgePanInvocations setObject:action forKey:key];
    if (self.view != nil && self.RightEdgePanGesture == nil)    {
        self.RightEdgePanGesture = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(rightEdgePanned:)];
        self.RightEdgePanGesture.edges = UIRectEdgeRight;
        self.RightEdgePanGesture.delegate = self;
        [self.view addGestureRecognizer:self.RightEdgePanGesture];
    }
}
#else
#endif
- (void) enableTapWithAction:(MPLAction*)invocation forKey:(NSString*)key {
    [self.TapInvocations setObject:invocation forKey:key];
    if (self.view != nil && self.TapGesture == nil)
    {
        self.TapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        self.TapGesture.numberOfTapsRequired = 1;
        [self.view addGestureRecognizer:self.TapGesture];
    }
}
- (void) enableDoubleTapWithAction:(MPLAction*)invocation forKey:(NSString*)key {
    [self.DoubleTapInvocations setObject:invocation forKey:key];
    if (self.view != nil && self.DoubleTapGesture == nil)
    {
        self.DoubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapped:)];
        self.DoubleTapGesture.numberOfTapsRequired = 2;
        [self.view addGestureRecognizer:self.DoubleTapGesture];
    }
}
- (void) enableLongPressWithAction:(MPLAction*)invocation forKey:(NSString*)key {
    [self.LongPressInvocations setObject:invocation forKey:key];
    if (self.view != nil && self.LongPressGesture == nil)
    {
        self.LongPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
        self.LongPressGesture.minimumPressDuration = .5;
        [self.view addGestureRecognizer:self.LongPressGesture];
    }
}

@end
