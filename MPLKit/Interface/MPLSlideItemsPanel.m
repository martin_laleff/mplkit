//
//  MPLSlideItemsPanel.m
//  MPLKit
//
//  Created by Martin Lalev on 11/20/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLSlideItemsPanel.h"

@implementation MPLSlideItemsPanel

@synthesize ItemCount;
@synthesize PositionChanged;
@synthesize SlidePosition;
@synthesize CurrentSlidePosition;
@synthesize IsHorizontal;
@synthesize SingleSlideDistance;
@synthesize Inverted;

- (void) loaderWithPosition:(CGFloat)position
     andSingleSlideDistance:(CGFloat)singleSlideDistance
            andIsHorizontal:(BOOL)isHorizontal
               andItemCount:(NSInteger)itemCount
              andInvocation:(MPLAction*)invocation
{
    self.PositionChanged = invocation;
    self.SingleSlideDistance = singleSlideDistance;
    self.SlidePosition = position;
    self.IsHorizontal = isHorizontal;
    self.ItemCount = itemCount;
    self.Inverted = YES;
    self.CurrentSlidePosition = position;
    
    [self enablePanWithAction:[MPLAction actionWithTarget:self andSelector:@selector(slidePanned:)] forKey:@"SlidePanel"];
    
    [self positionChanged:YES withActualDistance:0 fromLastPosition:self.SlidePosition codeInvoked:NO];
}

- (NSInteger) controlPositionForIndex:(NSInteger)index {
    return (index+self.CurrentSlidePosition) % self.ItemCount;
}
- (NSInteger) controlIndexAtPosition:(NSInteger)position {
    return (position + self.ItemCount - self.CurrentSlidePosition%self.ItemCount) % self.ItemCount;
}

- (void) slideTo:(NSInteger)position {
    float lastPosition = self.SlidePosition;
    self.SlidePosition = position;
    self.CurrentSlidePosition = position;
    if (self.SlidePosition < 0)
        self.SlidePosition = self.ItemCount + self.SlidePosition;
    if (self.SlidePosition >= self.ItemCount)
        self.SlidePosition -= self.ItemCount;
    
    NSInteger distance = ABS(position - lastPosition);
    
    [self positionChanged:NO withActualDistance:distance fromLastPosition:lastPosition codeInvoked:YES];
    [self positionChanged:YES withActualDistance:distance fromLastPosition:lastPosition codeInvoked:YES];
}
- (void) positionChanged:(BOOL)ended withActualDistance:(float)actualDistance fromLastPosition:(NSInteger)lastPosition codeInvoked:(BOOL)codeInvoked {
    CGFloat p = self.SlidePosition;
    
    for (NSInteger controlIndex = 0; controlIndex < self.ItemCount; controlIndex++)
    {
        if (p >= self.ItemCount)
            p -= self.ItemCount;
        
        NSInteger position = (NSInteger)p;
        CGFloat slideDistance = p - position;
        
        [self.PositionChanged performWithParameters:[MPLAction parametersForObjects:@(controlIndex), @(position), @(slideDistance), @(ended), @(lastPosition), @(actualDistance), @(codeInvoked), nil] forControl:self];
        
        p++;
    }
}

- (void) slidePanned:(MPLAction*)invocation {
    if (self.IsHorizontal != invocation.Control.PanStartHorizontal) return;
    NSInteger lastPosition = self.CurrentSlidePosition;
    BOOL ended = NO;
    float slideDistance = self.SlidePosition;
    self.SlideVelocity = invocation.Control.PanVelocityHorizontal;
    if (invocation.Control.PanGesture.state == UIGestureRecognizerStateEnded || invocation.Control.PanHorizontal != self.IsHorizontal)
    {
        NSInteger full = ((self.SlidePosition - (NSInteger)self.SlidePosition) == 0);
        ended = (invocation.Control.PanHorizontal == self.IsHorizontal);
        if (ended) self.SlidePosition = (NSInteger)self.SlidePosition + (!(self.IsHorizontal ? invocation.Control.PanHRight == !self.Inverted : invocation.Control.PanVDown == !self.Inverted) || full ? 0 : 1);
        else self.SlidePosition = self.CurrentSlidePosition;
        self.CurrentSlidePosition = self.SlidePosition;
    }
    else
    {
        CGFloat distance = ((self.IsHorizontal ? invocation.Control.PanDistanceToStart.width : invocation.Control.PanDistanceToStart.height) / self.SingleSlideDistance);
        if (distance > +1) distance = +1;
        if (distance < -1) distance = -1;
        
        self.SlidePosition = self.CurrentSlidePosition + (self.Inverted ? -1 : +1) * distance;
    }
    
    if (self.SlidePosition < 0)
        self.SlidePosition = self.ItemCount + self.SlidePosition;
    if (self.SlidePosition >= self.ItemCount)
        self.SlidePosition -= self.ItemCount;
    
    [self positionChanged:ended withActualDistance:slideDistance fromLastPosition:lastPosition codeInvoked:NO];
}

@end



@implementation MPLHorizontalSliderView

- (void) loaderWithItemsCount:(int)itemsCount andViewGenerator:(void (^)(int index, void(^loader)(MPLView* v)))viewGenerator andReloader:(void(^)(MPLView* v, NSInteger incrementBy))reloader andDidSlide:(void(^)(void))didSlide {
    self.viewReloader = reloader;
    self.didSlide = didSlide;
    
    self.views = [NSMutableArray array];
    for (int i = 0; i < itemsCount; i++) {
        viewGenerator(i, ^(MPLView* v){
            [v addTo:self];
            [v fillWidth];
            [v fillHeight];
            [self.views addObject:v];
            v.customData[@"index"] = @(i);
        });
    }
    
    [super loaderWithPosition:0
       andSingleSlideDistance:self.widthBounds
              andIsHorizontal:YES
                 andItemCount:self.views.count
                andInvocation:[MPLAction actionWithBlock:^(MPLAction *action) { [self didSlideViewAtIndex:[[action parameterAtIndex:0] integerValue] position:[[action parameterAtIndex:1]integerValue] slideDistance:[[action parameterAtIndex:2] floatValue] ended:[[action parameterAtIndex:3] boolValue] lastPosition:[[action parameterAtIndex:4] integerValue] actualDistance:[[action parameterAtIndex:5] floatValue] codeInvoked:[[action parameterAtIndex:6] boolValue]]; }]
     ];
    
    self.viewReloader(self.views[0],+1);
    self.viewReloader(self.views[2],-1);
}

- (void) didSlideViewAtIndex:(NSInteger)index position:(NSInteger)position slideDistance:(CGFloat)slideDistance ended:(BOOL)ended lastPosition:(NSInteger)lastPosition actualDistance:(float)actualDistance codeInvoked:(BOOL)codeInvoked {
    
    MPLPanel* p = self.views[index];
    float x = self.widthBounds-self.widthBounds*position;
    x-=slideDistance*self.widthBounds;
    int LP = [p.customData[@"index"] intValue];
    
    BOOL mostLeftToMostRight = (LP == self.views.count-1 && position == 0);
    BOOL mostRightToMostLeft = (LP == 0 && position == self.views.count-1);
    BOOL animate = ended && !mostLeftToMostRight && !mostRightToMostLeft;
    
    if (p.xOffset < 0 && x >= self.widthBounds) animate = false;
    if (p.xOffset > self.widthBounds && x < 0) animate = false;
    
    [UIView animateWithDuration:animate?.1+.2*ABS(x-p.xOffset)/self.widthBounds:0 animations:^{
        p.xOffset = x;
    } completion:^(BOOL finished) {
        if (ended) {
            p.customData[@"index"] = @(position);
            if (mostLeftToMostRight) self.viewReloader(p,+self.views.count);
            if (mostRightToMostLeft) self.viewReloader(p,-self.views.count);
            if (mostLeftToMostRight || mostRightToMostLeft) self.didSlide();
        }
    }];
    if (self.onDidSlide != nil)
        self.onDidSlide(index,position,slideDistance,ended,lastPosition,actualDistance,codeInvoked);
}

@end
