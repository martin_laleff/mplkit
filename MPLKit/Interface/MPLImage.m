//
//  FTImage.m
//  FTrack_Take2
//
//  Created by Administrator on 05/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MPLImage.h"
#import "MPLURLConnection.h"



@implementation MPLImage



@synthesize brokenImage;
@synthesize activity;

- (UIImage*) image { return self.imageView.image; }
- (void) setImage:(UIImage *)image { self.imageView.image = (image == nil ? self.brokenImage : image); }

- (UIImage*) imageResized { return self.imageView.image; }

- (void) assignImageResizedFitTo:(UIImage*)image finished:(void(^)(void))finished {
    self.imageToBeResized = image;
    [MPLAction performInBackgroundWithReturn:^id{
        return [image fitImageIn:CGSizeMake(self.widthBounds*2, self.heightBounds*2)];
    } finished:^(id result) {
        if (self.imageToBeResized == image)
            self.image = (UIImage*)result;
        finished();
    }];
}
- (void) assignImageResizedFillTo:(UIImage*)image finished:(void(^)(void))finished {
    self.imageToBeResized = image;
    [MPLAction performInBackgroundWithReturn:^id{
        return [image fillImageIn:CGSizeMake(self.widthBounds*2, self.heightBounds*2)];
    } finished:^(id result) {
        if (self.imageToBeResized == image)
            self.image = (UIImage*)result;
        finished();
    }];
}
- (void) fadeInImageWithDuration:(float)duration processer:(void(^)(void(^ready)(void)))processer finished:(void(^)(void))finished didFadeIn:(void(^)(void))didFadeIn {
    [self fadeInImageWithDuration:duration toAlpha:1 processer:processer finished:finished didFadeIn:didFadeIn];
}
- (void) fadeInImageWithDuration:(float)duration toAlpha:(float)toAlpha processer:(void(^)(void(^ready)(void)))processer finished:(void(^)(void))finished didFadeIn:(void(^)(void))didFadeIn {
    self.alpha = 0;
    processer(^{
        finished();
        [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            self.alpha = toAlpha;
        } completion:^(BOOL finished) {didFadeIn();}];
    });
}
- (void) setImageResized:(UIImage *)image {
//    [self fadeInImageWithDuration:.2 processer:^(void(^ready)(void)) {
    [self assignImageResizedFillTo:image finished:^{}];
//    } finished:^{} didFadeIn:^{}];
}

- (UIViewContentMode) contentMode { return self.imageView.contentMode; }
- (void) setContentMode:(UIViewContentMode)contentMode { self.imageView.contentMode = contentMode; }



- (UIImageView*) imageView { return (UIImageView *)self.view; }
- (UIView*) createView { return [[UIImageView alloc] init]; }

+ (MPLImage*) create:(void(^)(MPLImage* v))creater {
    MPLImage* v = [[MPLImage alloc] init];
    creater(v);
    return v;
}


- (void) loadWebImageFromLocation:(NSString*)imageLocation usingLocalCertificates:(NSArray*)localCertificates andCustomCA:(BOOL)customCA andSelfSignedCert:(BOOL)selfSignedCert withFinished:(void(^)(void))finished {
    if (imageLocation == nil) {
        finished();
        return;
    }
    self.image = nil;
    self.requestedImage = imageLocation;
    
    if ([MPLImage isCachedLocation:imageLocation]) {
        self.image = [MPLImage imageForCachedLocation:imageLocation];
        finished();
    }
    else {
        self.activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.activity.bounds = CGRectMake(0, 0, 20, 20);
        self.activity.center = self.imageView.center;
        self.activity.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self.imageView addSubview:self.activity];
        [self.activity startAnimating];
        
        MPLURLConnection* connection = [[MPLURLConnection alloc] initWithURL:[NSURL URLWithString:imageLocation]
                                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                             timeoutInterval:60
                                        ];
        if (localCertificates != nil)
            [connection setSSLAuthWithCertificates:localCertificates andCustomCA:customCA andSelfSignedCert:selfSignedCert];
        [connection startWithFinishedDataBlock:^(NSURLResponse *response, NSData *data, NSError *error) {
            if (error || data == nil) {
                NSLog(@"%@",error);
            }
            if (self.requestedImage != imageLocation) { return ; }
            UIImage* img = [UIImage imageWithData:[NSData dataWithData:data]];
//            if (img) [MPLImage cacheImage:img forLocation:imageLocation];
            [self fadeInImageWithDuration:.2 processer:^(void(^ready)(void)) {
                [self assignImageResizedFitTo:img finished:ready];
            } finished:^{
                [self.activity stopAnimating];
                [self.activity removeFromSuperview];
                self.activity = nil;
                
                finished();
            } didFadeIn:^{}];
//            self.image = img;
            
        }];
    }
}



static NSMutableDictionary* cacheStorage;
+ (NSMutableDictionary*) imageCache { return (!cacheStorage ? cacheStorage = [[NSMutableDictionary alloc] init] : cacheStorage); }
+ (BOOL) isCachedLocation:(NSString*)location { return [[MPLImage imageCache] objectForKey:location] != nil; }
+ (void) cacheImage:(UIImage*)image forLocation:(NSString*)location { [[MPLImage imageCache] setObject:image forKey:location]; }
+ (UIImage*) imageForCachedLocation:(NSString*)location { return (location.length == 0 ? nil : [[MPLImage imageCache] objectForKey:location]); }
+ (void) removeCacheLocation:(NSString*)location { [[MPLImage imageCache] removeObjectForKey:location]; }

@end
