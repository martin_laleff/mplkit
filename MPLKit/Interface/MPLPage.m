//
//  FTPage.m
//  FTrack_Take2
//
//  Created by Administrator on 21/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MPLPage.h"


@implementation MPLPage

- (UIPageControl*) pageControlView { return (UIPageControl*)self.view; }
- (UIView*) createView { return [[UIPageControl alloc] init]; }

- (NSInteger) numberOfPages { return self.pageControlView.numberOfPages; }
- (NSInteger) currentPage { return self.pageControlView.currentPage; }
- (BOOL) hidesForSinglePage { return self.pageControlView.hidesForSinglePage; }

- (void) setNumberOfPages:(NSInteger)numberOfPages { self.pageControlView.numberOfPages = numberOfPages; }
- (void) setCurrentPage:(NSInteger)currentPage  { self.pageControlView.currentPage = currentPage; }
- (void) setHidesForSinglePage:(BOOL)hidesForSinglePage { self.pageControlView.hidesForSinglePage = hidesForSinglePage; }

- (instancetype) assignValueChangeActionObject:(MPLAction *)action {
    [self addAction:action forEventType:UIControlEventValueChanged];
    return self;
}
- (instancetype) assignValueChangeAction:(void (^)(MPLPage *))action {
    return [self assignValueChangeActionObject:[MPLAction actionWithBlock:^(MPLAction *a) {
        action(self);
    }]];
    return self;
}

- (void) loaderWithCurrentPage:(NSInteger)currentPage
		 andNumberOfPages:(NSInteger)numberOfPages
	andHidesForSinglePage:(BOOL)hidesForSinglePage
	andValueChangedAction:(MPLAction*)valueChangedAction
{
	self.pageControlView.numberOfPages = numberOfPages;
	self.pageControlView.currentPage = currentPage;
	self.pageControlView.hidesForSinglePage = hidesForSinglePage;
	[self addAction:valueChangedAction forEventType:UIControlEventValueChanged];
}
+ (MPLPage*) create:(void(^)(MPLPage* v))creater {
    MPLPage* v = [[MPLPage alloc] init];
    creater(v);
    return v;
}

@end
