//
//  FTApplication.m
//  FTrack_Take2
//
//  Created by Martin Lalev on 9/6/11.
//  Copyright 2011 mART. All rights reserved.
//

#import "MPLApplication.h"
#import "MPLAlert.h"


@implementation MPLApplication

@synthesize started;

@synthesize window;
@dynamic rootController; - (MPLViewController*) rootController { return (MPLViewController*)self.window.rootViewController; }
@dynamic baseView; - (MPLView*) baseView { return self.rootController.baseView; }
@synthesize pushNotificationManager;

@synthesize OnTermination;

@synthesize launchedWithLocalNotification;
@synthesize launchedWithRemoteNotification;

@synthesize splashImageView;

@synthesize upgradedFrom;

#pragma mark -
#pragma mark Common methods

+ (instancetype) SharedApplication {
    return (MPLApplication*)[MPLApplication sharedApplication];
}
- (float) statusBarHeight { return MIN([UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height); }

#pragma mark -
#pragma mark Versioning and start count

- (NetworkStatus) reachabilityStatus {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    return [reachability currentReachabilityStatus];
}

- (NSArray*) allVersions { return @[@"1.0"]; }
- (NSString*) currentVersion { return [[self allVersions] lastObject]; }

- (NSString*) currentlyInstalledVersion { return [[NSUserDefaults standardUserDefaults] objectForKey:[self currentlyInstalledVersionKey]]; }
- (void) setCurrentlyInstalledVersion:(NSString*)value { [[NSUserDefaults standardUserDefaults] setObject:value forKey:[self currentlyInstalledVersionKey]]; }

- (NSString*) currentlyInstalledVersionKey { return @"current_version"; }
- (NSString*) currentInstallationStartCountKey { return @"start_count"; }
- (NSString*) totalStartCountKey { return @"start_count"; }
- (NSString*) startCountKeyForVersion:(NSString *)version { return [NSString stringWithFormat:@"start_count_%@", version]; }

- (NSInteger) currentInstallationStartCount { return [[[NSUserDefaults standardUserDefaults] objectForKey:[self currentInstallationStartCountKey]] integerValue]; }
- (void) setCurrentInstallationStartCount:(NSInteger)value { [[NSUserDefaults standardUserDefaults] setObject:@(value) forKey:[self currentInstallationStartCountKey]]; }

- (NSInteger) totalStartCount { return [[MPLKeychain objectForKeychainWithName:[self currentInstallationStartCountKey] andAccessGroup:nil] integerValue]; }
- (void) setTotalStartCount:(NSInteger)value { [MPLKeychain setObject:@(value) forKeychainWithName:[self currentInstallationStartCountKey] andAccessGroup:nil]; }

- (NSInteger) startCountForVersion:(NSString *)version { return [[MPLKeychain objectForKeychainWithName:[self startCountKeyForVersion:version] andAccessGroup:nil] integerValue]; }
- (void) setStartCountForVersion:(NSString *)version count:(NSInteger)value { [MPLKeychain setObject:@(value) forKeychainWithName:[self startCountKeyForVersion:version] andAccessGroup:nil]; }

- (NSInteger) currentVersionStartCount { return [self startCountForVersion:self.currentVersion]; }
- (void) setCurrentVersionStartCount:(NSInteger)value { [self setStartCountForVersion:self.currentVersion count:value]; }

- (void) upgradeToVersion:(NSString*)toVersion completion:(void(^)(void))completion {
    self.currentlyInstalledVersion = toVersion;
    completion();
}
- (void) upgradeVersionWithCompletion:(void(^)(void))completion {
    for (int i = 0; i < self.allVersions.count-1; i++) {
        if ([[self.allVersions objectAtIndex:i] isEqualToString:self.currentlyInstalledVersion]) {
            if (self.upgradedFrom == nil) self.upgradedFrom = self.currentlyInstalledVersion;
            
            [self upgradeToVersion:[self.allVersions objectAtIndex:i+1] completion:^{
                [self upgradeVersionWithCompletion:completion];
            }];
            return;
        }
    }
    completion();
}
- (void) applicationStarted:(BOOL)indicateStarted andIncreaseVersion:(BOOL)incVersion {
    if (indicateStarted) self.started = YES;
    if (incVersion) {
        self.currentInstallationStartCount++;
        self.currentVersionStartCount++;
        self.totalStartCount++;
    }
}

#pragma mark -
#pragma mark Loadings

- (id) init {
    if ([super init])
    {
        self.started = NO;
        self.delegate = self;
    }
    return self;
}
- (void) loadViewControllersWithOptions:(NSDictionary *)launchOptions { }
- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.launchedWithRemoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    self.launchedWithLocalNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    self.currentOrientation = [[UIDevice currentDevice] orientation];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChangedNotification:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [self loadViewControllersWithOptions:launchOptions];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}
- (void) setRootControllerForBaseView:(MPLView*)baseView {
    self.window.rootViewController = [MPLViewController viewControllerWithBaseView:baseView];
}

#pragma mark -
#pragma mark Keyboard handling

- (void) adjustToKeyboardSize:(CGSize)keyboardSize { }
- (void) keyboardWillHide:(NSNotification*)note { [self adjustToKeyboardSize:CGSizeZero]; }
- (void) keyboardWillShow:(NSNotification*)note { [self adjustToKeyboardSize:[[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size]; }
- (void) keyboardDidShow:(NSNotification*)note { [self adjustToKeyboardSize:[[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size]; }
- (void) keyboardDidHide:(NSNotification*)note { }
- (void) orientationChangedNotification:(NSNotification*)note {
    if (self.currentOrientation == [[UIDevice currentDevice] orientation]) return;
    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationFaceUp) return;
    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationFaceDown) return;
    self.currentOrientation = [[UIDevice currentDevice] orientation];
    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft)
        [self orientationChangedToLandscape:YES];
    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight)
        [self orientationChangedToLandscape:NO];
    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait)
        [self orientationChangedToPortrait:YES];
    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown)
        [self orientationChangedToPortrait:NO];
}
- (void) orientationChangedToLandscape:(BOOL)homeButtonLeft { }
- (void) orientationChangedToPortrait:(BOOL)homeButtonBottom { }

#pragma mark -
#pragma mark Splash management

- (UIImage*) defaultSplashImage {
    if ([UIScreen mainScreen].bounds.size.width == 320 && [UIScreen mainScreen].bounds.size.height == 480)
        return [UIImage imageNamed:@"LaunchImage"];
    else if ([UIScreen mainScreen].bounds.size.width == 320 && [UIScreen mainScreen].bounds.size.height == 568)
        return [UIImage imageNamed:@"LaunchImage-568h"];
    else if ([UIScreen mainScreen].bounds.size.width == 375 && [UIScreen mainScreen].bounds.size.height == 667)
        return [UIImage imageNamed:@"LaunchImage-800-667h"];
    else if ([UIScreen mainScreen].bounds.size.width == 375 && [UIScreen mainScreen].bounds.size.height == 736)
        return [UIImage imageNamed:@"LaunchImage-800-Portrait-736h"];
    else
        return nil;
}
- (void) createAndFadeSplashImageWithDuration:(float)duration andDelay:(float)delay andProcessing:(void(^)(void(^processingFinished)(void)))processing andFinished:(void(^)(void))finishedBlock {
    [self createSplashImageViewForImage:[self defaultSplashImage]];
    processing(^{
        [self fadeSplashImageWithDuration:duration andDelay:delay andFinished:finishedBlock];
    });
}
- (void) createAndFadeSplashImageViewForImage:(UIImage*)image withDuration:(float)duration andDelay:(float)delay andProcessing:(void(^)(void(^processingFinished)(void)))processing andFinished:(void(^)(void))finishedBlock {
    [self createSplashImageViewForImage:image];
    processing(^{
        [self fadeSplashImageWithDuration:duration andDelay:delay andFinished:finishedBlock];
    });
}

- (void) createAndFadeSplashImageViewWithDuration:(float)duration andDelay:(float)delay andFinished:(void(^)(void))finishedBlock {
    [self createAndFadeSplashImageViewForImage:[self defaultSplashImage] withDuration:duration andDelay:delay andFinished:finishedBlock];
}
- (void) createAndFadeSplashImageViewForImage:(UIImage*)image withDuration:(float)duration andDelay:(float)delay andFinished:(void(^)(void))finishedBlock {
    [self createAndFadeSplashImageViewForImage:image withDuration:duration andDelay:delay andProcessing:^(void(^processingFinished)(void)) {
        processingFinished();
    } andFinished:finishedBlock];
}

- (void) createSplashImageView {
    [self createSplashImageViewForImage:[self defaultSplashImage]];
}
- (void) createSplashImageViewForImage:(UIImage*)image {
    self.splashImageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.splashImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.splashImageView.image = image;
    [self.rootController.baseView.view addSubview:self.splashImageView];
}
- (void) fadeSplashImageWithDuration:(float)duration andDelay:(float)delay andFinished:(void(^)(void))finishedBlock {
    if (self.splashImageView == nil) { finishedBlock(); return; }
    
    [self.rootController.baseView.view bringSubviewToFront:self.splashImageView];
    
    [UIView animateWithDuration:duration
                          delay:delay
                        options:0
                     animations:^{ self.splashImageView.alpha = 0; }
                     completion:^(BOOL finished) {
                         [self.splashImageView removeFromSuperview];
                         self.splashImageView = nil;
                         finishedBlock();
                     }
     ];
}

#pragma mark -
#pragma mark Remote notification handling

- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [self.pushNotificationManager application:self didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}
- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [self.pushNotificationManager application:self didFailToRegisterForRemoteNotificationsWithError:error];
}
- (void) application:(MPLApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [self.pushNotificationManager application:self didReceiveRemoteNotification:userInfo];
}
- (void) application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [self.pushNotificationManager application:self didRegisterUserNotificationSettings:notificationSettings];
}
- (void) application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    [self.pushNotificationManager application:self handleActionWithIdentifier:identifier forRemoteNotification:userInfo completionHandler:completionHandler];
}



#pragma mark -
#pragma mark Asking For Rating

- (void) productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    SKStoreProductViewController* c = (SKStoreProductViewController*)self.rootController.presentedViewController;
    [self.rootController dismissViewControllerAnimated:YES completion:^{
        c.delegate = nil;
    }];
}
- (void) goToRatingForAppIdentifier:(NSString*)identifier presentController:(BOOL)presentController; {
    if (presentController) {
        SKStoreProductViewController* appStoreViewController = [[SKStoreProductViewController alloc] init];
        appStoreViewController.delegate = self;
        [appStoreViewController loadProductWithParameters:@{ SKStoreProductParameterITunesItemIdentifier:identifier } completionBlock:^(BOOL result, NSError *error) {
            // did show
        }];
        [self.rootController presentViewController:appStoreViewController animated:YES completion:^{}];
    }
    else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software", identifier]]];
    }
}
- (void) askForRatingConditioned:(BOOL)condition appIdentifier:(NSString*)identifier presentController:(BOOL)presentController message:(NSString*)message acceptText:(NSString*)accept declineText:(NSString*)decline laterText:(NSString*)later later:(void(^)(void))laterAction {
    if (!condition) return;
    [MPLAlert alertWithTitle:@"" message:message andStyle:UIAlertViewStyleDefault finishedDelegate:^(NSString *input1, NSString *input2, NSInteger index) {
        if (index == 1) {
            [self goToRatingForAppIdentifier:identifier presentController:NO];
        }
        else if (index == 2) {
            laterAction();
        }
    } cancelButtonTitle:decline andMoreButtonTitles:later.length == 0 ? @[accept] : @[accept,later]];
}

#pragma mark -
#pragma mark Application lifecycle

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}
- (void)applicationWillTerminate:(UIApplication *)application {
    [self.OnTermination perform];
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}

+ (void) logFonts {
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
}

@end
