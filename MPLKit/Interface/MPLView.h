//
//  FTControl.h
//  FTrack_Take2
//
//  Created by Administrator on 05/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Availability.h>
#import <AvailabilityInternal.h>
#import <AvailabilityMacros.h>
#import "MPLAction.h"
#import "MPLTheme.h"
@class MPLViewController;
@class MPLPanel;


@interface MPLTransition : NSObject

@property (nonatomic, assign) float defaultDuration;
@property (nonatomic, assign) UIViewAnimationOptions defaultOptions;
@property (nonatomic, assign) float transitionState;
@property (nonatomic, copy) float(^transitionStateGet)(void);
@property (nonatomic, copy) void(^transitionStateSet)(float value);
@property (nonatomic, copy) void(^fixTransitionStateEnabled)(BOOL enabled);
@property (nonatomic, copy) void(^beginTransitionStateTo)(float position, void(^finishedBlock)(void));
@property (nonatomic, copy) void(^endTransitionStateTo)(float position, void(^finishedBlock)(void));

- (id) initWithGet:(float(^)(void))getter
               set:(void(^)(float value))setter
  andFixTransition:(void(^)(BOOL enabled))fixTransitionStateEnabled
          andBegin:(void(^)(float position, void(^finishedBlock)(void)))beginTransitionStateTo
            andEnd:(void(^)(float position, void(^finishedBlock)(void)))endTransitionStateTo
;

- (void) clear;

- (instancetype) assignGetter:(float (^)(void))getter;
- (instancetype) assignSetter:(void (^)(float value))setter;
- (instancetype) assignFixer:(void (^)(BOOL enabled))fixer;
- (instancetype) assignBegin:(void(^)(float position, void(^finishedBlock)(void)))beginner;
- (instancetype) assignEnd:(void(^)(float position, void(^finishedBlock)(void)))ender;

+ (MPLTransition*) transitionWithGet:(float(^)(void))getter
                                 set:(void(^)(float value))setter
                    andFixTransition:(void(^)(BOOL enabled))fixTransitionStateEnabled
                            andBegin:(void(^)(float position, void(^finishedBlock)(void)))beginTransitionStateTo
                              andEnd:(void(^)(float position, void(^finishedBlock)(void)))endTransitionStateTo
;
+ (MPLTransition*) create:(void(^)(MPLTransition* t))creater;
+ (void(^)(BOOL enabled)) fixTransitionEmpty;
+ (void(^)(float position, void(^finishedBlock)(void))) beginEmpty;
+ (void(^)(float position, void(^finishedBlock)(void))) endEmpty;

- (void) setTransitionStateFixed:(float)value;

- (void) transitionStateTo:(float)position duration:(float)duration bounce:(float)bounce finished:(void(^)(void))finishedBlock;
- (void) transitionStateTo:(float)position duration:(float)duration finished:(void(^)(void))finishedBlock;
- (void) transitionStateTo:(float)position animated:(BOOL)animated finished:(void(^)(void))finishedBlock;

+ (MPLTransition*) emptyTransition;

@end

@interface MPLView : NSObject<UIGestureRecognizerDelegate> {
//    UIView* view;
//    UIButton* action_button;
//    void (^touchBlock)(MPLView*);
//    MPLAction* touchAction;
//    
//    NSMutableDictionary* actions;
//    NSMutableDictionary* customData;
//    MPLView* __weak parentView;
//    int activityIndicatorsCount;
//    UIActivityIndicatorView* activityIndicatorView;
//    
//    int userInteractionDisableCount;
//    
}

@property (nonatomic, assign) int activityIndicatorsCount;
@property (nonatomic, assign) int userInteractionDisableCount;

@property (nonatomic, strong) NSMutableDictionary* themeAttatchments;
@property (nonatomic, strong) NSMutableDictionary* themeAttatchmentsKeyMappings;
- (MPLThemeViewAttatchment*) themeAttatchmentForKey:(NSString*)themeAttatchementKey;
- (void) removeThemeAttatchmentFor:(NSString*)themeAttatchementKey;
- (void) addThemeAttatchment:(MPLThemeViewAttatchment*)themeAttatchment forKey:(NSString*)themeAttatchementKey;
- (instancetype) attatch:(NSString*)themeAttatchementKey toTheme:(MPLTheme*)theme forKey:(NSString*)key withParameters:(NSDictionary*)parameters andAction:(void (^)(MPLThemeViewAttatchment* themeAttatchment))actionBlock;
- (instancetype) attatch:(NSString*)themeAttatchementKey toTheme:(MPLTheme*)theme forKey:(NSString*)key andAction:(void (^)(MPLThemeViewAttatchment* themeAttatchment))actionBlock;
- (instancetype) attatchBackgroundColorToTheme:(MPLTheme*)theme forKey:(NSString*)key withAlpha:(float)alpha withDuration:(float)duration;
- (instancetype) attatchBackgroundColorToTheme:(MPLTheme*)theme forKey:(NSString*)key;

@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) CGSize sizeFrame;
@property (nonatomic, assign) CGFloat widthFrame;
@property (nonatomic, assign) CGFloat heightFrame;
@property (nonatomic, assign) CGPoint offset;
@property (nonatomic, assign) CGFloat xOffset;
@property (nonatomic, assign) CGFloat yOffset;

@property (nonatomic, assign) CGRect bounds;
@property (nonatomic, assign) CGSize sizeBounds;
@property (nonatomic, assign) CGFloat widthBounds;
@property (nonatomic, assign) CGFloat heightBounds;
@property (nonatomic, assign) CGPoint center;
@property (nonatomic, assign) CGFloat xCenter;
@property (nonatomic, assign) CGFloat yCenter;

@property (nonatomic, assign) UIViewAutoresizing mask;
@property (nonatomic, assign) float alpha;
@property (nonatomic, assign) BOOL visible;

@property (nonatomic, strong) NSMutableDictionary* actions;
@property (nonatomic, strong) NSMutableDictionary* customData;
@property (nonatomic, copy) void (^touchBlock)(MPLView*);
@property (nonatomic, strong) MPLAction* touchAction;
@property (weak, nonatomic, readonly) MPLAction* actionForTouch;
@property (nonatomic, strong) UIView* view;
@property (nonatomic, strong) UIButton* action_button;
@property (nonatomic, weak) MPLView* parentView;
@property (nonatomic, strong) UIActivityIndicatorView* activityIndicatorView;
@property (nonatomic, strong) NSMutableArray* frameObservers;

@property (nonatomic, weak) UIColor* backgroundColor;
@property (nonatomic, weak) UIImage* backgroundImage;

@property (nonatomic, assign) BOOL PanActive;
@property (nonatomic, assign) BOOL PanAllowScroll;
@property (nonatomic, assign) CGPoint PanLocationNow;
@property (nonatomic, assign) CGPoint PanLocationLast;
@property (nonatomic, assign) CGPoint PanLocationOnStart;
@property (nonatomic, assign) CGPoint PanStart;
@property (nonatomic, assign) CGPoint PanLast;
@property (nonatomic, assign) CGPoint PanNow;
@property (nonatomic, assign) BOOL PanHRight;
@property (nonatomic, assign) BOOL PanVDown;
@property (nonatomic, strong) UIPanGestureRecognizer* PanGesture;
#if MPLCanUseEdgePan
@property (nonatomic, strong) UIScreenEdgePanGestureRecognizer* LeftEdgePanGesture;
@property (nonatomic, strong) UIScreenEdgePanGestureRecognizer* RightEdgePanGesture;
#else
#endif
@property (nonatomic, strong) UITapGestureRecognizer* TapGesture;
@property (nonatomic, strong) UITapGestureRecognizer* DoubleTapGesture;
@property (nonatomic, strong) UILongPressGestureRecognizer* LongPressGesture;
@property (nonatomic, strong) NSDate* lastPanDate;
@property (nonatomic, strong) NSDate* currentPanDate;
@property (nonatomic, readonly) BOOL PanHorizontal;
@property (nonatomic, readonly) BOOL PanVertical;
@property (nonatomic, readonly) BOOL PanLeft;
@property (nonatomic, readonly) BOOL PanRight;
@property (nonatomic, readonly) BOOL PanUp;
@property (nonatomic, readonly) BOOL PanDown;
@property (nonatomic, readonly) BOOL PanStarted;
@property (nonatomic, readonly) BOOL PanStartHorizontal;
@property (nonatomic, readonly) BOOL PanStartVertical;
@property (nonatomic, readonly) BOOL PanStartLeft;
@property (nonatomic, readonly) BOOL PanStartRight;
@property (nonatomic, readonly) BOOL PanStartUp;
@property (nonatomic, readonly) BOOL PanStartDown;
@property (nonatomic, readonly) float PanVelocityHorizontal;
@property (nonatomic, readonly) CGSize PanDistanceToStart;
@property (nonatomic, readonly) CGSize PanDistanceToStartPrevious;
@property (nonatomic, readonly) CGSize PanDistanceToLastPan;
@property (nonatomic, strong) NSMutableDictionary* PanInvocations;
@property (nonatomic, strong) NSMutableDictionary* LeftEdgePanInvocations;
@property (nonatomic, strong) NSMutableDictionary* RightEdgePanInvocations;
@property (nonatomic, strong) NSMutableDictionary* TapInvocations;
@property (nonatomic, strong) NSMutableDictionary* DoubleTapInvocations;
@property (nonatomic, strong) NSMutableDictionary* LongPressInvocations;

- (id) init;


- (instancetype) putAtLeftNextTo:(MPLView*)relativeView atDistance:(float)distance andWidth:(float)width;
- (instancetype) putAtLeftAtDistance:(float)distance andWidth:(float)width;
- (instancetype) putAtLeftWithWidth:(float)width;
- (instancetype) putAtRightNextTo:(MPLView*)relativeView atDistance:(float)distance andWidth:(float)width;
- (instancetype) putAtRightAtDistance:(float)distance andWidth:(float)width;
- (instancetype) putAtRightWithWidth:(float)width;
- (instancetype) putAtLeftNextTo:(MPLView*)relativeLeftView atLeftDistance:(float)leftDistance atRightNextTo:(MPLView*)relativeRightView atRightDistance:(float)rightDistance withFixedWidth:(BOOL)fixedWidth;
- (instancetype) putAtLeftNextTo:(MPLView*)relativeLeftView atDistance:(float)leftDistance toRightAtDistance:(float)rightDistance withFixedWidth:(BOOL)fixedWidth;
- (instancetype) putAtRightNextTo:(MPLView*)relativeRightView atDistance:(float)rightDistance toLeftAtDistance:(float)leftDistance withFixedWidth:(BOOL)fixedWidth;
- (instancetype) putAtLeftAtDistance:(float)leftDistance toRightAtDistance:(float)rightDistance withFixedWidth:(BOOL)fixedWidth;
- (instancetype) fillWidthWithDistance:(float)distance;
- (instancetype) fillWidth;
- (instancetype) putBetweenLeft:(MPLView*)relativeLeftView atDistance:(float)leftDistance andRight:(MPLView*)relativeRightView atDistance:(float)rightDistance withWidth:(float)width andAlignment:(float)alignment;
- (instancetype) putBetweenLeft:(MPLView*)relativeLeftView andRight:(MPLView*)relativeRightView withWidth:(float)width andAlignment:(float)alignment;
- (instancetype) putAtCenterBetweenLeft:(MPLView*)relativeLeftView atDistance:(float)leftDistance andRight:(MPLView*)relativeRightView atDistance:(float)rightDistance withWidth:(float)width;
- (instancetype) putAtCenterBetweenLeft:(MPLView*)relativeLeftView andRight:(MPLView*)relativeRightView withWidth:(float)width;



#pragma mark -
#pragma mark Horizontal frame/mask setup

- (instancetype) putAtTopNextTo:(MPLView*)relativeView atDistance:(float)distance andHeight:(float)height;
- (instancetype) putAtTopAtDistance:(float)distance andHeight:(float)height;
- (instancetype) putAtTopWithHeight:(float)height;
- (instancetype) putAtBottomNextTo:(MPLView*)relativeView atDistance:(float)distance andHeight:(float)height;
- (instancetype) putAtBottomAtDistance:(float)distance andHeight:(float)height;
- (instancetype) putAtBottomWithHeight:(float)height;
- (instancetype) fillHeightFromTop:(MPLView*)relativeTopView atTopDistance:(float)topDistance andBottom:(MPLView*)relativeBottomView atBottomDistance:(float)bottomDistance;
- (instancetype) fillHeightFromTop:(MPLView*)relativeTopView atDistance:(float)topDistance toParentBottomAtDistance:(float)bottomDistance;
- (instancetype) fillHeightFromBottom:(MPLView*)relativeBottomView atDistance:(float)bottomDistance toParentTopAtDistance:(float)topDistance;
- (instancetype) fillHeightWithTopDistance:(float)topDistance andBottomDistance:(float)bottomDistance;
- (instancetype) fillHeightWithDistance:(float)distance;
- (instancetype) fillHeight;
- (instancetype) putBetweenTop:(MPLView*)relativeTopView atDistance:(float)topDistance andBottom:(MPLView*)relativeBottomView atDistance:(float)bottomDistance withHeight:(float)height andAlignment:(float)alignment;
- (instancetype) putBetweenTop:(MPLView*)relativeTopView andBottom:(MPLView*)relativeBottomView withHeight:(float)height andAlignment:(float)alignment;
- (instancetype) putAtCenterBetweenTop:(MPLView*)relativeTopView atDistance:(float)topDistance andBottom:(MPLView*)relativeBottomView atDistance:(float)bottomDistance withHeight:(float)height;
- (instancetype) putAtCenterBetweenTop:(MPLView*)relativeTopView andBottom:(MPLView*)relativeBottomView withHeight:(float)height;

- (void) rotate90:(BOOL)left;


@property (nonatomic, readonly) CGPoint maxContentCorner;
@property (nonatomic, readonly) CGPoint minContentCorner;
@property (nonatomic, readonly) CGSize contentSize;

- (void) addCustomData:(id)source forKey:(NSString*)key;
- (void) addAction:(MPLAction *)action forKey:(NSString*)actionKey;
- (void) addAction:(MPLAction*)action forEventType:(UIControlEvents)type;
- (void) performAction:(NSString*)actionKey forView:(MPLView*)_view;
- (void) performAction:(NSString*)actionKey;
- (void) performActionForControlEvent:(UIControlEvents)controlEvent;
- (MPLAction*) actionForKey:(NSString*)key;
- (MPLAction*) actionForControlEvent:(UIControlEvents)controlEvent;
- (instancetype) assignTouchAction:(void(^)(MPLView* v))action;
- (instancetype) assignTouchActionObject:(MPLAction*)action;
- (void) unassignTouchButton;

- (UIView*) createView;
- (void) createInside:(UIView *)parent;
- (void) removeFromSuperview;
- (void) destroyView;
- (instancetype) addTo:(MPLPanel*)parent forKey:(NSString*)key;
- (instancetype) addTo:(MPLPanel*)parent;

- (void) bringToFront;
- (void) sendToBack;

+ (void) stackVertically:(NSArray*)views;
+ (void) stackViewVerticaly:(MPLView*)v ontoOffset:(float*)y withPadding:(float)padding;

- (void) shake:(int)shakes withMaxShakes:(int)maxShakes andDistance:(float)distance andDuration:(float)duration;
- (void) colorizeForLayer:(int)layer;

- (void) addFrameObserver:(void(^)(MPLView* v))observer;
- (void) resetMask:(void(^)(MPLView* v))reset;

- (void) drawView;

- (UIImage*) viewImage;
- (UIImage*) viewImageScaledTo:(float)scale;
- (UIImage*) makeContextImage:(void(^)(CGContextRef context, CGSize size))maskSetter;
- (void) setContentsWithCGContext:(void(^)(CGContextRef context, CGSize size))maskSetter;
- (void) setMaskWithCGContext:(void(^)(CGContextRef context, CGSize size))maskSetter;
- (void) setMaskFromImage:(UIImage*)image;

- (void) setAnchorPoint:(CGPoint)anchorPoint;

- (void) setShadowWithRadius:(float)radius opacity:(float)opacity offset:(CGSize)offset color:(UIColor*)color;
- (void) setShadowWithRadius:(float)radius opacity:(float)opacity offset:(CGSize)offset;
- (void) setShadowWithRadius:(float)radius opacity:(float)opacity;

- (void) startActivityWithProcessing:(void(^)(void(^processingFinished)(void)))processing andFinished:(void(^)(void))finishedBlock;
- (void) hideActivityAnimated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) showActivityAnimated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) displayActivity:(BOOL)display animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) showActivityAnimated:(BOOL)animated withSize:(float)size andStyle:(UIActivityIndicatorViewStyle)style andDarkAlpha:(float)alpha andFinished:(void(^)(void))finishedBlock;
- (void) showActivityAnimated:(BOOL)animated withSize:(float)size andFrame:(CGRect)frame andStyle:(UIActivityIndicatorViewStyle)style andDarkAlpha:(float)alpha andFinished:(void(^)(void))finishedBlock;

- (CGSize) panDistanceFrom:(CGPoint)p1 to:(CGPoint)p2;
- (void) disablePanForKey:(NSString*)key;
#if MPLCanUseEdgePan
- (void) disableLeftEdgePanForKey:(NSString*)key;
- (void) disableRightEdgePanForKey:(NSString*)key;
#else
#endif
- (void) disableTapForKey:(NSString*)key;
- (void) disableDoubleTapForKey:(NSString*)key;
- (void) disableLongPressForKey:(NSString*)key;
- (void) enablePanWithAction:(MPLAction*)action forKey:(NSString*)key;
#if MPLCanUseEdgePan
- (void) enableLeftEdgePanWithAction:(MPLAction*)action forKey:(NSString*)key;
- (void) enableRightEdgePanWithAction:(MPLAction*)action forKey:(NSString*)key;
#else
#endif
- (void) enableTapWithAction:(MPLAction*)action forKey:(NSString*)key;
- (void) enableDoubleTapWithAction:(MPLAction*)action forKey:(NSString*)key;
- (void) enableLongPressWithAction:(MPLAction*)action forKey:(NSString*)key;

@end
