//
//  MPLWebView.h
//  MPLKit
//
//  Created by Martin Lalev on 9/26/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

@interface MPLWebView : MPLView<UIWebViewDelegate>

@property (weak, nonatomic, readonly) UIWebView* webView;
@property (nonatomic, copy) void (^didFinishLoading)(NSError* error);

+ (MPLWebView*) create:(void(^)(MPLWebView* v))creater;

- (void) goBackWithFinished:(void(^)(NSError* error))finishedLoading;
- (void) loadURL:(NSURL*)url withFinished:(void(^)(NSError* error))finishedLoading;


@end
