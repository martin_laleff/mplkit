//
//  MPLTextView.m
//  ShtrakPollApps
//
//  Created by Martin Lalev on 1/21/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLTextView.h"


@implementation MPLTextViewPanel

- (UITextView*)textView { return (UITextView*)self.view; }
- (UIView*) createView { return [[UIView alloc] init]; }

- (void) loaderWithPlaceholder:(NSString*)placeholder
                 andIsPassword:(BOOL)isPassword
               andKeyboardType:(UIKeyboardType)keyboardType
              andTextAlignment:(NSTextAlignment)textAlignment
{
    self.textView.secureTextEntry = isPassword;
    self.textView.keyboardType = keyboardType;
    self.textView.textAlignment = textAlignment;
    self.placeholderLabel.text = placeholder;
}

+ (MPLTextViewPanel*) create:(void(^)(MPLTextViewPanel* v))creater {
    MPLTextViewPanel* textbox = [[MPLTextViewPanel alloc] init];
    
    textbox.textView = [[UITextView alloc] initWithFrame:textbox.bounds];
    
    textbox.textView.font = [UIFont systemFontOfSize:[UIFont labelFontSize]];
    textbox.textView.delegate = textbox;
    
    textbox.placeholderLabel = [[UILabel alloc] initWithFrame:textbox.bounds];
    textbox.placeholderLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    textbox.placeholderLabel.numberOfLines = 0;
    textbox.placeholderLabel.textColor = [UIColor blackColor];
    
    creater(textbox);
    
    textbox.textView.bounds = textbox.bounds;
    textbox.textView.center = CGPointMake(textbox.widthBounds/2, textbox.heightBounds/2);
    
    textbox.placeholderLabel.bounds = textbox.bounds;
    textbox.placeholderLabel.center = CGPointMake(textbox.widthBounds/2, textbox.heightBounds/2);

    return textbox;
}

- (BOOL) textViewShouldEndEditing:(UITextView *)textView {
    [self performAction:@"Done"];
    self.placeholderLabel.hidden = (self.textView.text.length > 0);
    return YES;
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    self.placeholderLabel.hidden = YES;
    return YES;
}
- (void) textViewDidEndEditing:(UITextView *)textView {
    [self performAction:@"DidEndEditing"];
}
- (BOOL) textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"] && self.textView.returnKeyType == UIReturnKeyDone) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void) indicateValid:(BOOL)isValid {
    [self indicateValid:isValid withViewToShake:self];
}
- (void) indicateValid:(BOOL)isValid withViewToShake:(MPLView*)viewToShake {
    UIView* rightView = nil;
    if (!isValid) {
        rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        rightView.backgroundColor = [UIColor redColor];
        [viewToShake shake:0 withMaxShakes:6 andDistance:10 andDuration:0.1];
    }
    //    [self.textView setRightView:rightView];
}
- (BOOL) isEmail {
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"];
    return [emailTest evaluateWithObject:self.textView.text];
}

@end


@implementation MPLTextView

- (UITextView*)textView { return (UITextView*)self.view; }
- (UIView*) createView { return [[UITextView alloc] init]; }

- (void) destroyView {
    self.toolbar = nil;
    [super destroyView];
}

- (NSString*) text { return self.textView.text; }
- (void) setText:(NSString*)text {
    self.textView.text = text;
    self.placeholderLabel.hidden = (self.textView.text.length > 0);
    self.placeholderButton.hidden = (self.textView.text.length > 0);
}

- (void) enableToolbarAccessoryWithBarButtonGenerator:(void (^)(NSMutableArray *))barButtonsGenerator {
    self.toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    self.toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    NSMutableArray* barButtons = [NSMutableArray array];
    barButtonsGenerator(barButtons);
    [self.toolbar setItems:barButtons animated:NO];
    self.textView.inputAccessoryView = self.toolbar;
}

- (void) loaderWithPlaceholder:(NSString*)placeholder
                 andIsPassword:(BOOL)isPassword
               andKeyboardType:(UIKeyboardType)keyboardType
              andTextAlignment:(NSTextAlignment)textAlignment
{
    self.textView.secureTextEntry = isPassword;
    self.textView.keyboardType = keyboardType;
    self.textView.textAlignment = textAlignment;
    
    self.placeholderLabel.text = placeholder;
    self.placeholderLabel.frame = CGRectMake(5,0,self.bounds.size.width-10,self.bounds.size.height);
    self.placeholderLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

+ (MPLTextView*) create:(void(^)(MPLTextView* v))creater {
    MPLTextView* textbox = [[MPLTextView alloc] init];

    textbox.textView.font = [UIFont systemFontOfSize:[UIFont labelFontSize]];
    textbox.textView.delegate = textbox;
    
    textbox.placeholderLabel = [[UILabel alloc] initWithFrame:textbox.bounds];
    textbox.placeholderLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    textbox.placeholderLabel.numberOfLines = 0;
    textbox.placeholderLabel.textColor = [UIColor grayColor];
    
    textbox.placeholderButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [textbox.placeholderButton addTarget:textbox action:@selector(placeholderTapped) forControlEvents:UIControlEventTouchUpInside];
    [textbox.placeholderButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    textbox.placeholderButton.bounds = textbox.bounds;
    textbox.placeholderButton.center = CGPointMake(textbox.widthBounds/2, textbox.heightBounds/2);
    textbox.placeholderButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [textbox.textView addSubview:textbox.placeholderLabel];
    [textbox.textView addSubview:textbox.placeholderButton];

    creater(textbox);

    return textbox;
}


- (void) placeholderTapped {
    [self.textView becomeFirstResponder];
    self.placeholderLabel.hidden = YES;
    self.placeholderButton.hidden = YES;
}
- (BOOL) textViewShouldEndEditing:(UITextView *)textView {
    [self performAction:@"Done"];
    self.placeholderLabel.hidden = (self.textView.text.length > 0);
    self.placeholderButton.hidden = (self.textView.text.length > 0);
    return YES;
}
- (void) textViewDidEndEditing:(UITextView *)textView {
    [self performAction:@"DidEndEditing"];
}
- (BOOL) textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"] && self.textView.returnKeyType == UIReturnKeyDone) {
        [textView resignFirstResponder];
        return NO;
    }
    [MPLAction performAfter:.1 block:^{
        self.placeholderLabel.hidden = (self.textView.text.length > 0);
        self.placeholderButton.hidden = (self.textView.text.length > 0);
    }];
   [self performAction:@"WillChangeText"];
    return YES;
}

- (void) indicateValid:(BOOL)isValid {
    [self indicateValid:isValid withViewToShake:self];
}
- (void) indicateValid:(BOOL)isValid withViewToShake:(MPLView*)viewToShake {
    UIView* rightView = nil;
	if (!isValid) {
		rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
		rightView.backgroundColor = [UIColor redColor];
        [viewToShake shake:0 withMaxShakes:6 andDistance:10 andDuration:0.1];
	}
//    [self.textView setRightView:rightView];
}
- (BOOL) isEmail {
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"];
    return [emailTest evaluateWithObject:self.textView.text];
}

@end
