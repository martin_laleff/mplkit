//
//  EGORefreshTableHeaderView.h
//  Demo
//
//  Created by Devin Doty on 10/14/09October14.
//  Copyright 2009 enormego. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>



typedef enum{
	EGOOPullRefreshPulling = 0,
	EGOOPullRefreshNormal,
	EGOOPullRefreshLoading,
} EGOPullRefreshState;



@protocol EGORefreshTableHeaderDelegate;



@interface EGORefreshTableHeaderView : UIView {
	UIScrollView* container;
    NSString* lastUpdated;
	id __weak _delegate;
    BOOL IsRefreshing;
	EGOPullRefreshState _state;
    float offsetOnLoading;
    float offsetToRelease;
    
    NSString* pullDownToRefreshString;
    NSString* releaseToRefreshString;
    NSString* loadingString;
    
	UILabel *_lastUpdatedLabel;
	UILabel *_statusLabel;
	CALayer *_arrowImage;
	UIActivityIndicatorView *_activityView;
}

@property(nonatomic,weak) id <EGORefreshTableHeaderDelegate> delegate;
@property(nonatomic,strong) NSString* lastUpdated;
@property(nonatomic,assign) float offsetOnLoading;
@property(nonatomic,assign) float offsetToRelease;
@property(nonatomic,assign) BOOL IsRefreshing;
@property (nonatomic, assign) EGOPullRefreshState state;

@property(nonatomic,strong) NSString* pullDownToRefreshString;
@property(nonatomic,strong) NSString* releaseToRefreshString;
@property(nonatomic,strong) NSString* loadingString;

- (id)initAtContainer:(UIScrollView*)_scrollView withHeight:(float)height andOffset:(float)offset andLoadingOffset:(float)loadingOffset andDelegate:(id<EGORefreshTableHeaderDelegate>)__delegate arrowImage:(UIImage *)arrow backgroundColor:(UIColor*)backgroundColor textColor:(UIColor *)textColor
andPullDownToRefreshString:(NSString*)pullDownToRefresh
andReleaseToRefreshString:(NSString*)releaseToRefresh
     andLoadingString:(NSString*)loading
;

- (void) assignOffsetTo:(float)offset;
- (void) changeFontTo:(UIFont*)font;

- (void)refreshLastUpdatedDate;
- (void)egoRefreshScrollViewDidScroll:(UIScrollView *)scrollView;
- (void)egoRefreshScrollViewDidEndDragging:(UIScrollView *)scrollView;
- (void)egoRefreshScrollViewDataSourceDidFinishedLoading:(UIScrollView *)scrollView;

@end



@protocol EGORefreshTableHeaderDelegate
- (void) egoRefreshTableStateChanged;
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view;
@optional
- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view;
@end
