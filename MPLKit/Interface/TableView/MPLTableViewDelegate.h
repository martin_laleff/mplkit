//
//  MPLTableViewDelegate.h
//  Posledno
//
//  Created by Martin Lalev on 10/19/13.
//  Copyright (c) 2013 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MPLTableView;
@class MPLTableViewSection;

@protocol MPLTableViewDelegate <NSObject>

@optional
- (NSString*) listing:(MPLTableView*)listing cellIdentifierForSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data;
@optional
- (float) listing:(MPLTableView*)listing heightForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data;
@optional
- (MPLView*) listing:(MPLTableView*)listings cellForIdentifier:(NSString*)identifier withFrame:(CGRect)frame andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell;
@optional
- (void) listing:(MPLTableView*)listing cellReloadForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView;

@optional
- (void) listing:(MPLTableView*)listing stickyRowMoved:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)frame andState:(float)state;

@optional
- (void) listing:(MPLTableView*)listing cellCreatedForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView;

@optional
- (void) listing:(MPLTableView*)listing animateState:(float)animateState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)rect;

@optional
- (BOOL) listing:(MPLTableView*)listing isHeaderData:(id)data forIdentifier:(NSString*)identifier;
@optional
- (CGRect) listing:(MPLTableView*)listing rectForHeaderWithRequestedRect:(CGRect)r;

@optional
- (void) listing:(MPLTableView*)listing willDisplayCellForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView;
@optional
- (void) listing:(MPLTableView*)listing cellSelectedForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView;

@optional
- (float) listing:(MPLTableView*)listing heightForHeaderInSection:(MPLTableViewSection*)section;
@optional
- (float) listing:(MPLTableView*)listing heightForFooterInSection:(MPLTableViewSection*)section;

@optional
- (NSString*) listing:(MPLTableView*)listing titleForHeaderInSection:(MPLTableViewSection*)section;
@optional
- (NSString*) listing:(MPLTableView*)listing titleForFooterInSection:(MPLTableViewSection*)section;
@optional
- (NSString*) listing:(MPLTableView*)listing titleForIndexInSection:(MPLTableViewSection*)section;

@optional
- (void) listing:(MPLTableView*)listing willBeginDraggingTo:(float)scrolledTo;
@optional
- (void) listing:(MPLTableView*)listing willBeginDeceleratingTo:(float)scrolledTo;
@optional
- (void) listing:(MPLTableView*)listing didScrollTo:(float)scrolledTo;
@optional
- (void) listing:(MPLTableView*)listing didEndDraggingTo:(float)scrolledTo willDecelerate:(BOOL)decelerate;
@optional
- (void) listing:(MPLTableView*)listing didEndDeceleratingTo:(float)scrolledTo;

@end
