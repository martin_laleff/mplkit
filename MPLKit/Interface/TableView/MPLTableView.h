//
//  FTTableView.h
//  FTrack_Take2
//
//  Created by Martin Lalev on 9/22/11.
//  Copyright 2011 mART. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLView.h"
#import "EGORefreshTableHeaderView.h"
#import "MPLTableViewDelegate.h"
#import "BVReorderTableView.h"
#import "MPLScrollViewDragHideDelegate.h"
#import "MPLPanel.h"




@protocol MPLTableViewRefreshDelegate <NSObject>

- (void) listingShouldRefresh:(MPLTableView*)tableView;
@optional
- (void) refreshStateChanged:(MPLTableView*)tableView;

@end




@interface MPLTableViewGridRowDataWrapper : NSObject

@property (nonatomic, assign) int maxObjects;
@property (nonatomic, strong) NSMutableArray* objects;
- (BOOL) addItem:(id)item;
+ (NSMutableArray*) generateGridListContainersForItems:(NSArray*)_items maxObjects:(int)maxObjects;

@end





@class MPLTableViewIdentifier;
@interface MPLTableViewExpandableCellWraper : MPLPanel
@property (nonatomic, weak) MPLTableViewIdentifier* identifier;
@property (nonatomic, assign) NSInteger heightState;
@property (nonatomic, assign) NSInteger row;
- (void) loadHeightState:(NSInteger)heightState atRow:(NSInteger)row;
- (void) loadForRow:(NSInteger)row;
@end

@interface MPLTableViewIdentifier : NSObject

@property (nonatomic, strong) NSMutableDictionary* heightStates;
@property (nonatomic, strong) NSMutableDictionary* rowHeightStates;

@property (nonatomic, strong) NSString* identifier;
@property (nonatomic, strong) MPLTableViewSection* section;
@property (nonatomic, copy) MPLView* (^cellCreated)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame);
@property (nonatomic, copy) float (^getHeight)(MPLTableViewIdentifier* section, NSInteger row, id data);
@property (nonatomic, copy) void (^reloadCell)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView);
@property (nonatomic, copy) BOOL (^isHeader)(MPLTableViewIdentifier* section, id data);
@property (nonatomic, copy) BOOL (^dataBelongs)(MPLTableViewIdentifier* section, id data);
@property (nonatomic, copy) void (^heightStateChanged)(MPLTableViewIdentifier* section, NSDictionary* rowsHeightStates);

- (instancetype) assignPanelFactory:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame, MPLPanel* p))cellCreated;
- (instancetype) assignPanelReloader:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLPanel* cellView))reloadCell;
- (instancetype) assignViewFactory:(MPLView* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated;
- (instancetype) assignCustomHeight:(float (^)(MPLTableViewIdentifier* section, NSInteger row, id data))getHeight;
- (instancetype) assignStaticHeight:(float)height;
- (instancetype) assignStateHeight;
- (instancetype) assignHeight:(float (^)(MPLTableViewIdentifier* section, NSInteger row, id data))getHeight forState:(int)state;
- (instancetype) dataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs;
- (instancetype) dataIsClass:(Class)cl;
- (instancetype) dataIsString:(NSString*)s;
- (instancetype) dataStartsWithString:(NSString*)s;
- (instancetype) dataIsString:(NSString*)s separator:(NSString*)sep index:(int)index;
- (instancetype) assignAsHeader;
- (instancetype) assignReloader:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell;


- (float)heightForHeightState:(NSInteger)heightState atRow:(NSInteger)row;
- (NSInteger) heightStateForRow:(NSInteger)row;
- (float) heightForRow:(NSInteger)row;
- (void) setRowsHeightStates:(NSDictionary*)rowsHeightStates andFinished:(void(^)(void))finishedBlock;
- (void) setRow:(NSInteger)row heightState:(NSInteger)heightState andFinished:(void(^)(void))finishedBlock;
- (void) toggleSingleBinaryHeightStateForRow:(MPLTableViewExpandableCellWraper*)v simultaniously:(BOOL)simultaniously;
- (void) setAllRowsToDefaultHeightState;

+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) identifierIsHeader;
+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) identifierNotHeader;
+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) dataIsClass:(Class)c;
+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) dataIsString:(NSString*)s;
+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) dataStartsWithString:(NSString*)s;
+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) dataIsString:(NSString*)s separator:(NSString*)sep index:(int)index;
+ (float(^)(MPLTableViewIdentifier* section, NSInteger row, id data)) height:(float)h;
+ (void(^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView)) emptyReload;

+ (MPLTableViewIdentifier*) identifierForString:(NSString*)identifier andSection:(MPLTableViewSection*)section;
+ (MPLTableViewIdentifier*) identifierFor:(NSString*)identifier
                               andSection:(MPLTableViewSection*)section
                           andCellCreated:(MPLView* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated
                             andGetHeight:(float (^)(MPLTableViewIdentifier* section, NSInteger row, id data))getHeight
                            andReloadCell:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell
                              andIsHeader:(BOOL (^)(MPLTableViewIdentifier* section, id data))isHeader
                           andDataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs
;
+ (MPLTableViewIdentifier*) identifierFor:(NSString*)identifier
                               andSection:(MPLTableViewSection*)section
                           andCellCreated:(MPLTableViewExpandableCellWraper* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated
                          andNormalHeight:(float(^)(MPLTableViewIdentifier *section, NSInteger row, id data))normalHeight
                        andExpandedHeight:(float(^)(MPLTableViewIdentifier *section, NSInteger row, id data))expandedHeight
                            andReloadCell:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell
                              andIsHeader:(BOOL (^)(MPLTableViewIdentifier* section, id data))isHeader
                           andDataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs
                    andHeightStateChanged:(void (^)(MPLTableViewIdentifier* section, NSDictionary* rowsHeightStates))heightStateChanged
;
+ (MPLTableViewIdentifier*) identifierFor:(NSString*)identifier
                               andSection:(MPLTableViewSection*)section
                           andCellCreated:(MPLTableViewExpandableCellWraper* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated
                          andHeightStates:(NSDictionary*)heightStates
                    andDefaultStateHeight:(float(^)(MPLTableViewIdentifier *section, NSInteger row, id data))defaultHeight
                            andReloadCell:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell
                              andIsHeader:(BOOL (^)(MPLTableViewIdentifier* section, id data))isHeader
                           andDataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs
                    andHeightStateChanged:(void (^)(MPLTableViewIdentifier* section, NSDictionary* rowsHeightStates))heightStateChanged
;

- (void) destroyIdentifier;

@end





@interface MPLTableViewSection : NSObject {
    @public NSMutableArray* cleanRowData;
}

@property (nonatomic, weak) MPLTableView* tableView;

@property (nonatomic, strong) id sectionData;
@property (nonatomic, strong) NSMutableArray* sectionRowsData;
@property (nonatomic, strong) NSMutableDictionary* cellViews;
@property (nonatomic, assign) UIEdgeInsets cellsInsets;
@property (nonatomic, assign) float spacing;

@property (nonatomic, strong) NSMutableArray* stickyRowsIndexPathsSections;
@property (nonatomic, strong) NSMutableDictionary* identifierDescriptors;

- (MPLTableViewIdentifier*) identifierForKey:(NSString*)key;

- (void) deleteObjectsFromIndex:(NSInteger)indexFrom toIndex:(NSInteger)indexTo animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) deleteObjectAtIndex:(NSInteger)index animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) deleteObjectsFromIndex:(NSInteger)index animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) deleteAllObjectsAnimated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) deleteObjectsAtIndices:(NSArray*)indices animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) insertObjects:(NSArray*)objects toIndex:(NSInteger)index animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) addObjects:(NSArray*)objects animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) insertObject:(id)object toIndex:(NSInteger)index animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;
- (void) addObject:(id)object animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock;

- (NSIndexPath*)indexPathForCleanDataIndex:(NSInteger)row;
- (NSInteger) cleanDataIndexForIndexPath:(NSIndexPath*)indexPath;

- (NSIndexPath*) indexPathForRow:(NSInteger)row;
- (NSInteger) cleanDataIndexForRow:(NSInteger)row;
- (UITableViewCell*) cellForRow:(NSInteger)row;
- (MPLView*) cellViewForRow:(NSInteger)row;

- (NSInteger) rowForCleanDataIndex:(NSInteger)index;

- (NSInteger) cleanDataIndexForData:(id)data;
- (NSInteger) rowForData:(id)data;
- (UITableViewCell*) cellForData:(id)data;
- (MPLView*) cellViewForData:(id)data;

- (UITableViewCell*) cellForCellView:(MPLView*)cellView;


+ (MPLTableViewSection*) sectionWithSectionData:(id)data andCellsInsets:(UIEdgeInsets)insets andSpacing:(float)spacingSize;

- (MPLTableViewIdentifier*) addIdentifierFor:(NSString*)identifier
                              andCellCreated:(MPLView* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated
                                andGetHeight:(float (^)(MPLTableViewIdentifier* section, NSInteger row, id data))getHeight
                               andReloadCell:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell
                                 andIsHeader:(BOOL (^)(MPLTableViewIdentifier* section, id data))isHeader
                              andDataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs
;
- (MPLTableViewIdentifier*) addIdentifier:(MPLView* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated
                             andGetHeight:(float (^)(MPLTableViewIdentifier* section, NSInteger row, id data))getHeight
                            andReloadCell:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell
                              andIsHeader:(BOOL (^)(MPLTableViewIdentifier* section, id data))isHeader
                           andDataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs
;
- (MPLTableViewIdentifier*) addIdentifierFor:(NSString*)identifier create:(void(^)(MPLTableViewIdentifier* identifier))creater;
- (MPLTableViewIdentifier*) addIdentifier:(void(^)(MPLTableViewIdentifier* identifier))creater;

- (void) destroySection;

@end








@interface MPLTableView : MPLView<UITableViewDataSource, UITableViewDelegate, ReorderTableViewDelegate, EGORefreshTableHeaderDelegate> {
    UITableViewStyle tableViewStyle;
    UITableViewCellAccessoryType accessoryType;
    UITableViewCellSelectionStyle selectionStyle;
    BOOL hideEmptySections;
    BOOL autoSnapsToRow;
    
    NSMutableArray* allSections;
    
    NSMutableArray* stickyRowsIndexPaths;
    NSMutableDictionary* stickyRowsIdentifierViews;
    NSMutableDictionary* stickyRowsIndexPathView;
    NSMutableDictionary* stickyRowsIndexedPreviousIndexPaths;
    float stickyRowsTopInset;
    float stickyRowsBottomInset;
    BOOL stickyRowBottomDisabled;
    
    EGORefreshTableHeaderView* refreshView;
    
    id<MPLTableViewDelegate> listingDelegate;
    id<MPLTableViewRefreshDelegate> refreshDelegate;
    
    
@public BOOL isRefreshing;
    BOOL animationFinished;
    BOOL isEnabled;
@public BOOL isReloading;
@public CGPoint targetContentsOffset;
@public float stickyRowsSpacing;
    
    id<MPLScrollViewDragHideDelegate> dragHideDelegate;
    float drag_hide_start_state;
    float drag_hide_start_location;
    float currentTopFix;
    float currentBottomFix;

}




@property (nonatomic, assign) UITableViewStyle tableViewStyle;
@property (nonatomic, assign) UITableViewCellAccessoryType accessoryType;
@property (nonatomic, assign) UITableViewCellSelectionStyle selectionStyle;
@property (nonatomic, assign) BOOL hideEmptySections;
@property (nonatomic, assign) BOOL autoSnapsToRow;

@property (nonatomic, assign) UIEdgeInsets cellSeparatorInsets;

@property (nonatomic, strong) NSMutableArray* allSections;
@property (weak, nonatomic, readonly) MPLTableViewSection* defaultSection;
@property (weak, nonatomic, readonly) NSMutableArray* sections;

@property (nonatomic, strong) NSMutableArray* stickyRowsIndexPaths;
@property (nonatomic, strong) NSMutableDictionary* stickyRowsIdentifierViews;
@property (nonatomic, strong) NSMutableDictionary* stickyRowsIndexPathView;
@property (nonatomic, strong) NSMutableDictionary* stickyRowsIndexedPreviousIndexPaths;
@property (nonatomic, assign) float stickyRowsTopInset;
@property (nonatomic, assign) float stickyRowsBottomInset;
@property (nonatomic, assign) BOOL stickyRowBottomDisabled;

@property (nonatomic, strong) EGORefreshTableHeaderView* refreshView;

@property (nonatomic, strong) id<MPLScrollViewDragHideDelegate> dragHideDelegate;
@property (nonatomic, strong) id<MPLTableViewDelegate> listingDelegate;
@property (nonatomic, strong) id refreshDelegate;

@property (weak, nonatomic, readonly) BVReorderTableView* tableView;

@property (nonatomic, strong) NSMutableDictionary* cellPool;




@property (nonatomic, copy) void (^didScrollTo)(float scrolledTo);
@property (nonatomic, copy) void (^scrollEnded)(void);
@property (nonatomic, copy) BOOL (^canMoveRow)(MPLTableViewSection* section, NSInteger row, id data, NSString* identifier);
@property (nonatomic, copy) void (^didMoveRow)(NSIndexPath* fromIndexPath, NSIndexPath* toIndexPath);
@property (nonatomic, copy) void (^moveStarted)(MPLTableViewSection* section, NSInteger row, id data, NSString* identifier);
@property (nonatomic, copy) void (^moveFinished)(MPLTableViewSection* section, NSInteger row, id data, NSString* identifier);
@property (nonatomic, copy) void (^didEndDragging)(float scrolledTo, BOOL decelerate);
@property (nonatomic, copy) void (^didEndDecelerating)(float scrolledTo);
@property (nonatomic, copy) void (^didEndScrollingWithTargetOffset)(CGPoint* scrolledTo);
@property (nonatomic, copy) void (^stickyRowMoved)(NSString *identifier, MPLTableViewSection *section, NSInteger row, id data, UITableViewCell *cell, MPLView * cellView, CGRect frame, float state);

- (instancetype) assignDidScrollTo:(void (^)(float scrolledTo))didScrollTo;
- (instancetype) assignCanMoveRow:(BOOL (^)(MPLTableViewSection* section, NSInteger row, id data, NSString* identifier))canMoveRow;
- (instancetype) assignDidMoveRow:(void (^)(NSIndexPath* fromIndexPath, NSIndexPath* toIndexPath))didMoveRow;
- (instancetype) assignScrollEnded:(void (^)(void))scrollEnded;
- (instancetype) assignMoveStarted:(void (^)(MPLTableViewSection* section, NSInteger row, id data, NSString* identifier))moveStarted;
- (instancetype) assignMoveFinished:(void (^)(MPLTableViewSection* section, NSInteger row, id data, NSString* identifier))moveFinished;
- (instancetype) assignDidEndDragging:(void (^)(float scrolledTo, BOOL decelerate))didEndDragging;
- (instancetype) assignDidEndDecelerating:(void (^)(float scrolledTo))didEndDecelerating;
- (instancetype) assignStickyRowMoved:(void (^)(NSString *identifier, MPLTableViewSection *section, NSInteger row, id data, UITableViewCell *cell, MPLView * cellView, CGRect frame, float state))stickyRowMoved;





- (void) toggleExpansionForData:(id)v row:(NSInteger)row identifiers:(NSArray*)expandableIdentifiers simultaniously:(BOOL)simultaniously finished:(void(^)(void))finished;

- (int) calculateTargetIndexForCurrentOffset:(float)py andQuantifiedWidth:(float)ww andSkipHeight:(float)skipH;
- (float) calculateTargetOffsetForCurrentOffset:(float)py andQuantifiedWidth:(float)ww andSkipHeight:(float)skipH;

+ (MPLTableView*) create:(void(^)(MPLTableView* v))creater;
- (void) loaderWithDelegate:(id<MPLTableViewDelegate>)delegate
             andCellsInsets:(UIEdgeInsets)insets
                 andSpacing:(float)spacing
;
- (void) loaderWithCellsInsets:(UIEdgeInsets)insets
                    andSpacing:(float)spacing
;

- (void) loader;
- (void) loadWithTableViewStyle:(UITableViewStyle)tableViewStyle
               andAccessoryType:(UITableViewCellAccessoryType)accesoryType
              andSelectionStyle:(UITableViewCellSelectionStyle)selectionStyle
                    andDelegate:(id<MPLTableViewDelegate>)ldelegate
;

- (NSIndexPath*) indexPathForData:(id)data;
- (void) reloadVisibleCells;
- (void) reloadData;

- (void) snapToRow;
- (void) stopScrolling;
- (void) update:(BOOL)requireData;

- (void) assignCellSeparatorInsets:(UIEdgeInsets)insets;

- (NSIndexPath*) indexPathForHeaderBeforeIndexPath:(NSIndexPath*)indexPath;
- (NSIndexPath*) indexPathForHeaderAfterHeaderIndexPath:(NSIndexPath*)indexPath;
- (void) reloadStickyRows;
- (void) stickyRowNavigationForIndexPath:(NSIndexPath*)ip withOffset:(float)scrollOffset andReady:(void(^)(BOOL))readyBlock;

- (MPLTableViewSection*) sectionAtIndex:(NSInteger)index;
- (void) addSectionWithSectionData:(id)data andCellsInsets:(UIEdgeInsets)insets andSpacing:(float)spacingSize;

- (void) scrollToIndexPath:(NSIndexPath*)indexPath withOffset:(float)offset animated:(BOOL)animated withFinished:(void(^)(void))finished;
- (void) scrollToOffset:(CGPoint)offset animated:(BOOL)animated withFinished:(void(^)(void))finished;
- (void) scrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)position animated:(BOOL)animated withFinished:(void(^)(void))finished;


- (void) enablePullToRefreshWithDelegate:(id<MPLTableViewRefreshDelegate>)delegate andHeight:(float)height andOffset:(float)offset andLoadingOffset:(float)loadingOffset andBackgroundColor:(UIColor*)backgroundColor andTextColor:(UIColor*)textColor andArrowImage:(UIImage*)arrowImage
              andPullDownToRefreshString:(NSString*)pullDownToRefresh
               andReleaseToRefreshString:(NSString*)releaseToRefresh
                        andLoadingString:(NSString*)loading
;
- (void) stopRefreshing;

- (void) animatePopForState:(float)visibilityState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)r;
- (void) animateFlipFromLeftForState:(float)visibilityState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)r;
- (void) animateFlipFromRightForState:(float)visibilityState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)r;
- (void) animateSlideFromLeftForState:(float)visibilityState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)r;
- (void) animateSlideFromRightForState:(float)visibilityState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)r;
- (float) showListWithDurationStart:(float)durBase andDurationStep:(float)durInt reversed:(BOOL)reversed delay:(BOOL)delay andBounce:(float)bounce withFinished:(void (^)(void))finishedBlock;
- (float) hideListWithDurationStart:(float)durBase andDurationStep:(float)durInt reversed:(BOOL)reversed delay:(BOOL)delay andBounce:(float)bounce withFinished:(void (^)(void))finishedBlock;

- (NSString*) cellIdentifierForSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data;
- (void) generateCellPoolForIdentifier:(MPLTableViewIdentifier*)identifier count:(int)count;

- (BOOL) section:(MPLTableViewSection*)section isHeaderData:(id)data forIdentifier:(NSString*)identifier;
- (float) heightForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data;
- (MPLView*) cellForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andParent:(UIView*)pview andFrame:(CGRect)rect andCell:(UITableViewCell*)cell;

- (void) foreachVisibleRow:(void(^)(MPLTableViewSection* section,UITableViewCell* cell,MPLView* cellView,id data,NSInteger row,NSInteger dataIndex))enumerator;

- (void) hideBottomWithHeight:(float)height;
- (void) setTopInset:(float)topInset andBottomInset:(float)bottomInset;

@property (nonatomic, assign) float lastSpeedX;
@property (nonatomic, assign) float lastSpeedY;
@property (nonatomic, assign) float lastOffsetX;
@property (nonatomic, assign) float lastOffsetY;
@property (nonatomic, assign) NSTimeInterval lastOffsetCapture;
- (float) calculateSpeedX;
- (float) calculateSpeedY;
- (void) resetSpeedCalculations;

@end
