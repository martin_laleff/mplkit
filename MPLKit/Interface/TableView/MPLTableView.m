//
//  FTTableView.m
//  FTrack_Take2
//
//  Created by Martin Lalev on 9/22/11.
//  Copyright 2011 mART. All rights reserved.
//

#import "MPLTableView.h"
#import "MPLPanel.h"
#import <QuartzCore/QuartzCore.h>




@implementation MPLTableViewGridRowDataWrapper

- (id) initWithMaxObjects:(int)maxObjects {
    if ([super init]) { self.objects = [NSMutableArray array]; self.maxObjects = maxObjects; }
    return self;
}
- (BOOL) addItem:(id)item {
    [self.objects addObject:item];
    return self.objects.count == self.maxObjects;
}

+ (NSMutableArray*) generateGridListContainersForItems:(NSArray*)_items maxObjects:(int)maxObjects {
    NSMutableArray* items = [NSMutableArray arrayWithArray:_items];
    NSMutableArray* result = [NSMutableArray array];
    MPLTableViewGridRowDataWrapper* rd = [[MPLTableViewGridRowDataWrapper alloc] initWithMaxObjects:maxObjects];
    while (items.count > 0) {
        if ([rd addItem:[items firstObject]]) {
            [result addObject:rd];
            rd = [[MPLTableViewGridRowDataWrapper alloc] initWithMaxObjects:maxObjects];
        }
        [items removeObjectAtIndex:0];
    }
    if (rd.objects.count > 0) {
        [result addObject:rd];
    }
    //    [result insertObject:@"LOGO" atIndex:0];
    return result;
}


@end





@implementation MPLTableViewExpandableCellWraper

- (void) loadHeightState:(NSInteger)heightState atRow:(NSInteger)row {
    self.heightState = heightState;
//    self.heightFrame = [self.identifier heightForHeightState:heightState atRow:row];
}
- (void) loadForRow:(NSInteger)row {
    self.row = row;
    [self loadHeightState:[self.identifier heightStateForRow:self.row] atRow:row];
}

@end

@implementation MPLTableViewIdentifier

- (void) destroyIdentifier {
    self.heightStates = nil;
    self.rowHeightStates = nil;
    self.identifier = nil;
    self.section = nil;
    self.cellCreated = nil;
    self.getHeight = nil;
    self.reloadCell = nil;
    self.isHeader = nil;
    self.dataBelongs = nil;
    self.heightStateChanged = nil;
}



- (instancetype) assignPanelFactory:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame, MPLPanel* p))cellCreated {
    return [self assignViewFactory:^MPLView *(MPLTableViewIdentifier *section, NSInteger row, id data, UITableViewCell *cell, CGRect frame) {
        return [MPLPanel create:^(MPLPanel* v) {
            v.frame = frame;
            cellCreated(section,row,data,cell,frame,v);
        }];
    }];
}
- (instancetype) assignPanelReloader:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLPanel* cellView))reloadCell {
    return [self assignReloader:^(MPLTableViewIdentifier *section, NSInteger row, id data, UITableViewCell *cell, MPLView *cellView) {
        reloadCell(section,row,data,cell,(MPLPanel*)cellView);
    }];
}
- (instancetype) assignViewFactory:(MPLView* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated { self.cellCreated = cellCreated; return self; }
- (instancetype) assignCustomHeight:(float (^)(MPLTableViewIdentifier* section, NSInteger row, id data))getHeight { self.getHeight = getHeight; return self; }
- (instancetype) assignStaticHeight:(float)height { return [self assignCustomHeight:[MPLTableViewIdentifier height:height]]; }
- (instancetype) assignStateHeight {
    self.heightStates = [NSMutableDictionary dictionary];
    self.heightStateChanged = ^(MPLTableViewIdentifier *section, NSDictionary *rowsHeightStates) {};
    return [self assignCustomHeight:^float(MPLTableViewIdentifier *section, NSInteger row, id data) { return [section heightForRow:row]; }]; }
- (instancetype) assignHeight:(float (^)(MPLTableViewIdentifier* section, NSInteger row, id data))getHeight forState:(int)state {
    float(^handlerCopy)(MPLTableViewIdentifier *section, NSInteger row, id data) = [getHeight copy];
    self.heightStates[@(state)] = handlerCopy;
    return self;
}

- (instancetype) dataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs { self.dataBelongs = dataBelongs; return self; }
- (instancetype) dataIsClass:(Class)cl { return [self dataBelongs:[MPLTableViewIdentifier dataIsClass:cl]]; }
- (instancetype) dataIsString:(NSString*)s { return [self dataBelongs:[MPLTableViewIdentifier dataIsString:s]]; }
- (instancetype) dataStartsWithString:(NSString*)s { return [self dataBelongs:[MPLTableViewIdentifier dataStartsWithString:s]]; }
- (instancetype) dataIsString:(NSString*)s separator:(NSString*)sep index:(int)index { return [self dataBelongs:[MPLTableViewIdentifier dataIsString:s separator:sep index:index]]; }
- (instancetype) assignAsHeader { self.isHeader = [MPLTableViewIdentifier identifierIsHeader]; return self; }
- (instancetype) assignReloader:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell { self.reloadCell = reloadCell; return self; }


+ (MPLTableViewIdentifier*) identifierForString:(NSString*)identifier andSection:(MPLTableViewSection*)section {
    MPLTableViewIdentifier* tvi = [[MPLTableViewIdentifier alloc] init];
    tvi.identifier = identifier;
    tvi.section = section;
    [tvi.section.identifierDescriptors setObject:tvi forKey:identifier];
    return tvi;
}
+ (MPLTableViewIdentifier*) identifierFor:(NSString*)identifier
                               andSection:(MPLTableViewSection*)section
                           andCellCreated:(MPLTableViewExpandableCellWraper* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated
                          andNormalHeight:(float(^)(MPLTableViewIdentifier *section, NSInteger row, id data))normalHeight
                        andExpandedHeight:(float(^)(MPLTableViewIdentifier *section, NSInteger row, id data))expandedHeight
                            andReloadCell:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell
                              andIsHeader:(BOOL (^)(MPLTableViewIdentifier* section, id data))isHeader
                           andDataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs
                    andHeightStateChanged:(void (^)(MPLTableViewIdentifier* section, NSDictionary* rowsHeightStates))heightStateChanged
{
    NSMutableDictionary* hs = [NSMutableDictionary dictionary];
    
    float(^handlerCopy)(MPLTableViewIdentifier *section, NSInteger row, id data) = [expandedHeight copy];
    hs[@(1)] = handlerCopy;
    

    MPLTableViewIdentifier* i = [MPLTableViewIdentifier identifierFor:identifier andSection:section
                                  andCellCreated:cellCreated
                                 andHeightStates:hs
                           andDefaultStateHeight:normalHeight
                                   andReloadCell:reloadCell
                                     andIsHeader:isHeader
                                  andDataBelongs:dataBelongs
                           andHeightStateChanged:heightStateChanged
            ];
    
    return i;
}
+ (MPLTableViewIdentifier*) identifierFor:(NSString*)identifier
                               andSection:(MPLTableViewSection*)section
                           andCellCreated:(MPLTableViewExpandableCellWraper* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated
                          andHeightStates:(NSDictionary*)heightStates
                    andDefaultStateHeight:(float(^)(MPLTableViewIdentifier *section, NSInteger row, id data))defaultHeight
                            andReloadCell:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell
                              andIsHeader:(BOOL (^)(MPLTableViewIdentifier* section, id data))isHeader
                           andDataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs
                    andHeightStateChanged:(void (^)(MPLTableViewIdentifier* section, NSDictionary* rowsHeightStates))heightStateChanged
{
    MPLTableViewIdentifier* i =
    [MPLTableViewIdentifier identifierFor:identifier andSection:section
                           andCellCreated:^MPLView *(MPLTableViewIdentifier *section, NSInteger row, id data, UITableViewCell *cell, CGRect frame) {
                               MPLTableViewExpandableCellWraper* ecw = (MPLTableViewExpandableCellWraper*)cellCreated(section,row,data,cell,frame);
                               ecw.identifier = section;
                               return ecw;
                           }
                             andGetHeight:^float(MPLTableViewIdentifier *section, NSInteger row, id data) {
                                 return [section heightForRow:row];
                             }
                            andReloadCell:^(MPLTableViewIdentifier *section, NSInteger row, id data, UITableViewCell *cell, MPLView *cellView) {
                                [(MPLTableViewExpandableCellWraper*)cellView loadForRow:row];
                                reloadCell(section,row,data,cell,cellView);
                            }
                              andIsHeader:isHeader
                           andDataBelongs:dataBelongs
     ];
    i.heightStateChanged = heightStateChanged;
    NSMutableDictionary* hs = [NSMutableDictionary dictionary];
    [hs addEntriesFromDictionary:heightStates];
    
    float(^handlerCopy)(MPLTableViewIdentifier *section, NSInteger row, id data) = [defaultHeight copy];
    hs[@(0)] = handlerCopy;
    
    i.heightStates = hs;
    return i;
}
+ (MPLTableViewIdentifier*) identifierFor:(NSString*)identifier
                               andSection:(MPLTableViewSection*)section
                           andCellCreated:(MPLView* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated
                             andGetHeight:(float (^)(MPLTableViewIdentifier* section, NSInteger row, id data))getHeight
                            andReloadCell:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell
                              andIsHeader:(BOOL (^)(MPLTableViewIdentifier* section, id data))isHeader
                           andDataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs
{
    MPLTableViewIdentifier* tvi = [MPLTableViewIdentifier identifierForString:identifier andSection:section];
    tvi.cellCreated = cellCreated;
    tvi.getHeight = getHeight;
    tvi.reloadCell = reloadCell;
    tvi.isHeader = isHeader;
    tvi.dataBelongs = dataBelongs;
    tvi.rowHeightStates = [NSMutableDictionary dictionary];
    return tvi;
}
- (float)heightForHeightState:(NSInteger)heightState atRow:(NSInteger)row {
    float(^ghs)(MPLTableViewIdentifier *section, NSInteger row, id data) = self.heightStates[@(heightState)];
    return ghs(self,row,[self.section->cleanRowData objectAtIndex:[self.section cleanDataIndexForRow:row]]);
}
- (NSInteger) heightStateForRow:(NSInteger)row { return [[self.rowHeightStates objectForKey:@(row)] intValue]; }
- (float) heightForRow:(NSInteger)row { return [self heightForHeightState:[self heightStateForRow:row] atRow:row]; }
- (void) setRowsHeightStates:(NSDictionary*)rowsHeightStates andFinished:(void(^)(void))finishedBlock {
    
    for (NSNumber* row in rowsHeightStates.allKeys) {
        if ([[rowsHeightStates objectForKey:row] intValue] == 0)
            [self.rowHeightStates removeObjectForKey:row];
        else
            [self.rowHeightStates setObject:[rowsHeightStates objectForKey:row] forKey:row];
    }
    
    NSMutableArray* rowViews = [NSMutableArray array];
    for (NSNumber* row in rowsHeightStates.allKeys) {
        UITableViewCell* cell = [self.section.tableView.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[row intValue] inSection:[self.section.tableView.sections indexOfObject:self.section]]];
        MPLTableViewExpandableCellWraper* cellView = (MPLTableViewExpandableCellWraper*)[self.section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
        if (cellView != nil) [rowViews addObject:cellView];
    }
    
    if (rowViews.count == 0) {
        finishedBlock();
        return;
    }
    
    [UIView animateWithDuration:.30 delay:0 options:0 animations:^{
        for (MPLTableViewExpandableCellWraper* cellWraper in rowViews)
            [cellWraper loadHeightState:[[rowsHeightStates objectForKey:@(cellWraper.row)] intValue] atRow:cellWraper.row];
    } completion:^(BOOL finished) {
    }];
    [self.section.tableView update:YES];
    [UIView animateWithDuration:.30 delay:0 options:0 animations:^{
        [self.section.tableView reloadStickyRows];
        self.heightStateChanged(self,rowsHeightStates);
    } completion:^(BOOL finished) {
        finishedBlock();
    }];
}
- (void) setRow:(NSInteger)row heightState:(NSInteger)heightState andFinished:(void(^)(void))finishedBlock {
    [self setRowsHeightStates:@{@(row):@(heightState)} andFinished:finishedBlock];
}
- (void) toggleBinaryHeightStateForRow:(MPLTableViewExpandableCellWraper*)v {
    [self setRow:v.row heightState:1-v.heightState andFinished:^{}];
}
- (void) toggleSingleBinaryHeightStateForRow:(MPLTableViewExpandableCellWraper*)v simultaniously:(BOOL)simultaniously {
    if (self.rowHeightStates.count > 0) {
        if ([self.rowHeightStates.allKeys[0] intValue] == v.row) {
            [self setRow:v.row heightState:0 andFinished:^{}];
        }
        else {
            if (simultaniously) {
                [self setRowsHeightStates:@{self.rowHeightStates.allKeys[0]:@(0),@(v.row):@(1)} andFinished:^{}];
            }
            else {
                __weak MPLTableViewIdentifier* weakSelf = self;
                [self setRow:[self.rowHeightStates.allKeys[0] intValue] heightState:0 andFinished:^{
                    [weakSelf setRow:v.row heightState:1 andFinished:^{}];
                }];
            }
        }
    }
    else
        [self setRow:v.row heightState:1-v.heightState andFinished:^{}];
}
- (void) setAllRowsToDefaultHeightState {
    NSMutableDictionary* rhs = [NSMutableDictionary dictionary];
    for (NSNumber* row in self.rowHeightStates.allKeys) {
        [rhs setObject:@(0) forKey:row];
    }
    [self setRowsHeightStates:rhs andFinished:^{}];
}

+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) identifierIsHeader { return ^BOOL(MPLTableViewIdentifier *section, id data) { return YES; }; }
+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) identifierNotHeader { return ^BOOL(MPLTableViewIdentifier *section, id data) { return NO; }; }
+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) dataIsClass:(Class)cl { return [^BOOL(MPLTableViewIdentifier *section, id data) { return [data isKindOfClass:cl]; } copy]; }
+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) dataIsString:(NSString*)s { return [^BOOL(MPLTableViewIdentifier *section, id data) { return [data isKindOfClass:[NSString class]] && [((NSString*)data) isEqualToString:s]; } copy]; }
+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) dataStartsWithString:(NSString*)s { return [^BOOL(MPLTableViewIdentifier *section, id data) {
    if ([data isKindOfClass:[NSString class]]) {
        NSString* str = (NSString*)data;
        if (str.length >= s.length)
            if ([[str substringToIndex:s.length] isEqualToString:s])
                return YES;
    }
    return NO;
} copy];}
+ (BOOL(^)(MPLTableViewIdentifier* section, id data)) dataIsString:(NSString*)s separator:(NSString*)sep index:(int)index { return [^BOOL(MPLTableViewIdentifier *section, id data) { return [data isKindOfClass:[NSString class]] && [[[((NSString*)data) componentsSeparatedByString:sep] objectAtIndex:index] isEqualToString:s]; } copy]; }
+ (float(^)(MPLTableViewIdentifier* section, NSInteger row, id data)) height:(float)h { return [^float(MPLTableViewIdentifier* section, NSInteger row, id data) { return h; } copy]; }
+ (void(^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView)) emptyReload { return ^(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView) {}; }

@end




@implementation MPLTableViewSection

- (void) destroySection {
    self.sectionData = nil;
    self.sectionRowsData = nil;
    self.stickyRowsIndexPathsSections = nil;
    for (MPLTableViewIdentifier* identifier in self.identifierDescriptors.allValues)
        [identifier destroyIdentifier];
    self.identifierDescriptors = nil;

    for (MPLView* v in self.cellViews.allValues) {
        [v destroyView];
    }
    self.cellViews = nil;
}

- (MPLTableViewIdentifier*) identifierForKey:(NSString*)key { return ((MPLTableViewIdentifier*)[self.identifierDescriptors objectForKey:key]); }

- (NSInteger) sectionIndex { return [self.tableView.sections indexOfObject:self]; }

- (NSInteger) cleanDataIndexForIndexPath:(NSIndexPath*)indexPath {
    NSInteger row = indexPath.row;
    if (row == 0) {
        // top inset
        return -1;
    }
    if (row == self.sectionRowsData.count-1) {
        // bottom inset
        return -1;
    }
    row--;
    if (self.spacing > 0) {
        if (row%2 == 1) {
            // spacing
            return -1;
        }
        else {
            return row/2;
        }
    }
    else {
        return row;
    }
}
- (NSIndexPath*) indexPathForCleanDataIndex:(NSInteger)row {
    return [self indexPathForRow:1+row*(self.spacing > 0 ? 2 : 1)];
}

// Row = cell row
// Index = data index
- (NSIndexPath*) indexPathForRow:(NSInteger)row { return [NSIndexPath indexPathForRow:(row < 0 ? 0 : row) inSection:[self sectionIndex]]; }
- (NSInteger) cleanDataIndexForRow:(NSInteger)row {
    return [self cleanDataIndexForIndexPath:[self indexPathForRow:row]];
}
- (UITableViewCell*) cellForRow:(NSInteger)row {
    return [self.tableView.tableView cellForRowAtIndexPath:[self indexPathForRow:row]];
}
- (MPLView*) cellViewForRow:(NSInteger)row {
    return [self.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)[self cellForRow:row]]];
}

- (NSInteger) rowForCleanDataIndex:(NSInteger)index { return [self indexPathForCleanDataIndex:index].row; }



- (MPLView*) cellViewForCell:(UITableViewCell*)cell {
    return [self.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
}
- (UITableViewCell*) cellForCellView:(MPLView*)cellView {
    UIView* v = cellView.view;
    while (v != nil) {
        if ([v isKindOfClass:[UITableViewCell class]]) return (UITableViewCell*)v;
        v = v.superview;
    }
    return nil;
}


- (NSInteger) cleanDataIndexForData:(id)data {
    return [self->cleanRowData indexOfObject:data];
}
- (NSInteger) rowForData:(id)data {
    return [self rowForCleanDataIndex:[self cleanDataIndexForData:data]];
}
- (UITableViewCell*) cellForData:(id)data {
    return [self cellForRow:[self rowForData:data]];
}
- (MPLView*) cellViewForData:(id)data {
    return [self cellViewForRow:[self rowForData:data]];
}


- (void) reloadStickyRows {
    id previousSRIP = [NSNull null];
    if ([self sectionIndex] > 0) {
        MPLTableViewSection* s = [self.tableView sectionAtIndex:[self sectionIndex]-1];
        if (s.stickyRowsIndexPathsSections.count > 0) previousSRIP = [s.stickyRowsIndexPathsSections lastObject];
    }
    
    [self.tableView.stickyRowsIndexPaths removeObjectsInArray:self.stickyRowsIndexPathsSections];
    [self.stickyRowsIndexPathsSections removeAllObjects];
    
    NSInteger i = 1;
    for (id obj in self->cleanRowData) {
        
        NSString* identifier = [self.tableView cellIdentifierForSection:self andRow:i andData:obj];
        
        [self.tableView.stickyRowsIndexedPreviousIndexPaths setObject:previousSRIP forKey:[self indexPathForRow:i]];
        
        if ([self.tableView section:self isHeaderData:obj forIdentifier:identifier]) {
            [self.stickyRowsIndexPathsSections addObject:[self indexPathForRow:i]];
            previousSRIP = [self indexPathForRow:i];
        }
        if (self.spacing > 0)
            [self.tableView.stickyRowsIndexedPreviousIndexPaths setObject:previousSRIP forKey:[self indexPathForRow:i+1]];
        
        i+=(self.spacing > 0 ? 2 : 1);
    }
    
    if ([self sectionIndex] < self.tableView.sections.count - 1) {
        MPLTableViewSection* s = [self.tableView sectionAtIndex:[self sectionIndex]+1];
        for (NSInteger j = 0; j < s.sectionRowsData.count; j++) {
            id obj = [s.sectionRowsData objectAtIndex:j];
            NSString* identifier = [self.tableView cellIdentifierForSection:self andRow:j andData:obj];
            
            if ([self.tableView section:self isHeaderData:obj forIdentifier:identifier])
                break;
            
            if (self.spacing > 0)
                [self.tableView.stickyRowsIndexedPreviousIndexPaths setObject:previousSRIP forKey:[s indexPathForRow:j]];
        }
    }
    
    [self.tableView.stickyRowsIndexPaths addObjectsFromArray:self.stickyRowsIndexPathsSections];
}

- (NSArray*) deleteDataAndGetRowsForObjectsFromIndex:(NSInteger)indexFrom toIndex:(NSInteger)indexTo andDataOffset:(NSInteger)offset {
    NSMutableArray* indexPathsToRemove = [NSMutableArray array];
    indexFrom-=offset;
    indexTo-=offset;
    
    if (indexFrom < 0) indexFrom = 0;
    if (indexTo > self->cleanRowData.count) indexTo = self->cleanRowData.count;
    
    indexTo--;
    
    
    NSInteger indexPathRow = [self indexPathForCleanDataIndex:indexFrom].row;
    NSInteger indexPathRowOffset = [self indexPathForCleanDataIndex:indexFrom+offset].row;
    
    for (NSInteger i = 0; i <= indexTo-indexFrom; i++) {
        NSInteger ipr = indexPathRowOffset+i*(self.spacing > 0 ? 2 : 1);
        
        [self->cleanRowData removeObjectAtIndex:indexFrom];
        [self.sectionRowsData removeObjectAtIndex:indexPathRow];
        [indexPathsToRemove addObject:[self indexPathForRow:ipr]];
        
        if (self.spacing > 0) {
            if (indexFrom != self->cleanRowData.count) {
                [self.sectionRowsData removeObjectAtIndex:indexPathRow];
                [indexPathsToRemove addObject:[self indexPathForRow:ipr+1]];
            }
            else if (indexFrom != 0) {
                indexPathRow--;
                [self.sectionRowsData removeObjectAtIndex:indexPathRow];
                [indexPathsToRemove addObject:[self indexPathForRow:indexPathRow]];
            }
        }
    }
    return indexPathsToRemove;
}
- (void) deleteObjectsFromIndex:(NSInteger)indexFrom toIndex:(NSInteger)indexTo animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock {
    NSArray* indexPathsToRemove = [self deleteDataAndGetRowsForObjectsFromIndex:indexFrom toIndex:indexTo andDataOffset:0];
    
    self.tableView->isReloading = YES;
    if (animated)
        [self.tableView.tableView deleteRowsAtIndexPaths:indexPathsToRemove withRowAnimation:animated ? UITableViewRowAnimationFade : UITableViewRowAnimationNone];
    else
        [self.tableView.tableView reloadData];
    
    [self reloadStickyRows];
    
    [UIView animateWithDuration:(animated ? .35 : 0) animations:^{
        self.tableView->isReloading = NO;
        [self.tableView reloadStickyRows];
        [self.tableView reloadVisibleCells];
    } completion:^(BOOL finished) {
        finishedBlock();
    }];
}
- (void) deleteObjectAtIndex:(NSInteger)index animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock { [self deleteObjectsFromIndex:index toIndex:index+1 animated:animated andFinished:finishedBlock]; }
- (void) deleteObjectsFromIndex:(NSInteger)index animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock { [self deleteObjectsFromIndex:index toIndex:self->cleanRowData.count animated:animated andFinished:finishedBlock]; }
- (void) deleteAllObjectsAnimated:(BOOL)animated andFinished:(void(^)(void))finishedBlock { [self deleteObjectsFromIndex:0 toIndex:self->cleanRowData.count animated:animated andFinished:finishedBlock]; }
- (void) deleteObjectsAtIndices:(NSArray*)indices animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock {
    NSMutableArray* indexPathsToRemove = [NSMutableArray array];
    
    indices = [indices sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSNumber* n1 = (NSNumber*)obj1;
        NSNumber* n2 = (NSNumber*)obj2;
        return [n1 compare:n2];
    }];
    if (indices.count == 0) {
        finishedBlock();
        return;
    }
    if (indices.count == 1) {
        [self deleteObjectAtIndex:[[indices objectAtIndex:0] intValue] animated:animated andFinished:finishedBlock];
        return;
    }
    
    
    NSInteger rc = 0;
    NSInteger from = [[indices objectAtIndex:0] intValue];
    NSInteger last = from;
    for (NSInteger i = 1; i < indices.count; i++) {
        NSInteger index = [[indices objectAtIndex:i] intValue];
        last++;
        if (last == index) {
            if (i < indices.count - 1) continue;
            last++;
            [indexPathsToRemove addObjectsFromArray:[self deleteDataAndGetRowsForObjectsFromIndex:from toIndex:last andDataOffset:rc]];
            rc += last-from;
        }
        else {
            [indexPathsToRemove addObjectsFromArray:[self deleteDataAndGetRowsForObjectsFromIndex:from toIndex:last andDataOffset:rc]];
            rc += last-from;
            from = index;
            last = from;
            if (i == indices.count - 1) {
                last++;
                [indexPathsToRemove addObjectsFromArray:[self deleteDataAndGetRowsForObjectsFromIndex:from toIndex:last andDataOffset:rc]];
            }
        }
    }
    
    self.tableView->isReloading = YES;
    if (animated)
        [self.tableView.tableView deleteRowsAtIndexPaths:indexPathsToRemove withRowAnimation:animated ? UITableViewRowAnimationFade : UITableViewRowAnimationNone];
    else
        [self.tableView.tableView reloadData];
    
    [self reloadStickyRows];
    
    [UIView animateWithDuration:(animated ? .5 : 0) animations:^{
        self.tableView->isReloading = NO;
        [self.tableView reloadStickyRows];
        [self.tableView reloadVisibleCells];
    } completion:^(BOOL finished) {
        finishedBlock();
    }];
}

- (void) insertObjects:(NSArray*)objects toIndex:(NSInteger)index animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock {
    NSMutableArray* indexPathsToAdd = [NSMutableArray array];
    if (index > self->cleanRowData.count) index = self->cleanRowData.count;
    
    void (^addObject)(id obj, NSInteger ind) = ^(id obj, NSInteger ind){
        [self.sectionRowsData insertObject:obj atIndex:ind];
        [indexPathsToAdd addObject:[self indexPathForRow:ind]];
    };
    for (NSInteger i = 0; i < objects.count; i++) {
        id object = [objects objectAtIndex:i];
        NSInteger cellIndex = 1 + index * (self.spacing > 0 ? 2 : 1);
        
        if (self->cleanRowData.count == index) {
            [self->cleanRowData addObject:object];
            
            if (self.spacing > 0 && index > 0) cellIndex--;
            if (self.spacing > 0 && self->cleanRowData.count > 1) { addObject(@(self.spacing),cellIndex); cellIndex++; }
            
            addObject(object,cellIndex);
        }
        else {
            [self->cleanRowData insertObject:object atIndex:index];
            
            addObject(object, cellIndex);
            if (self.spacing > 0) addObject(@(self.spacing), cellIndex+1);
            
        }
        
        index++;
    }
    
    self.tableView->isReloading = YES;
    
    if (animated)
        [self.tableView.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:animated ? UITableViewRowAnimationFade : UITableViewRowAnimationNone];
    else
        [self.tableView.tableView reloadData];
    
    [self reloadStickyRows];
    
    [UIView animateWithDuration:(animated ? .5 : 0) animations:^{
        self.tableView->isReloading = NO;
        [self.tableView reloadStickyRows];
        [self.tableView reloadVisibleCells];
    } completion:^(BOOL finished) {
//        finishedBlock();
    }];
    if (animated) [MPLAction performAfter:.5 block:^{ finishedBlock(); }];
    else finishedBlock();
}
- (void) addObjects:(NSArray*)objects animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock { [self insertObjects:objects toIndex:self->cleanRowData.count animated:animated andFinished:finishedBlock]; }

- (void) insertObject:(id)object toIndex:(NSInteger)index animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock { [self insertObjects:@[object] toIndex:index animated:animated andFinished:finishedBlock]; }
- (void) addObject:(id)object animated:(BOOL)animated andFinished:(void(^)(void))finishedBlock { [self addObjects:@[object] animated:animated andFinished:finishedBlock]; }




+ (MPLTableViewSection*) sectionWithSectionData:(id)data andCellsInsets:(UIEdgeInsets)insets andSpacing:(float)spacingSize {
    MPLTableViewSection* section = [[MPLTableViewSection alloc] init];
    section->cleanRowData = [NSMutableArray array];
    section.sectionData = data;
    section.sectionRowsData = [NSMutableArray arrayWithObjects:@(insets.top),@(insets.bottom), nil];
    section.cellsInsets = insets;
    section.spacing = spacingSize;
    section.cellViews = [NSMutableDictionary dictionary];
    section.stickyRowsIndexPathsSections = [NSMutableArray array];
    section.identifierDescriptors = [NSMutableDictionary dictionary];
    return section;
}

- (MPLTableViewIdentifier*) addIdentifierFor:(NSString*)identifier
                              andCellCreated:(MPLView* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated
                                andGetHeight:(float (^)(MPLTableViewIdentifier* section, NSInteger row, id data))getHeight
                               andReloadCell:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell
                                 andIsHeader:(BOOL (^)(MPLTableViewIdentifier* section, id data))isHeader
                              andDataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs
{ return [MPLTableViewIdentifier identifierFor:identifier andSection:self andCellCreated:cellCreated andGetHeight:getHeight andReloadCell:reloadCell andIsHeader:isHeader andDataBelongs:dataBelongs]; }
- (MPLTableViewIdentifier*) addIdentifier:(MPLView* (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, CGRect frame))cellCreated
                             andGetHeight:(float (^)(MPLTableViewIdentifier* section, NSInteger row, id data))getHeight
                            andReloadCell:(void (^)(MPLTableViewIdentifier* section, NSInteger row, id data, UITableViewCell* cell, MPLView* cellView))reloadCell
                              andIsHeader:(BOOL (^)(MPLTableViewIdentifier* section, id data))isHeader
                           andDataBelongs:(BOOL (^)(MPLTableViewIdentifier* section, id data))dataBelongs
{
    NSMutableString* identifier = [NSMutableString string];
    for (int i = 0; i < 10; i++) [identifier appendFormat:@"%c",'a'+arc4random()%('z'-'a')];
    return [self addIdentifierFor:identifier andCellCreated:cellCreated andGetHeight:getHeight andReloadCell:reloadCell andIsHeader:isHeader andDataBelongs:dataBelongs]; }

- (MPLTableViewIdentifier*) addIdentifier:(void(^)(MPLTableViewIdentifier* identifier))creater {
    NSMutableString* identifier = [NSMutableString string];
    for (int i = 0; i < 10; i++) [identifier appendFormat:@"%c",'a'+arc4random()%('z'-'a')];
    MPLTableViewIdentifier* i = [MPLTableViewIdentifier identifierFor:identifier
                                                           andSection:self
                                                       andCellCreated:^MPLView *(MPLTableViewIdentifier *section, NSInteger row, id data, UITableViewCell *cell, CGRect frame) { return nil; } andGetHeight:[MPLTableViewIdentifier height:40]
                                                        andReloadCell:[MPLTableViewIdentifier emptyReload]
                                                          andIsHeader:[MPLTableViewIdentifier identifierNotHeader]
                                                       andDataBelongs:^BOOL(MPLTableViewIdentifier *section, id data) { return YES; }];
    creater(i);
    return i;
}
- (MPLTableViewIdentifier*) addIdentifierFor:(NSString*)identifier create:(void(^)(MPLTableViewIdentifier* identifier))creater {
    MPLTableViewIdentifier* i = [MPLTableViewIdentifier identifierFor:identifier
                                                           andSection:self
                                                       andCellCreated:^MPLView *(MPLTableViewIdentifier *section, NSInteger row, id data, UITableViewCell *cell, CGRect frame) { return nil; } andGetHeight:[MPLTableViewIdentifier height:40]
                                                        andReloadCell:[MPLTableViewIdentifier emptyReload]
                                                          andIsHeader:[MPLTableViewIdentifier identifierNotHeader]
                                                       andDataBelongs:^BOOL(MPLTableViewIdentifier *section, id data) { return YES; }];
    creater(i);
    return i;
}

@end






@implementation MPLTableView

@synthesize tableViewStyle;
@synthesize accessoryType;
@synthesize selectionStyle;
@synthesize hideEmptySections;
@synthesize autoSnapsToRow;

@synthesize allSections;

@synthesize stickyRowsIndexPaths;
@synthesize stickyRowsIndexPathView;
@synthesize stickyRowsIdentifierViews;
@synthesize stickyRowsIndexedPreviousIndexPaths;
@synthesize stickyRowsTopInset;
@synthesize stickyRowsBottomInset;
@synthesize stickyRowBottomDisabled;

@synthesize refreshView;

@synthesize listingDelegate;
@synthesize refreshDelegate;
@synthesize dragHideDelegate;




@synthesize didScrollTo = _didScrollTo;
@synthesize canMoveRow = _canMoveRow;
@synthesize didMoveRow = _didMoveRow;
@synthesize scrollEnded = _scrollEnded;
@synthesize moveStarted = _moveStarted;
@synthesize moveFinished = _moveFinished;
@synthesize didEndDragging = _didEndDragging;
@synthesize didEndDecelerating = _didEndDecelerating;
@synthesize stickyRowMoved = _stickyRowMoved;

- (instancetype) assignDidScrollTo:(void (^)(float))didScrollTo { self.didScrollTo = didScrollTo; return self; }
- (instancetype) assignCanMoveRow:(BOOL (^)(MPLTableViewSection *, NSInteger, id, NSString *))canMoveRow { self.canMoveRow = canMoveRow; return self; }
- (instancetype) assignDidMoveRow:(void (^)(NSIndexPath *, NSIndexPath *))didMoveRow { self.didMoveRow = didMoveRow; return self; }
- (instancetype) assignScrollEnded:(void (^)(void))scrollEnded { self.scrollEnded = scrollEnded; return self; }
- (instancetype) assignMoveStarted:(void (^)(MPLTableViewSection *, NSInteger, id, NSString *))moveStarted { self.moveStarted = moveStarted; return self; }
- (instancetype) assignMoveFinished:(void (^)(MPLTableViewSection *, NSInteger, id, NSString *))moveFinished { self.moveFinished = moveFinished; return self; }
- (instancetype) assignDidEndDragging:(void (^)(float, BOOL))didEndDragging { self.didEndDragging = didEndDragging; return self; }
- (instancetype) assignDidEndDecelerating:(void (^)(float))didEndDecelerating { self.didEndDecelerating = didEndDecelerating; return self; }
- (instancetype) assignStickyRowMoved:(void (^)(NSString *, MPLTableViewSection *, NSInteger, id, UITableViewCell *, MPLView *, CGRect, float))stickyRowMoved { self.stickyRowMoved = stickyRowMoved; return self; }



#pragma mark -
#pragma mark Section management

- (void) addSectionWithSectionData:(id)data andCellsInsets:(UIEdgeInsets)insets andSpacing:(float)spacingSize {
    MPLTableViewSection* section = [MPLTableViewSection sectionWithSectionData:data andCellsInsets:insets andSpacing:spacingSize];
    section.tableView = self;
    [self.allSections addObject:section];
}
- (MPLTableViewSection*) sectionAtIndex:(NSInteger)index { return (MPLTableViewSection*)[self.sections objectAtIndex:index]; }
- (MPLTableViewSection*) defaultSection { return (self.sections.count > 0 ? [self.sections objectAtIndex:0] : nil); }
- (NSMutableArray*) sections {
    if (!self.hideEmptySections) return self.allSections;
    
    NSMutableArray* s = [NSMutableArray array];
    for (MPLTableViewSection* section in self.allSections)
        if ([section.sectionRowsData count] > 0)
            [s addObject:section];
    
    return s;
}



#pragma mark -
#pragma mark Animating methods

- (void) animatePopForState:(float)visibilityState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)r {
}
- (void) animateFlipFromLeftForState:(float)visibilityState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)c andFrame:(CGRect)r {
    c.view.layer.anchorPoint = CGPointMake(0, .5);
    
    CATransform3D t = CATransform3DIdentity;
    t.m34 = -1./500.0f;
    t = CATransform3DRotate(t, (1-visibilityState)*M_PI/2, 0, 1, 0);
    c.view.layer.transform = t;
    
    c.center = CGPointMake(r.origin.x, r.origin.y + r.size.height/2);
    c.sizeBounds = r.size;
}
- (void) animateFlipFromRightForState:(float)visibilityState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)r {
    cellView.view.layer.anchorPoint = CGPointMake(1, .5);
    
    CATransform3D t = CATransform3DIdentity;
    t.m34 = -1./500.0f;
    t = CATransform3DRotate(t, (1-visibilityState)*M_PI/2, 0, 1, 0);
    cellView.view.layer.transform = t;
    
    cellView.center = CGPointMake(r.origin.x + r.size.width, r.origin.y + r.size.height/2);
    cellView.sizeBounds = r.size;
}
- (void) animateSlideFromLeftForState:(float)visibilityState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)r {
    cellView.center = CGPointMake(-r.size.width/2 + (r.size.width + r.origin.x)*visibilityState, cellView.yOffset);
}
- (void) animateSlideFromRightForState:(float)visibilityState forIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView andFrame:(CGRect)r {
    float farX = cell.bounds.size.width+r.size.width/2;
    float theX = r.origin.x + r.size.width/2;
    cellView.xCenter = farX-(farX-theX)*visibilityState;
}

- (void) setVisibility:(float)visibilityState forIdentifier:(NSString*)identifier forSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView {
    float x = section.cellsInsets.left;
    float w = (cell.bounds.size.width-section.cellsInsets.left-section.cellsInsets.right);
    float h = cell.bounds.size.height;
    CGRect r = CGRectMake(x, 0, w, h);
    
    if ([self.listingDelegate respondsToSelector:@selector(listing:animateState:forIdentifier:andSection:andRow:andData:andCell:andView:andFrame:)])
        [self.listingDelegate listing:self animateState:visibilityState forIdentifier:identifier andSection:section andRow:row andData:data andCell:cell andView:cellView andFrame:r];
}
- (void) setVisibility:(float)state forIndexPath:(NSIndexPath*)indexPath {
    MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
    NSInteger row = indexPath.row;
    id data = [section.sectionRowsData objectAtIndex:row];
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
    MPLView* cellView = [section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
    NSString* identifier = [self cellIdentifierForSection:section andRow:row andData:data];
    
    [self setVisibility:state forIdentifier:identifier forSection:section andRow:row andData:data andCell:cell andView:cellView];
}

- (float) hideListWithDurationStart:(float)durBase andDurationStep:(float)durInt reversed:(BOOL)reversed delay:(BOOL)delay andBounce:(float)bounce withFinished:(void (^)(void))finishedBlock {
    if (self.tableView.isDecelerating) [self.tableView setContentOffset:self.tableView.contentOffset animated:NO];
    
    void (^finish)(void) = ^() {
        self.view.alpha = 0;
        finishedBlock();
    };
    if (self.tableView.decelerating || self.tableView.dragging || self.tableView.indexPathsForVisibleRows.count == 0 || self.view.alpha == 0)
    {
        finish();
        return 0;
    }
    NSInteger index = 0;
    
    
    MPLView* finalC = nil;
    
    for (NSIndexPath* indexPath in self.tableView.indexPathsForVisibleRows)
    {
        MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
        MPLView* cellView = [section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
        
        
        
        NSInteger ind = (!reversed ? index : self.tableView.indexPathsForVisibleRows.count - 1 - index);
        if ((index == 0 && reversed) || (index == self.tableView.indexPathsForVisibleRows.count - 1 && !reversed))
            finalC = cellView;
        
        [UIView animateWithDuration:durBase + ind * durInt
                              delay:(delay ? ind * (durInt*2+durBase) : 0)
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^(void){
                             [self setVisibility:1+bounce forIndexPath:indexPath];
                         }
                         completion:^(BOOL finished){
                             //if (!finished) return;
                             [UIView animateWithDuration:durBase
                                                   delay:0
                                                 options:UIViewAnimationOptionCurveEaseIn
                                              animations:^(void){
                                                  [self setVisibility:0 forIndexPath:indexPath];
                                              }
                                              completion:^(BOOL finished){
                                                  //if (!finished) return;
                                                  cellView.view.alpha = 0;
                                                  if (cellView == finalC)
                                                  {
                                                      finish();
                                                  }
                                              }
                              ];
                         }
         ];
        index ++;
    }
    return durBase + durInt * self.tableView.indexPathsForVisibleRows.count;
}
- (float) showListWithDurationStart:(float)durBase andDurationStep:(float)durInt reversed:(BOOL)reversed delay:(BOOL)delay andBounce:(float)bounce withFinished:(void (^)(void))finishedBlock {
    // Preapare to show
    BOOL en = isEnabled;
    isEnabled = YES;
    for (NSIndexPath* indexPath in self.tableView.indexPathsForVisibleRows)
    {
        MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
        MPLView* cellView = [section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
        
        
        cellView.view.alpha = 0;
        [self setVisibility:1 forIndexPath:indexPath];
        [self setVisibility:0 forIndexPath:indexPath];
    }
    isEnabled = en;
    // Start showing
    animationFinished=NO;
    self.view.alpha = 1;
    if (self.tableView.indexPathsForVisibleRows.count > 0)
    {
        NSInteger index = (reversed ? self.tableView.indexPathsForVisibleRows.count - 1 : 0);
        for (NSIndexPath* indexPath in self.tableView.indexPathsForVisibleRows)
        {
            MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
            UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
            MPLView* cellView = [section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
            
            
            
            cellView.view.alpha = 1;
            [UIView animateWithDuration:durBase + index * durInt
                                  delay:(delay ? index * (durInt*2+durBase) : 0)
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^(void){
                                 [self setVisibility:1+bounce forIndexPath:indexPath];
                             }
                             completion:^(BOOL finished){
                                 //if (!finished) return;
                                 [UIView animateWithDuration:durBase
                                                       delay:0
                                                     options:UIViewAnimationOptionCurveEaseIn
                                                  animations:^(void){
                                                      [self setVisibility:1 forIndexPath:indexPath];
                                                  }
                                                  completion:^(BOOL finished){
                                                      //if (!finished) return;
                                                      
                                                      if (index == self.tableView.indexPathsForVisibleRows.count-1)
                                                      {
                                                          isEnabled = !NO;
                                                          animationFinished=YES;
                                                          
                                                          [self.tableView setNeedsDisplay];
                                                          
                                                          finishedBlock();
                                                      }
                                                  }
                                  ];
                             }
             ];
            
            
            index += (reversed ? -1 : +1);
        }
    }
    else {
        isEnabled = !NO;
        animationFinished=YES;
        
        [self.tableView setNeedsDisplay];
        
        finishedBlock();
    }
    return durBase + durInt * self.tableView.indexPathsForVisibleRows.count;
}



#pragma mark -
#pragma mark Common methods

- (void) reloadData { [self.tableView reloadData]; }
- (void) reloadVisibleCells {
    for (NSIndexPath* indexPath in self.tableView.indexPathsForVisibleRows) {
        
        MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
        NSInteger row = indexPath.row;
        id data = [section.sectionRowsData objectAtIndex:row];
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
        MPLView* cellView = [section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
        NSString* identifier = [self cellIdentifierForSection:section andRow:row andData:data];
        
        [self cellReloadForIdentifier:identifier andSection:section andRow:row andData:data andCell:cell andView:cellView];
    }
}
- (void) snapToRow {
    if (self.tableView.dragging) return;
    NSIndexPath* ip = [self.tableView.indexPathsForVisibleRows objectAtIndex:0];
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:ip];
    
    if (ABS(cell.frame.origin.y - self.tableView.contentOffset.y) > cell.frame.size.height/2)
        ip = [NSIndexPath indexPathForRow:ip.row+1 inSection:ip.section];
    
    if (!self.tableView.decelerating)
        [self.tableView scrollToRowAtIndexPath:ip
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
}
- (void) stopScrolling {
    [self.tableView setContentOffset:self.tableView.contentOffset animated:NO];
}
- (NSIndexPath*) indexPathForData:(id)data {
    for (NSInteger i = 0; i < self.sections.count; i++) {
        MPLTableViewSection* section = [self.sections objectAtIndex:i];
        NSArray* items = section.sectionRowsData;
        NSInteger j = [items indexOfObject:data];
        if (j >= 0 && j < items.count)
            return [NSIndexPath indexPathForRow:j inSection:i];
    }
    return nil;
}
- (void) assignCellSeparatorInsets:(UIEdgeInsets)insets {
    self.cellSeparatorInsets = insets;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:insets];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:insets];
    }

}



#pragma mark -
#pragma mark Initialization

- (BVReorderTableView*)tableView { return (BVReorderTableView*)self.view; }
- (UIView*) createView { return [[BVReorderTableView alloc] initWithFrame:self.frame style:self.tableViewStyle]; }
- (void) loaderWithDelegate:(id<MPLTableViewDelegate>)delegate
             andCellsInsets:(UIEdgeInsets)insets
                 andSpacing:(float)spacing
{
    self.listingDelegate = delegate;
    [self addSectionWithSectionData:nil andCellsInsets:insets andSpacing:spacing];
}
- (void) loaderWithCellsInsets:(UIEdgeInsets)insets
                    andSpacing:(float)spacing
{
    [self loaderWithDelegate:nil andCellsInsets:insets andSpacing:spacing];
}
- (void) loader
{
    [self loaderWithCellsInsets:UIEdgeInsetsMake(0, 0, 0, 0) andSpacing:0];
}
+ (MPLTableView*) create:(void(^)(MPLTableView* v))creater {
    MPLTableView* v = [[MPLTableView alloc] init];
    [v loadWithTableViewStyle:UITableViewStylePlain
             andAccessoryType:UITableViewCellAccessoryNone
            andSelectionStyle:UITableViewCellSelectionStyleNone
                  andDelegate:nil
     ];
    creater(v);
    return v;
}
- (void) loadWithTableViewStyle:(UITableViewStyle)_tableViewStyle
               andAccessoryType:(UITableViewCellAccessoryType)_accesoryType
              andSelectionStyle:(UITableViewCellSelectionStyle)_selectionStyle
                    andDelegate:(id<MPLTableViewDelegate>)delegate
{
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.autoresizesSubviews = NO;
    self.tableView.canReorder = NO;
    self.tableView.canCancelContentTouches = YES;
    
    self.tableViewStyle = _tableViewStyle;
    self.accessoryType = _accesoryType;
    self.selectionStyle = _selectionStyle;
    self.hideEmptySections = NO;
    self.autoSnapsToRow = NO;
    self.allSections = [NSMutableArray array];
    self.listingDelegate = delegate;
    
    self.stickyRowsIndexedPreviousIndexPaths = [NSMutableDictionary dictionary];
    self.stickyRowsIndexPaths = [NSMutableArray array];
    self.stickyRowsIdentifierViews = [NSMutableDictionary dictionary];
    self.stickyRowsIndexPathView = [NSMutableDictionary dictionary];
    self->stickyRowsSpacing = 0;
}


#pragma mark -
#pragma mark Refresh view delegate

- (BOOL) egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view {
    return self->isRefreshing;
}
- (void) egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view {
    self->isRefreshing = YES;
    if ([self.refreshDelegate respondsToSelector:@selector(listingShouldRefresh:)])
        [self.refreshDelegate performSelector:@selector(listingShouldRefresh:) withObject:self];
}
- (void) egoRefreshTableStateChanged {
    if ([self.refreshDelegate respondsToSelector:@selector(refreshStateChanged:)])
        [self.refreshDelegate performSelector:@selector(refreshStateChanged:) withObject:self];
}
- (void) enablePullToRefreshWithDelegate:(id<MPLTableViewRefreshDelegate>)delegate
                               andHeight:(float)height
                               andOffset:(float)offset
                        andLoadingOffset:(float)loadingOffset
                      andBackgroundColor:(UIColor*)backgroundColor
                            andTextColor:(UIColor*)textColor
                           andArrowImage:(UIImage*)arrowImage
              andPullDownToRefreshString:(NSString*)pullDownToRefresh
               andReleaseToRefreshString:(NSString*)releaseToRefresh
                        andLoadingString:(NSString*)loading
{
    self.refreshView = [[EGORefreshTableHeaderView alloc] initAtContainer:self.tableView
                                                                withHeight:height
                                                                 andOffset:offset
                                                          andLoadingOffset:loadingOffset
                                                               andDelegate:self
                                                                arrowImage:arrowImage
                                                           backgroundColor:backgroundColor
                                                                 textColor:textColor
                                                andPullDownToRefreshString:pullDownToRefresh
                                                 andReleaseToRefreshString:releaseToRefresh
                                                          andLoadingString:loading
                         ];
    self.refreshDelegate = delegate;
}
- (void) stopRefreshing {
    self->isRefreshing = NO;
    [self.refreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
}



#pragma mark -
#pragma mark Custom headers management

- (CGRect) rectForHeaderAtIndexPath:(NSIndexPath*)indexPath andFixTop:(BOOL*)fixTop andFixBottom:(BOOL*)fixBottom andIsBottom:(BOOL)bottom {
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
    CGRect rect = [cell convertRect:(CGRect){CGPointZero,cell.frame.size} toView:self.view];
    if (!cell) {
        MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
        id data = [section.sectionRowsData objectAtIndex:indexPath.row];
        NSString* identifier = [self cellIdentifierForSection:section andRow:indexPath.row andData:data];
        float height = [self heightForIdentifier:identifier andSection:section andRow:indexPath.row andData:data];
        rect = CGRectMake(section.cellsInsets.left, self.tableView.contentOffset.y + (bottom ? self.heightBounds : 0), self.widthBounds-section.cellsInsets.left-section.cellsInsets.right, height);
        if ([self.listingDelegate respondsToSelector:@selector(listing:rectForHeaderWithRequestedRect:)])
            rect = [self.listingDelegate listing:self rectForHeaderWithRequestedRect:rect];
    }
    if ([self.listingDelegate respondsToSelector:@selector(listing:rectForHeaderWithRequestedRect:)])
        rect = [self.listingDelegate listing:self rectForHeaderWithRequestedRect:rect];
    
    if (*fixTop) {
        if (rect.origin.y < self.tableView.contentOffset.y+self.stickyRowsTopInset)
            rect = CGRectMake(rect.origin.x, self.tableView.contentOffset.y+self.stickyRowsTopInset, rect.size.width, rect.size.height);
        else
            (*fixTop) = NO;
    }
    if (*fixBottom) {
        if (!self.stickyRowBottomDisabled) {
            if ([indexPath compare:[self.stickyRowsIndexPaths objectAtIndex:0]] != NSOrderedSame) {
                if (rect.origin.y + rect.size.height > self.tableView.contentOffset.y+self.heightBounds-self.stickyRowsBottomInset)
                    rect = CGRectMake(rect.origin.x, self.tableView.contentOffset.y+self.heightBounds-self.stickyRowsBottomInset-rect.size.height, rect.size.width, rect.size.height);
                else
                    (*fixBottom) = NO;
            }
            else
                (*fixBottom) = NO;
        }
        else
            (*fixBottom) = NO;
    }
    //    if ([self.listingDelegate respondsToSelector:@selector(listing:rectForHeaderWithRequestedRect:)])
    //        rect = [self.listingDelegate listing:self rectForHeaderWithRequestedRect:rect];
    
    return rect;
}
- (MPLView*) viewForHeaderAtIndexPath:(NSIndexPath*)indexPath withFrame:(CGRect)frame {
    MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
    id data = [section.sectionRowsData objectAtIndex:indexPath.row];
    NSString* identifier = [self cellIdentifierForSection:section andRow:indexPath.row andData:data];
    
    if (![self.stickyRowsIdentifierViews objectForKey:identifier]) [self.stickyRowsIdentifierViews setObject:[NSMutableArray array] forKey:identifier];
    NSMutableArray* hvi = [self.stickyRowsIdentifierViews objectForKey:identifier];
    for (MPLView* hv in hvi) {
        if (hv.view.hidden)
            return hv;
    }
    
    float height = [self heightForIdentifier:identifier andSection:section andRow:indexPath.row andData:data];
    CGRect rect = CGRectMake(section.cellsInsets.left, 0, self.widthBounds-section.cellsInsets.left-section.cellsInsets.right, height);
    
    if ([self.listingDelegate respondsToSelector:@selector(listing:rectForHeaderWithRequestedRect:)])
        rect = [self.listingDelegate listing:self rectForHeaderWithRequestedRect:rect];
    
    MPLView* result = [self cellForIdentifier:identifier andSection:section andRow:indexPath.row andData:data andParent:self.view andFrame:rect andCell:nil];
    [hvi addObject:result];
    return result;
}

- (NSIndexPath*) indexPathForHeaderBeforeIndexPath:(NSIndexPath*)indexPath {
    id data = [self.stickyRowsIndexedPreviousIndexPaths objectForKey:indexPath];
    if (!data || [data isKindOfClass:[NSNull class]]) return nil;
    return data;
}
- (NSIndexPath*) indexPathForHeaderAfterHeaderIndexPath:(NSIndexPath*)indexPath {
    NSInteger index = [self.stickyRowsIndexPaths indexOfObject:indexPath];
    return (index < self.stickyRowsIndexPaths.count-1 ? [self.stickyRowsIndexPaths objectAtIndex:index+1] : nil);
}

- (void) reloadStickyRows {
    //    if (![self.listingDelegate respondsToSelector:@selector(listing:isHeaderData:forIdentifier:)]) return;
    
    NSMutableDictionary* currentIndexedHeaders = [NSMutableDictionary dictionary];
    
    NSMutableArray* visibleIndexPathHeaders = [NSMutableArray array];
    NSIndexPath* extraHeaderTopIndexPath = nil;
    NSIndexPath* extraHeaderBottomIndexPath = nil;
    
    for (NSIndexPath* indexPath in self.tableView.indexPathsForVisibleRows) {
        MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
        id data = [section.sectionRowsData objectAtIndex:indexPath.row];
        NSString* identifier = [self cellIdentifierForSection:section andRow:indexPath.row andData:data];
        
        if ([self section:section isHeaderData:data forIdentifier:identifier]) {
            [visibleIndexPathHeaders addObject:indexPath];
            MPLView* ihv = [self.stickyRowsIndexPathView objectForKey:indexPath];
            if (ihv) [currentIndexedHeaders setObject:ihv forKey:indexPath];
        }
    }
    if (visibleIndexPathHeaders.count > 0)
        extraHeaderTopIndexPath = [self indexPathForHeaderBeforeIndexPath:[visibleIndexPathHeaders objectAtIndex:0]];
    else if (self.tableView.indexPathsForVisibleRows.count > 0)
        extraHeaderTopIndexPath = [self indexPathForHeaderBeforeIndexPath:[self.tableView.indexPathsForVisibleRows objectAtIndex:0]];
    
    if (!self.stickyRowBottomDisabled) {
        if (visibleIndexPathHeaders.count > 0)
            extraHeaderBottomIndexPath = [self indexPathForHeaderAfterHeaderIndexPath:[visibleIndexPathHeaders lastObject]];
        else if (extraHeaderTopIndexPath)
            extraHeaderBottomIndexPath = [self indexPathForHeaderAfterHeaderIndexPath:extraHeaderTopIndexPath];
    }
    //    else if (self.tableView.indexPathsForVisibleRows.count > 0)
    //        extraHeaderBottomIndexPath = [self indexPathForHeaderAfterHeaderIndexPath:[self.tableView.indexPathsForVisibleRows lastObject]];
    
    
    if (extraHeaderTopIndexPath) {
        NSIndexPath* indexPath = extraHeaderTopIndexPath;
        if (visibleIndexPathHeaders.count == 0)
            [visibleIndexPathHeaders addObject:indexPath];
        else
            [visibleIndexPathHeaders insertObject:indexPath atIndex:0];
        
        MPLView* ihv = [self.stickyRowsIndexPathView objectForKey:indexPath];
        if (ihv) [currentIndexedHeaders setObject:ihv forKey:indexPath];
    }
    if (extraHeaderBottomIndexPath) {
        NSIndexPath* indexPath = extraHeaderBottomIndexPath;
        [visibleIndexPathHeaders addObject:indexPath];
        MPLView* ihv = [self.stickyRowsIndexPathView objectForKey:indexPath];
        if (ihv) [currentIndexedHeaders setObject:ihv forKey:indexPath];
    }
    
    
    // add previous and next indexpaths
    
    
    for (NSValue* key in self.stickyRowsIndexPathView) {
        id obj = [currentIndexedHeaders objectForKey:key];
        if (obj == nil)
            ((MPLView*)[self.stickyRowsIndexPathView objectForKey:key]).view.hidden = YES;
    }
    
    [self.stickyRowsIndexPathView removeAllObjects];
    [self.stickyRowsIndexPathView addEntriesFromDictionary:currentIndexedHeaders];
    
    NSInteger indexPathIndex = 0;
    for (NSIndexPath* indexPath in visibleIndexPathHeaders) {
        MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
        id data = [section.sectionRowsData objectAtIndex:indexPath.row];
        NSString* identifier = [self cellIdentifierForSection:section andRow:indexPath.row andData:data];
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.showsReorderControl = YES;
        
        
        BOOL fixedTop = YES;
        BOOL fixedBottom = YES;
        CGRect rect = [self rectForHeaderAtIndexPath:indexPath andFixTop:&fixedTop andFixBottom:&fixedBottom andIsBottom:indexPath == extraHeaderBottomIndexPath];
        
        if (visibleIndexPathHeaders.count > 1) {
            if (indexPath == extraHeaderBottomIndexPath) {
                fixedTop = !YES; fixedBottom = !YES;
                CGRect lrect = [self rectForHeaderAtIndexPath:[visibleIndexPathHeaders objectAtIndex:visibleIndexPathHeaders.count-2] andFixTop:&fixedTop andFixBottom:&fixedBottom andIsBottom:NO];
                float by = lrect.origin.y + lrect.size.height + self->stickyRowsSpacing;
                if (by > rect.origin.y)
                    rect = CGRectMake(rect.origin.x, rect.origin.y + (by-rect.origin.y), rect.size.width, rect.size.height);
            }
            else if (indexPathIndex < visibleIndexPathHeaders.count-1-(extraHeaderBottomIndexPath == nil ? 0 : 1)) {
                //            else if ([visibleIndexPathHeaders lastObject] != indexPath) {// if (indexPath == extraHeaderTopIndexPath) {
                fixedTop = !YES; fixedBottom = !YES;
                CGRect frect = [self rectForHeaderAtIndexPath:[visibleIndexPathHeaders objectAtIndex:indexPathIndex+1] andFixTop:&fixedTop andFixBottom:&fixedBottom andIsBottom:[visibleIndexPathHeaders objectAtIndex:1] == extraHeaderBottomIndexPath];
                float by = rect.origin.y + rect.size.height + self->stickyRowsSpacing;
                if (by > frect.origin.y)
                    rect = CGRectMake(rect.origin.x, rect.origin.y - (by-frect.origin.y), rect.size.width, rect.size.height);
            }
            else if (fixedBottom && visibleIndexPathHeaders.count > 2) {
                fixedTop = YES; fixedBottom = YES;
                CGRect lrect = [self rectForHeaderAtIndexPath:[visibleIndexPathHeaders objectAtIndex:visibleIndexPathHeaders.count-3] andFixTop:&fixedTop andFixBottom:&fixedBottom andIsBottom:NO];
                float by = lrect.origin.y + lrect.size.height + self->stickyRowsSpacing;
                if (by > rect.origin.y)
                    rect = CGRectMake(rect.origin.x, rect.origin.y + (by-rect.origin.y), rect.size.width, rect.size.height);
            }
        }
        
        if ([self.listingDelegate respondsToSelector:@selector(listing:rectForHeaderWithRequestedRect:)])
            rect = [self.listingDelegate listing:self rectForHeaderWithRequestedRect:rect];
        
        MPLView* cellView = [self.stickyRowsIndexPathView objectForKey:indexPath];
        if (!cellView) {
            cellView = [self viewForHeaderAtIndexPath:indexPath withFrame:rect];
            [self.stickyRowsIndexPathView setObject:cellView forKey:indexPath];
            cellView.view.hidden = NO;
        }
        
        [self cellReloadForIdentifier:identifier andSection:section andRow:indexPath.row andData:data andCell:cell andView:cellView];
        
        if (!CGRectEqualToRect(rect, cellView.frame))
            cellView.frame = rect;
        
        float state = (rect.origin.y - self->stickyRowsTopInset-self.tableView.contentOffset.y)/(self.heightBounds-rect.size.height);
        if ([self.listingDelegate respondsToSelector:@selector(listing:stickyRowMoved:andSection:andRow:andData:andCell:andView:andFrame:andState:)])
            [self.listingDelegate listing:self stickyRowMoved:identifier andSection:section andRow:indexPath.row andData:data andCell:cell andView:cellView andFrame:rect andState:state];
        
        indexPathIndex ++;
    }
}

- (float) positionStateForStickyRowView:(MPLView*)ap {
    float headerY = ap.yOffset;
    float srtop = self.stickyRowsTopInset;
    float offsetY = self.tableView.contentOffset.y;
    float listH = self.heightBounds;
    float headerH = ap.heightFrame;
    
    return (headerY-srtop-offsetY)/(listH-headerH-srtop);
}



#pragma mark -
#pragma mark Scrolling management

- (void) scrollToOffset:(CGPoint)offset animated:(BOOL)animated withFinished:(void(^)(void))finished {
    if (animated && !CGPointEqualToPoint(offset, self.tableView.contentOffset)) {
        __weak MPLTableView* weakSelf = self;
        self.scrollEnded = ^{ finished(); weakSelf.scrollEnded = nil; };
        [self.tableView setContentOffset:offset animated:YES];
    }
    else {
        [self.tableView setContentOffset:offset animated:NO];
        finished();
    }
}
- (void) scrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)position animated:(BOOL)animated withFinished:(void(^)(void))finished {
    if (animated) {
        __weak MPLTableView* weakSelf = self;
        self.scrollEnded = ^{ finished(); weakSelf.scrollEnded = nil; };
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:position animated:animated];
    }
    else {
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:position animated:animated];
        finished();
    }
}

- (void) scrollToIndexPath:(NSIndexPath*)indexPath withOffset:(float)offset animated:(BOOL)animated withFinished:(void (^)(void))finished {
    CGRect r = [self.tableView rectForRowAtIndexPath:indexPath];
    if (self.tableView.contentOffset.y == r.origin.y+offset) finished();
    else [self scrollToOffset:CGPointMake(r.origin.x, r.origin.y+offset) animated:animated withFinished:finished];
}
- (void) stickyRowNavigationForIndexPath:(NSIndexPath*)ip withOffset:(float)scrollOffset andReady:(void(^)(BOOL))readyBlock {
    MPLView* ap = [self.stickyRowsIndexPathView objectForKey:ip];
    int state = (int)([self positionStateForStickyRowView:ap]*100);
    BOOL scrolledBack = (state == 0);
    if (scrolledBack) {
        ip = [self indexPathForHeaderBeforeIndexPath:ip];
        if (!ip) { ip = [NSIndexPath indexPathForRow:0 inSection:0]; scrollOffset = 0; }
    }
    [self scrollToIndexPath:ip withOffset:scrollOffset animated:YES withFinished:^{
        readyBlock(scrolledBack);
    }];
}



#pragma mark -
#pragma mark Delegate helper methods

- (BOOL) isEmptyCell:(NSString*)identifier {
    return identifier.length >= 9 && [[identifier substringWithRange:NSMakeRange(0, 9)] isEqualToString:@"EMPTYCELL"];
}
- (float) emptyCellHeight:(NSString*)identifier {
    return ([self isEmptyCell:identifier] ? [[identifier substringFromIndex:9] floatValue] : 0);
}

- (NSString*) cellIdentifierForSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data {
    if ([data isKindOfClass:[NSNumber class]])
        return [NSString stringWithFormat:@"EMPTYCELL%f", [data floatValue]];
    else {
        for (MPLTableViewIdentifier* tvi in section.identifierDescriptors.allValues)
            if (tvi.dataBelongs(tvi,data)) return tvi.identifier;
        
        if ([self.listingDelegate respondsToSelector:@selector(listing:cellIdentifierForSection:andRow:andData:)])
            return [self.listingDelegate listing:self cellIdentifierForSection:section andRow:row andData:data];
        else
            return nil;
    }
}
- (BOOL) section:(MPLTableViewSection*)section isHeaderData:(id)data forIdentifier:(NSString*)identifier {
    MPLTableViewIdentifier* tvi = [section.identifierDescriptors objectForKey:identifier];
    if (tvi != nil)
        return tvi.isHeader(tvi,data);
    else {
        if ([self.listingDelegate respondsToSelector:@selector(listing:isHeaderData:forIdentifier:)])
            return [self.listingDelegate listing:self isHeaderData:data forIdentifier:identifier];
        else
            return NO;
    }
}
- (float) heightForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data {
    if ([self isEmptyCell:identifier])
        return [self emptyCellHeight:identifier];
    else {
        MPLTableViewIdentifier* tvi = [section.identifierDescriptors objectForKey:identifier];
        if (tvi != nil)
            return tvi.getHeight(tvi,row,data);
        else
            return [self.listingDelegate listing:self heightForIdentifier:identifier andSection:section andRow:row andData:data];
    }
    
}
- (MPLView*) cellForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andParent:(UIView*)pview andFrame:(CGRect)rect andCell:(UITableViewCell*)cell {
    MPLView* c = nil;
    if (![self isEmptyCell:identifier]) {
        MPLTableViewIdentifier* tvi = [section.identifierDescriptors objectForKey:identifier];
        if (tvi != nil)
            c = tvi.cellCreated(tvi,row,data,cell,rect);
        else
            c = [self.listingDelegate listing:self cellForIdentifier:identifier withFrame:rect andSection:section andRow:row andData:data andCell:cell];
    }
    if (c) {
        c.parentView = self;
        [c createInside:pview];
        if ([self.listingDelegate respondsToSelector:@selector(listing:cellCreatedForIdentifier:andSection:andRow:andData:andCell:andView:)])
            [self.listingDelegate listing:self cellCreatedForIdentifier:identifier andSection:section andRow:row andData:data andCell:cell andView:c];
    }
    
    return c;
}
- (MPLView*) cellForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell {
    CGRect rect = CGRectMake(section.cellsInsets.left, 0, cell.bounds.size.width-section.cellsInsets.left-section.cellsInsets.right, [self heightForIdentifier:identifier andSection:section andRow:row andData:data]);
    return [self cellForIdentifier:identifier andSection:section andRow:row andData:data andParent:cell andFrame:rect andCell:cell];
}
- (void) cellReloadForIdentifier:(NSString*)identifier andSection:(MPLTableViewSection*)section andRow:(NSInteger)row andData:(id)data andCell:(UITableViewCell*)cell andView:(MPLView*)cellView {
    
    {
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:self.cellSeparatorInsets];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:self.cellSeparatorInsets];
        }
    }
    
    if (![self isEmptyCell:identifier]) {
        if (cellView == nil) return;
        if (self->isReloading) return;
        
        MPLTableViewIdentifier* tvi = [section.identifierDescriptors objectForKey:identifier];
        if (tvi != nil)
            tvi.reloadCell(tvi,row,data,cell,cellView);
        else
            [self.listingDelegate listing:self cellReloadForIdentifier:identifier andSection:section andRow:row andData:data andCell:cell andView:cellView];
    }
}
- (void) generateCellPoolForIdentifier:(MPLTableViewIdentifier*)identifier count:(int)count {
    if (self.cellPool == nil) self.cellPool = [NSMutableDictionary dictionary];
    NSString* reusableIdentifier = identifier.identifier;
    MPLTableViewSection* section = identifier.section;
    self.cellPool[reusableIdentifier] = [NSMutableArray array];
    
    for (int i = 0; i < count; i++) {
        UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
        float height = [self heightForIdentifier:reusableIdentifier andSection:section andRow:0 andData:nil];
        //    float height = [self heightForIdentifier:reusableIdentifier andSection:section andRow:indexPath.row andData:[section.sectionRowsData objectAtIndex:indexPath.row]];
        cell.frame = CGRectMake(0, 0, self.tableView.frame.size.height, height);
        //            cell.frame = CGRectMake(0, 0, self.tableView.contentSize.width, self.tableView.rowHeight);
        cell.accessoryType = self.accessoryType;
        cell.selectionStyle = self.selectionStyle;
        cell.backgroundColor = [UIColor clearColor];
        cell.autoresizesSubviews = NO;
        [self.cellPool[reusableIdentifier] addObject:cell];
    }
}



#pragma mark -
#pragma mark Drag hide methods

- (float) dragHideStateForScrolledTo:(float)scrolledTo {
    float zeroBoundDistanceFix = self->drag_hide_start_location-[self.dragHideDelegate dragHideZeroBound];
    BOOL isOutOfWaitingBound = (self->drag_hide_start_state > 0 && [self.dragHideDelegate dragHideStartDistance] > zeroBoundDistanceFix);
    
    float ratio = [self.dragHideDelegate dragHideRatioOutOfMinimumBounds:isOutOfWaitingBound];
    float minDistance = (isOutOfWaitingBound ? (zeroBoundDistanceFix < 0 ? 0 : zeroBoundDistanceFix) : [self.dragHideDelegate dragHideStartDistance]);
    
    float distance = (scrolledTo - self->drag_hide_start_location);
    if (ABS(distance) < minDistance) distance = 0;
    if (distance > 0) distance -= minDistance;
    if (distance < 0) distance += minDistance;
    
    float state = self->drag_hide_start_state + ratio*distance;
    if (state < 0) state = 0;
    if (state > 1) state = 1;
    return state;
}
- (void) dragHideEndedTo:(float)scrolledTo {
    float state = [self dragHideStateForScrolledTo:scrolledTo];
    if (scrolledTo < [self.dragHideDelegate dragHideZeroBound]) state = 0;
    
    [UIView animateWithDuration:.3 animations:^{
        [self.dragHideDelegate setDragHideState:state < .5 ? 0 : 1];
        [self reloadStickyRows];
    }];
}



#pragma mark -
#pragma mark Views delegate methods

// Delegate
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}
// Delegate
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}
// TODO: Custom delegate should be implemented here
// DataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
    
    
    
    NSString* reusableIdentifier = [self cellIdentifierForSection:section andRow:indexPath.row andData:[section.sectionRowsData objectAtIndex:indexPath.row]];
    if (!reusableIdentifier)
        reusableIdentifier = [[NSNumber numberWithLong:(long)[section.sectionRowsData objectAtIndex:indexPath.row]] stringValue];
    
    BOOL isHeader = [self section:section isHeaderData:[section.sectionRowsData objectAtIndex:indexPath.row] forIdentifier:reusableIdentifier];
    
    UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    MPLView* c = [section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
    if (!cell || !c)
    {
        if (!cell)
        {
            if ([self.cellPool[reusableIdentifier] count] > 0) {
                cell = [self.cellPool[reusableIdentifier] lastObject];
                [self.cellPool[reusableIdentifier] removeLastObject];
                float height = [self heightForIdentifier:reusableIdentifier andSection:section andRow:indexPath.row andData:[section.sectionRowsData objectAtIndex:indexPath.row]];
                cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, height);
            }
            else {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
                float height = [self heightForIdentifier:reusableIdentifier andSection:section andRow:indexPath.row andData:[section.sectionRowsData objectAtIndex:indexPath.row]];
                cell.frame = CGRectMake(0, 0, self.tableView.contentSize.width, height);
                //            cell.frame = CGRectMake(0, 0, self.tableView.contentSize.width, self.tableView.rowHeight);
                cell.accessoryType = self.accessoryType;
                cell.selectionStyle = self.selectionStyle;
                cell.backgroundColor = [UIColor clearColor];
                cell.autoresizesSubviews = NO;
            }
        }
        
        if (!isHeader) {
            c = [self cellForIdentifier:reusableIdentifier andSection:section andRow:indexPath.row andData:[section.sectionRowsData objectAtIndex:indexPath.row] andCell:cell];
            cell.autoresizesSubviews = YES;
            c.mask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            if (c)
                [section.cellViews setObject:c forKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
        }
    }
    //    if (!isHeader)
    //        [self cellReloadForIdentifier:reusableIdentifier andSection:section andRow:indexPath.row andData:[section.sectionRowsData objectAtIndex:indexPath.row] andCell:cell andView:c];
    
    return cell;
}
// Delegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
    NSInteger row = indexPath.row;
    id data = [section.sectionRowsData objectAtIndex:row];
    //if ([self.tableView cellForRowAtIndexPath:indexPath] != cell) MPLLog(@"SOMETHING IS WRONG WITH WILL DISPLAY CELL");
    MPLView* cellView = [section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
    NSString* identifier = [self cellIdentifierForSection:section andRow:row andData:data];
    
    [self cellReloadForIdentifier:identifier andSection:section andRow:indexPath.row andData:[section.sectionRowsData objectAtIndex:indexPath.row] andCell:cell andView:cellView];
    //	if ([self.listingDelegate respondsToSelector:@selector(listing:willDisplayCellForIdentifier:andSection:andRow:andData:andCell:andView:)])
    //        [self.listingDelegate listing:self willDisplayCellForIdentifier:identifier andSection:section andRow:row andData:data andCell:cell andView:cellView];
}



#pragma mark -
#pragma mark Options delegate methods

// DataSource
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (![self.listingDelegate respondsToSelector:@selector(listing:titleForIndexInSection:)])
        return nil;
    
    NSMutableArray* titles = [NSMutableArray array];
    for (MPLTableViewSection* section in self.sections)
        [titles addObject:[self.listingDelegate listing:self titleForIndexInSection:section]];
    return titles;
}
// DataSource
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ([self.listingDelegate respondsToSelector:@selector(listing:titleForHeaderInSection:)])
        return [self.listingDelegate listing:self titleForHeaderInSection:[self sectionAtIndex:section]];
    return @"";
}
// DataSource
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if ([self.listingDelegate respondsToSelector:@selector(listing:titleForFooterInSection:)])
        return [self.listingDelegate listing:self titleForFooterInSection:[self sectionAtIndex:section]];
    return @"";
}
// Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([self.listingDelegate respondsToSelector:@selector(listing:heightForHeaderInSection:)])
        return [self.listingDelegate listing:self heightForHeaderInSection:[self sectionAtIndex:section]];
    return 0;
}
// Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if ([self.listingDelegate respondsToSelector:@selector(listing:heightForFooterInSection:)])
        return [self.listingDelegate listing:self heightForFooterInSection:[self sectionAtIndex:section]];
    return 0;
}
// Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
    NSInteger row = indexPath.row;
    id data = [section.sectionRowsData objectAtIndex:row];
    //UITableViewCell* cell = [self.TableView cellForRowAtIndexPath:indexPath];
    //MPLView* cellView = [section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
    NSString* identifier = [self cellIdentifierForSection:section andRow:row andData:data];
    
    return [self heightForIdentifier:identifier andSection:section andRow:indexPath.row andData:[section.sectionRowsData objectAtIndex:indexPath.row]];
}
// Delegate
//- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 0;
//}
// DataSource
//- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {}



#pragma mark -
#pragma mark ScrollView handlers

- (void) scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if ([self.listingDelegate respondsToSelector:@selector(listing:willBeginDeceleratingTo:)])
        [self.listingDelegate listing:self willBeginDeceleratingTo:self.tableView.contentOffset.y];
}
- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.listingDelegate respondsToSelector:@selector(listing:willBeginDraggingTo:)])
        [self.listingDelegate listing:self willBeginDraggingTo:self.tableView.contentOffset.y];
    
    if (self.dragHideDelegate != nil) {
        self->drag_hide_start_location = self.tableView.contentOffset.y;
        self->drag_hide_start_state = [self.dragHideDelegate dragHideState];
    }
}
- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.refreshView egoRefreshScrollViewDidScroll:self.tableView];
    
    if (self.didScrollTo != nil) self.didScrollTo(self.tableView.contentOffset.y);
    
    if ([self.listingDelegate respondsToSelector:@selector(listing:didScrollTo:)])
        [self.listingDelegate listing:self didScrollTo:self.tableView.contentOffset.y];
    
    if (self.dragHideDelegate != nil) {
        [self.dragHideDelegate setDragHideState:[self dragHideStateForScrolledTo:self.tableView.contentOffset.y]];
    }
    
    [self reloadStickyRows];
}
- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void){
        if (self.autoSnapsToRow)
            [self snapToRow];
        
        if (self.didEndDecelerating != nil)
            self.didEndDecelerating(self.tableView.contentOffset.y);
        
        if (self.dragHideDelegate != nil) {
            [self dragHideEndedTo:self.tableView.contentOffset.y];
        }
        
    });
}
- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.refreshView egoRefreshScrollViewDidEndDragging:self.tableView];
    if (!decelerate && self.autoSnapsToRow)
        [self snapToRow];
    
    if (self.didEndDragging != nil)
        self.didEndDragging(self.tableView.contentOffset.y,decelerate);
    
    if (self.dragHideDelegate != nil) {
        if (!decelerate) [self dragHideEndedTo:self.tableView.contentOffset.y];
    }
}
- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)_targetContentOffset {
    if (self.didEndScrollingWithTargetOffset != nil) self.didEndScrollingWithTargetOffset(_targetContentOffset);
    self->targetContentsOffset = (*_targetContentOffset);
}
- (void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    if (self.scrollEnded != nil)
        self.scrollEnded();
    [self scrollViewDidEndDecelerating:scrollView];
}


#pragma mark -
#pragma mark Selection delegate methods

// Delegate
//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    return indexPath;
//}
// Delegate
//- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
//    return indexPath;
//}
// Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
    NSInteger row = indexPath.row;
    id data = [section.sectionRowsData objectAtIndex:row];
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
    MPLView* cellView = [section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
    NSString* identifier = [self cellIdentifierForSection:section andRow:row andData:data];
    
    if ([self.listingDelegate respondsToSelector:@selector(listing:cellSelectedForIdentifier:andSection:andRow:andData:andCell:andView:)])
        [self.listingDelegate listing:self cellSelectedForIdentifier:identifier andSection:section andRow:row andData:data andCell:cell andView:cellView];
    
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
}
// Delegate
//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
//	//if ([self hasActionForType:TVADidDeselectRow])
//	//	[self performActionForType:TVADidDeselectRow withIndexPath:indexPath andCell:nil];
//}
// Delegate
//- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
//	//if ([self hasActionForType:TVAAccessoryTypeForRow])
//	//	[self performActionForType:TVAAccessoryTypeForRow withIndexPath:indexPath andCell:nil];
//}



#pragma mark -
#pragma mark Data delegate methods

// DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView { return [self.sections count]; }
// DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section { return [[self sectionAtIndex:section].sectionRowsData count]; }



#pragma mark -
#pragma mark Editing delegate methods

// DataSource
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//	if (editingStyle == UITableViewCellEditingStyleDelete) [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade]; // Delete the row from the data source.
//	else if (editingStyle == UITableViewCellEditingStyleInsert) {} // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//}
//
// DataSource
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//	//if ([self hasActionForType:TVACanEditRow])
//	//	[self performActionForType:TVACanEditRow withIndexPath:indexPath andCell:nil];
//	return YES;
//}
//
// Delegate
//- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
//	//if ([self hasActionForType:TVAWillBeginEditingRow])
//	//	[self performActionForType:TVAWillBeginEditingRow withIndexPath:indexPath andCell:nil];
//}
// Delegate
//- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
//	//if ([self hasActionForType:TVADidEndEditingRow])
//	//	[self performActionForType:TVADidEndEditingRow withIndexPath:indexPath andCell:nil];
//}
// Delegate
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}
// Delegate
//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
//	//if (![self hasActionForType:TVATitleForDeleteConfirmationButtonForRow])
//    return @"Delete";//[[self nilDelegate] tableView:tableView titleForDeleteConfirmationButtonForRowAtIndexPath:indexPath];
//
//	//return [self performActionForType:TVATitleForDeleteConfirmationButtonForRow withIndexPath:indexPath andCell:nil];
//}
// Delegate
//- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
//	//if (![self hasActionForType:TVAShouldIndentWhileEditingRow])
//    return YES;
//
//	//return [[self performActionForType:TVAShouldIndentWhileEditingRow withIndexPath:indexPath andCell:nil] boolValue];
//}



#pragma mark -
#pragma mark Moving delegate methods

// DataSource
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.canMoveRow != nil) {
        MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
        NSInteger row = indexPath.row;
        id data = [section.sectionRowsData objectAtIndex:row];
        NSString* identifier = [self cellIdentifierForSection:section andRow:row andData:data];
        return self.canMoveRow(section,row,data,identifier);
    }
    
    return ![[self.defaultSection.sectionRowsData objectAtIndex:indexPath.row] isKindOfClass:[NSNumber class]];
    //	return YES;
}
//
// DataSource
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
//
// Delegate
//- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
//	//if (![self hasActionForType:TVATargetIndexPathForMove])
//    return proposedDestinationIndexPath;
//
//	//return [self performActionForType:TVATargetIndexPathForMove withIndexPath:sourceIndexPath andSecondaryIndexPath:proposedDestinationIndexPath andCell:nil];
//}



// This method is called when the long press gesture is triggered starting the re-ording process.
// You insert a blank row object into your data source and return the object you want to save for
// later. This method is only called once.
- (id)saveObjectAndInsertBlankRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
    NSInteger row = indexPath.row;
    id object = [section.sectionRowsData objectAtIndex:row];
    NSString* identifier = [self cellIdentifierForSection:section andRow:row andData:object];
    
    [self.defaultSection.sectionRowsData replaceObjectAtIndex:indexPath.row withObject:@([self.tableView.delegate tableView:self.tableView heightForRowAtIndexPath:indexPath])];
    
    if (self.moveStarted != nil)
        self.moveStarted(section,row,object,identifier);
    
    return object;
}

// This method is called when the selected row is dragged to a new position. You simply update your
// data source to reflect that the rows have switched places. This can be called multiple times
// during the reordering process.
- (void)moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    id object = [self.defaultSection.sectionRowsData objectAtIndex:fromIndexPath.row];
    [self.defaultSection.sectionRowsData removeObjectAtIndex:fromIndexPath.row];
    [self.defaultSection.sectionRowsData insertObject:object atIndex:toIndexPath.row];
    
    
    
    NSInteger fromI = [self.defaultSection cleanDataIndexForIndexPath:fromIndexPath];
    NSInteger toI = [self.defaultSection cleanDataIndexForIndexPath:toIndexPath];
    
    object = [self.defaultSection->cleanRowData objectAtIndex:fromI];
    [self.defaultSection->cleanRowData removeObjectAtIndex:fromI];
    [self.defaultSection->cleanRowData insertObject:object atIndex:toI];
    
    if (self.didMoveRow != nil) {
        self.didMoveRow(fromIndexPath,toIndexPath);
    }
}


// This method is called when the selected row is released to its new position. The object is the same
// object you returned in saveObjectAndInsertBlankRowAtIndexPath:. Simply update the data source so the
// object is in its new position. You should do any saving/cleanup here.
- (void)finishReorderingWithObject:(id)object atIndexPath:(NSIndexPath *)indexPath; {
    [self.defaultSection.sectionRowsData replaceObjectAtIndex:indexPath.row withObject:object];
    
    MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
    NSInteger row = indexPath.row;
    //id object = [section.sectionRowsData objectAtIndex:row];
    NSString* identifier = [self cellIdentifierForSection:section andRow:row andData:object];
    
    if (self.moveFinished != nil)
        self.moveFinished(section,row,object,identifier);
    // do any additional cleanup here
}




- (int) calculateTargetIndexForCurrentOffset:(float)py andQuantifiedWidth:(float)ww andSkipHeight:(float)skipH {
    float to = py - self.defaultSection.cellsInsets.top + self.defaultSection.spacing/2 - skipH;
    float w = ww+self.defaultSection.spacing;
    int c = (int)(to/w);
    if (to-c*w+self.heightBounds/2 > w) c++;
    return c;
}
- (float) calculateTargetOffsetForCurrentOffset:(float)py andQuantifiedWidth:(float)ww andSkipHeight:(float)skipH {
    return [self calculateTargetIndexForCurrentOffset:py andQuantifiedWidth:ww andSkipHeight:skipH]*(ww+self.defaultSection.spacing);
}

- (void) setTopInset:(float)topInset andBottomInset:(float)bottomInset {
    float defaultT = self.defaultSection.cellsInsets.top;
    float defaultB = self.defaultSection.cellsInsets.bottom;
    // resize bottom inset row
    if (defaultT+self->currentTopFix > 0)
        [self.defaultSection.sectionRowsData removeObjectAtIndex:0];
    if (defaultB+self->currentBottomFix > 0)
        [self.defaultSection.sectionRowsData removeLastObject];
    if (defaultT+topInset > 0)
        [self.defaultSection.sectionRowsData insertObject:@(defaultT+topInset) atIndex:0];
    if (defaultB+bottomInset > 0)
        [self.defaultSection.sectionRowsData addObject:@(defaultB+bottomInset)];
    
    // reset insets
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(topInset, 0, bottomInset, 0);
//    self.defaultSection.cellsInsets = UIEdgeInsetsMake(defaultT+topInset, self.defaultSection.cellsInsets.left, defaultB+bottomInset, self.defaultSection.cellsInsets.right);
    
    // update table
    [self update:YES];
    self->currentTopFix = topInset;
    self->currentBottomFix = bottomInset;
}
- (void) update:(BOOL)requireData {
    if (self.defaultSection->cleanRowData.count > 0 || !requireData) {
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    }
}

- (void) hideBottomWithHeight:(float)height {
    [self setTopInset:0 andBottomInset:height];
}


- (void) toggleExpansionForData:(id)v row:(NSInteger)row identifiers:(NSArray*)expandableIdentifiers simultaniously:(BOOL)simultaniously finished:(void(^)(void))finished {
    [self getChangeExpandedRowForData:v row:row identifiers:expandableIdentifiers exists:^(MPLTableViewIdentifier *changeTVI, NSInteger changeRow) {
        [self getCurrentlyExpandedRowForIdentifiers:expandableIdentifiers exists:^(MPLTableViewIdentifier *currentTVI, NSInteger currentRow) {
            if (currentRow == changeRow && currentTVI == changeTVI) {
                [changeTVI setRow:changeRow heightState:0 andFinished:finished];
            }
            else {
                if (simultaniously) {
                    
                    [currentTVI.rowHeightStates removeObjectForKey:@(currentRow)];
                    [changeTVI.rowHeightStates setObject:@(1) forKey:@(changeRow)];
                    
                    UITableViewCell* currentCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:currentRow inSection:[self.sections indexOfObject:self.defaultSection]]];
                    UITableViewCell* changeCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:changeRow inSection:[self.sections indexOfObject:self.defaultSection]]];
                    MPLTableViewExpandableCellWraper* currentView = (MPLTableViewExpandableCellWraper*)[self.defaultSection.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)currentCell]];
                    MPLTableViewExpandableCellWraper* changeView = (MPLTableViewExpandableCellWraper*)[self.defaultSection.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)changeCell]];
                    
                    if (currentView != nil || changeView != nil) {
                        [UIView animateWithDuration:.20 delay:0 options:0 animations:^{
                            if ([currentView isKindOfClass:[MPLTableViewExpandableCellWraper class]] || currentView == nil)
                                [currentView loadHeightState:0 atRow:currentRow];
                            else {
                                MPLLog(@"WHAT!?");
                            }
                            if ([changeView isKindOfClass:[MPLTableViewExpandableCellWraper class]] || changeView == nil)
                                [changeView loadHeightState:1 atRow:changeRow];
                            else {
                                MPLLog(@"WHAT!?");
                            }
                        } completion:^(BOOL _finished) { finished(); }];
                        [self update:YES];
                        [UIView animateWithDuration:.20 delay:0 options:0 animations:^{ [self reloadStickyRows]; } completion:^(BOOL _finished) {}];
                    }
                }
                else {
                    [currentTVI setRow:currentRow heightState:0 andFinished:^{
                        [changeTVI setRow:changeRow heightState:1 andFinished:finished];
                    }];
                }
            }
        } notAvailable:^{
            [changeTVI setRow:changeRow heightState:1 andFinished:finished];
        }];
    }];
}

- (void) getChangeExpandedRowForData:(id)v row:(NSInteger)row identifiers:(NSArray*)identifiers exists:(void(^)(MPLTableViewIdentifier* changeTVI, NSInteger changeRow))exists {
    for (MPLTableViewIdentifier* i in identifiers)
        if (i.dataBelongs(i,v)) {
            exists(i, row);
            return;
        }
}

- (void) getCurrentlyExpandedRowForIdentifiers:(NSArray*)identifiers exists:(void(^)(MPLTableViewIdentifier* currentTVI, NSInteger currentRow))exists notAvailable:(void(^)(void))notExisting {
    for (MPLTableViewIdentifier* i in identifiers)
        if (i.rowHeightStates.count > 0) {
            exists(i, [i.rowHeightStates.allKeys[0] intValue]);
            return;
        }
    notExisting();
}


- (void) foreachVisibleRow:(void(^)(MPLTableViewSection* section,UITableViewCell* cell,MPLView* cellView,id data,NSInteger row,NSInteger dataIndex))enumerator {
    for (NSIndexPath* indexPath in self.tableView.indexPathsForVisibleRows)
    {
        MPLTableViewSection* section = [self sectionAtIndex:indexPath.section];
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
        MPLView* cellView = [section.cellViews objectForKey:[NSValue valueWithPointer:(__bridge const void *)cell]];
        id data = [section.sectionRowsData objectAtIndex:indexPath.row];
        enumerator(section,cell,cellView,data,indexPath.row,[section cleanDataIndexForRow:indexPath.row]);
    }
}

- (void) resetSpeedCalculations {
    self.lastOffsetX = 0;
    self.lastOffsetY = 0;
    self.lastOffsetCapture = 0;
}
- (float) calculateSpeedX {
    float currentOffsetX = self.tableView.contentOffset.x;
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];

    NSTimeInterval timeDiff = currentTime - self.lastOffsetCapture;
    if(timeDiff > 0.1) {
        CGFloat distance = currentOffsetX - self.lastOffsetX;
        //The multiply by 10, / 1000 isn't really necessary.......
        CGFloat scrollSpeedNotAbs = (distance * 10) / 1000; //in pixels per millisecond

        CGFloat scrollSpeed = fabsf(scrollSpeedNotAbs);

        int direction = (self.lastOffsetX > currentOffsetX ? -1 : 1);

        self.lastOffsetX = currentOffsetX;
        self.lastOffsetCapture = currentTime;

        self.lastSpeedX = direction * scrollSpeed;
    }
    return self.lastSpeedX;
}
- (float) calculateSpeedY {
    float currentOffsetY = self.tableView.contentOffset.y;
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];

    NSTimeInterval timeDiff = currentTime - self.lastOffsetCapture;
    if(timeDiff > 0.1) {
        CGFloat distance = currentOffsetY - self.lastOffsetY;
        //The multiply by 10, / 1000 isn't really necessary.......
        CGFloat scrollSpeedNotAbs = (distance * 10) / 1000; //in pixels per millisecond

        CGFloat scrollSpeed = fabsf(scrollSpeedNotAbs);

        int direction = (self.lastOffsetY > currentOffsetY ? -1 : 1);

        self.lastOffsetY = currentOffsetY;
        self.lastOffsetCapture = currentTime;

        self.lastSpeedY = direction * scrollSpeed;
    }
    return self.lastSpeedY;
}

//@property (nonatomic, retain) EGORefreshTableHeaderView* refreshView;

- (void) destroyView {
    self.stickyRowsIndexPaths = nil;
    self.stickyRowsIndexedPreviousIndexPaths = nil;
    self.dragHideDelegate = nil;
    self.listingDelegate = nil;
    self.refreshDelegate = nil;
    self.didScrollTo = nil;
    self.scrollEnded = nil;
    self.canMoveRow = nil;
    self.didMoveRow = nil;
    self.moveStarted = nil;
    self.moveFinished = nil;
    self.didEndDragging = nil;
    self.didEndDecelerating = nil;
    self.didEndScrollingWithTargetOffset = nil;
    self.stickyRowMoved = nil;
    
    for (NSArray* sriv in self.stickyRowsIdentifierViews)
        for (MPLView* v in sriv)
            [v destroyView];
    self.stickyRowsIdentifierViews = nil;
    
    self.stickyRowsIndexPathView = nil;
    
    self.refreshView = nil;
    
    for (MPLTableViewSection* s in self.allSections) {
        [s destroySection];
    }
    self.allSections = nil;
    [super destroyView];
}

@end
