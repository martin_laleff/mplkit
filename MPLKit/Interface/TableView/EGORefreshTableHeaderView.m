//
//  EGORefreshTableHeaderView.m
//  Demo
//
//  Created by Devin Doty on 10/14/09October14.
//  Copyright 2009 enormego. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "EGORefreshTableHeaderView.h"


#define TEXT_COLOR	 [UIColor colorWithRed:87.0/255.0 green:108.0/255.0 blue:137.0/255.0 alpha:1.0]
#define FLIP_ANIMATION_DURATION 0.18f


@interface EGORefreshTableHeaderView (Private)
- (void)setState:(EGOPullRefreshState)aState;
@end

@implementation EGORefreshTableHeaderView

@synthesize delegate=_delegate;
@synthesize lastUpdated;
@synthesize offsetOnLoading;
@synthesize offsetToRelease;
@synthesize IsRefreshing;

@synthesize pullDownToRefreshString;
@synthesize releaseToRefreshString;
@synthesize loadingString;
- (id)initAtContainer:(UIScrollView*)_scrollView
           withHeight:(float)height
            andOffset:(float)offset
     andLoadingOffset:(float)loadingOffset
          andDelegate:(id<EGORefreshTableHeaderDelegate>)__delegate
           arrowImage:(UIImage *)arrowImage
      backgroundColor:(UIColor*)backgroundColor
            textColor:(UIColor *)textColor
andPullDownToRefreshString:(NSString*)pullDownToRefresh
andReleaseToRefreshString:(NSString*)releaseToRefresh
     andLoadingString:(NSString*)loading
{
    CGRect frame = CGRectMake(0, -_scrollView.bounds.size.height+offset, _scrollView.bounds.size.width, _scrollView.bounds.size.height);
    if((self = [super initWithFrame:frame])) {
        self.releaseToRefreshString = releaseToRefresh;
        self.pullDownToRefreshString = pullDownToRefresh;
        self.loadingString = loading;
        
		container = _scrollView;
        self.delegate = __delegate;
        self.alpha = 0;
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		self.backgroundColor = backgroundColor;
        self.offsetOnLoading = offset+height;
        self.offsetToRelease = height;
        
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, frame.size.height - height/2-20/2, self.frame.size.width, 20.0f)];
		label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		label.font = [UIFont systemFontOfSize:12.0f];
		label.textColor = textColor;
//		label.shadowColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
//		label.shadowOffset = CGSizeMake(0.0f, 1.0f);
		label.backgroundColor = [UIColor clearColor];
		label.textAlignment = NSTextAlignmentCenter;
		[self addSubview:label];
		_lastUpdatedLabel=label;
		
		label = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, frame.size.height - height/2-20/2, self.frame.size.width, 20.0f)];
		label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		label.font = [UIFont boldSystemFontOfSize:13.0f];
		label.textColor = textColor;
//		label.shadowColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
//		label.shadowOffset = CGSizeMake(0.0f, 1.0f);
		label.backgroundColor = [UIColor clearColor];
		label.textAlignment = NSTextAlignmentCenter;
		[self addSubview:label];
		_statusLabel=label;
		
		CALayer *layer = [CALayer layer];
		layer.frame = CGRectMake(25.0f, frame.size.height - height/2-55./2, 30.0f, 55.0f);
		layer.contentsGravity = kCAGravityResizeAspect;
		layer.contents = (id)arrowImage.CGImage;
		
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 40000
		if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
			layer.contentsScale = [[UIScreen mainScreen] scale];
		}
#endif
		
		[[self layer] addSublayer:layer];
		_arrowImage=layer;
		
		UIActivityIndicatorView *view = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		view.frame = CGRectMake(25.0f, frame.size.height - height/2-20./2, 20.0f, 20.0f);
		[self addSubview:view];
		_activityView = view;
		
		
		[self setState:EGOOPullRefreshNormal];
        
        [container addSubview:self];
        
        [self refreshLastUpdatedDate];
    }
	
    return self;
	
}

- (void) assignOffsetTo:(float)offset {
    self.frame = CGRectMake(self.frame.origin.x, -self.frame.size.height+offset, container.frame.size.width, self.frame.size.height);
    self.offsetOnLoading = self.offsetToRelease;//-(-self.frame.size.height+offset);
    
}
- (void) changeFontTo:(UIFont *)font {
    _statusLabel.font = font;
}

#pragma mark -
#pragma mark Setters

- (void)refreshLastUpdatedDate {
	
	if ([_delegate respondsToSelector:@selector(egoRefreshTableHeaderDataSourceLastUpdated:)]) {
		
		NSDate *date = [_delegate egoRefreshTableHeaderDataSourceLastUpdated:self];
		
		[NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehaviorDefault];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateStyle:NSDateFormatterShortStyle];
		[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        self.lastUpdated = [NSString stringWithFormat:@"Last Updated: %@", [dateFormatter stringFromDate:date]];
		
	} else {
		
		self.lastUpdated = nil;
		
	}
    _lastUpdatedLabel.text = self.lastUpdated;
    
}

- (void)setState:(EGOPullRefreshState)aState{
	
	switch (aState) {
		case EGOOPullRefreshPulling:
			
			_statusLabel.text = self.releaseToRefreshString;
			[CATransaction begin];
			[CATransaction setAnimationDuration:FLIP_ANIMATION_DURATION];
			_arrowImage.transform = CATransform3DMakeRotation((M_PI / 180.0) * 180.0f, 0.0f, 0.0f, 1.0f);
			[CATransaction commit];
			
			break;
		case EGOOPullRefreshNormal:
			
			if (_state == EGOOPullRefreshPulling) {
				[CATransaction begin];
				[CATransaction setAnimationDuration:FLIP_ANIMATION_DURATION];
				_arrowImage.transform = CATransform3DIdentity;
				[CATransaction commit];
			}
			
			_statusLabel.text = self.pullDownToRefreshString;
			[_activityView stopAnimating];
			[CATransaction begin];
			[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
			_arrowImage.hidden = NO;
			_arrowImage.transform = CATransform3DIdentity;
			[CATransaction commit];
			
			[self refreshLastUpdatedDate];
			
			break;
		case EGOOPullRefreshLoading:
			
			_statusLabel.text = self.loadingString;
			[_activityView startAnimating];
			[CATransaction begin];
			[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
			_arrowImage.hidden = YES;
			[CATransaction commit];
			
			break;
		default:
			break;
	}
	_state = aState;
    [self.delegate egoRefreshTableStateChanged];
}
- (EGOPullRefreshState) state { return _state; }

- (void) resetIndicatorAlphaAndPosition {
    if (_state != EGOOPullRefreshNormal)
        self.alpha = 1;
    else
        self.alpha = (container.contentOffset.y > 0 ? 0 : -container.contentOffset.y/self.offsetToRelease);
}


#pragma mark -
#pragma mark ScrollView Methods

- (void)egoRefreshScrollViewDidScroll:(UIScrollView *)scrollView {
	
	if (_state == EGOOPullRefreshLoading) {
		
		CGFloat offset = MAX(scrollView.contentOffset.y * -1, 0);
//		offset = MIN(offset, self.offsetOnLoading);
        offset = self.offsetOnLoading;
		scrollView.contentInset = UIEdgeInsetsMake(offset, 0.0f, 0.0f, 0.0f);
		
	} else if (scrollView.isDragging && !scrollView.isDecelerating) {
		
		if (_state == EGOOPullRefreshPulling && scrollView.contentOffset.y > -self.offsetToRelease && scrollView.contentOffset.y < 0.0f && !self.IsRefreshing) {
			[self setState:EGOOPullRefreshNormal];
		} else if (_state == EGOOPullRefreshNormal && scrollView.contentOffset.y < -self.offsetToRelease && !self.IsRefreshing && container.isDragging) {
			[self setState:EGOOPullRefreshPulling];
		}
		
        [self resetIndicatorAlphaAndPosition];
		if (scrollView.contentInset.top != 0) {
			scrollView.contentInset = UIEdgeInsetsZero;
		}
		
	}
    [self resetIndicatorAlphaAndPosition];
	
}

- (void)egoRefreshScrollViewDidEndDragging:(UIScrollView *)scrollView {
	
	if (scrollView.contentOffset.y <= - self.offsetToRelease && !self.IsRefreshing) {
		
		[self setState:EGOOPullRefreshLoading];
        [UIView animateWithDuration:.1
                         animations:^{
                             [self resetIndicatorAlphaAndPosition];
                             scrollView.contentInset = UIEdgeInsetsMake(self.offsetOnLoading, 0.0f, 0.0f, 0.0f);
                         }
                         completion:^(BOOL finished) {
                             if ([_delegate respondsToSelector:@selector(egoRefreshTableHeaderDidTriggerRefresh:)]) {
                                 self.IsRefreshing = YES;
                                 [_delegate egoRefreshTableHeaderDidTriggerRefresh:self];
                             }
                         }
         ];
		
	}
	else {
        [self resetIndicatorAlphaAndPosition];
    }
}

- (void)egoRefreshScrollViewDataSourceDidFinishedLoading:(UIScrollView *)scrollView {
	
    [UIView animateWithDuration:.25
                     animations:^{
                         self.IsRefreshing = NO;
                         [self setState:EGOOPullRefreshNormal];
                         [self resetIndicatorAlphaAndPosition];
                         [scrollView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}


#pragma mark -
#pragma mark Dealloc

- (void)dealloc {
	
	_delegate=nil;
	_activityView = nil;
	_statusLabel = nil;
	_arrowImage = nil;
	_lastUpdatedLabel = nil;
}

@end
