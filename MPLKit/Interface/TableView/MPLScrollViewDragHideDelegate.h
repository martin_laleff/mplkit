//
//  MPLScrollViewDragHideDelegate.h
//  Mixweet
//
//  Created by Martin Lalev on 3/6/14.
//
//

#import <Foundation/Foundation.h>

@protocol MPLScrollViewDragHideDelegate <NSObject>

- (float) dragHideZeroBound;
- (float) dragHideStartDistance;
- (float) dragHideRatioOutOfMinimumBounds:(BOOL)outOfBounds;
- (float) dragHideState;
- (void) setDragHideState:(float)state;

@end
