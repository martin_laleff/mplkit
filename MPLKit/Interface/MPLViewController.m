    //
//  FTViewController.m
//  FTrack_Take2
//
//  Created by Martin Lalev on 9/6/11.
//  Copyright 2011 mART. All rights reserved.
//

#import "MPLViewController.h"
#import "MPLApplication.h"
#import "MPLScrollPanel.h"


@implementation MPLViewController

@synthesize baseView = _baseView;

- (void) loadBaseView:(MPLView*)baseView {
    baseView.view.frame = CGRectMake(baseView.view.frame.origin.x, -self.view.frame.origin.y, baseView.view.frame.size.width, baseView.view.frame.size.height);
    self.baseView = baseView;
    [self.baseView createInside:self.view];
}
+ (MPLViewController*) viewControllerWithBaseView:(MPLView*)baseView {
	MPLViewController* VC = [[MPLViewController alloc] init];
    if (baseView != nil) [VC loadBaseView:baseView];
    return VC;
}


- (void) dealloc {
	[self.baseView destroyView];
	
}


@end
