//
//  FTSlider.h
//  FTrack_Take2
//
//  Created by Administrator on 08/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLView.h"

@interface MPLSlider : MPLView

@property (weak, nonatomic, readonly) UISlider* sliderView;
@property (nonatomic, assign) float value;
@property (nonatomic, assign) float minimumValue;
@property (nonatomic, assign) float maximumValue;

- (void) loaderWithValue:(CGFloat)value
                  andMinValue:(CGFloat)minValue
                  andMaxValue:(CGFloat)maxValue
                  andMinImage:(UIImage*)minImage
                  andMaxImage:(UIImage*)maxImage
         andValueChangedAction:(MPLAction*)valueChangedAction;

- (void) loaderWithValue:(CGFloat)value
                   andMinValue:(CGFloat)minValue
                   andMaxValue:(CGFloat)maxValue
         andValueChangedAction:(MPLAction*)valueChangedAction;

@end
