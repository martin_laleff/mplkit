//
//  FTView.h
//  FTrack_Take2
//
//  Created by Administrator on 05/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPLView.h"

#define SUBVIEWKEY @"K"
#define SUBVIEWVALUE @"V"
#define SUBVIEWDICT( k , v ) @{ @"K": (k), @"V":(v)}
@interface MPLPanel : MPLView {
    NSMutableDictionary* subViews;
    NSMutableArray* subViewsStack;
}

@property (nonatomic, strong) UIVisualEffectView* blurView;
@property (nonatomic, strong) UIView* panelView;
@property (nonatomic, strong) NSMutableDictionary* subViews;
@property (nonatomic, strong) NSMutableArray* subViewsStack;

+ (instancetype) create:(void(^)(id v))creater;


- (NSString*) keyForSubview:(MPLView*)view;
- (MPLView*) subviewForKey:(NSString*)key;

- (MPLPanel*) addSubview:(MPLView*)control forKey:(NSString*)key;

- (void) removeSubviewForKey:(NSString*)key andDestroy:(BOOL)destroy;
- (void) removeSubview:(MPLView*)view andDestroy:(BOOL)destroy;

- (void) enableBlurWithStyle:(UIBlurEffectStyle)style;

@end
