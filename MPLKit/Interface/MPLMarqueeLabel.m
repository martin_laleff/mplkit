//
//  FTMarqueeLabel.m
//  Mixweet
//
//  Created by Martin Lalev on 10/15/13.
//
//

#import "MPLMarqueeLabel.h"

@implementation MPLMarqueeLabel

- (MarqueeLabel*) marqueeLabel { return (MarqueeLabel*)self.view; }
- (UIView*) createView { return [[MarqueeLabel alloc] init]; }

#pragma mark -
#pragma mark Properties

@dynamic textAlignment;
@dynamic text;
@dynamic font;
@dynamic numberOfLines;
@dynamic adjustToFitWidth;

- (NSTextAlignment) textAlignment { return self.marqueeLabel.textAlignment; }
- (UIColor*) textColor { return self.marqueeLabel.textColor; }
- (NSString*) text { return self.marqueeLabel.text; }
- (UIFont*) font { return self.marqueeLabel.font; }
- (NSInteger) numberOfLines { return self.marqueeLabel.numberOfLines; }
- (BOOL) adjustToFitWidth { return self.marqueeLabel.adjustsFontSizeToFitWidth; }

- (void) setText:(NSString *)text { self.marqueeLabel.text = text; }
- (void) setTextColor:(UIColor *)textColor { self.marqueeLabel.textColor = textColor; }
- (void) setTextAlignment:(NSTextAlignment)textAlignment { self.marqueeLabel.textAlignment = textAlignment; }
- (void) setFont:(UIFont *)font { self.marqueeLabel.font = font; }
- (void) setNumberOfLines:(NSInteger)numberOfLines { self.marqueeLabel.numberOfLines = numberOfLines; }
- (void) setAdjustToFitWidth:(BOOL)adjustToFitWidth { self.marqueeLabel.adjustsFontSizeToFitWidth = adjustToFitWidth; }

- (void) sizeToFitHeight {
    CGFloat w = self.widthFrame;
    [self.marqueeLabel sizeToFit];
    self.widthFrame = w;
}
- (void) sizeToFitWidth {
    CGFloat h = self.heightFrame;
    [self.marqueeLabel sizeToFit];
    self.heightFrame = h;
}
static NSString* _mpl_attatchmentTextColorAlphaParameter = @"alpha";
- (instancetype) attatchTextColorToTheme:(MPLTheme*)theme forKey:(NSString*)key withAlpha:(float)alpha {
    return [self attatch:@"textColor" toTheme:theme forKey:key withParameters:@{_mpl_attatchmentTextColorAlphaParameter:@(alpha)} andAction:^(MPLThemeViewAttatchment* themeAttatchment) {
        self.textColor = [[themeAttatchment.theme colorForKey:themeAttatchment.key] colorWithAlphaComponent:[[themeAttatchment.parameters objectForKey:_mpl_attatchmentTextColorAlphaParameter] floatValue]];
    }];
}
- (instancetype) attatchTextColorToTheme:(MPLTheme*)theme forKey:(NSString*)key {
    return [self attatch:@"textColor" toTheme:theme forKey:key andAction:^(MPLThemeViewAttatchment* themeAttatchment) {
        self.textColor = [themeAttatchment.theme colorForKey:themeAttatchment.key];
    }];
}

- (instancetype) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                        andColor:(UIColor*)textColor
                         andFont:(UIFont*)font
{
    self.text = text;
    self.textAlignment = textAlignment;
    if (font != nil)
        self.font = font;
    if (textColor != nil)
        self.textColor = textColor;
    return self;
}
- (instancetype) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                         andFont:(UIFont*)font
{
    return [self textLoaderWithText:text
                   andTextAlignment:textAlignment
                           andColor:nil
                            andFont:font
            ];
}
- (instancetype) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
{
    return [self textLoaderWithText:text
                   andTextAlignment:textAlignment
                           andColor:nil
                            andFont:nil
            ];
}
- (instancetype) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                        andColor:(UIColor*)textColor
{
    return [self textLoaderWithText:text
                   andTextAlignment:textAlignment
                           andColor:textColor
                            andFont:nil
            ];
}
- (instancetype) assignFontWithLabelSizeFix:(int)fix {
    self.font = [UIFont systemFontOfSize:[UIFont labelFontSize]+fix];
    return self;
}
- (instancetype) assignFontWithButtonSizeFix:(int)fix {
    self.font = [UIFont systemFontOfSize:[UIFont buttonFontSize]+fix];
    return self;
}
- (void) loader {
    self.marqueeLabel.marqueeType = MLContinuous;
    self.marqueeLabel.animationCurve = UIViewAnimationOptionCurveLinear;
    self.marqueeLabel.continuousMarqueeExtraBuffer = 50.0f;
    self.marqueeLabel.fadeLength = 10;
    self.marqueeLabel.numberOfLines = 1;
}


- (void) loadWithText:(NSString*)text
     andTextAlignment:(NSTextAlignment)textAlignment
         andTextColor:(UIColor*)textColor
              andFont:(UIFont*)font {
    [self loader];
    
    
	self.marqueeLabel.text = text;
	self.marqueeLabel.backgroundColor = [UIColor clearColor];
	self.marqueeLabel.textAlignment = textAlignment;
	
	if (textColor)
		self.marqueeLabel.textColor = textColor;
    
	if (font)
		self.marqueeLabel.font = font;
    
}

+ (MPLMarqueeLabel*) create:(void(^)(MPLMarqueeLabel* v))creater {
    MPLMarqueeLabel* v = [[MPLMarqueeLabel alloc] init];
    [v loader];
    creater(v);
    return v;
}

@end
