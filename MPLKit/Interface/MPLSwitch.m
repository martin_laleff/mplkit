//
//  FTSwitch.m
//  FTrack_Take2
//
//  Created by Administrator on 19/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MPLSwitch.h"


@implementation MPLSwitch

- (UISwitch*) switchView { return (UISwitch*)self.view; }
- (UIView*) createView { return [[UISwitch alloc] init]; }

- (void) loaderWithSwitchedOn:(BOOL)on andAction:(MPLAction*)onSwitched
{
	self.switchView.on = on;
	[self addAction:onSwitched forEventType:UIControlEventValueChanged];
}

@end
