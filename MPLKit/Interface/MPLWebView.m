//
//  MPLWebView.m
//  MPLKit
//
//  Created by Martin Lalev on 9/26/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLWebView.h"

@implementation MPLWebView

- (UIWebView*) webView { return (UIWebView*)self.view; }
- (UIView*) createView {
    UIWebView* v = [[UIWebView alloc] init];
    v.delegate = self;
    return v;
}

- (id) init {
    if ([super init]) {
    }
    return self;
}
- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if (self.didFinishLoading != nil) {
        self.didFinishLoading(nil);
        self.didFinishLoading = nil;
    }
}
- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if (self.didFinishLoading != nil) {
        self.didFinishLoading(error);
        self.didFinishLoading = nil;
    }
}
- (void) goBackWithFinished:(void(^)(NSError* error))finishedLoading {
    self.didFinishLoading = finishedLoading;
    [self.webView goBack];
}

- (void) loadURL:(NSURL*)url withFinished:(void(^)(NSError* error))finishedLoading {
    self.didFinishLoading = finishedLoading;
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

+ (MPLWebView*) create:(void(^)(MPLWebView* v))creater {
    MPLWebView* v = [[MPLWebView alloc] init];
    creater(v);
    return v;
}

@end
