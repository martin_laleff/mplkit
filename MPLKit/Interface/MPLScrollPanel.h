//
//  FTScrollPanel.h
//  FTrack_Take2
//
//  Created by Martin Lalev on 9/6/11.
//  Copyright 2011 mART. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLView.h"
#import "MPLPanel.h"

@interface MPLScrollPanel : MPLPanel

@property (weak, nonatomic, readonly) UIScrollView* scrollView;
@property (nonatomic, assign) float bottomPadding;
@property (nonatomic, assign) float rightPadding;

- (CGSize) calculateContentSize;
- (void) loader;

@end
