//
//  FTLabel.m
//  FTrack_Take2
//
//  Created by Administrator on 05/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MPLLabel.h"

@implementation MPLLabel

#pragma mark -
#pragma mark View generation

@dynamic labelView;
- (UILabel*) labelView { return (UILabel*)self.view; }
- (UIView*) createView { return [[UILabel alloc] init]; }

#pragma mark -
#pragma mark Properties

- (NSTextAlignment) textAlignment { return self.labelView.textAlignment; }
- (UIColor*) textColor { return self.labelView.textColor; }
- (NSString*) text { return self.labelView.text; }
- (UIFont*) font { return self.labelView.font; }
- (NSInteger) numberOfLines { return self.labelView.numberOfLines; }
- (BOOL) adjustToFitWidth { return self.labelView.adjustsFontSizeToFitWidth; }
- (float) textWidth { return [MPLLabel widthForLabelWithText:self.text andFont:self.font andAlignment:self.textAlignment]; }
- (float) textHeight { return [MPLLabel heightForLabelWithText:self.text andWidth:self.widthBounds andFont:self.font andAlignment:self.textAlignment]; }

- (void) setText:(NSString *)text { self.labelView.text = text; }
- (void) setTextColor:(UIColor *)textColor { self.labelView.textColor = textColor; }
- (void) setTextAlignment:(NSTextAlignment)textAlignment { self.labelView.textAlignment = textAlignment; }
- (void) setFont:(UIFont *)font { self.labelView.font = font; }
- (void) setNumberOfLines:(NSInteger)numberOfLines { self.labelView.numberOfLines = numberOfLines; }
- (void) setAdjustToFitWidth:(BOOL)adjustToFitWidth { self.labelView.adjustsFontSizeToFitWidth = adjustToFitWidth; }

#pragma mark -
#pragma mark Custom methods

- (MPLLabel*) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                        andColor:(UIColor*)textColor
                         andFont:(UIFont*)font
{
    self.text = text;
    self.textAlignment = textAlignment;
    if (font != nil)
        self.font = font;
    if (textColor != nil)
        self.textColor = textColor;
    return self;
}
- (MPLLabel*) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                         andFont:(UIFont*)font
{
    return [self textLoaderWithText:text
                   andTextAlignment:textAlignment
                           andColor:nil
                            andFont:font
            ];
}
- (MPLLabel*) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
{
    return [self textLoaderWithText:text
                   andTextAlignment:textAlignment
                           andColor:nil
                            andFont:nil
            ];
}
- (MPLLabel*) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                        andColor:(UIColor*)textColor
{
    return [self textLoaderWithText:text
                   andTextAlignment:textAlignment
                           andColor:textColor
                            andFont:nil
            ];
}
- (MPLLabel*) assignFontWithLabelSizeFix:(int)fix {
    self.font = [UIFont systemFontOfSize:[UIFont labelFontSize]+fix];
    return self;
}
- (MPLLabel*) assignFontWithButtonSizeFix:(int)fix {
    self.font = [UIFont systemFontOfSize:[UIFont buttonFontSize]+fix];
    return self;
}

- (void) sizeToFitHeight {
    CGFloat w = self.widthFrame;
    [self.labelView sizeToFit];
    self.widthFrame = w;
}
- (void) sizeToFitWidth {
    CGFloat h = self.heightFrame;
    [self.labelView sizeToFit];
    self.heightFrame = h;
}

static NSString* _mpl_attatchmentTextColorAlphaParameter = @"alpha";
- (instancetype) attatchTextColorToTheme:(MPLTheme*)theme forKey:(NSString*)key withAlpha:(float)alpha {
    return [self attatch:@"textColor" toTheme:theme forKey:key withParameters:@{_mpl_attatchmentTextColorAlphaParameter:@(alpha)} andAction:^(MPLThemeViewAttatchment* themeAttatchment) {
        self.textColor = [[themeAttatchment.theme colorForKey:themeAttatchment.key] colorWithAlphaComponent:[[themeAttatchment.parameters objectForKey:_mpl_attatchmentTextColorAlphaParameter] floatValue]];
    }];
}
- (instancetype) attatchTextColorToTheme:(MPLTheme*)theme forKey:(NSString*)key {
    return [self attatch:@"textColor" toTheme:theme forKey:key andAction:^(MPLThemeViewAttatchment* themeAttatchment) {
        self.textColor = [themeAttatchment.theme colorForKey:themeAttatchment.key];
    }];
}


#pragma mark -
#pragma mark initializations

+ (MPLLabel*) create:(void(^)(MPLLabel* v))creater {
    MPLLabel* v = [[MPLLabel alloc] init];
    v.labelView.baselineAdjustment = UIBaselineAdjustmentNone;
    v.backgroundColor = [UIColor clearColor];
    creater(v);
    return v;
}

#pragma mark -
#pragma mark Common methods

- (float) widthForText:(NSString*)text {
    return [MPLLabel widthForLabelWithText:text andFont:self.font andAlignment:self.textAlignment];
}
- (float) heightForText:(NSString*)text {
    return [MPLLabel heightForLabelWithText:text andWidth:self.widthFrame andFont:self.font andAlignment:self.textAlignment];
}

+ (float) widthForLabelWithText:(NSString*)text andFont:(UIFont*)font andAlignment:(NSTextAlignment)alignment {
    UILabel* lblDescription = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10000)];
    lblDescription.font = font;
    lblDescription.numberOfLines = 1;
    lblDescription.text = text;
    lblDescription.textAlignment = alignment;
    [lblDescription sizeToFit];
    float height = lblDescription.bounds.size.width;
    return height;
}

+ (float) widthForLabelWithAttributedText:(NSAttributedString*)text andFont:(UIFont*)font andAlignment:(NSTextAlignment)alignment {
    UILabel* lblDescription = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10000)];
    lblDescription.font = font;
    lblDescription.numberOfLines = 1;
    lblDescription.attributedText = text;
    lblDescription.textAlignment = alignment;
    [lblDescription sizeToFit];
    float height = lblDescription.bounds.size.width;
    return height;
}

+ (float) heightForLabelWithText:(NSString*)text andWidth:(float)width andFont:(UIFont*)font andAlignment:(NSTextAlignment)alignment {
    UILabel* lblDescription = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 10000)];
    lblDescription.font = font;
    lblDescription.numberOfLines = 0;
    lblDescription.text = text;
    lblDescription.textAlignment = alignment;
    [lblDescription sizeToFit];
    float height = lblDescription.bounds.size.height;
    return height;
}

+ (float) heightForLabelWithAttributedText:(NSAttributedString*)text andWidth:(float)width andFont:(UIFont*)font andAlignment:(NSTextAlignment)alignment {
    UILabel* lblDescription = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 10000)];
    lblDescription.font = font;
    lblDescription.numberOfLines = 0;
    lblDescription.attributedText = text;
    lblDescription.textAlignment = alignment;
    [lblDescription sizeToFit];
    float height = lblDescription.bounds.size.height;
    return height;
}

@end
