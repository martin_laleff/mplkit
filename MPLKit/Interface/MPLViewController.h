//
//  FTViewController.h
//  FTrack_Take2
//
//  Created by Martin Lalev on 9/6/11.
//  Copyright 2011 mART. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPLView.h"

@interface MPLViewController : UIViewController

@property (nonatomic, strong) MPLView* baseView;

- (void) loadBaseView:(MPLView*)baseView;
+ (MPLViewController*) viewControllerWithBaseView:(MPLView*)_baseView;

@end
