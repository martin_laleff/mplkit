//
//  FTSlider.m
//  FTrack_Take2
//
//  Created by Administrator on 08/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MPLSlider.h"

@implementation MPLSlider

- (UISlider*) sliderView { return (UISlider*)self.view; }
- (UIView*) createView { return [[UISlider alloc] init]; }

- (float) value { return self.sliderView.value; }
- (float) minimumValue { return self.sliderView.minimumValue; }
- (float) maximumValue { return self.sliderView.maximumValue; }

- (void) setValue:(float)value { self.sliderView.value = value; }
- (void) setMinimumValue:(float)minimumValue { self.sliderView.minimumValue = minimumValue; }
- (void) setMaximumValue:(float)maximumValue { self.sliderView.maximumValue = maximumValue; }

- (void) loaderWithValue:(CGFloat)value
                  andMinValue:(CGFloat)minValue
                  andMaxValue:(CGFloat)maxValue
                  andMinImage:(UIImage*)minImage
                  andMaxImage:(UIImage*)maxImage
         andValueChangedAction:(MPLAction*)valueChangedAction
{
	self.sliderView.minimumValue = minValue;
	self.sliderView.maximumValue = maxValue;
	self.sliderView.value = value;
	self.sliderView.minimumValueImage = minImage;
	self.sliderView.maximumValueImage = maxImage;
	[self addAction:valueChangedAction forEventType:UIControlEventValueChanged];
}

- (void) loaderWithValue:(CGFloat)value
                   andMinValue:(CGFloat)minValue
                   andMaxValue:(CGFloat)maxValue
         andValueChangedAction:(MPLAction*)valueChangedAction
{
    [self loaderWithValue:value andMinValue:minValue andMaxValue:maxValue andMinImage:nil andMaxImage:nil andValueChangedAction:valueChangedAction];
}

@end
