//
//  FTLabel.h
//  FTrack_Take2
//
//  Created by Administrator on 05/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLView.h"

@interface MPLLabel : MPLView

@property (weak, nonatomic, readonly) UILabel* labelView;

@property (nonatomic, assign) NSTextAlignment textAlignment;
@property (nonatomic, weak) UIColor* textColor;
@property (nonatomic, weak) NSString* text;
@property (nonatomic, weak) UIFont* font;
@property (nonatomic, assign) NSInteger numberOfLines;
@property (nonatomic, assign) BOOL adjustToFitWidth;
@property (nonatomic, readonly) float textWidth;
@property (nonatomic, readonly) float textHeight;

- (void) sizeToFitHeight;
- (void) sizeToFitWidth;
- (instancetype) attatchTextColorToTheme:(MPLTheme*)theme forKey:(NSString*)key withAlpha:(float)alpha;
- (instancetype) attatchTextColorToTheme:(MPLTheme*)theme forKey:(NSString*)key;

- (MPLLabel*) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                        andColor:(UIColor*)textColor
                         andFont:(UIFont*)font
;
- (MPLLabel*) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                         andFont:(UIFont*)font
;
- (MPLLabel*) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
;
- (MPLLabel*) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                        andColor:(UIColor*)textColor
;
- (MPLLabel*) assignFontWithLabelSizeFix:(int)fix;
- (MPLLabel*) assignFontWithButtonSizeFix:(int)fix;

+ (MPLLabel*) create:(void(^)(MPLLabel* v))creater;

+ (float) widthForLabelWithText:(NSString*)text andFont:(UIFont*)font andAlignment:(NSTextAlignment)alignment;
+ (float) widthForLabelWithAttributedText:(NSAttributedString*)text andFont:(UIFont*)font andAlignment:(NSTextAlignment)alignment;
+ (float) heightForLabelWithText:(NSString*)text andWidth:(float)width andFont:(UIFont*)font andAlignment:(NSTextAlignment)alignment;
+ (float) heightForLabelWithAttributedText:(NSAttributedString*)text andWidth:(float)width andFont:(UIFont*)font andAlignment:(NSTextAlignment)alignment;

- (float) widthForText:(NSString*)text;
- (float) heightForText:(NSString*)text;

@end
