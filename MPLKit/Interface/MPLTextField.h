//
//  FTTextbox.h
//  FTrack_Take2
//
//  Created by Martin Lalev on 9/6/11.
//  Copyright 2011 mART. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLView.h"

@interface MPLTextField : MPLView<UITextFieldDelegate>

@property (weak, nonatomic, readonly) UITextField* textFieldView;
@property (nonatomic, strong) UIToolbar* toolbar;

- (void) indicateValid:(BOOL)isValid;
- (void) indicateValid:(BOOL)isValid withViewToShake:(MPLView*)viewToShake;
- (BOOL) isEmail;

- (void) loaderWithPlaceholder:(NSString*)placeholder
                 andIsPassword:(BOOL)isPassword
               andKeyboardType:(UIKeyboardType)keyboardType
              andTextAlignment:(NSTextAlignment)textAlignment
;
- (void) enableToolbarAccessoryWithBarButtonGenerator:(void (^)(NSMutableArray *))barButtonsGenerator;

+ (MPLTextField*) create:(void(^)(MPLTextField* v))creater;

@end
