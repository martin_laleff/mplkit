//
//  FTAlert.h
//  TestAppProject
//
//  Created by Martin Lalev on 4/29/13.
//  Copyright (c) 2013 Shtrak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^MPLAlertFinished)(NSString* input1, NSString* input2, NSInteger index);

@interface MPLAlert : UIAlertView<UIAlertViewDelegate>
{
    MPLAlertFinished Finished;
}

@property (nonatomic, copy) MPLAlertFinished Finished;

+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle andMoreButtonTitles:(NSArray *)otherButtonTitles;
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle;
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style cancelButtonTitle:(NSString *)cancelButtonTitle;

+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style andCreated:(void(^)(MPLAlert* alert))created finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle andMoreButtonTitles:(NSArray *)otherButtonTitles;
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style andCreated:(void(^)(MPLAlert* alert))created finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style andCreated:(void(^)(MPLAlert* alert))created finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle;
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style andCreated:(void(^)(MPLAlert* alert))created cancelButtonTitle:(NSString *)cancelButtonTitle;

@end
