
#import "MPLPanel.h"


@implementation MPLPanel

@synthesize subViews;
@synthesize subViewsStack;

- (UIView*) panelView { return (UIView*)self.view; }
- (UIView*) createView { return [[UIView alloc] init]; }

- (id) init {
    if ([super init]) {
        self.subViews = [NSMutableDictionary dictionary];
        self.subViewsStack = [NSMutableArray array];
    }
    return self;
}
+ (instancetype) create:(void(^)(id v))creater {
    id v = [[self alloc] init];
    creater(v);
    return v;
}

- (void) destroyView {
    while (self.subViews.count > 0)
        [self removeSubviewForKey:[self.subViews.allKeys objectAtIndex:0] andDestroy:YES];
    self.subViews = nil;
    self.subViewsStack = nil;
    [self.blurView removeFromSuperview];
    self.blurView = nil;
    [super destroyView];
}
- (void) dealloc {
    [self destroyView];
}

- (void) colorizeForLayer:(int)layer {
    [super colorizeForLayer:layer];
    for (MPLView* subview in self.subViewsStack)
        [subview colorizeForLayer:layer+1];
}


- (NSString*) keyForSubview:(MPLView*)_view {
    for (NSString* key in [self.subViews allKeys])
        if ([self.subViews objectForKey:key] == _view)
            return key;
    return nil;
}
- (MPLView*) subviewForKey:(NSString*)key {
    return [self.subViews objectForKey:key];
}

- (MPLPanel*) addSubview:(MPLView*)control forKey:(NSString*)key {
    if (control.parentView != nil) {
        [(MPLPanel*)control.parentView removeSubview:control andDestroy:NO];
    }
    [self.subViews setObject:control forKey:key];
    [self.subViewsStack addObject:control];
    control.parentView = self;
    [control createInside:self.panelView];
    if (self.action_button != nil) { [self.view bringSubviewToFront:self.action_button]; }
    return self;
}
- (void) removeSubviewForKey:(NSString*)key andDestroy:(BOOL)destroy {
    if (key == nil) return;
    MPLView* c = [self.subViews objectForKey:key];
    [self.subViews removeObjectForKey:key];
    [self.subViewsStack removeObject:c];
    if (destroy) [c destroyView];
    else [c removeFromSuperview];
}
- (void) removeSubview:(MPLView*)_view andDestroy:(BOOL)destroy { [self removeSubviewForKey:[self keyForSubview:_view] andDestroy:destroy]; }

- (void) enableBlurWithStyle:(UIBlurEffectStyle)style {
    self.blurView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:style]];
    self.blurView.frame = self.bounds;
    [self.view addSubview:self.blurView];
}

@end
