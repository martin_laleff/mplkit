//
//  FTSwitch.h
//  FTrack_Take2
//
//  Created by Administrator on 19/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLView.h"


@interface MPLSwitch : MPLView

@property (weak, nonatomic, readonly) UISwitch* switchView;

- (void) loaderWithSwitchedOn:(BOOL)_on andAction:(MPLAction*)onSwitched;

@end
