//
//  FTScrollPanel.m
//  FTrack_Take2
//
//  Created by Martin Lalev on 9/6/11.
//  Copyright 2011 mART. All rights reserved.
//

#import "MPLScrollPanel.h"

@implementation MPLScrollPanel

@synthesize scrollView;
@synthesize bottomPadding = _bottomPadding;
@synthesize rightPadding = _rightPadding;

- (UIScrollView*)scrollView { return (UIScrollView*)self.view; }
- (UIView*) createView { return [[UIScrollView alloc] init]; }

- (void) setBottomPadding:(float)bottomPadding {
    _bottomPadding = bottomPadding;
    [self resetSize];
}
- (void) setRightPadding:(float)rightPadding {
    _rightPadding = rightPadding;
    [self resetSize];
}

- (void) loader {
    self.scrollView.canCancelContentTouches = YES;
}

- (MPLPanel*) addSubview:(MPLView *)control forKey:(NSString *)key {
    MPLPanel* result = [super addSubview:control forKey:key];
    [self resetSize];
    [control addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
    [control addObserver:self forKeyPath:@"bounds" options:NSKeyValueObservingOptionNew context:nil];
    [control addObserver:self forKeyPath:@"center" options:NSKeyValueObservingOptionNew context:nil];
    [control addObserver:self forKeyPath:@"alpha" options:NSKeyValueObservingOptionNew context:nil];
    return result;
}
- (void) removeSubviewForKey:(NSString *)key andDestroy:(BOOL)destroy {
	MPLView* control = [self.subViews objectForKey:key];
    [control removeObserver:self forKeyPath:@"frame"];
    [control removeObserver:self forKeyPath:@"bounds"];
    [control removeObserver:self forKeyPath:@"center"];
    [control removeObserver:self forKeyPath:@"alpha"];
    [super removeSubviewForKey:key andDestroy:destroy];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [self resetSize];
}

- (CGSize) calculateContentSize {
    float minX = 0;
    float minY = 0;
    for (MPLView* v in self.subViews.allValues) {
        if (!v.visible || v.view.hidden) continue;
        float r = v.xOffset+v.widthFrame;
        float b = v.yOffset+v.heightFrame;
        if (r > minX) minX = r;
        if (b > minY) minY = b;
    }
    return CGSizeMake(minX, minY);
}
- (void) resetSize {
    CGSize ccs = [self calculateContentSize];
    self.scrollView.contentSize = CGSizeMake(ccs.width+self.rightPadding, ccs.height+self.bottomPadding);
}

@end
