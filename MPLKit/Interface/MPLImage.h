//
//  FTImage.h
//  FTrack_Take2
//
//  Created by Administrator on 05/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLView.h"

@interface MPLImage : MPLView {
    UIActivityIndicatorView* activity;
    UIImage* brokenImage;
}

@property (nonatomic, strong) NSString* requestedImage;
@property (nonatomic, strong) UIActivityIndicatorView* activity;
@property (nonatomic, strong) UIImage* brokenImage;

@property (nonatomic, weak) UIImage* image;
@property (nonatomic, strong) UIImage* imageToBeResized;
@property (nonatomic, weak) UIImage* imageResized;
@property (nonatomic, assign) UIViewContentMode contentMode;

@property (weak, nonatomic, readonly) UIImageView* imageView;

- (void) loadWebImageFromLocation:(NSString*)imageLocation usingLocalCertificates:(NSArray*)localCertificates andCustomCA:(BOOL)customCA andSelfSignedCert:(BOOL)selfSignedCert withFinished:(void(^)(void))finished;
- (void) assignImageResizedFitTo:(UIImage*)image finished:(void(^)(void))finished;
- (void) assignImageResizedFillTo:(UIImage*)image finished:(void(^)(void))finished;
- (void) fadeInImageWithDuration:(float)duration processer:(void(^)(void(^ready)(void)))processer finished:(void(^)(void))finished didFadeIn:(void(^)(void))didFadeIn;
- (void) fadeInImageWithDuration:(float)duration toAlpha:(float)toAlpha processer:(void(^)(void(^ready)(void)))processer finished:(void(^)(void))finished didFadeIn:(void(^)(void))didFadeIn;

+ (MPLImage*) create:(void(^)(MPLImage* v))creater;

@end
