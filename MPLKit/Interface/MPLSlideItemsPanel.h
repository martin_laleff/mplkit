//
//  MPLSlideItemsPanel.h
//  MPLKit
//
//  Created by Martin Lalev on 11/20/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPLSlideItemsPanel : MPLPanel
{
    NSInteger ItemCount;
    CGFloat SlidePosition;
    CGFloat SingleSlideDistance;
    NSInteger CurrentSlidePosition;
    BOOL IsHorizontal;
    MPLAction* PositionChanged;
    BOOL Inverted;
}

@property (nonatomic, assign) NSInteger ItemCount;
@property (nonatomic, assign) CGFloat SlidePosition;
@property (nonatomic, assign) CGFloat SingleSlideDistance;
@property (nonatomic, assign) NSInteger CurrentSlidePosition;
@property (nonatomic, assign) BOOL IsHorizontal;
@property (nonatomic, strong) MPLAction* PositionChanged;
@property (nonatomic, assign) BOOL Inverted;
@property (nonatomic, assign) CGFloat SlideVelocity;

- (void) slideTo:(NSInteger)position;
- (void) positionChanged:(BOOL)ended withActualDistance:(float)actualDistance fromLastPosition:(NSInteger)lastPosition codeInvoked:(BOOL)codeInvoked;

- (NSInteger) controlPositionForIndex:(NSInteger)index;
- (NSInteger) controlIndexAtPosition:(NSInteger)position;

- (void) loaderWithPosition:(CGFloat)position
     andSingleSlideDistance:(CGFloat)singleSlideDistance
            andIsHorizontal:(BOOL)isHorizontal
               andItemCount:(NSInteger)itemCount
              andInvocation:(MPLAction*)invocation
;

@end

@interface MPLHorizontalSliderView : MPLSlideItemsPanel

@property (nonatomic, strong) NSMutableArray* views;
@property (nonatomic, copy) void(^viewReloader)(MPLView* v, NSInteger incrementBy);
@property (nonatomic, copy) void(^didSlide)(void);

@property (nonatomic, copy) void(^onDidSlide)(NSInteger index,NSInteger position,CGFloat slideDistance,BOOL ended,NSInteger lastPosition,float actualDistance,BOOL codeInvoked);
- (void) didSlideViewAtIndex:(NSInteger)index position:(NSInteger)position slideDistance:(CGFloat)slideDistance ended:(BOOL)ended lastPosition:(NSInteger)lastPosition actualDistance:(float)actualDistance codeInvoked:(BOOL)codeInvoked;

- (void) loaderWithItemsCount:(int)itemsCount andViewGenerator:(void (^)(int index, void(^loader)(MPLView* v)))viewGenerator andReloader:(void(^)(MPLView* v, NSInteger incrementBy))reloader andDidSlide:(void(^)(void))didSlide;

@end
