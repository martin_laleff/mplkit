//
//  FTSegment.m
//  FTrack_Take2
//
//  Created by Administrator on 08/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MPLSegment.h"


@implementation MPLSegment

- (UISegmentedControl*) segmentedControlView { return (UISegmentedControl*)self.view; }
- (UIView*) createView { return [[UISegmentedControl alloc] init]; }

- (NSInteger) selectedIndex { return self.segmentedControlView.selectedSegmentIndex; }
- (void) setSelectedIndex:(NSInteger)selectedIndex { if (selectedIndex >= 0 && selectedIndex < self.segmentedControlView.numberOfSegments) self.segmentedControlView.selectedSegmentIndex = selectedIndex; }

- (void) insertItem:(id)item atIndex:(NSUInteger)index animated:(BOOL)animated {
    if ([item isKindOfClass:[NSString class]]) {
        [(UISegmentedControl*)self.segmentedControlView insertSegmentWithTitle:(NSString*)item atIndex:index animated:animated];
    }
    else if ([item isKindOfClass:[UIImage class]]) {
        [self.segmentedControlView insertSegmentWithImage:(UIImage*)item atIndex:index animated:animated];
    }
    else {
    }
}
- (void) addItem:(id)item animated:(BOOL)animated {
    [self insertItem:item atIndex:self.segmentedControlView.numberOfSegments animated:animated];
}
- (void) removeItemAtIndex:(NSUInteger)index animated:(BOOL)animated {
    [self.segmentedControlView removeSegmentAtIndex:index animated:animated];
}

- (void) loaderWithItems:(NSArray*)items andSelectedIndex:(int)selectedIndex andAction:(MPLAction*)selectionChanged {
    for (id item in items) [self addItem:item animated:NO];
    self.selectedIndex = selectedIndex;
	
    [self addAction:selectionChanged forEventType:UIControlEventValueChanged];
}
- (void) loaderWithItems:(NSArray*)items andAction:(MPLAction*)selectionChanged {
    [self loaderWithItems:items andSelectedIndex:-1 andAction:selectionChanged];
}

@end
