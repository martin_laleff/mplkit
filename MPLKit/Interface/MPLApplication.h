//
//  FTApplication.h
//  FTrack_Take2
//
//  Created by Martin Lalev on 9/6/11.
//  Copyright 2011 mART. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "MPLViewController.h"
#import "MPLPushNotificationsManager.h"
#import "MPLKeychain.h"

#define FTKeyForID(obj) [NSNumber numberWithLong:(long)obj]



@interface MPLApplication : UIApplication<UIApplicationDelegate, SKStoreProductViewControllerDelegate> {
    BOOL started;
    
    UIWindow* window;
    MPLPushNotificationsManager* pushNotificationManager;
    
    MPLAction* OnTermination;
    
    UILocalNotification* launchedWithLocalNotification;
    NSDictionary* launchedWithRemoteNotification;
    
    UIImageView* splashImageView;
    
    NSString* upgradedFrom;
}

@property (nonatomic, assign) BOOL started;

@property (nonatomic, readonly) float statusBarHeight;

@property (nonatomic, strong) UIWindow* window;
@property (weak, nonatomic, readonly) MPLViewController* rootController;
@property (weak, nonatomic, readonly) MPLView* baseView;
@property (nonatomic, strong) MPLPushNotificationsManager* pushNotificationManager;
@property (nonatomic, readonly) NetworkStatus reachabilityStatus;

@property (nonatomic, strong) MPLAction* OnTermination;

@property (nonatomic, strong) UILocalNotification* launchedWithLocalNotification;
@property (nonatomic, strong) NSDictionary* launchedWithRemoteNotification;

@property (nonatomic, strong) UIImageView* splashImageView;

@property (weak, nonatomic, readonly) NSString* currentVersion;
@property (nonatomic, weak) NSString* currentlyInstalledVersion;
@property (nonatomic, assign) NSInteger currentVersionStartCount;
@property (nonatomic, assign) NSInteger currentInstallationStartCount;
@property (nonatomic, assign) NSInteger totalStartCount;

@property (nonatomic, strong) NSString* upgradedFrom;

@property (nonatomic, assign) UIDeviceOrientation currentOrientation;

- (NSArray*) allVersions;
- (NSString*) currentInstallationStartCountKey;
- (NSString*) totalStartCountKey;
- (NSString*) startCountKeyForVersion:(NSString*)version;
- (NSInteger) startCountForVersion:(NSString*)version;
- (void) setStartCountForVersion:(NSString*)version count:(NSInteger)value;

- (void) applicationStarted:(BOOL)indicateStarted andIncreaseVersion:(BOOL)incTotal;

+ (instancetype) SharedApplication;

- (void) setRootControllerForBaseView:(MPLView*)baseView;

- (void) adjustToKeyboardSize:(CGSize)keyboardSize;

- (void) loadViewControllersWithOptions:(NSDictionary *)launchOptions;

- (void) createAndFadeSplashImageWithDuration:(float)duration andDelay:(float)delay andProcessing:(void(^)(void(^processingFinished)(void)))processing andFinished:(void(^)(void))finishedBlock;
- (void) createAndFadeSplashImageViewForImage:(UIImage*)image withDuration:(float)duration andDelay:(float)delay andProcessing:(void(^)(void(^processingFinished)(void)))processing andFinished:(void(^)(void))finishedBlock;
- (void) createAndFadeSplashImageViewWithDuration:(float)duration andDelay:(float)delay andFinished:(void(^)(void))finishedBlock;
- (void) createAndFadeSplashImageViewForImage:(UIImage*)image withDuration:(float)duration andDelay:(float)delay andFinished:(void(^)(void))finishedBlock;
- (void) createSplashImageView;
- (void) createSplashImageViewForImage:(UIImage*)image;
- (void) fadeSplashImageWithDuration:(float)duration andDelay:(float)delay andFinished:(void(^)(void))finishedBlock;
- (UIImage*) defaultSplashImage;

- (void) orientationChangedToLandscape:(BOOL)homeButtonLeft;
- (void) orientationChangedToPortrait:(BOOL)homeButtonBottom;

- (void) askForRatingConditioned:(BOOL)condition appIdentifier:(NSString*)identifier presentController:(BOOL)presentController message:(NSString*)message acceptText:(NSString*)accept declineText:(NSString*)decline laterText:(NSString*)later later:(void(^)(void))laterAction;

+ (void) logFonts;

@end
