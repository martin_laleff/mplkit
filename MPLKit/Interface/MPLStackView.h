//
//  MPLStackView.h
//  Posledno
//
//  Created by Martin Lalev on 12/12/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLPanel.h"

@interface MPLStackView : MPLPanel

@property (nonatomic, assign) float spacing;
@property (nonatomic, strong) MPLView* lastAddedView;

- (void) loader;
- (CGSize) calculateContentSize;
- (void) resetSize;

- (void) assignNextSpacing:(float)spacing;

- (void) addView:(MPLView*)v withHeight:(float)height withHorizontalMargin:(float)horizontalMargin;
- (void) addView:(MPLView*)v withHeight:(float)height;

- (MPLImage*) addImage:(UIImage*)image withHeight:(float)height;
- (MPLImage*) addImage:(UIImage*)image;

- (MPLLabel*) addLabelWithText:(NSString*)text andAlignment:(NSTextAlignment)alignment andFont:(UIFont*)font andColor:(UIColor*)color andHeight:(float)height;
- (MPLLabel*) addLabelWithText:(NSString*)text andAlignment:(NSTextAlignment)alignment andFont:(UIFont*)font andColor:(UIColor*)color;

- (MPLLabel*) addButtonWithText:(NSString*)text andBackgroundColor:(UIColor*)backgroundColor andTextColor:(UIColor*)textColor andFont:(UIFont*)font andAction:(void(^)(void))action;

@end
