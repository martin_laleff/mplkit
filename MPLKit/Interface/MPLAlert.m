//
//  FTAlert.m
//  TestAppProject
//
//  Created by Martin Lalev on 4/29/13.
//  Copyright (c) 2013 Shtrak. All rights reserved.
//

#import "MPLAlert.h"

@implementation MPLAlert

@synthesize Finished;

- (id) initWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle
{
    if ([super initWithTitle:title message:message delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil])
    {
        self.Finished = finished;
        self.alertViewStyle = style;
    }
    return self;
}

- (void) finishedWithIndex:(NSInteger)buttonIndex
{
    NSString* input1 = (self.alertViewStyle == UIAlertViewStyleDefault ? nil : [self textFieldAtIndex:0].text);
    NSString* input2 = (self.alertViewStyle != UIAlertViewStyleLoginAndPasswordInput ? nil : [self textFieldAtIndex:1].text);
    self.Finished(input1, input2, buttonIndex);
    [MPLAction performAfter:1 block:^{
    }];
}
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self finishedWithIndex:buttonIndex];
}
- (void) alertViewCancel:(UIAlertView *)alertView
{
    [self finishedWithIndex:self.cancelButtonIndex];
}
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
}

+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style andCreated:(void(^)(MPLAlert* alert))created finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle andMoreButtonTitles:(NSArray *)otherButtonTitles
{
    MPLAlert* alert = [[MPLAlert alloc] initWithTitle:title message:message andStyle:style finishedDelegate:finished cancelButtonTitle:cancelButtonTitle];
    for (NSString *anOtherButtonTitle in otherButtonTitles)
        [alert addButtonWithTitle:anOtherButtonTitle];
    created(alert);
    [alert show];
}

+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style andCreated:(void(^)(MPLAlert* alert))created finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    MPLAlert* alert = [[MPLAlert alloc] initWithTitle:title message:message andStyle:style finishedDelegate:finished cancelButtonTitle:cancelButtonTitle];
    va_list args;
    va_start(args, otherButtonTitles);
    for (NSString *anOtherButtonTitle = otherButtonTitles; anOtherButtonTitle != nil; anOtherButtonTitle = va_arg(args, NSString*))
        [alert addButtonWithTitle:anOtherButtonTitle];
    va_end(args);
    created(alert);
    [alert show];
}
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style andCreated:(void(^)(MPLAlert* alert))created finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle
{
    [MPLAlert alertWithTitle:title message:message andStyle:style andCreated:created finishedDelegate:finished cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
}
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style andCreated:(void(^)(MPLAlert* alert))created cancelButtonTitle:(NSString *)cancelButtonTitle
{
    [MPLAlert alertWithTitle:title message:message andStyle:style andCreated:created finishedDelegate:^(NSString* input1, NSString* input2, NSInteger index){} cancelButtonTitle:cancelButtonTitle];
}


+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle andMoreButtonTitles:(NSArray *)otherButtonTitles {
    MPLAlert* alert = [[MPLAlert alloc] initWithTitle:title message:message andStyle:style finishedDelegate:finished cancelButtonTitle:cancelButtonTitle];
    for (NSString *anOtherButtonTitle in otherButtonTitles)
        [alert addButtonWithTitle:anOtherButtonTitle];
    [alert show];
}
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... {
    MPLAlert* alert = [[MPLAlert alloc] initWithTitle:title message:message andStyle:style finishedDelegate:finished cancelButtonTitle:cancelButtonTitle];
    va_list args;
    va_start(args, otherButtonTitles);
    for (NSString *anOtherButtonTitle = otherButtonTitles; anOtherButtonTitle != nil; anOtherButtonTitle = va_arg(args, NSString*))
        [alert addButtonWithTitle:anOtherButtonTitle];
    va_end(args);
    [alert show];
}
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style finishedDelegate:(MPLAlertFinished)finished cancelButtonTitle:(NSString *)cancelButtonTitle {
    [self alertWithTitle:title message:message andStyle:style andCreated:^(MPLAlert*alert){} finishedDelegate:finished cancelButtonTitle:cancelButtonTitle];
}
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message andStyle:(UIAlertViewStyle)style cancelButtonTitle:(NSString *)cancelButtonTitle {
    [self alertWithTitle:title message:message andStyle:style andCreated:^(MPLAlert*alert){} cancelButtonTitle:cancelButtonTitle];
}


@end
