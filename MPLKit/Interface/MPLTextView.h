//
//  MPLTextView.h
//  ShtrakPollApps
//
//  Created by Martin Lalev on 1/21/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPLView.h"

@interface MPLTextViewPanel : MPLView<UITextViewDelegate>

@property (nonatomic, weak) NSString* text;
@property (nonatomic, strong) UITextView* textView;
@property (nonatomic, strong) UILabel* placeholderLabel;

- (void) indicateValid:(BOOL)isValid;
- (void) indicateValid:(BOOL)isValid withViewToShake:(MPLView*)viewToShake;
- (BOOL) isEmail;

- (void) loaderWithPlaceholder:(NSString*)placeholder
                 andIsPassword:(BOOL)isPassword
               andKeyboardType:(UIKeyboardType)keyboardType
              andTextAlignment:(NSTextAlignment)textAlignment
;
+ (MPLTextViewPanel*) create:(void(^)(MPLTextViewPanel* v))creater;

@end

@interface MPLTextView : MPLView<UITextViewDelegate>

@property (nonatomic, weak) NSString* text;
@property (weak, nonatomic, readonly) UITextView* textView;
@property (nonatomic, strong) UILabel* placeholderLabel;
@property (nonatomic, strong) UIButton* placeholderButton;
@property (nonatomic, strong) UIToolbar* toolbar;

- (void) indicateValid:(BOOL)isValid;
- (void) indicateValid:(BOOL)isValid withViewToShake:(MPLView*)viewToShake;
- (BOOL) isEmail;

- (void) enableToolbarAccessoryWithBarButtonGenerator:(void(^)(NSMutableArray* barButtons))barButtonsGenerator;
- (void) loaderWithPlaceholder:(NSString*)placeholder
                 andIsPassword:(BOOL)isPassword
               andKeyboardType:(UIKeyboardType)keyboardType
              andTextAlignment:(NSTextAlignment)textAlignment
;
+ (MPLTextView*) create:(void(^)(MPLTextView* v))creater;

@end
