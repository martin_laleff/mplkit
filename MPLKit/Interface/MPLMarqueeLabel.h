//
//  FTMarqueeLabel.h
//  Mixweet
//
//  Created by Martin Lalev on 10/15/13.
//
//

#import "MPLView.h"
#import "MarqueeLabel.h"

@interface MPLMarqueeLabel : MPLView

@property (weak, nonatomic, readonly) MarqueeLabel* marqueeLabel;
@property (nonatomic, assign) NSTextAlignment textAlignment;
@property (nonatomic, weak) UIColor* textColor;
@property (nonatomic, weak) NSString* text;
@property (nonatomic, weak) UIFont* font;
@property (nonatomic, assign) NSInteger numberOfLines;
@property (nonatomic, assign) BOOL adjustToFitWidth;

- (void) sizeToFitHeight;
- (void) sizeToFitWidth;
- (instancetype) attatchTextColorToTheme:(MPLTheme*)theme forKey:(NSString*)key withAlpha:(float)alpha;
- (instancetype) attatchTextColorToTheme:(MPLTheme*)theme forKey:(NSString*)key;

- (instancetype) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                        andColor:(UIColor*)textColor
                         andFont:(UIFont*)font
;
- (instancetype) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                         andFont:(UIFont*)font
;
- (instancetype) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
;
- (instancetype) textLoaderWithText:(NSString*)text
                andTextAlignment:(NSTextAlignment)textAlignment
                        andColor:(UIColor*)textColor
;
- (MPLMarqueeLabel*) assignFontWithLabelSizeFix:(int)fix;
- (MPLMarqueeLabel*) assignFontWithButtonSizeFix:(int)fix;

+ (MPLMarqueeLabel*) create:(void(^)(MPLMarqueeLabel* v))creater;

@end
