//
//  MPLNavigationSubScreen.m
//  MPLKit
//
//  Created by Martin Lalev on 12/29/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLNavigationSubScreen.h"
#import "MPLNavigationScreen.h"

@implementation MPLNavigationSubScreen

- (id) init { return [self initWithNavigationScreen:[[MPLNavigationHeaderContentScreen alloc] init]]; }
- (id) initWithNavigationScreen:(MPLNavigationScreen*)navigationScreen {
    if ([super init]) {
        self.navigationScreen = navigationScreen;
        [self.navigationScreen.subScreens addObject:self];
    }
    return self;
}
- (void) destroyScreen {
    [super destroyScreen];
    self.willClose = nil;
    self.didClose = nil;
    [self.navigationScreen.subScreensStack removeObject:self];
    [self.navigationScreen.subScreens removeObject:self];
    self.navigationScreen = nil;
}
- (MPLNavigationTransitionStash) closeWithStash {
    MPLNavigationTransitionStash closeStash = MPLNavigationTransitionStashTogether;
    if (self.openedWithStash == MPLNavigationTransitionStashAfter) closeStash = MPLNavigationTransitionStashBefore;
    if (self.openedWithStash == MPLNavigationTransitionStashBefore) closeStash = MPLNavigationTransitionStashAfter;
    return closeStash;
}



#pragma mark -
#pragma mark Push/pop

- (void) willPopWithFinished:(void(^)(void))finished {
    if (self.willClose != nil) {
        self.willClose(^{
            self.willClose = nil;
            finished();
        });
    }
    else {
        finished();
    }
}
- (void) didPopWithFinished:(void(^)(void))finished {
    if (self.didClose != nil) {
        self.didClose(^{
            self.didClose = nil;
            finished();
        });
    }
    else {
        finished();
    }
}

- (instancetype) assignWillPop:(void(^)(void(^ready)(void)))willPop { self.willClose = willPop; return self; }
- (instancetype) assignDidPop:(void(^)(void(^ready)(void)))didPop { self.didClose = didPop; return self; }

// Public point - called by other classes (never by screen)
- (instancetype) pushAnimated:(BOOL)animated stashing:(MPLNavigationTransitionStash)stashing finished:(void(^)(void))finished {
    if (self.navigationScreen.topSubScreen == nil) {
        if (self.navigationScreen.rootSubScreen == self)
            [self.navigationScreen openScreenAnimated:animated finished:finished];
        else
            [self.navigationScreen pushSubScreen:self animated:NO stashing:stashing finished:^{
                [self.navigationScreen openScreenAnimated:animated finished:finished];
            }];
    }
    else
        [self.navigationScreen pushSubScreen:self animated:animated stashing:stashing finished:finished];
    return self;
}
- (void) popAnimated:(BOOL)animated withFinished:(void(^)(void))finished {
    void (^didFinish)(void) = ^{ [self didPopWithFinished:finished]; };
    if (!self.isOpened) finished();
    else
        [self willPopWithFinished:^{
            if (self == self.navigationScreen.rootSubScreen)
                [self.navigationScreen closeScreenAnimated:animated finished:didFinish];
            else if (self == self.navigationScreen.topSubScreen)
                [self.navigationScreen popSubScreen:self.navigationScreen.topSubScreen animated:animated finished:didFinish];
            else
                [self.navigationScreen goBackToSubScreen:[self.navigationScreen subScreenBefore:self] animated:animated finished:didFinish];
        }];
}

@end
