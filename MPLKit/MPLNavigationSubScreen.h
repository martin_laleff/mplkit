//
//  MPLNavigationSubScreen.h
//  MPLKit
//
//  Created by Martin Lalev on 12/29/14.
//  Copyright (c) 2014 Martin Lalev. All rights reserved.
//

#import "MPLScreen.h"

typedef enum : NSUInteger {
    MPLNavigationTransitionStashTogether,
    MPLNavigationTransitionStashBefore,
    MPLNavigationTransitionStashAfter,
} MPLNavigationTransitionStash;

@class MPLNavigationScreen;



@interface MPLNavigationSubScreen : MPLScreen

@property (nonatomic, assign) MPLNavigationTransitionStash openedWithStash;
@property (nonatomic, readonly) MPLNavigationTransitionStash closeWithStash;

@property (nonatomic, copy) void(^willClose)(void(^ready)(void));
@property (nonatomic, copy) void(^didClose)(void(^ready)(void));

@property (nonatomic, strong) MPLNavigationScreen* navigationScreen;
- (id) initWithNavigationScreen:(MPLNavigationScreen*)navigationScreen;

- (instancetype) assignWillPop:(void(^)(void(^ready)(void)))willPop;
- (instancetype) assignDidPop:(void(^)(void(^ready)(void)))didPop;

- (instancetype) pushAnimated:(BOOL)animated stashing:(MPLNavigationTransitionStash)stashing finished:(void(^)(void))finished;
- (void) popAnimated:(BOOL)animated withFinished:(void(^)(void))finished;

@end
