//
//  NSData+AES256.h
//  Mixweet
//
//  Created by Martin Lalev on 2/4/14.
//
//

#import <Foundation/Foundation.h>

@interface NSData (AES256)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

@end
