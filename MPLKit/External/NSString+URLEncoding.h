//
//  NSString+URLEncoding.h
//  ShtrakPollApps
//
//  Created by Martin Lalev on 11/13/13.
//  Copyright (c) 2013 Martin Lalev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLEncoding)

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
-(NSString *)urlDecodeUsingEncoding:(NSStringEncoding)encoding;

@end
