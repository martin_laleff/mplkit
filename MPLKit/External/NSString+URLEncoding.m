//
//  NSString+URLEncoding.m
//  ShtrakPollApps
//
//  Created by Martin Lalev on 11/13/13.
//  Copyright (c) 2013 Martin Lalev. All rights reserved.
//

#import "NSString+URLEncoding.h"

@implementation NSString (URLEncoding)

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding {
    NSString *result = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                             (__bridge CFStringRef)self,
                                                                                             NULL,
                                                                                             CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                             kCFStringEncodingUTF8);
    return result;
//    return [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}
-(NSString *)urlDecodeUsingEncoding:(NSStringEncoding)encoding {
    
    NSString *result = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
                                                                                                             (__bridge CFStringRef)[self stringByReplacingOccurrencesOfString:@"+" withString:@" "],
                                                                                                             CFSTR(""),
                                                                                                             kCFStringEncodingUTF8);
    return result;
//    return [self stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end