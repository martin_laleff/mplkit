//
//  NSData+AES256.m
//  Mixweet
//
//  Created by Martin Lalev on 2/4/14.
//
//

#import <CommonCrypto/CommonCryptor.h>
#import "NSData+AES256.h"

@implementation NSData (AES256)

- (NSData *)AES256WithKey:(NSString *)key encrypt:(BOOL)encrypt {
	char keyPtr[kCCKeySizeAES256+1];
	bzero(keyPtr, sizeof(keyPtr));
	[key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
	
	void *buffer = malloc(self.length + kCCBlockSizeAES128);
	
	size_t numBytesEncrypted = 0;
	CCCryptorStatus cryptStatus = CCCrypt(encrypt ? kCCEncrypt : kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                          keyPtr, kCCKeySizeAES256,
                                          NULL /* initialization vector (optional) */,
                                          self.bytes, self.length, /* input */
                                          buffer, self.length + kCCBlockSizeAES128, /* output */
                                          &numBytesEncrypted);

    NSData* result = (cryptStatus == kCCSuccess ? [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted] : nil);
	free(buffer);
	return result;
}

- (NSData *)AES256EncryptWithKey:(NSString *)key { return [self AES256WithKey:key encrypt:YES]; }
- (NSData *)AES256DecryptWithKey:(NSString *)key { return [self AES256WithKey:key encrypt:NO]; }

@end
